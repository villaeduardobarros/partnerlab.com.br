<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Send{

    public function contact($data=NULL) 
    {
        $CI =& get_instance();

        $CI->load->library('email');
        $configMail = $this->configSmtp();
        $CI->email->initialize($configMail);
        $CI->email->clear(TRUE);

        $message = $CI->load->view('mail-html/contact', $data, true);

        $CI->email->from('authinnovoweb@gmail.com', 'Partnerlab');
        $CI->email->to($data['recipient']);
        $CI->email->subject($data['subject']);
        $CI->email->message($message);
        if ($CI->email->send()) {
            return true;
        } else {
            echo $CI->email->print_debugger();
        }
    }

    function configSmtp() {
        $configMail['mailtype']         = "html";
        $configMail['protocol']         = "smtp";
        $configMail['smtp_host']        = "smtp.gmail.com";
        $configMail['smtp_port']        = "465";
        $configMail['smtp_auth']        = TRUE;
        $configMail['smtp_timeout']     = "10";
        $configMail['smtp_user']        = "authinnovoweb@gmail.com";
        $configMail['smtp_pass']        = "Gn8OOk!Z?Qt7";
        $configMail['charset']          = "utf-8";
        $configMail['crlf']             = "\r\n";
        $configMail['newline']          = "\r\n";
        $configMail['wordwrap']         = TRUE;
        return $configMail;
    }

}
?>