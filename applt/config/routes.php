<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// empresa
$route['empresa'] 		= 'company/company/index';
// calibracao
$route['calibracao'] 	= 'calibration/calibration/index';
// produtos
$route['produtos'] 							= 'products/products/index';
$route['produtos/(:num)/(:any)'] 			= 'products/products/details/$1';
$route['produtos/categoria/(:num)/(:any)'] 	= 'products/products/index/$1';

// servicos
$route['servicos'] 		= 'services/services/index';
// contato
$route['contato'] 		= 'contact/contact/index';

// area do cliente
$route['area-cliente'] 							= 'login/login/index';
$route['area-cliente/sair'] 					= 'logout/logout/index';
$route['area-cliente/home'] 					= 'area/area/index';
$route['area-cliente/home/(:num)/(:any)'] 		= 'area/area/download_archive/$1/$2';
$route['area-cliente/rastreabilidade'] 			= 'area/area/traceability';
$route['area-cliente/rastreabilidade/(:any)'] 	= 'area/area/download_archive_traceability/$1';