<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('seo/md_seo');
		$this->load->model('md_home');
		$this->load->model('products/md_products');
	}

	public function index()
	{		
		// theme
		$this->output->set_template('theme');
		// seo
		$data['session'] = 'home';
		$seo = $this->md_seo->searchSeo($data['session']);
		$data['seoTitle'] = ($seo->num_rows() > 0) ? $seo->row()->title : NULL;
		$data['seoDescription'] = ($seo->num_rows() > 0) ? $seo->row()->description : NULL;
		$data['seoKeywords'] = ($seo->num_rows() > 0) ? $seo->row()->keywords : NULL;
		$data['seoRobots'] = ($seo->num_rows() > 0) ? $seo->row()->robots : NULL;

		$data['banners'] = $this->md_home->searchBanners(NULL, TRUE);
		$data['products'] = $this->md_products->searchProducts(NULL, NULL, FALSE, 4, TRUE);
		$this->load->view('home', $data);
	}

}