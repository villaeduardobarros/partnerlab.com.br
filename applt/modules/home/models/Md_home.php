<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_home extends CI_Model {
    
    function searchBanners($codban=NULL, $is_active=NULL)
	{
		if ($codban) {
			$this->db->where('codban', $codban);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		return $this->db->get('banners');
		//echo $this->db->last_query();
	}
    
}