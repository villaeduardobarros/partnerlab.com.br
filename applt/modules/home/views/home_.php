<?php
$conexao = mysql_connect("mysql.partnerlab.com.br", "partnerlab", "part753lab") or trigger_error(mysql_error(),E_USER_ERROR);
mysql_select_db("partnerlab", $conexao);

mysql_query("SET NAMES 'utf8'");
mysql_query('SET character_set_connection=utf8');
mysql_query('SET character_set_client=utf8');
mysql_query('SET character_set_results=utf8');

$query_qBanner = sprintf("SELECT * FROM banners");
$qBanner = mysql_query($query_qBanner, $conexao) or die(mysql_error());
$row_qBanner = mysql_fetch_assoc($qBanner);
$totalRows_qBanner = mysql_num_rows($qBanner);

$query_qProduto = sprintf("SELECT * FROM produtos ORDER BY rand() limit 4");
$qProduto = mysql_query($query_qProduto, $conexao) or die(mysql_error());
$row_qProduto = mysql_fetch_assoc($qProduto);
$totalRows_qProduto = mysql_num_rows($qProduto);
?>

<?php /*if ($banners->num_rows() > 0) { ?>
	<div class="container slides-container">
		<div class="slider">
			<div class="flexslider">
				<ul class="slides">

					<?php foreach ($banners->result() as $banner) { ?>

						<li><img src="../uploads/banners/<?php echo $banner->photo; ?>" alt="<?php echo $banner->title; ?><?php echo ($banner->subtitle) ? ' - '.$banner->subtitle : NULL; ?>" />

							<?php if ($banner->title) { ?>

								<div class="slide-caption">
									<h3><?php echo $banner->title; ?></h3>
									<p><?php echo $banner->subtitle; ?></p>
									<?php if ($banner->url) { ?>
										<a href="<?php echo $banner->url; ?>" class="button color medium">Saiba Mais</a>
									<?php } ?>
								</div>

							<?php } ?>

						</li>

					<?php } ?>

				</ul>
			</div>
		</div>
	</div>

<?php } */?>

<?php if($totalRows_qBanner > 0){ ?>
<div class="slider slides-container">
    <div class="flexslider">
        <ul class="slides">
            <?php do { ?>
            <li><img src="/old/images/banners/<?php echo $row_qBanner['img']; ?>" alt="" />
            	<?php if($row_qBanner['texto'] != ""){ ?>
                <div class="slide-caption">
                	<h3><?php echo $row_qBanner['titulo']; ?></h3>
                    <p><?php echo $row_qBanner['texto']; ?></p>
                    <?php if($row_qBanner['url'] != ""){ ?><a href="<?php echo $row_qBanner['url']; ?>" class="button color medium">Saiba Mais</a><?php } ?>
                </div>
                <?php } ?>
            </li>
            <?php } while ($row_qBanner = mysql_fetch_assoc($qBanner)); ?>
      </ul>
    </div>
</div>
<?php } ?>


<div id="wrapper">
	
		<div class="row" style="padding-left: 10px;">
			<div class="col-md-4">
				<h3>Conheça a Partner<span>lab</span></h3>
				<a href="empresa"><img src="assets/images/img-conheca.jpg" width="300"></a>
				<p>Temos uma estrutura formada para atendimentos realizados tanto em nossa empresa, quanto na planta de nossos clientes.</p>
				<a href="empresa" class="button color medium">Saiba Mais</a>
			</div>

			<div class="col-md-4">
				<h3>Calibração</h3>
				<a href="calibracao"><img src="assets/images/img-calibracao.jpg" width="300"></a>
				<p>Nosso laboratório possui monitoramento de temperatura e Umidade, visando sempre a melhor condição para realização da calibração.</p>
				<a href="calibracao" class="button color medium">Saiba Mais</a>
			</div>

			<div class="col-md-4">
				<h3>Entre em Contato</h3>
				<a href="contato"><img src="assets/images/img-contato.jpg" width="300"></a>
				<p>Acesse nossa página de contato, envie suas dúvidas ou sugestões, nossa equipe irá atendê-lo.</p>
				<a href="contato" class="button color medium">Fale Conosco</a>
			</div>
		</div>
	
</div>




<div class="bg-products">
	<div>
		<div class="row" style="padding-left: 10px;">
			<div class="sixteen columns col-md-12">
				<h3>Produtos em <span>Destaque</span></h3>
			</div>
		</div>

		<div class="row" style="padding-left: 10px;">

		<?php if ($products->num_rows() > 0){ ?>

			<?php foreach($products->result() as $product) { ?>

				<?php
					$gallery = $this->md_products->searchGallery($product->codprd, TRUE);
					if ($gallery->num_rows() > 0){

						if ($gallery->row()->main == 1) {

							$photo = '../uploads/galleries/products/'.$product->codprd.'/'.$gallery->row()->photo;

						} else $photo = '../uploads/partnerlab.jpg';

					} else $photo = '../uploads/partnerlab.jpg';
				?>
	
				<div class="col-md-3">
					<div class="picture">
						<a href="produtos/<?php echo $product->codprd; ?>/<?php echo url_title($product->title); ?>">
							<img src="<?php echo $photo; ?>" alt="<?php echo $product->title; ?>"/>
							<div class="image-overlay-link"></div>
						</a>
					</div>
					<div class="item-description">
						<h5><a href="produtos/<?php echo $product->codprd; ?>/<?php echo url_title($product->title); ?>"><?php echo $product->title; ?></a></h5>
						<!--<p><?php //echo strip_tags(cortaString($row_qProduto['texto'], 80)); ?>--></p>
					</div>
				</div>

			<?php } ?>

		<?php } else { ?>

			<div class="sixteen columns">
				<h5>Nenhum produto cadastrado!</h5>
			</div>

		<?php } ?>

		</div>

	</div>
</div>