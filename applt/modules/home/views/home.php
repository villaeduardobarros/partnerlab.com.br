<?php
$conexao = mysql_connect("mysql.partnerlab.com.br", "partnerlab", "part753lab") or trigger_error(mysql_error(),E_USER_ERROR);
mysql_select_db("partnerlab", $conexao);

mysql_query("SET NAMES 'utf8'");
mysql_query('SET character_set_connection=utf8');
mysql_query('SET character_set_client=utf8');
mysql_query('SET character_set_results=utf8');

$query_qBanner = sprintf("SELECT * FROM banners");
$qBanner = mysql_query($query_qBanner, $conexao) or die(mysql_error());
$row_qBanner = mysql_fetch_assoc($qBanner);
$totalRows_qBanner = mysql_num_rows($qBanner);

$query_qProduto = sprintf("SELECT * FROM produtos ORDER BY rand() limit 4");
$qProduto = mysql_query($query_qProduto, $conexao) or die(mysql_error());
$row_qProduto = mysql_fetch_assoc($qProduto);
$totalRows_qProduto = mysql_num_rows($qProduto);
?>

<?php if (!$this->agent->is_mobile()) { ?>

	<?php if($totalRows_qBanner > 0){ ?>
		<div class="slider slides-container hidden-sm">
			<div class="flexslider" id="sessionHome">
				<ul class="slides">
					<?php do { ?>
						<li>
							<img src="/old/images/banners/<?php echo $row_qBanner['img']; ?>" alt="" class="img-fluid">
							<?php if ($row_qBanner['texto'] != "") { ?>
								<div class="slide-caption">
									<h3><?php echo $row_qBanner['titulo']; ?></h3>
									<p><?php echo $row_qBanner['texto']; ?></p>
						
									<?php if ($row_qBanner['url'] != "") { ?>
										<a href="<?php echo $row_qBanner['url']; ?>" class="button color medium">Saiba Mais</a>
									<?php } ?>
								</div>
							<?php } ?>
						</li>
					<?php } while ($row_qBanner = mysql_fetch_assoc($qBanner)); ?>
				</ul>
			</div>
		</div>
	<?php } ?>

<?php } ?>


<div id="wrapper">
	<div class="container">
	
		<div class="row">

			<div class="col-12 col-sm-12 col-md-4 col-lg-4 modules">
				<div id="page-title">
					<h2>Conheça a Partner<span>lab</span></h2>
					<div id="bolded-line"></div>
				</div>

				<a href="empresa">
					<img src="assets/images/img-conheca.jpg" class="img-home img-fluid" width="300">
					<p>Temos uma estrutura formada para atendimentos realizados tanto em nossa empresa, quanto na planta de nossos clientes.</p>
					<button class="button color medium">Saiba Mais</button>
				</a>
			</div>

			<div class="col-12 col-sm-12 col-md-4 col-lg-4 modules">
				<div id="page-title">
					<h2>Calibração</h2>
					<div id="bolded-line"></div>
				</div>

				<a href="calibracao">
					<img src="assets/images/img-calibracao.jpg" class="img-home img-fluid" width="300">
					<p>Nosso laboratório possui monitoramento de temperatura e Umidade, visando sempre a melhor condição para realização da calibração.</p>
					<button class="button color medium">Saiba Mais</button>
				</a>
			</div>

			<div class="col-12 col-sm-12 col-md-4 col-lg-4 modules">
				<div id="page-title">
					<h2>Entre em contato</h2>
					<div id="bolded-line"></div>
				</div>

				<a href="contato">
					<img src="assets/images/img-contato.jpg" class="img-home img-fluid" width="300">
					<p>Acesse nossa página de contato, envie suas dúvidas ou sugestões, nossa equipe irá atendê-lo.</p>
					<button class="button color medium">Fale Conosco</button>
				</a>
			</div>

		</div>

	</div>
</div>


<?php if ($products->num_rows() > 0){ ?>

	<div class="bg-products">
		<div class="container">

			<div class="row">

				<div class="col-12 col-sm-12 col-md-12 col-lg-12">
					<div id="page-title">
						<h2>Produtos em <span>Destaque</span></h2>
						<div id="bolded-line"></div>
					</div>
				</div>

			</div>

			<div class="row">

				<?php foreach($products->result() as $product) { ?>

					<?php
						$gallery = $this->md_products->searchGallery($product->codprd, TRUE);
						if ($gallery->num_rows() > 0){

							if ($gallery->row()->main == 1) {

								$photo = '../uploads/galleries/products/'.$product->codprd.'/'.$gallery->row()->photo;

							} else $photo = '../uploads/partnerlab.jpg';

						} else $photo = '../uploads/partnerlab.jpg';
					?>
		
					<div class="col-12 col-sm-12 col-md-3 col-lg-3">
						<div class="picture">
							<a href="produtos/<?php echo $product->codprd; ?>/<?php echo url_title($product->title); ?>">
								<img src="<?php echo $photo; ?>" alt="<?php echo $product->title; ?>" class="img-fluid">
								<div class="image-overlay-link"></div>
							</a>
						</div>
						<div class="item-description">
							<a href="produtos/<?php echo $product->codprd; ?>/<?php echo url_title($product->title); ?>">
								<h5><?php echo $product->title; ?></h5>
							</a>
							<!--<p><?php //echo strip_tags(cortaString($row_qProduto['texto'], 80)); ?></p>-->
						</div>
					</div>

				<?php } ?>

			</div>

		</div>
	</div>

<?php } ?>