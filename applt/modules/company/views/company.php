<div id="sessionCampany" class="banner-interno"></div>

<div id="wrapper">
	<div class="container">
	
		<div class="row not-margin">
	
			<div class="col-md-12">
				<div id="page-title">
					<h2>Empresa</h2>
					<div id="bolded-line"></div>
				</div>

				<h3 class="headline no-margin">Partnerlab Equipamentos de Laboratório</h3>
				
				<p>Em novembro de 2012 a Partnerlab Equipamentos de Laboratório, deu início a suas atividades, buscando inovar e surpreender o segmento de laboratórios.</p>

				<p>Contando com uma equipe altamente qualificada com mais de 15 anos de experiência no setor, e conhecendo a necessidade de serviços de qualidade à preços acessíveis, instituímos como o nosso nome diz uma parceria com os laboratórios.</p>
				
				<p>Focados na prestação de serviços de manutenção e calibração de equipamentos de laboratório, temos uma estrutura formada para atendimentos realizados tanto em nossa empresa, quanto na planta de nossos clientes.</p>
			</div>

		</div>

	</div>
</div>