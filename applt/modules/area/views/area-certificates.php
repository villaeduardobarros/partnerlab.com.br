﻿<div id="sessionArea" class="banner-interno"></div>

<div id="wrapper">

	<div class="container">
		<div class="sixteen columns">
			<div id="page-title">
				<h2>Certificados</h2>
				<div id="bolded-line"></div>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="sixteen columns">
			<table id="tableItems" class="display" width="100%">
                <thead>
                    <tr>
                        <th width="80%">Título</th>
						<th width="20%" style="text-align:center !important;">Data/Hora</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach ($archives->result() as $arc) { ?>

                        <?php list($date, $time) = explode(' ', $arc->datetime); ?>

                		<tr>
                			<td>
                                <a href="<?php echo base_url(); ?>uploads/archives/customers/<?php echo $codctm; ?>/<?php echo $arc->file; ?>" target="_blank" style="text-decoration:none;">
                                    <?php echo $arc->title; ?>
                                </a>
                            </td>
                			<td style="text-align:center !important;"><?php echo implode('/', array_reverse(explode('-', $date))).' '.$time; ?></td>
                		</tr>

                	<?php } ?>
                	
                </tbody>
            </table>			
		</div>

	</div>

	<br><br><br>
</div>