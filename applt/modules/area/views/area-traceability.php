﻿<div id="sessionAreaTrac" class="banner-interno"></div>

<div id="wrapper">

	<div class="container">
		<div class="sixteen columns">
			<div id="page-title">
				<h2>Rastreabilidade</h2>
				<div id="bolded-line"></div>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="sixteen columns">
			<label style="float:right;">
				<input type="text" name="search" id="search" style="float:left;">
				<button id="btnSearch" style="float:right;background:#ff4f00;color:#ffffff;padding:5px;border:1px solid #ff4f00;border-radius:4px;margin:3px 0 0 10px;">Pesquisar</button>
			</label>
		</div>

        <br>

        <div class="sixteen columns list-trac"></div>

	</div>

	<br><br><br>
</div>