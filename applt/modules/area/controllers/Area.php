<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Area extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->helper('security');
		$this->load->helper('cookie');
		$this->load->model('md_area');
		if (!$this->session->userdata('logged-ctm-in')) {
			redirect('area-cliente/sair');
			exit;
		}
	}

	public function index()
	{
		// theme
		$this->output->set_template('theme-customers');
		// css
		$this->load->css('assets/css/bootstrap.min.css');
		$this->load->css('assets/css/flexslider.css');
		$this->load->css('assets/css/swipebox.css');
		$this->load->css('assets/js/datatables/jquery.dataTables.min.css');
		$this->load->css('assets/css/style.css');
		// js
		$this->load->js('assets/js/bootstrap.min.js');
		$this->load->js('assets/js/flexslider.js');
		$this->load->js('assets/js/jquery.swipebox.min.js');
		$this->load->js('assets/js/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/js/custom.js');
			
		$data['session'] = 'login-archives';
		$data['menu'] = 'login-certificates';
		$data['codctm'] = $this->session->userdata('login-ctm-codctm');
		$data['archives'] = $this->md_area->searchArchives(NULL, $data['codctm']);
		$this->load->view('area-certificates', $data);
	}

	public function download_archive($codctm, $archive)
	{
		$this->load->helper('download');
		force_download($_SERVER['DOCUMENT_ROOT'].'uploads/archives/customers/'.$codctm.'/'.$archive, NULL);
	}

	public function traceability()
	{
		// theme
		$this->output->set_template('theme-customers');
		// css
		$this->load->css('assets/css/bootstrap.min.css');
		$this->load->css('assets/css/flexslider.css');
		$this->load->css('assets/css/swipebox.css');
		$this->load->css('assets/js/datatables/jquery.dataTables.min.css');
		$this->load->css('assets/css/style.css');
		// js
		$this->load->js('assets/js/bootstrap.min.js');
		$this->load->js('assets/js/flexslider.js');
		$this->load->js('assets/js/jquery.swipebox.min.js');
		$this->load->js('assets/js/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/js/custom.js');
			
		$data['session'] = 'login-archives';
		$data['menu'] = 'login-traceability';
		$data['archives'] = $this->md_area->searchArchives(NULL, NULL, TRUE);
		$this->load->view('area-traceability', $data);
	}

	public function searchTraceability()
	{
		$title = $this->input->post('title', TRUE);

		$html = '<table id="tableItems" class="display" width="100%">
            <thead>
                <tr>
                    <th width="80%">Título</th>
                    <th width="20%" style="text-align:center !important;">Data/Hora</th>
                </tr>
            </thead>
            <tbody>';

	            $archives = $this->md_area->searchArchives(NULL, NULL, NULL, $title);
	            if ($archives->num_rows() > 0) {

	            	foreach ($archives->result() as $arc) {

	                    list($date, $time) = explode(' ', $arc->datetime);

	            		$html .= '<tr>
	            			<td>
	                            <a href="'.base_url().'uploads/archives/traceability/'.$arc->file.'" target="_blank" style="text-decoration:none;">'.$arc->title.'</a>
	                        </td>
	            			<td style="text-align:center !important;">'.implode('/', array_reverse(explode('-', $date))).' '.$time.'</td>
	            		</tr>';

	            	}

	            } else {

	            	$html .= '<tr>
	            		<td style="text-align:center !important;">Nenhum arquivo encontrado</td>
	            	</tr>';

	            }
            	
            $html .= '</tbody>
        </table>';		
		
		echo $html;
	}

	public function download_archive_traceability($archive)
	{
		$this->load->helper('download');
		force_download($_SERVER['DOCUMENT_ROOT'].'uploads/archives/traceability/'.$archive, NULL);
	}

}