<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_area extends CI_Model {

	public function searchArchives($codarc=NULL, $codctm=NULL, $traceability=NULL, $title=NULL)
	{
		if ($codarc) {
			$this->db->where('codarc', $codarc);
		}
		if ($codctm) {
			$this->db->where('customers_codctm', $codctm);
		}
		if ($traceability) {
			$this->db->where('traceability', 1);
		}
		if ($title) {
			$this->db->like('title', $title, 'both');
		}
		return $this->db->get('archives');
		//echo $this->db->last_query();
	}

}