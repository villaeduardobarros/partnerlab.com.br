<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_products extends CI_Model {

	public function searchProducts($codprd=NULL, $codcat=NULL, $order=NULL, $limit=NULL, $is_active=NULL)
	{
		if ($codprd) {
			$this->db->where('codprd', $codprd);
		}
		if ($codcat) {
			$this->db->where('categories_codcat', $codcat);
		}
		if ($is_active == TRUE) {
			$this->db->where('products.is_active', 1);
		}
		if ($order == FALSE) {
			$this->db->order_by('RAND()', NULL, FALSE);
		} else {
			$this->db->order_by('products.position', 'ASC');
		}
		if ($limit) {
			$this->db->limit($limit);
		}
		return $this->db->get('products');
		//echo $this->db->last_query();
	}


	// categorias
	public function searchCategories($codcat=NULL, $order=NULL, $is_active=NULL)
	{
		if ($codcat) {
			$this->db->where('codcat', $codcat);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		if ($order == FALSE) {
			$this->db->order_by('title', 'ASC');
		} else {
			$this->db->order_by('position', 'ASC');
		}
		return $this->db->get('categories');
		//echo $this->db->last_query();
	}


	// categorias
	public function searchGallery($codprd=NULL, $main=NULL)
	{
		if ($codprd) {
			$this->db->where('products_codprd', $codprd);
		}
		if ($main == TRUE) {
			$this->db->where('main', 1);
		}
		return $this->db->get('galleries');
		//echo $this->db->last_query();
	}
	
}