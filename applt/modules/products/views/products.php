<div id="sessionProducts" class="banner-interno"></div>

<div id="wrapper">
	<div class="container">
	
		<div class="row">
	
			<div class="col-md-3">
				<div id="page-title">
					<h2>Categorias</h2>
					<div id="bolded-line"></div>
				</div>

				<div class="categories">
					<ul>
						<?php foreach ($categories->result() as $categ) { ?>

							<li>
								<a href="produtos/categoria/<?php echo $categ->codcat; ?>/<?php echo url_title($categ->title); ?>" <?php echo (!is_null($categSelect)) ? (($categSelect == $categ->codcat) ? 'class="selected"' : NULL) : NULL; ?>>
									<?php echo $categ->title; ?>
								</a>
							</li>

						<?php } ?>
					</ul>
				</div>
			</div>

			<div class="col-md-9">
				<div id="page-title">
					<h2>Produtos <?php //echo $row_qCategoria['titulo']; ?></h2>
					<div id="bolded-line"></div>
				</div>

				<?php if ($products->num_rows() > 0) { ?>

					<div class="row">

						<?php
							$count = 1; 
							foreach ($products->result() as $prod) {
						?>

								<?php
									$gallery = $this->md_products->searchGallery($prod->codprd, TRUE);
									if ($gallery->num_rows() > 0) {

										if ($gallery->row()->main == 1) {

											$photo = '../uploads/galleries/products/'.$prod->codprd.'/'.$gallery->row()->photo;

										} else $photo = '../uploads/partnerlab.jpg';

									} else $photo = '../uploads/partnerlab.jpg';
								?>
							
								<div class="col-12 col-sm-12 col-md-4 col-lg-4">
									<div class="picture">
										<a href="produtos/<?php echo $prod->codprd; ?>/<?php echo url_title($prod->title); ?>" rel="image" row="nofollow" title="<?php echo $prod->title; ?>">
											<img src="<?php echo $photo; ?>" alt="<?php echo $prod->title; ?>" class="img-fluid">
											<div class="image-overlay-link"></div>
										</a>
									</div>
									<div class="item-description alt">
										<h5 style="min-height:48px;">
											<a href="produtos/<?php echo $prod->codprd; ?>/<?php echo url_title($prod->title); ?>">
												<?php echo $prod->title; ?>
											</a>
										</h5>
										<!--<p style="min-height:65px;"><?php //echo $prod->description; ?></p>-->
									</div>
								</div>

								<?php echo ($count % 3 == 0) ? '</div><div class="row">' : NULL; ?>

						<?php
								$count++;
							}
						?>
					</ul>

				<?php } ?>

			</div>

		</div>

	</div>
</div>