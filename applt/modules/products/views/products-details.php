<div id="sessionProducts" class="banner-interno"></div>

<div id="wrapper">
	<div class="container">
	
		<div class="row">
	
			<div class="col-md-12">
				<div id="page-title">
					<h2>Produto / <?php echo $product->title; ?></h2>
					<div id="bolded-line"></div>
				</div>

				<h3 class="headline no-margin"><?php echo $product->title; ?></h3>
				
				<?php echo $product->description; ?>
			</div>

		</div>

		<?php
			$galleries = $this->md_products->searchGallery($product->codprd);
			if ($galleries->num_rows() > 0) {
		?>

				<div class="row">
					<div class="col-md-12">
						<br>
						<br>

						<p>*FOTO ILUSTRATIVA</p>
						
						<h3 class="headline no-margin">GALERIA DE FOTOS</h3>

						<ul id="galeriafotos">
							<?php foreach ($galleries->result() as $gallery) { ?>

								<li class="col-12 col-sm-12 col-md-3 col-lg-3">
									<div class="picture">
										<a href="../uploads/galleries/products/<?php echo $codprd; ?>/<?php echo $gallery->photo; ?>" class="swipebox" rel="swipebox" title="<?php echo $gallery->title; ?>">
											<img src="../uploads/galleries/products/<?php echo $codprd; ?>/<?php echo $gallery->photo; ?>" alt="<?php echo $gallery->title; ?>"/>
											<div class="image-overlay-link"></div>
										</a>
									</div>
								</li>

							<?php } ?>
						</ul>

					</div>
				</div>

		<?php } ?>
	
	</div>
</div>