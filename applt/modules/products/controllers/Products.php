<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('seo/md_seo');
		$this->load->model('md_products');
	}

	public function index($codcat=NULL)
	{        
		// theme
		$this->output->set_template('theme');

		// seo
		$data['session'] = 'products';
		$seo = $this->md_seo->searchSeo($data['session']);
		$data['seoTitle'] = ($seo->num_rows() > 0) ? $seo->row()->title : NULL;
		$data['seoDescription'] = ($seo->num_rows() > 0) ? $seo->row()->description : NULL;
		$data['seoKeywords'] = ($seo->num_rows() > 0) ? $seo->row()->keywords : NULL;
		$data['seoRobots'] = ($seo->num_rows() > 0) ? $seo->row()->robots : NULL;

		$data['categories'] = $this->md_products->searchCategories(NULL, FALSE, TRUE);

		$data['categSelect'] = ($codcat) ? $codcat : NULL;
		$data['products'] = $this->md_products->searchProducts(NULL, $data['categSelect'], TRUE, NULL, TRUE);

		$this->load->view('products', $data);
	}


	public function details($codprd)
	{
		// theme
		$this->output->set_template('theme');

		// seo
		$data['session'] = 'products';
		$seo = $this->md_seo->searchSeo($data['session']);
		$data['seoTitle'] = ($seo->num_rows() > 0) ? $seo->row()->title : NULL;
		$data['seoDescription'] = ($seo->num_rows() > 0) ? $seo->row()->description : NULL;
		$data['seoKeywords'] = ($seo->num_rows() > 0) ? $seo->row()->keywords : NULL;
		$data['seoRobots'] = ($seo->num_rows() > 0) ? $seo->row()->robots : NULL;


		$products = $this->md_products->searchProducts($codprd, NULL, TRUE, NULL, TRUE);
		if ($products->num_rows() > 0) {

			$data['codprd'] = $codprd;
			$data['product'] = $products->row();

		} else redirect('produtos');

		$this->load->view('products-details', $data);
	}

}