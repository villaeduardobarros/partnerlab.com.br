<div id="sessionServices" class="banner-interno"></div>

<div id="wrapper">
	<div class="container">
	
		<div class="row not-margin">
	
			<div class="col-md-12">
				<div id="page-title">
					<h2>Serviços</h2>
					<div id="bolded-line"></div>
				</div>

				<h3 class="headline no-margin">Conheça</h3>

				<p>Empresa preparada para realizar MANUTENÇÃO PREVENTIVA, CORRETIVA e CALIBRAÇÃO de equipamentos de laboratório, mantendo a qualidade dos equipamentos de acordo com as características de fabricação, proporcionando elevada confiabilidade de resultados no processo de trabalho.</p>

				<p>Equipe técnica qualificada com treinamentos nas empresas autorizadas e parceiras.</p>

				<p>Oferecemos contrato de serviço de manutenção e calibração com gerenciamento dos equipamentos na base do cliente, evitando assim desconforto e preocupação com vencimento do próximo serviço.</p>

				<p>Empresa Credenciada no IPEM para realização de serviços de manutenção em balanças com capacidade até 100Kg.
			</div>

		</div>

	</div>
</div>