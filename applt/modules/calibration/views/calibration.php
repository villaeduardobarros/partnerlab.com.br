<div id="sessionCalibration" class="banner-interno"></div>

<div id="wrapper">
	<div class="container">
	
		<div class="row">
	
			<div class="col-md-12">
				<div id="page-title">
					<h2>Calibração</h2>
					<div id="bolded-line"></div>
				</div>

				<!-- <h3 class="headline no-margin">Galeria</h3> -->

				<ul class="list-horizontal">
					<li>
						<a href="assets/images/calibration/img3_g.jpg" class="swipebox" rel="swipebox">
							<img src="assets/images/calibration/img3.jpg">
						</a>
					</li>
					<li>
						<a href="assets/images/calibration/img4_g.jpg" class="swipebox" rel="swipebox">
							<img src="assets/images/calibration/img4.jpg">
						</a>
					</li>
				</ul>

				<br><br>

				<h3 class="headline no-margin">Conheça</h3>

				<p>Calibrações realizadas com padrões Rastreados junto à Rede Brasileira de Calibração (RBC), ou órgãos internacionalmente reconhecidos como o NIST e PTB.

				<p>Os serviços de calibração podem ser realizados em nosso laboratório ou nas dependências do cliente.

				<p>Nosso laboratório possui monitoramento de temperatura e Umidade, visando sempre a melhor condição para realização da calibração.</p>
			</div>

		</div>

	</div>

</div>