<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Calibration extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('seo/md_seo');
	}

	public function index()
	{
		// theme
		$this->output->set_template('theme');
		
		$data['session'] = 'calibration';

		// seo
		$seo = $this->md_seo->searchSeo($data['session']);
		$data['seoTitle'] = ($seo->num_rows() > 0) ? $seo->row()->title : NULL;
		$data['seoDescription'] = ($seo->num_rows() > 0) ? $seo->row()->description : NULL;
		$data['seoKeywords'] = ($seo->num_rows() > 0) ? $seo->row()->keywords : NULL;
		$data['seoRobots'] = ($seo->num_rows() > 0) ? $seo->row()->robots : NULL;

		$this->load->view('calibration', $data);
	}

}