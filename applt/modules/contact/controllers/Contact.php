<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('seo/md_seo');
	}

	public function index()
	{
		// theme
		$this->output->set_template('theme');
		// seo
		$data['session'] = 'contact';
		$seo = $this->md_seo->searchSeo($data['session']);
		$data['seoTitle'] = ($seo->num_rows() > 0) ? $seo->row()->title : NULL;
		$data['seoDescription'] = ($seo->num_rows() > 0) ? $seo->row()->description : NULL;
		$data['seoKeywords'] = ($seo->num_rows() > 0) ? $seo->row()->keywords : NULL;
		$data['seoRobots'] = ($seo->num_rows() > 0) ? $seo->row()->robots : NULL;

		$this->load->view('contact', $data);
	}

	public function sendContact()
	{
		// dados de envio
		$arrEmail = array(
			'subject' 		=> 'Contato Partnerlab',
			'recipient' 	=> 'contato@innovoweb.com.br',
			'name' 			=> $this->input->post('name', TRUE),
			'email' 		=> $this->input->post('email', TRUE),
			'phone' 		=> $this->input->post('phone', TRUE),
			'city' 			=> $this->input->post('city', TRUE),
			'uf' 			=> $this->input->post('uf', TRUE),
			'message' 		=> $this->input->post('message', TRUE)
		);

		$this->load->library('send');
		if ($this->send->contact($arrEmail)) {
			
			echo 'success';

		} else echo 'erro';

	} 

}