<div id="sessionContact" class="banner-interno"></div>

<div id="wrapper">
	<div class="container">
	
		<div class="row">
	
			<div class="col-md-12">
				<div id="page-title">
					<h2>Contato</h2>
				<div id="bolded-line"></div>
				</div>
			</div>

			<div class="col-md-6">
				<form method="post" id="contactForm">

					<div class="field alerts"></div>

					<div class="field">
						<input type="text" name="name" id="name" class="text" placeholder="Nome" required autocomplete="off" />
					</div>

					<div class="field">
						<input type="text" name="email" id="email" class="text" placeholder="E-mail" required autocomplete="off" />
					</div>

					<div class="field">
						<input type="text" name="phone" id="phone" class="text" placeholder="Telefone" required autocomplete="off" />
					</div>
					<div class="clear"></div>

					<div class="field">
						<input type="text" name="city" id="city" class="text" placeholder="Cidade" required autocomplete="off" />
					</div>

					<div class="field">
						<input type="text" name="uf" id="uf" class="text" placeholder="UF" required autocomplete="off" />
					</div>

					<div class="field">
						<textarea name="message" id="message" class="text textarea" placeholder="Mensagem" required></textarea>
					</div>

					<div class="field">
						<input type="submit" value="Enviar" id="buttonForm" class="button color" style="margin-right:20px;"/>
						<input type="reset" value="Limpar" class="button color"/>
					</div>

				</form>
			</div>

			<div class="col-md-6 google-map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.3941976755646!2d-47.760220099999984!3d-21.176493899999986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94b9c0a1bcfd0359%3A0x23b852349b9aa274!2sR.+Virg%C3%ADlio+de+Carvalho+Neves+Neto%2C+926+-+Res.+e+Comercial+Palmares%2C+Ribeir%C3%A3o+Preto+-+SP%2C+14092-440!5e0!3m2!1sen!2sbr!4v1435078468455" height="400" frameborder="0" style="border:0; width:100%;"></iframe>

		
			</div>

		</div>

		<div class="row not-margin">

			<div class="col-md-12 fone">
				<span>16</span> 3624.7700 | 3237.3861
			</div>

		</div>

	</div>
</div>