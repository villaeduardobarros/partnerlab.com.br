<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_seo extends CI_Model {
    
    public function searchSeo($session)
	{
		if ($session) {
			$this->db->where('session', $session);
		}
		return $this->db->get('seo');
		//echo $this->db->last_query();
	}
    
}