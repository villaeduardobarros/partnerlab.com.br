<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->helper('security');
		$this->load->helper('cookie');
		$this->load->model('md_login');
	}

	public function index()
	{
		// theme
		$this->output->set_template('theme-customers');
		// css
		$this->load->css('assets/css/style.css');
		$this->load->css('assets/css/flexslider.css');
		$this->load->css('assets/css/swipebox.css');
		// js
		$this->load->js('assets/js/jquery.min.js');
		$this->load->js('assets/js/flexslider.js');
		$this->load->js('assets/js/jquery.swipebox.min.js');
		$this->load->js('assets/js/custom.js');
		$this->load->js('assets/js/jquery.validate.js');
			
		$data['session'] = 'login-customers';
		$this->load->view('login', $data);
	}

	public function action()
	{
		$user 		= strtolower($this->input->post('user', TRUE));
		$password 	= $this->input->post('password', TRUE);

		if (!empty($user) || !empty($password)) {

			// criptografa a senha
			$passwordCripty	= do_hash($password.'cust'.$user, 'md5');
			$passwordHash	= hash('whirlpool', $passwordCripty);
			//echo $user.' <> '.$password.' <> '.$passwordHash; exit;
		
			$userLogin = $this->md_login->searchCustomers(NULL, $user, $passwordHash, TRUE);
			if ($userLogin->num_rows() > 0) {

				// pega os dados do usuario
				$rowusr = $userLogin->row();
					
				$arrLogin = array(
					'login-ctm-codctm' 	=> $rowusr->codctm,
					'login-ctm-name' 	=> $rowusr->name,
					'login-ctm-email' 	=> $rowusr->email,
					'login-ctm-user' 	=> $rowusr->user,
					'logged-ctm-in' 	=> TRUE
				);
				$this->session->set_userdata($arrLogin);
				
				echo 'success';

			} else {
				echo 'incorrect-data';
			}

		} else {
			echo 'wrong-fill';
		}
	}
	
}
