<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_login extends CI_Model {

	public function searchCustomers($codctm=NULL, $user=NULL, $password=NULL, $is_active=NULL)
	{
		if ($codctm) {
			$this->db->where('codctm', $codctm);
		}
		if ($user) {
			$this->db->where('user', $user);
		}
		if ($password) {
			$this->db->where('password', $password);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		return $this->db->get('customers');
		//echo $this->db->last_query();
	}

}