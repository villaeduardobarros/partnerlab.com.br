﻿<div id="sessionLogin" class="banner-interno"></div>

<div id="wrapper">
	<div class="container">
		<div class="sixteen columns">
			<div id="page-title">
				<h2>Área do Cliente</h2>
				<div id="bolded-line"></div>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="eight columns">
			<div>
				<form method="post" id="loginForm">

					<div class="field alerts"></div>

					<div class="field">
						<input type="text" name="user" id="user" class="text" placeholder="Usuário" required autocomplete="off" />
					</div>

					<div class="field">
						<input type="password" name="password" id="password" class="text" placeholder="Senha" required autocomplete="off" />
					</div>

					<div class="field">
						<input type="submit" value="Acessar" id="buttonForm" class="button color" style="margin-right:20px;"/>
						<input type="reset" value="Limpar" class="button color"/>
					</div>

				</form>

				<br><br><br>

			</div>
		</div>

	</div>
</div>