<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('cookie');
		$this->load->library('session');
	}
	
	public function index()
	{
		$this->session->unset_userdata('login-ctm-codctm');
		$this->session->unset_userdata('login-ctm-name');
		$this->session->unset_userdata('login-ctm-email');
		$this->session->unset_userdata('login-ctm-user');
		$this->session->unset_userdata('logged-ctm-in');

        redirect('area-cliente');
	}

}