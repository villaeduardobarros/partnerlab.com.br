<header>
	<div class="container">
		<div class="row not-margin">

			<div class="col-12">
				<ul class="list-horizontal ft-right address">
					<li>
						<i>
							<img src="assets/images/icons/icon-phone.png" width="12" height="12" alt="phone">
						</i> (16) 3624.7700 | 3237.3861
					</li>
					<li>
						<i>
							<img src="assets/images/icons/icon-mail.png" width="16" height="12" alt="contato">
						</i> <a href="mailto:partnerlab@partnerlab.com.br">partnerlab@partnerlab.com.br</a>
					</li>
				</ul>
			</div>

		</div>
	</div>

	<nav class="navbar navbar-expand-lg">
		<div class="container">
			<a class="navbar-brand not-padding" href="home">
				<img src="assets/images/logo.png" alt="<?php echo $this->config->item('title'); ?>">
			</a>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars" aria-controls="navbars" aria-expanded="false">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse justify-content-end" id="navbars">
				<ul class="navbar-nav">
					<li class="nav-item <?php echo ($session == 'home') ? 'active' : NULL; ?>">
						<a class="nav-link" href="home">HOME</a>
					</li>
					<li class="nav-item <?php echo ($session == 'company') ? 'active' : NULL; ?>">
						<a class="nav-link" href="empresa">EMPRESA</a>
					</li>
					<li class="nav-item <?php echo ($session == 'calibration') ? 'active' : NULL; ?>">
						<a class="nav-link" href="calibracao">CALIBRAÇÃO</a>
					</li>
					<li class="nav-item <?php echo ($session == 'products') ? 'active' : NULL; ?>">
						<a class="nav-link" href="produtos">PRODUTOS</a>
					</li>
					<li class="nav-item <?php echo ($session == 'services') ? 'active' : NULL; ?>">
						<a class="nav-link" href="servicos">SERVIÇOS</a>
					</li>
					<li class="nav-item <?php echo ($session == 'contact') ? 'active' : NULL; ?>">
						<a class="nav-link" href="contato">CONTATO</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>