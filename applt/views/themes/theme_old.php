<?php
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
?>
<!doctype html>
<html lang="en">
<head>
	<base href="<?php echo base_url(); ?>">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Innovoweb Soluções Digitais">
	<title>Partnerlab - Área do Cliente</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/flexslider.css">
	<link rel="stylesheet" href="assets/css/swipebox.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="canonical" href="<?php echo current_url(); ?>" />
	<link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
	<script src="assets/js/jquery.min.js"></script>
	<script>var baseUrl = '<?php echo base_url(); ?>';</script>
	<style>
		@media only screen and (max-width: 768px) {
			.slides-container{
				display: none;
			}
		}
	</style>
</head>
<body>
	<header id="top-info">
		
			<div class="row" style="padding-right: 10px; padding-left: 10px;">
				<div class="col-sm-12">
					<div id="contact-details">
						<ul>
							<li><i><img src="assets/images/icons/icon-phone.png" width="12" height="12"></i>(16) 3624 – 7700 | 3237 -3861</li>
							<li><i><img src="assets/images/icons/icon-mail.png" width="16" height="12"></i><a href="mailto:partnerlab@partnerlab.com.br">partnerlab@partnerlab.com.br</a></li>
						</ul>
					</div>
				</div>
			</div>
		
	    <div class="clear"></div>
	</header>

	<header id="header">
	    
			<div class="container-fluid" style="padding-left: 10px;">
				<div class="row" style="padding-left: 10px;">
					<div id="logo">
						<a href="./"><img src="assets/images/logo.png" alt="<?php echo $this->config->item('title'); ?>" /></a>
					</div>
					<nav class="navbar navbar-expand-sm" id="navigation" style="margin-right:0px;">
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#nav" aria-expanded="false" style="margin-left:30px;">
							<span class="navbar-toggler-icon" style="background-image: url('../../../assets/images/icons/Hamburger_icon.svg.png');"></span>
						</button>
					
						<div class="navbar-collapse collapse" id="nav">
						
							<ul class="navbar-nav" style="padding-left: 60px;">
								<li class="nav-item"><a href="./" class="<?php echo ($session == 'home') ? 'selected' : NULL; ?>">HOME</a></li>
								<li class="nav-item"><a href="empresa" class="<?php echo ($session == 'company') ? 'selected' : NULL; ?>">EMPRESA</a></li>
								<li class="nav-item"><a href="calibracao" class="<?php echo ($session == 'calibration') ? 'selected' : NULL; ?>">CALIBRAÇÃO</a></li>
								<li class="nav-item"><a href="produtos" class="<?php echo ($session == 'products') ? 'selected' : NULL; ?>">PRODUTOS</a></li>
								<li class="nav-item"><a href="servicos" class="<?php echo ($session == 'services') ? 'selected' : NULL; ?>">SERVIÇOS</a></li>
								<li class="nav-item"><a href="contato" class="<?php echo ($session == 'contact') ? 'selected' : NULL; ?>">CONTATO</a></li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
			<div class="clear"></div>	
		
	</header>

	<?php // conteudo ?>
	<?php echo $output; ?>

	<div id="footer">
		<div style="padding-left: 30px; padding-right: 30px;">
			<div class="row">

				<div class="col-md-4">
					<div class="headline"><h4>Menu</h4></div>
					<div class="left">
						<a href="./">Home</a><br>
						<a href="empresa">Empresa</a><br>
						<a href="calibracao">Calibração</a>
					</div>
					<div class="left">
						<a href="produtos">Produtos</a><br>
						<a href="servicos">Serviços</a><br>
						<a href="contato">Contato</a>
					</div>
					<div class="clear"></div>
				</div>

				<div class="col-md-4">
					<div class="headline"><h4>Localização</h4></div>
					<p>
						Rua Virgilio de Carvalho Neves Neto, 926 <br>
						Ribeirão Preto - SP - CEP: 14092-440<br>
						(16)  3624 – 7700 | 3237 -3861<br />
						Email: partnerlab@partnerlab.com.br
					</p>
				</div>

				<div class="col-md-4">
					<div class="headline"><h4>Redes Sociais</h4></div>
					<p>Venha fazer parte de nossa rede social! Participe de promoções exclusivas e muito mais...</p>
					<ul class="social-icons">
						<li class="facebook"><a href="#">Facebook</a></li>
						<li class="twitter"><a href="#">Twitter</a></li>
					</ul>
				</div>
		</div>

		<div id="footer-bottom">
			<div style="padding-left: 30px; padding-right: 30px;">
				<div>Partnerlab © 2015. Todos direitos reservados. 
					<span class="ijust">
						<a href="http://ijust.com.br/" target="_blank">
							<img src="assets/images/ijust.png" width="45" height="33" />
						</a>
					</span>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/flexslider.js"></script>
	<script src="assets/js/jquery.swipebox.min.js"></script>
	<!--<script src="assets/js/jquery-3.4.1.slim.min.js"></script>-->
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery.validate.js"></script>
	<script src="assets/js/custom.js"></script>

</body>
</html>