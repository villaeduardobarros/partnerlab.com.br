<?php
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
?>
<!doctype html>
<html lang="en">
<head>
	<base href="<?php echo base_url(); ?>">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="Innovoweb Soluções Digitais">
	<title>Partnerlab - Área do Cliente</title>
	<link rel="canonical" href="<?php echo current_url(); ?>" />
	<link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/swipebox.css" rel="stylesheet" type="text/css">
	<link href="assets/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/style.css" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
	<script src="assets/js/jquery.min.js"></script>
	<script>var baseUrl = '<?php echo base_url(); ?>';</script>
</head>
<body>
	<div id="top-info">
		<div class="container">
			<div class="sixteen columns">
				<div id="contact-details">
					<ul>
						<li>
							<i>
								<img src="assets/images/icons/icon-phone.png" width="12" height="12" alt="phone">
							</i>(16) 3624.7700 | 3237.3861
						</li>
						<li>
							<i>
								<img src="assets/images/icons/icon-mail.png" width="16" height="12" alt="contato">
							</i>
							<a href="mailto:partnerlab@partnerlab.com.br">partnerlab@partnerlab.com.br</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>

	<div id="header">
		<div class="container">
			<div id="logo">
				<a href="./" rel="nofollow">
					<img src="assets/images/logo.png" alt="<?php echo $this->config->item('title'); ?>">
				</a>
			</div>
			<div id="navigation">
				<ul id="nav">
					<?php if ($session == 'login-archives') { ?>

						<li>
							<a href="area-cliente/home"<?php echo ($menu == 'login-certificates') ? ' class="selected"' : NULL; ?>>CERTIFICADOS</a>
						</li>
						<li>
							<a href="area-cliente/rastreabilidade"<?php echo ($menu == 'login-traceability') ? ' class="selected"' : NULL; ?>>Rastreabilidade</a>
						</li>
						<li>
							<a href="area-cliente/sair">Sair</a>
						</li>

					<?php } ?>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<?php echo $output; ?>

	<footer id="footer">
		<div class="container">

			<div class="one-third column footer-link">
				<div class="headline"><h4>Menu</h4></div>
				<div class="left">
					<a href="./" rel="nofollow">Home</a><br>
					<a href="empresa" rel="nofollow">Empresa</a><br>
					<a href="calibracao" rel="nofollow">Calibração</a>
				</div>
				<div class="left">
					<a href="produtos" rel="nofollow">Produtos</a><br>
					<a href="servicos" rel="nofollow">Serviços</a><br>
					<a href="contato" rel="nofollow">Contato</a>
				</div>
				<div class="clear"></div>
			</div>

			<div class="one-third column">
				<div class="headline"><h4>Localização</h4></div>
				<p>
					Rua Virgilio de Carvalho Neves Neto, 926 <br>
					Ribeirão Preto/SP - CEP: 14092-440<br>
					(16) 3624.7700 | 3237.3861<br />
					Email: partnerlab@partnerlab.com.br
				</p>
			</div>

			<div class="one-third column">
				<div class="headline"><h4>Redes Sociais</h4></div>
				<p>Venha fazer parte de nossa rede social! Participe de promoções exclusivas e muito mais...</p>

				<ul class="social-icons">
					<li class="facebook">
						<a href="#" rel="nofollow">Facebook</a>
					</li>
					<li class="twitter">
						<a href="#" rel="nofollow">Twitter</a>
					</li>
				</ul>
			</div>

		</div>

		<div id="footer-bottom">
			<div class="container">
				<div>
					Partnerlab © 2015. Todos direitos reservados. 
					<span class="ijust">
						<a href="http://ijust.com.br/" target="_blank" rel="nofollow">
							<img src="assets/images/ijust.png" width="45" height="33" alt="alt="iJust Agencia Interativa"" />
						</a>
					</span>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</footer>

	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/flexslider.js"></script>
	<script src="assets/js/jquery.swipebox.min.js"></script>
	<script src="assets/js/datatables/jquery.dataTables.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery.validate.js"></script>
	<script src="assets/js/custom.js"></script>

</body>
</html>