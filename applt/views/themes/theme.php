<?php
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
?>
<!doctype html>
<html lang="en">
<head>
	<base href="<?php echo base_url(); ?>">
	<title><?php echo $seoTitle; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo $seoDescription; ?>">
	<meta name="keywords" content="<?php echo $seoKeywords; ?>">
	<meta name="robots" content="<?php echo $seoRobots; ?>">
	<meta name="author" content="Innovoweb Soluções Digitais">
	<link rel="canonical" href="<?php echo current_url(); ?>" />
	<link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
	<link href="http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/flexslider.css">
	<link rel="stylesheet" href="assets/css/swipebox.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<script src="assets/js/jquery.min.js"></script>
	<script>var baseUrl = '<?php echo base_url(); ?>';</script>
</head>
<body>

	<?php // topo ?>
	<?php include('header.php'); ?>

	<?php // conteudo ?>
	<?php echo $output; ?>

	<?php // rodape ?>
	<?php include('footer.php'); ?>	

	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/flexslider.js"></script>
	<script src="assets/js/jquery.swipebox.min.js"></script>
	<!-- <script src="assets/js/jquery-3.4.1.slim.min.js"></script> -->
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/jquery.validate.js"></script>
	<script src="assets/js/custom.js"></script>

</body>
</html>