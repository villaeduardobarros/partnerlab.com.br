<footer>
	<div class="container ">

		<?php if (!$this->agent->is_mobile()) { ?>

			<div class="row">
		
				<div class="col-12 col-lg-4 footer-link">
					<div class="headline"><h4>Menu</h4></div>
					<div class="row">
						<div class="col-6">
							<a href="home" rel="nofollow">Home</a><br>
							<a href="empresa" rel="nofollow">Empresa</a><br>
							<a href="calibracao" rel="nofollow">Calibração</a>
						</div>
						<div class="col-6">
							<a href="produtos" rel="nofollow">Produtos</a><br>
							<a href="servicos" rel="nofollow">Serviços</a><br>
							<a href="contato" rel="nofollow">Contato</a>
						</div>
					</div>
				</div>

				<div class="col-12 col-lg-4 footer-link">
					<div class="headline"><h4>Localização</h4></div>
					<p class="text-white">
						Rua Virgilio de Carvalho Neves Neto, 926 <br>
						Ribeirão Preto/SP - CEP: 14092-440<br>
						(16) 3624.7700 | 3237.3861<br />
						Email: partnerlab@partnerlab.com.br
					</p>
				</div>

				<div class="col-12 col-lg-4 footer-link">
					<div class="headline"><h4>Redes Sociais</h4></div>
					<p class="text-white">Venha fazer parte de nossa rede social! Participe de promoções exclusivas e muito mais...</p>

					<ul class="social-icons">
						<li class="facebook">
							<a href="#" rel="nofollow">Facebook</a>
						</li>
						<li class="twitter">
							<a href="#" rel="nofollow">Twitter</a>
						</li>
					</ul>
				</div>

			</div>

		<?php } else { ?>

			<div class="row">

				<div class="col-12 justify-content-center footer-link">
					<div class="headline"><h4>Localização</h4></div>
					<p>
						Rua Virgilio de Carvalho Neves Neto, 926 <br>
						Ribeirão Preto/SP - CEP: 14092-440<br>
						(16) 3624.7700 | 3237.3861<br />
						Email: partnerlab@partnerlab.com.br
					</p>
				</div>

				<div class="col-12 justify-content-center footer-link">
					<div class="headline"><h4>Redes Sociais</h4></div>
					<p>Venha fazer parte de nossa rede social! Participe de promoções exclusivas e muito mais...</p>

					<ul class="social-icons">
						<li class="facebook">
							<a href="#" rel="nofollow">Facebook</a>
						</li>
						<li class="twitter">
							<a href="#" rel="nofollow">Twitter</a>
						</li>
					</ul>
				</div>

			</div>

		<?php } ?>

		<div class="row not-margin">

			<div class="col-sm-12 col-md-12 col-lg-12">
				Partnerlab © 2015. Todos direitos reservados. 
				<span class="ijust">
					<a href="http://ijust.com.br/" target="_blank" rel="nofollow">
						<img src="assets/images/ijust.png" width="45" height="33" alt="iJust Agencia Interativa" />
					</a>
				</span>
			</div>

		</div>

	</div>
</footer>