<html>
<head>
<meta charset="utf-8" />
</head>
<body style="font-family:Arial, Helvetica, sans-serif; margin:0; padding:0;">

    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;font-family:tahoma;font-size:13px;">
        <tr style="background:#f39c12;color:#ffffff;">
            <td width="50%" style="padding:20px;">
                <img src="https://www.partnerlab.com.br/frontend/assets/images/logo.png" width="50">
            </td>
            <td width="50%" style="text-align:right;padding:20px;font-size:20px;"><?php echo $subject; ?></td>
        </tr>
        <tr>
            <td style="padding:30px;"><strong>Nome:</strong></td>
            <td style="padding:30px;"><?php echo $name; ?></td>
        </tr>
        <tr>
            <td style="padding:30px;"><strong>E-mail:</strong></td>
            <td style="padding:30px;"><?php echo $email; ?></td>
        </tr>
        <tr>
            <td style="padding:30px;"><strong>Telefone:</strong></td>
            <td style="padding:30px;"><?php echo $phone; ?></td>
        </tr>
        <tr>
            <td style="padding:30px;"><strong>Cidade/UF:</strong></td>
            <td style="padding:30px;"><?php echo $city.(($uf) ? ' / '.$uf : NULL); ?></td>
        </tr>
        <tr>
            <td colspan="2" style="padding:30px;"><strong>Mensagem:</strong><br><?php echo $message; ?></td>
        </tr>
    </table>

</body>
</html>