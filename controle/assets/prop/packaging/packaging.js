$(document).ready(function () {

    if (document.getElementById('tableItems') !== null) 
    {
        // Lista de Registros - DATATABLE
        $('#tableItems').dataTable({
            'pagingType': 'simple_numbers',
            'order': [],
            'columnDefs': [{
                'targets': [0], 
                'searchable': true, 
                'orderable': true, 
                'visible': true
            }]
        });
    }


    if (document.getElementById('packagingForm') !== null) 
    {
        // ação do form
        var action = $('input[name=action]').val();

        // validação formulário
        $('#packagingForm').validate({
            ignore: '', 
            rules: {
                title:  'required',
            },
            messages: {
                title:  'Campo obrigatório',
            }, 
            submitHandler: function(form) {
                
                $.post('packaging/packaging/action', $(form).serialize(), function(dataReturn) {
                    
                    $('#buttonForm').prop('disabled', true);

                    if (dataReturn == 'success') {

                        if (action == 'cad') {
    
                            // limpa tudo relacionado ao form
                            $('#packagingForm')[0].reset();
    
                            // mensagem sucesso cad
                            $('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Embalagem cadastrada com sucesso</p>').show();
                            $('body,html').animate({ scrollTop:0 }, 600);
                            setTimeout('document.location.reload(true);', 1800);
                            $('#buttonForm').prop('disabled', false);
    
                        } else {
    
                            // mensagem sucesso alt
                            $('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Embalagem alterada com sucesso</p>').show();
                            $('body,html').animate({ scrollTop:0 }, 600);
                            setTimeout(function(){ location.href='packaging' }, 1800);
    
                        }
    
                    } else {

                        // mensagem erro
                        $('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar a embalagem</p>').show();
                        $('body,html').animate({ scrollTop:0 }, 600);
                        setTimeout(function(){ $('#messageAlert').hide(); }, 1800);
                        $('#buttonForm').prop('disabled', false);
                        
                    }

                });

            }
        });
    }

});

var deletePacking = function(codpac){

    // dados que será exibido no modal
    var item = {};
    item['title']       = 'Confirmação';
    item['message']     = 'Tem certeza que deseja apagar esta embalagem?';
    item['button']      = 'Ok';
    $('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

        // exibe o modal
        $('#modalStsWarning').modal({
            backdrop: 'static',
            keyboard: false, 
            show:     true
        });

        $('#modalStsWarning').on('click', '.btn-ok', function(e) {
            
            $.post('packaging/packaging/actionDelete', { codpac:codpac }, function(dataReturn) {
                
                if (dataReturn == 'success'){

                    $('#modalStsWarning').modal('hide');
                    
                    // dados que será exibido no modal simples
                    var item = {};
                    item['title'] = 'Aviso!';
                    item['message'] = 'Embalagem apagada com sucesso'
                    item['button']  = 'Ok';
                    simpleDisplaysModal('#getModalSts', item, 'packaging');

                } else {

                    // dados que será exibido no modal simples
                    var item = {};
                    item['title'] = 'Aviso!';
                    item['message'] = 'Não foi possível apagar a embalagem selecionado'
                    item['button']  = 'Ok';
                    simpleDisplaysModal('#getModalSts', item, 'packaging');

                }

            });

        });

        // quando fechar o modal
        $('#modalStsWarning').on('hidden.bs.modal', function (){
            $('#getModal').html('');
        });

    });

}