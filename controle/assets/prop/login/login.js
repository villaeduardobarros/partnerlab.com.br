$(document).ready(function(){

	if (document.getElementById('loginForm') !== null) {
		$('input[name=txtuser]').focus();

		$("#loginForm").submit(function(e) {
			e.preventDefault();
			autenticar('login');
		});
	}  

	if (document.getElementById('loginForgotPasswordForm') !== null) {
		$('input[name=txtemail]').focus();

		$("#loginForgotPasswordForm").submit(function(e) {
			e.preventDefault();
			autenticar('forgot-password');
		});
	}

	if (document.getElementById('loginConfirmsDataForm') !== null) {
		$('input[name=txtcodrdf]').focus();

		$("#loginConfirmsDataForm").submit(function(e) {
			e.preventDefault();
			autenticar('confirms-data');
		});
	}

	if (document.getElementById('loginChangePasswordForm') !== null) {
		$('input[name=txtpass]').focus();

		$("#loginChangePasswordForm").submit(function(e) {
			e.preventDefault();
			autenticar('change-password');
		});

	}

});

var autenticar = function(type){
	var flag = true;
	var validaEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	// funcao de login
	if (type == 'login') {

		var usuario = $('input[name=txtuser]');
		var senha   = $('input[name=txtpass]');

		if (usuario.val() == '') {

			flag = false;
			usuario.focus();
			$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Informe seu usuário').show();
			return false;

		} else if (senha.val() == '') {
			
			flag = false;
			senha.focus();
			$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Informe sua senha').show();
			return false;

		}

	} else if (type == 'forgot-password') {

		var email     = $('input[name=txtemail]');

		if (email.val() == '') {
		
			flag = false;
			email.focus();
			$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Informe seu e-mail').show();
			return false;

		} else if(!email.val().match(validaEmail)) {
			
			flag = false;
			email.focus();
			$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Informe um e-mail válido').show();
			return false;

		}

	} else if (type == 'confirma-dados') {

		var codrdf = $('input[name=txtcodrdf]');

		if (codrdf.val() == '') {

			flag = false;
			codrdf.focus();
			$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Informado o código').show();
			return false;

		}

	} else if (type == 'trocar-senha') {

		var usuario  = $('input[name=txtuser]');
		var senha    = $('input[name=txtpass]');
		var cfmsenha = $('input[name=txtcfmsenha]');

		if (usuario.val() == '') {

			flag = false;
			usuario.focus();
			$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Informe seu usuário').show();
			return false;

		} else if (senha.val() == '') {
			
			flag = false;
			senha.focus();
			$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Informe sua senha').show();
			return false;

		} else if (senha.val() != cfmsenha.val()) {
			
			flag = false;
			cfmsenha.focus();
			$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> As senhas não coincidem').show();
			return false;

		}

	}

	if (flag == true) {

		// funcao de login
		if (type == 'login') {

			$.post('login/login/action', $('#loginForm').serialize(), function(dataReturn) {
				$('#messageAlert').removeClass().addClass('callout callout-warning').html('<i class="fa fa-spinner fa-spin"></i> Processando...').show();
				$('#buttonForm').prop('disabled', true);
				setTimeout(function(){
					if (dataReturn == 'success') {

						// limpa tudo relacionado ao form
						$('#loginForm')[0].reset();

						$('#messageAlert').removeClass().addClass('callout callout-success').html('<i class="fa fa-random"></i> Redirecionamento ativo').show();
						setTimeout(function(){ $('#messageAlert').hide(); window.location=baseUrl+'home'; }, 1200);

					} else if (dataReturn == 'wrong-fill') {

						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Preencha todos os campos').show();
						$('#buttonForm').prop('disabled', false);

					} else {

						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Dados inválidos').show();
						$('#buttonForm').prop('disabled', false);

					}
				}, 1200);
			});

		// funcao esqueci minha senha
		} else if (type == 'forgot-password') {

			$.post('login/login/actionForgotPassword', $('#loginForgotPasswordForm').serialize(), function(dataReturn) {
				$('#messageAlert').removeClass().addClass('callout callout-warning').html('<i class="fa fa-spinner fa-spin"></i> Processando...').show();
				$('#buttonForm').prop('disabled', true);
				setTimeout(function(){
					var items = dataReturn.split('|');
					if (items[0] == 'success') {

						// limpa tudo relacionado ao form
						$('#loginForgotPasswordForm')[0].reset();

						$('#messageAlertConcl').html('Mensagem enviada para <strong>'+items[1]+'</strong>, contendo as instruções para recuperar sua senha.').show();
						setTimeout(function(){ $('#messageAlertConcl').hide(); window.location=baseUrl+'logout'; }, 2600);

					} else if (items[0] == 'wrong-fill') {

						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Preencha todos os campos').show();
						$('#buttonForm').prop('disabled', false);

					} else if (items[0] == 'error-email') {

						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> E-mail não encontrado').show();
						$('#buttonForm').prop('disabled', false);

					} else {
						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Dados inválidos').show();
						$('#buttonForm').prop('disabled', false);
					}
				}, 1200);
			});

		// funcao confirmar alteracao da senha
		} else if (type == 'confirms-data') {

			$.post('login/login/actionConfirmsData', $('#loginConfirmsDataForm').serialize(), function(dataReturn) {
				$('#messageAlert').removeClass().addClass('callout callout-warning').html('<i class="fa fa-spinner fa-spin"></i> Processando...').show();
				$('#buttonForm').prop('disabled', true);
				setTimeout(function(){
					var items = dataReturn.split('|');
					if (items[0] == 'success') {

						// limpa tudloginConfirmaSenhaFormo relacionado ao form
						$('#loginConfirmsDataForm')[0].reset();

						// mensagem sucesso
						$('#messageAlert').removeClass().addClass('callout callout-success').html('<i class="fa fa-random"></i> Redirecionamento ativo').show();
						setTimeout(function(){ $('#messageAlert').hide(); window.location=baseUrl+'login/troca-senha/'+items[1] }, 1200);

					} else if (items[0] == 'error-code') {

						// mensagem erro
						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Código inválido').show();
						$('#buttonForm').prop('disabled', false);

					} else {

						// mensagem erro
						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Dados inválidos').show();
						$('#buttonForm').prop('disabled', false);

					}
				}, 1200);
			});

		// funcao troca senha
		} else if (type == 'change-password') {

			$.post('login/login/actionChangePassword', $('#loginChangePasswordForm').serialize(), function(dataReturn) {
				$('#messageAlert').removeClass().addClass('callout callout-warning').html('<i class="fa fa-spinner fa-spin"></i> Processando...').show();
				$('#buttonForm').prop('disabled', true);
				setTimeout(function(){
					if (dataReturn == 'success') {

						// limpa tudo relacionado ao form
						$('#loginChangePasswordForm')[0].reset();

						// mensagem sucesso
						$('#messageAlert').removeClass().addClass('callout callout-success').html('<i class="fa fa-random"></i> Redirecionamento ativo').show();
						setTimeout(function(){ $('#messageAlert').hide(); window.location=baseUrl+'logout'; }, 1200);

					} else {
						// mensagem erro
						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<i class="fa fa-exclamation-circle"></i> Não foi possível alterar').show();
						$('#buttonForm').prop('disabled', false);
					}
				}, 1200);
			});

		}
	
	} else {
		$('#buttonForm').prop('disabled', false);
	}
}