$(document).ready(function () {

    if (document.getElementById('tableItems') !== null) 
    {
        // Lista de Registros - DATATABLE
        $('#tableItems').dataTable({
            'pagingType': 'simple_numbers',
            'order': [],
            'columnDefs': [{
                'targets': [0], 
                'searchable': true, 
                'orderable': true, 
                'visible': true
            }]
        });
    }


    if (document.getElementById('brandsForm') !== null) 
    {
        // ação do form
        var action = $('input[name=action]').val();

        // validação formulário
        $('#brandsForm').validate({
            ignore: '', 
            rules: {
                title:  'required',
            },
            messages: {
                title:  'Campo obrigatório',
            }, 
            submitHandler: function(form) {
                
                $.post('brands/brands/action', $(form).serialize(), function(dataReturn) {
                    
                    $('#buttonForm').prop('disabled', true);

                    if (dataReturn == 'success') {

                        if (action == 'cad') {
    
                            // limpa tudo relacionado ao form
                            $('#brandsForm')[0].reset();
    
                            // mensagem sucesso cad
                            $('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Marca cadastrada com sucesso</p>').show();
                            $('body,html').animate({ scrollTop:0 }, 600);
                            setTimeout('document.location.reload(true);', 1800);
                            $('#buttonForm').prop('disabled', false);
    
                        } else {
    
                            // mensagem sucesso alt
                            $('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Marca alterada com sucesso</p>').show();
                            $('body,html').animate({ scrollTop:0 }, 600);
                            setTimeout(function(){ location.href='brands' }, 1800);
    
                        }
    
                    } else {

                        // mensagem erro
                        $('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar a marca</p>').show();
                        $('body,html').animate({ scrollTop:0 }, 600);
                        setTimeout(function(){ $('#messageAlert').hide(); }, 1800);
                        $('#buttonForm').prop('disabled', false);
                        
                    }

                });

            }
        });
    }

});

var deleteBrand = function(codbrd){

    // dados que será exibido no modal
    var item = {};
    item['title']       = 'Confirmação';
    item['message']     = 'Tem certeza que deseja apagar esta marca?';
    item['button']      = 'Ok';
    $('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

        // exibe o modal
        $('#modalStsWarning').modal({
            backdrop: 'static',
            keyboard: false, 
            show:     true
        });

        $('#modalStsWarning').on('click', '.btn-ok', function(e) {
            
            $.post('brands/brands/actionDelete', { codbrd:codbrd }, function(dataReturn) {
                
                if (dataReturn == 'success'){

                    $('#modalStsWarning').modal('hide');
                    
                    // dados que será exibido no modal simples
                    var item = {};
                    item['title'] = 'Aviso!';
                    item['message'] = 'Marca apagada com sucesso.'
                    item['button']  = 'Ok';
                    simpleDisplaysModal('#getModalSts', item, 'brands');

                } else {

                    // dados que será exibido no modal simples
                    var item = {};
                    item['title'] = 'Aviso!';
                    item['message'] = 'Não foi possível apagar a marca selecionada.'
                    item['button']  = 'Ok';
                    simpleDisplaysModal('#getModalSts', item, 'brands');

                }

            });

        });

        // quando fechar o modal
        $('#modalStsWarning').on('hidden.bs.modal', function (){
            $('#getModal').html('');
        });

    });

}