$(document).ready(function () {

	if (document.getElementById('tableItems') !== null) 
	{
        // Lista de Registros - DATATABLE
        $('#tableItems').dataTable({
            'pagingType': 'simple_numbers',
            'order': [],
            'columnDefs': [{
                'targets': [0], 
                'searchable': true, 
                'orderable': true, 
                'visible': true
            }]
        });
    }


	if (document.getElementById('usersForm') !== null) 
	{
		// ação do form
		var action = $('input[name=action]').val();

		// validação formulário
		$('#usersForm').validate({
			ignore: '', 
			rules: {
				name: 	'required',
				email: 	'required',
				user: 	'required',
				password: {
					required: function(element) {
						if (action == 'cad') {
							return true;
						}
					}
                }
            },
			messages: {
				name: 		'Campo obrigatório',
				email: 		'Campo obrigatório',
				user: 		'Campo obrigatório',
				password: 	'Campo obrigatório',
			},
			submitHandler: function(form) {

				var formData = new FormData(form);

				$.ajax({
					url: 			'users/users/action',
					type: 			'POST',
					data: 			formData,
					mimeType: 		'multipart/form-data',
					contentType: 	false,
					cache: 			false,
					processData: 	false,
					success: function(dataReturn) {

						if (dataReturn == 'success') {

							if (action == 'cad') {

								// limpa tudo relacionado ao form
								$('#usersForm')[0].reset();

								// mensagem sucesso cad
								$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Usuário cadastrado com sucesso</p>').show();
								$('body,html').animate({ scrollTop:0 }, 600);
								setTimeout('document.location.reload(true);', 1800);
								$('#buttonForm').prop('disabled', false);

							} else {

								// mensagem sucesso alt
								$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Usuário alterado com sucesso</p>').show();
								$('body,html').animate({ scrollTop:0 }, 600);
								setTimeout(function(){ location.href='users' }, 1800);

							}

						} else if (dataReturn == 'extension-not-allowed') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O arquivo não possui uma extensão permitida</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar o usuário</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 1800);
							$('#buttonForm').prop('disabled', false);

						}

					}    
				});

			}

		});

	}

});

var deleteUsers = function(codusr){

	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar este usuário?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('users/users/actionDelete', { codusr:codusr }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Usuário apagado com sucesso.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'users');

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível apagar o usuário selecionada.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'users');

				}

			});
			

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}


// monta a tela inicial das permissões
var loadPermission = function(codusr)
{
	// chama a tela com todos os usuarios
	$('#getModal').load('modal/modal_users/loadPermission', {'codusr':codusr}, function(){
		
		// exibe o modal
		$('#modalPermission').modal({
            backdrop: 'static',
            keyboard: false, 
            show:     true
        });

		// bloqueia o botão
		//$('#vincUrsPerm').prop('disabled', true);

		// ao selecionar o sistema principal
		//$('select[name=system_main]').change(function(){
		//	var opc = $(this).val();
		//	ricePermission(codusr, opc);
		//});

	});
}

// monta o conteudo de dentro do modal ao selecionar o sistema principal
/*var ricePermission = function(codusr, opc)
{
	$.post('modal/modal_users/ricePermission', {'codusr':codusr, 'opc':opc}, function(dataReturn)
	{
		// exibe a lista de permissões
		$('.list-permission').html(dataReturn);

		// libera o botão
		$('#vincUrsPerm').prop('disabled', false);
	});
}*/


// validar formulario
var validarPerm = function()
{
	var codusr  = $('input[name=codusr]').val();

	// percorre todos as opções selecionadas e adiciona no array
	var arr = new Array();
	$('input[name="permission[]"]').each(function() {  
		if($(this).is(':checked')){
			arr.push($(this).val());
		}
	});

	inserirPerm(codusr, arr.toString());
}


// inserir permissao para o usuario
var inserirPerm = function(codusr, perms)
{
	// vincula as opções selecionadas ao usuário
	$.post('modal/modal_users/inserirPerm', {'codusr':codusr, 'perms':perms}, function(dataReturnPerm) {
		if (dataReturnPerm == 'success') {

			$('#messageAlertModal').removeClass().addClass('callout callout-success').html('<p>Permissões vinculadas com sucesso</p>').show();
            $('#permUsrForm').animate({ scrollTop: $(elemento).offset().top }, 600);
            setTimeout(function(){ $('#messageAlertModal').hide(); }, 1500);

		}
	});
}