$(document).ready(function () {

	if (document.getElementById('tableItems') !== null) 
	{
		// Lista de Registros - DATATABLE
		$('#tableItems').dataTable({
			'pagingType': 'simple_numbers',
			'order': [],
			'columnDefs': [{
				'targets': [0], 
				'searchable': true, 
				'orderable': true, 
				'visible': true
			}]
		});
	}


	if (document.getElementById('customersForm') !== null) 
	{
		mask();

		// ação do form
		var action = $('input[name=action]').val();

		// validação formulário
		$('#customersForm').validate({
			ignore: '',
			rules: {
				name:       'required',
				email:      'required',
				user:       'required',
				password: {
					required: function(element) {
						return action == 'cad';
					}
				}
			},
			messages: {
				name:       'Campo obrigatório',
				email:      'Campo obrigatório',
				user:       'Campo obrigatório',
				password:   'Campo obrigatório',
			}, 
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				$.post('customers/customers/action', $(form).serialize(), function(dataReturn) {

					$('#buttonForm').prop('disabled', true);

					if (dataReturn == 'success') {

						if (action == 'cad') {

							// limpa tudo relacionado ao form
							$('#customersForm')[0].reset();

							// mensagem sucesso cad
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Cliente cadastrado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout('document.location.reload(true);', 1800);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem sucesso alt
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Cliente alterado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ location.href='customers' }, 1800);

						}

					} else {

						// mensagem erro
						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar o cliente</p>').show();
						$('body,html').animate({ scrollTop:0 }, 600);
						setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
						$('#buttonForm').prop('disabled', false);

					}

				});

			}
		});
	}


	if (document.getElementById('archiveCustomerForm') !== null) 
	{
		// validação formulário
		$('#archiveCustomerForm').validate({
			ignore: '',
			rules: {
				file: 	'required'
			},
			messages: {
				file: 	'Campo obrigatório'
			}, 
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				var formData = new FormData(form);

				$.ajax({
					url: 			'customers/customers/actionArchive',
					type: 			'POST',
					data: 			formData,
					mimeType: 		'multipart/form-data',
					contentType: 	false,
					cache: 			false,
					processData: 	false,
					success: function(dataReturn) {

						var items = dataReturn.split('|');

						if (items[0] == 'success') {

							// limpa tudo relacionado ao form
							$('#archiveCustomerForm')[0].reset();

							// mensagem sucesso cad
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Certificado cadastrado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout('document.location.reload(true);', 1800);
							$('#buttonForm').prop('disabled', false);

						} else if (items[0] == 'oversize') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O tamanho do arquivo excede 12Mb</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else if (items[0] == 'extension-not-allowed') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O arquivo não possui uma extensão permitida</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar o banner</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						}

					}    
				});

			}

		});

	}


	if (document.getElementById('archiveTraceabilityForm') !== null) 
	{
		// validação formulário
		$('#archiveTraceabilityForm').validate({
			ignore: '',
			rules: {
				file: 	'required'
			},
			messages: {
				file: 	'Campo obrigatório'
			}, 
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				var formData = new FormData(form);

				$.ajax({
					url: 			'customers/customers/actionTraceability',
					type: 			'POST',
					data: 			formData,
					mimeType: 		'multipart/form-data',
					contentType: 	false,
					cache: 			false,
					processData: 	false,
					success: function(dataReturn) {

						if (dataReturn == 'success') {

							// limpa tudo relacionado ao form
							$('#archiveTraceabilityForm')[0].reset();

							// mensagem sucesso cad
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Arquivo cadastrado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout('document.location.reload(true);', 1800);
							$('#buttonForm').prop('disabled', false);

						} else if (dataReturn == 'oversize') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O tamanho do arquivo excede 12Mb</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else if (dataReturn == 'extension-not-allowed') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O arquivo não possui uma extensão permitida</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar o arquivo</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						}

					}    
				});

			}

		});
	}

});


var deleteClient = function(codctm){

	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar este cliente?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('customers/customers/actionDelete', { codctm:codctm }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Cliente apagado com sucesso'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'customers');

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível apagar o cliente selecionado'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'customers');

				}

			});

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}


var deleteArchive = function(codarc){

	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar este certificado?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('customers/customers/actionArchiveDelete', { codarc:codarc }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Certificado apagado com sucesso'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível apagar o certificado selecionado'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				}

			});

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}


var deleteTraceability = function(codarc){

	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar este arquivo?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('customers/customers/actionDeleteTraceability', { codarc:codarc }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Arquivo apagado com sucesso'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível apagar o arquivo selecionado'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				}

			});

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}