$(document).ready(function () {

	if (document.getElementById('tableItems') !== null) 
	{
		// Lista de Registros - DATATABLE
        $('#tableItems').dataTable({
            'pagingType': 'simple_numbers',
            'order': [],
            'columnDefs': [{
                'targets': [0], 
                'searchable': true, 
                'orderable': true, 
                'visible': true
            }]
        });
	}


	if (document.getElementById('receiptControlForm') !== null) 
	{
		$('input#receiving_date').mask('99/99/9999');

		// ação do form
		var action = $('input[name=action]').val();

		// validação formulário
		$('#receiptControlForm').validate({
			ignore: '', 
			rules: {
				invoice: 			'required',
				volumes: 			'required',
				is_breakdown: 		'required',
				customer: 			'required',
				packaging: 			'required',
				transport: 			'required'
			},
			messages: {
				invoice:            'Campo obrigatório',
				volumes:            'Campo obrigatório',
				is_breakdown:       'Campo obrigatório',
				customer:           'Campo obrigatório',
				packaging:          'Campo obrigatório',
				transport:          'Campo obrigatório'
			}, 
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				$.post('receipt_control/receipt_control/action', $(form).serialize(), function(dataReturn) {

					if (dataReturn == 'success') {

						if (action == 'cad') {

							// limpa tudo relacionado ao form
							$('#receiptControlForm')[0].reset();

							// mensagem sucesso cad
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Controle de recebimento cadastrado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout('document.location.reload(true);', 1800);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem sucesso alt
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Controle de recebimento alterado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ location.href='receipt-control' }, 1800);

						}

					} else {

						// mensagem erro
						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar o controle de recebimento</p>').show();
						$('body,html').animate({ scrollTop:0 }, 600);
						setTimeout(function(){ $('#messageAlert').hide(); }, 1800);
						$('#buttonForm').prop('disabled', false);

					}

				});

			}
		});
	}


	if (document.getElementById('receiptControlEquipForm') !== null) 
	{
		// ação do form
		var action = $('input[name=action]').val();

		// validação formulário
		$('#receiptControlEquipForm').validate({
			ignore: '', 
			rules: {
				order_service: 	'required',
				accessory: 		'required',
				equipment: 		'required',
				brand: 			'required',
				model: 			'required'
			},
			messages: {
				order_service: 	'Campo obrigatório',
				accessory: 		'Campo obrigatório',
				equipment: 		'Campo obrigatório',
				brand: 			'Campo obrigatório',
				model: 			'Campo obrigatório'
			}, 
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				$.post('receipt_control/receipt_control/actionEquipment', $(form).serialize(), function(dataReturn) {

					if (dataReturn == 'success') {

						// limpa tudo relacionado ao form
						$('#receiptControlEquipForm')[0].reset();

						// mensagem sucesso cad
						$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Equipamento vinculado com sucesso</p>').show();
						$('body,html').animate({ scrollTop:0 }, 600);
						setTimeout('document.location.reload(true);', 1800);
						$('#buttonForm').prop('disabled', false);

					} else {

						// mensagem erro
						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível vincular o equipamento</p>').show();
						$('body,html').animate({ scrollTop:0 }, 600);
						setTimeout(function(){ $('#messageAlert').hide(); }, 1800);
						$('#buttonForm').prop('disabled', false);

					}

				});

			}
		});
	}


	if (document.getElementById('galleriesForm') !== null) 
	{
		// ação do form
		var action = $('input[name=action]').val();

		// validação formulário
		$('#galleriesForm').validate({
			ignore: '', 
			rules: {
				type: 		'required',
				photo: 		'required'
			},
			messages: {
				type: 		'Campo obrigatório',
				photo: 		'Campo obrigatório'
			},
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				var formData = new FormData(form);

				$.ajax({
					url: 			'receipt_control/receipt_control/actionGallery',
					type: 			'POST',
					data: 			formData,
					mimeType: 		'multipart/form-data',
					contentType: 	false,
					cache: 			false,
					processData: 	false,
					success: function(dataReturn) {

						if (dataReturn == 'success') {

							// limpa tudo relacionado ao form
							$('#galleriesForm')[0].reset();

							// mensagem sucesso cad
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Imagem cadastrado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout('document.location.reload(true);', 1800);
							$('#buttonForm').prop('disabled', false);

						} else if (dataReturn == 'extension-not-allowed') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O arquivo não possui uma extensão permitida</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar a imagem</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 1800);
							$('#buttonForm').prop('disabled', false);

						}

					}    
				});

			}

		});

	}


	if (document.getElementById('accessoriesForm') !== null) 
	{
		mask();

		// ação do form
		var action = $('input[name=action]').val();

		// validação formulário
		$('#accessoriesForm').validate({
			ignore: '', 
			rules: {
				title: 		'required',
				amount: 	'required',
				photo: 		'required'
			},
			messages: {
				title: 		'Campo obrigatório',
				amount: 	'Campo obrigatório',
				photo: 		'Campo obrigatório'
			},
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				var formData = new FormData(form);

				$.ajax({
					url: 			'receipt_control/receipt_control/actionAccessory',
					type: 			'POST',
					data: 			formData,
					mimeType: 		'multipart/form-data',
					contentType: 	false,
					cache: 			false,
					processData: 	false,
					success: function(dataReturn) {

						if (dataReturn == 'success') {

							// limpa tudo relacionado ao form
							$('#accessoriesForm')[0].reset();

							// mensagem sucesso cad
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Acessório vinculado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout('document.location.reload(true);', 1800);
							$('#buttonForm').prop('disabled', false);

						} else if (dataReturn == 'extension-not-allowed') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O arquivo não possui uma extensão permitida</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível vincular este acessório</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 1800);
							$('#buttonForm').prop('disabled', false);

						}

					}    
				});

			}

		});

	}


	if (document.getElementById('printCtrl') !== null) 
	{
		$('#printCtrl').click(function(){
			$('.printDiv').printThis({
				header: null
			});
		});		
	}

});

var deleteReceipt = function(codrct)
{
	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar este controle de recebimento?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('receipt_control/receipt_control/actionDelete', { codrct:codrct }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Controle de recebimento apagado com sucesso'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'receipt-control');

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível apagar o controle de recebimento selecionando'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'receipt-control');

				}

			});

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}

var deleteRecContEquip = function(codeqp)
{
	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar este equipamento?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('receipt_control/receipt_control/actionDeleteEquipment', { codeqp:codeqp }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Equipamento desvinculado com sucesso'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'receipt-control/equipment/'+codeqp);

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível desvinculado o equipamento selecionando'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'receipt-control/equipment/'+codeqp);

				}

			});

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}

var deleteGallery = function(codgal){

	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar esta imagem?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('receipt_control/receipt_control/actionDeleteGalleries', { codgal:codgal }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Imagem apagada com sucesso.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível apagar a imagem selecionada.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				}

			});
			

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}

var deleteAccessory = function(codacs){

	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja desvincular este acessório?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('receipt_control/receipt_control/actionDeleteAccessories', { codacs:codacs }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Acessório desvincular com sucesso.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível desvincular este acessório selecionada.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				}

			});
			

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}