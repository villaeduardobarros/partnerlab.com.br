$(document).ready(function () {

	if (document.getElementById('tableItems') !== null) 
	{
        // Lista de Registros - DATATABLE
        $('#tableItems').dataTable({
            'pagingType': 'simple_numbers',
            'order': [],
            'columnDefs': [{
                'targets': [0], 
                'searchable': true, 
                'orderable': true, 
                'visible': true
            }]
        });
    }


	if (document.getElementById('productsForm') !== null) 
	{
		// ação do form
		var action = $('input[name=action]').val();

		// validação formulário
		$('#productsForm').validate({
			ignore: '', 
			rules: {
				title: 		'required',
				category: 	'required'
			},
			messages: {
				title: 		'Campo obrigatório',
				category: 	'Campo obrigatório'
			},
			submitHandler: function(form) {

				var formData = new FormData(form);

				$.ajax({
					url: 			'products/products/action',
					type: 			'POST',
					data: 			formData,
					mimeType: 		'multipart/form-data',
					contentType: 	false,
					cache: 			false,
					processData: 	false,
					success: function(dataReturn) {

						if (dataReturn == 'success') {

							if (action == 'cad') {

								// limpa tudo relacionado ao form
								$('#productsForm')[0].reset();

								// mensagem sucesso cad
								$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Produto cadastrado com sucesso</p>').show();
								$('body,html').animate({ scrollTop:0 }, 600);
								setTimeout('document.location.reload(true);', 1800);
								$('#buttonForm').prop('disabled', false);

							} else {

								// mensagem sucesso alt
								$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Produto alterado com sucesso</p>').show();
								$('body,html').animate({ scrollTop:0 }, 600);
								setTimeout(function(){ location.href='products' }, 1800);

							}

						} else if (dataReturn == 'extension-not-allowed') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O arquivo não possui uma extensão permitida</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar o produto</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 1800);
							$('#buttonForm').prop('disabled', false);

						}

					}    
				});

			}

		});

	}


	if (document.getElementById('galleriesForm') !== null) 
	{
		// ação do form
		var action = $('input[name=action]').val();

		// validação formulário
		$('#galleriesForm').validate({
			ignore: '', 
			rules: {
				photo: 		'required'
			},
			messages: {
				photo: 		'Campo obrigatório'
			},
			submitHandler: function(form) {

				var formData = new FormData(form);

				$.ajax({
					url: 			'products/products/actionGallery',
					type: 			'POST',
					data: 			formData,
					mimeType: 		'multipart/form-data',
					contentType: 	false,
					cache: 			false,
					processData: 	false,
					success: function(dataReturn) {

						if (dataReturn == 'success') {

							// limpa tudo relacionado ao form
							$('#galleriesForm')[0].reset();

							// mensagem sucesso cad
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Imagem cadastrado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout('document.location.reload(true);', 1800);
							$('#buttonForm').prop('disabled', false);

						} else if (dataReturn == 'extension-not-allowed') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O arquivo não possui uma extensão permitida</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar a imagem</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 1800);
							$('#buttonForm').prop('disabled', false);

						}

					}    
				});

			}

		});

	}

});

var deleteProduct = function(codprd){

	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar este produto?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('products/products/actionDelete', { codprd:codprd }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Produto apagado com sucesso.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'products');

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível apagar o produto selecionada.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'products');

				}

			});
			

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}


var deleteGallery = function(codgal){

	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar esta imagem?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('products/products/actionDeleteGalleries', { codgal:codgal }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Imagem apagada com sucesso.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível apagar a imagem selecionada.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, true);

				}

			});
			

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}