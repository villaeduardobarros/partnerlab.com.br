$(document).ready(function () {

	if (document.getElementById('tableItems') !== null) 
	{
        // Lista de Registros - DATATABLE
        $('#tableItems').dataTable({
            'pagingType': 'simple_numbers',
            'order': [],
            'columnDefs': [{
                'targets': [0], 
                'searchable': true, 
                'orderable': true, 
                'visible': true
            }]
        });
    }


	if (document.getElementById('bannersForm') !== null) 
	{
		// ação do form
		var action = $('input[name=action]').val();

		// validação formulário
		$('#bannersForm').validate({
			ignore: '', 
			rules: {
				title: 		'required',
				photo: {
					required: function(){
						if (action == 'cad') {
							return true;
						}
					}
				}
			},
			messages: {
				title: 		'Campo obrigatório',
				photo: 		'Campo obrigatório',
			},
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				var formData = new FormData(form);

				$.ajax({
					url: 			'banners/banners/action',
					type: 			'POST',
					data: 			formData,
					mimeType: 		'multipart/form-data',
					contentType: 	false,
					cache: 			false,
					processData: 	false,
					success: function(dataReturn) {

						if (dataReturn == 'success') {

							if (action == 'cad') {

								// limpa tudo relacionado ao form
								$('#bannersForm')[0].reset();

								// mensagem sucesso cad
								$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Banner cadastrado com sucesso</p>').show();
								$('body,html').animate({ scrollTop:0 }, 600);
								setTimeout('document.location.reload(true);', 1800);
								$('#buttonForm').prop('disabled', false);

							} else {

								// mensagem sucesso alt
								$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Banner alterado com sucesso</p>').show();
								$('body,html').animate({ scrollTop:0 }, 600);
								setTimeout(function(){ location.href='banners' }, 1800);

							}

						} else if (dataReturn == 'extension-not-allowed') {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>O arquivo não possui uma extensão permitida</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						} else {

							// mensagem erro
							$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar o banner</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout(function(){ $('#messageAlert').hide(); }, 2600);
							$('#buttonForm').prop('disabled', false);

						}

					}    
				});

			}

		});

	}

});

var deleteBanner = function(codban){

	// dados que será exibido no modal
	var item = {};
	item['title']       = 'Confirmação';
	item['message']     = 'Tem certeza que deseja apagar este banner?';
	item['button']      = 'Ok';
	$('#getModal').load('modal/modal_status/mountsWarningConfirmation', {'item':item}, function() {

		// exibe o modal
		$('#modalStsWarning').modal({
			backdrop: 'static',
			keyboard: false, 
			show:     true
		});

		$('#modalStsWarning').on('click', '.btn-ok', function(e) {

			$.post('banners/banners/actionDelete', { codban:codban }, function(dataReturn) {

				if (dataReturn == 'success'){

					$('#modalStsWarning').modal('hide');

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Banner apagado com sucesso.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'banners');

				} else {

					// dados que será exibido no modal simples
					var item = {};
					item['title'] = 'Aviso!';
					item['message'] = 'Não foi possível apagar o banner selecionada.'
					item['button']  = 'Ok';
					simpleDisplaysModal('#getModalSts', item, 'banners');

				}

			});
			

		});

		// quando fechar o modal
		$('#modalStsWarning').on('hidden.bs.modal', function (){
			$('#getModal').html('');
		});

	});

}