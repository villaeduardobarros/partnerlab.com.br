// exibe o modal simples
var simpleDisplaysModal = function(div, item, redirect)
{
	$(div).load('modal/modal_status/mountsSimpleNotice', {item:item}, function() {

        // exibe o modal
        $('#modalStsNotice').modal({
            backdrop: 'static',
            keyboard: false, 
            show:     true
        });

        if (redirect) {
        	
        	$('#modalStsNotice').on('click', '.btn-ok', function(e) {
        		if (redirect == true) {
        			document.location.reload(true);
        		} else {
        			location.href = baseUrl+redirect;
        		}        	    
        	});
        	
    	} else {

    		// quando fechar o modal
	        $('#modalStsNotice').on('hidden.bs.modal', function (){
	            $(div).html('');
	            $('#buttonForm').prop('disabled', false);
	        });

    	}

    });

    return false;
}


// ***********************************************************
// imprimir (passa a url completa)
var imprimir = function(url){
	window.open(url);
}

// ***********************************************************
// imprimir (passa a url completa)
function imprimirDiv(div, esconder){
    var restorepage  = $('body').html();
    var printcontent = $('#' + div).clone();
    $('body').empty().html(printcontent);
    $(esconder).hide();
    window.print();
    $('body').html(restorepage);
}

// ***********************************************************
// select2
var select2 = function(){
	$('.select2').select2();
};

// ***********************************************************
// mascaras
var mask = function(){	
	// data
	if ($('input').hasClass('date')) {
		$(".date").datepicker({
			dateFormat: 'dd/mm/yy',
			dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			nextText: 'Próximo',
			prevText: 'Anterior'
		}).on('changeDate', function(ev){
			if ($('.date').valid()) {
				$('.date').removeClass('invalid').addClass('success');
			}
		}).mask('99/99/9999');
	}

	// hora
	if ($('input').hasClass('hours')) {
		$('.hours').mask('99:99');
	}

	// data e hora
	if ($('input').hasClass('datehour')) {
		$('.datehour').datetimepicker({
				lang: 'pt-BR',
				format: 'd/m/Y H:i',
				weekStart: true,
				todayBtn:  true,
				autoclose: true,
				todayHighlight: true,
				rtl: false
		}).mask('99/99/9999 99:99');
	}

	// mes e ano
	if ($('input').hasClass('monthyear')) {
		$('.monthyear').mask('99/9999');
	}

	// porcentagem
	if ($('input').hasClass('valor')) {
		$('.valor').maskMoney({
			thousands:'.', 
			decimal: ',', 
			affixesStay: true,
			allowZero: true
		});
	}

	// porcentagem
	if ($('input').hasClass('vlr-negative')) {
		$('.vlr-negative').maskMoney({
			thousands:'', 
			decimal: ',', 
			affixesStay: true,
			allowZero: true,
			allowNegative: true
		});
	}

	// cpf
	if ($('input').hasClass('cpf')) {
		$('.cpf').mask('999.999.999-99');
	}

	// cnpj
	if ($('input').hasClass('cnpj')) {
		$('.cnpj').mask('99.999.999/9999-99');
	}

	// inscrição estadual
	if ($('input').hasClass('ie')) {
		$('.ie').mask('999.999.999.999');
	}

	// cep
	if ($('input').hasClass('zipcode')) {
		$('.zipcode').mask('99999-999');
	}

	// telefone
	if ($('input').hasClass('phone')) {
		var SPMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		spOptions = {
			onKeyPress: function(val, e, field, options) {
				field.mask(SPMaskBehavior.apply({}, arguments), options);
			}
		};
  		$('.phone').mask(SPMaskBehavior, spOptions);
	}

	// placa veiculo
	if ($('input').hasClass('vehicle-plate')) {
		$('.vehicle-plate').mask('AAA-9999');
	}

	// somente numero
	if ($('input').hasClass('just-number')) {
		$('.just-number').focus(function(){
			$(this).keypress(checkNumber);
		});
	}

	// tags
	if ($('input').hasClass('tags')) {
		$('.tags').tagsinput({
			allowDuplicates: false, 
			trimValue: true, 
			width: 'auto',
			onAddTag: addTag, 
			onRemoveTag: removeTag
		});
	}

	// cor
	if ($('div').hasClass('cor')) {
		$('.cor').colorpicker().on('changeColor', function(e){
			var cor = e.color.toString('hex');
			$('input[name=txtcor]').val(cor);
		});
	}
}
// ***********************************************************


// ***********************************************************
// função que aceita somente número
var checkNumber = function(e){
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
	}
}
// ***********************************************************


// ***********************************************************
// função para contar caracteres
var countAndLimit = function(){
	$.fn.extend({
		limiter: function(limit, elem){
			$(this).on('keyup focus', function(){
				setCount(this, elem);
			});

			function setCount(src, elem){
				var chars = src.value.length;
				if (chars > limit) {
					src.value = src.value.substr(0, limit);
					chars = limit;
				}
				elem.html(limit-chars+' caracteres');
			}
			setCount($(this)[0], elem);
		}
	});
}
// ***********************************************************


// ***********************************************************
// ***** Data PT-BR ******************************************
var dateBR = function(){
	jQuery.validator.addMethod("dateBR", function (value, element) {
		//contando chars
		if (value.length != 10)
		return this.optional(element) || false;
		// verificando data
		var data = value;
		var dia = data.substr(0, 2);
		var barra1 = data.substr(2, 1);
		var mes = data.substr(3, 2);
		var barra2 = data.substr(5, 1);
		var ano = data.substr(6, 4);
		if (data.length != 10 || barra1 != "/" || barra2 != "/" || isNaN(dia) || isNaN(mes) || isNaN(ano) || dia > 31 || mes > 12) {
			return this.optional(element) || false;
		}
		if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia == 31) {
			return this.optional(element) || false;
		}
		if (mes == 2 && (dia > 29 || (dia == 29 && ano % 4 !== 0))) {
			return this.optional(element) || false;
		}
		if (ano < 1900) {
			return this.optional(element) || false;
		}
		return this.optional(element) || true;
	}, "Informe uma data válida");
}
// ***********************************************************


// ***********************************************************
// ***** CPF *************************************************
var cpf = function(){
	jQuery.validator.addMethod("cpf", function(value, element) {
		value = jQuery.trim(value);
		value = value.replace('.','');
		value = value.replace('.','');
		cpf = value.replace('-','');
		while(cpf.length < 11) cpf = "0"+ cpf;
		var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
		var a = [];
		var b = new Number;
		var c = 11;
		for (i=0; i<11; i++){
			a[i] = cpf.charAt(i);
			if (i < 9) b += (a[i] * --c);
		}
		if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
		b = 0;
		c = 11;
		for (y=0; y<10; y++) b += (a[y] * c--);
		if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }

		var retorno = true;
		if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;

		return this.optional(element) || retorno;
	}, "Informe um CPF válido");
}
// ***********************************************************


// ***********************************************************
// ***** CNPJ ************************************************
var cnpj = function(){
	jQuery.validator.addMethod('cnpj', function(cnpj, element) {
		cnpj = jQuery.trim(cnpj);

		// DEIXA APENAS OS NÚMEROS
		cnpj = cnpj.replace('/', '');
		cnpj = cnpj.replace('.', '');
		cnpj = cnpj.replace('-', '');

		var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais=1;

		if (cnpj.length < 14 && cnpj.length < 15) {
			return this.optional(element) || false;
		}
		for (i = 0; i < cnpj.length - 1; i++) {
			if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
				digitos_iguais = 0;
				break;
			}
		}

		if (!digitos_iguais) {
			tamanho = cnpj.length - 2;
			numeros = cnpj.substring(0,tamanho);
			digitos = cnpj.substring(tamanho);
			soma = 0;
			pos = tamanho - 7;

			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2) {
					pos = 9;
				}
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(0)) {
				return this.optional(element) || false;
			}
			tamanho = tamanho + 1;
			numeros = cnpj.substring(0,tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2) {
					pos = 9;
				}
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(1)) {
				return this.optional(element) || false;
			}
			return this.optional(element) || true;
		} else {
			return this.optional(element) || false;
		}
	}, 'Informe um CNPJ válido');
}