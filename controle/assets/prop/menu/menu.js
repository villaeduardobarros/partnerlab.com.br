$(document).ready(function () {

    if (document.getElementById('menForm') !== null) 
    {
        // ação do form
        var acao = $('input[name=acao]').val();

        // validação formulário
        $('#menForm').validate({
            ignore: '', 
            rules: {
                txttitulo:  'required',
            },
            messages: {
                txttitulo:  'Campo obrigatório',
            }, 
            submitHandler: function(form) {
				
				$.post('menu/menu/acao', $(form).serialize(), function(dataReturn) {
                    $('#buttonForm').prop('disabled', true);
                    if (dataReturn == 'sucesso') {

						if (acao == 'cad') {
	
							// limpa tudo relacionado ao form
							$('#menForm')[0].reset();
	
							// mensagem sucesso cad
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Item do menu cadastrado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout('document.location.reload(true);', 1500);
							$('#buttonForm').prop('disabled', false);
	
						} else {
	
							// mensagem sucesso alt
							$('#messageAlert').removeClass().addClass('callout callout-success').html('<p>Item do menu alterado com sucesso</p>').show();
							$('body,html').animate({ scrollTop:0 }, 600);
							setTimeout('document.location.reload(true);', 1500);
	
						}
	
					} else {
						// mensagem erro
						$('#messageAlert').removeClass().addClass('callout callout-danger').html('<h4>Atenção!</h4><p>Não foi possível cadastrar o item do menu</p>').show();
						$('body,html').animate({ scrollTop:0 }, 600);
						setTimeout(function(){ $('#messageAlert').hide(); }, 1500);
						$('#buttonForm').prop('disabled', false);
					}
				});
			}
        });
    }

});