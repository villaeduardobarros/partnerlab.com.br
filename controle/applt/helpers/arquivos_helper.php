<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('contaArq'))
{    
    function contaArq($tabela=NULL, $wherecampo=NULL, $whereid=NULL)
    {
        $CI =& get_instance();
        $CI->load->model('arquivos/md_arquivos');
        $CI->load->model('usuarios/md_usuarios');
        
        // ordenacao fixa pela data de envio
        $arquivos = $CI->md_arquivos->buscaArquivos($tabela, $wherecampo, $whereid, 'ASC');
        return $arquivos->num_rows();
    }
}


if (!function_exists('montalistaArq'))
{    
	function montalistaArq($tabela=NULL, $wherecampo=NULL, $whereid=NULL, $remove=TRUE)
	{
		$CI =& get_instance();
        $CI->load->model('arquivos/md_arquivos');
        $CI->load->model('usuarios/md_usuarios');

        $html = NULL;

		//caminho dos arquivos
		$pasta = 'uploads/arquivos/'.$tabela.'/';
        
        $arquivos = $CI->md_arquivos->buscaArquivos($tabela, $wherecampo, $whereid);
        if ($arquivos->num_rows() > 0) {

            $html .= '<table class="table-condensed lista-arq" style="width:100% !important;">
                <tr>
                    <th width="'.(($remove == TRUE) ? '41%' : '44%').'">Arquivo</th>
                    <th width="'.(($remove == TRUE) ? '30%' : '38%').'">Usuário</th>
                    <th width="'.(($remove == TRUE) ? '23%' : '18%').'">Data e Hora</th>';
                    if ($remove == TRUE) {
                        $html .= '<th width="6%"></th>';
                    }
                $html .= '</tr>';

            	$count = 1;
                foreach ($arquivos->result() as $arq) {

                    $nomeUsr = $CI->md_usuarios->buscaUsuario($arq->codusr, NULL, NULL, NULL, NULL, NULL)->row()->nome;

                    $html .= '<tr>
                        <td>';
                            
                            if (is_array(getimagesize('uploads/arquivos/'.$tabela.'/'.$arq->arquivo))) { 

                                $html .= '<a href="uploads/arquivos/'.$tabela.'/'.$arq->arquivo.'" rel="image_group" data-name="Nome do arquivo: '.$arq->nome.', enviado por '.$nomeUsr.' às '.$arq->datahora.'">';

                            } else {

                                $html .= '<a href="arq/'.$whereid.'/'.$tabela.'/'.$arq->arquivo.'">';

                            }
                                $html .= $arq->nome.'
                            </a>
                        </td>
                        <td>'.$nomeUsr.'</td>
                        <td>'.$arq->datahora.'</td>';

                        if ($remove == TRUE) {

                            $html .= '<td>
                                <a class="btn btn-xs btn-danger" onclick="remArquivo('.$arq->codarq.', \''.$arq->arquivo.'\', \''.$tabela.'\', \''.$wherecampo.'\', '.$whereid.')" style="cursor:pointer;">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </td>';
                        }

                        $html .= '</td>          
                    </tr>';

                    $count++;
                }

            $html .= '</table>';
        
        }

        return $html;
	}
}