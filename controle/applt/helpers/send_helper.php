<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Enviar
{
    public function envioPadrao($data=NULL, $externo=NULL) 
    {
        $CI =& get_instance();

        $configMail = $this->configSmtp($externo);
        $CI->load->library('email');
        $CI->email->initialize($configMail);
        $CI->email->clear(TRUE);

        $mensagem = $CI->load->view('mail-html/padrao', $data, true);

        // a pedido do Ivan foi colocado o nome da pessoa que abriu o chamado
        $tituloFrom = (isset($data['from']) ? 'Gepro - '.$data['from'] : 'Gepro');

        //$CI->email->from('ti01@vv.com.br', 'Eduardo');
        $CI->email->from('naoresponder@vv.com.br', $tituloFrom);
        $CI->email->to($data['dest']);
        $CI->email->subject($data['ass_email']);
        $CI->email->message($mensagem);
        if ($CI->email->send()) {
            return true;
        } else {
            $texto   = "++++++++++++++++++++++++++++++++++++++++ ".date('Y-m-d')."\r\n";
            $texto  .= $CI->email->print_debugger();
            $texto   = "+++++++++++++++++++++++++++++++++++++++++++++++++++\r\n\r\n";
            // arquivo que ira salvar
            $caminho = fopen($_SERVER['DOCUMENT_ROOT'].'/gepro/uploads/log_error.txt', 'a');
            fwrite($caminho, $texto);
            fclose($caminho);
            exit;
        }
    }

    public function stcPrj($data=NULL, $externo=NULL) 
    {
        $CI =& get_instance();

        $configMail = $this->configSmtp($externo);
        $CI->load->library('email');
        $CI->email->initialize($configMail);
        $CI->email->clear(TRUE);

        $mensagem = $CI->load->view('mail-html/stc-prj', $data, true);

        //$CI->email->from('ti01@vv.com.br', 'Eduardo');
        $CI->email->from('naoresponder@vv.com.br', 'Gepro');
        $CI->email->to($data['dest']);
        $CI->email->subject($data['assunto']);
        $CI->email->message($mensagem);
        if ($CI->email->send()) {
            return true;
        } else {
            echo $CI->email->print_debugger();
        }
    }


    public function stcAtivo($data=NULL, $externo=NULL) 
    {
        $CI =& get_instance();

        $configMail = $this->configSmtp($externo);
        $CI->load->library('email');
        $CI->email->initialize($configMail);
        $CI->email->clear(TRUE);

        $mensagem = $CI->load->view('mail-html/stc-ativo', $data, true);

        //$CI->email->from('ti01@vv.com.br', 'Eduardo');
        $CI->email->from('naoresponder@vv.com.br', 'Gepro');
        $CI->email->to($data['dest']);
        $CI->email->subject($data['assunto']);
        $CI->email->message($mensagem);
        if ($CI->email->send()) {
            return true;
        } else {
            echo $CI->email->print_debugger();
        }
    }


    public function cadUsuario($data=NULL, $externo=NULL) 
    {
        $CI =& get_instance();

        $configMail = $this->configSmtp($externo);
        $CI->load->library('email');
        $CI->email->initialize($configMail);
        $CI->email->clear(TRUE);

        $mensagem = $CI->load->view('mail-html/cad-usuario', $data, true);

        //$CI->email->from('ti01@vv.com.br', 'Eduardo');
        $CI->email->from('naoresponder@vv.com.br', 'Gepro');
        $CI->email->to($data['dest']);
        $CI->email->subject($data['assunto']);
        $CI->email->message($mensagem);
        if ($CI->email->send()) {
            return true;
        } else {
            echo $CI->email->print_debugger();
        }
    }

    public function forgotPassword($data=NULL) 
    {
        $CI =& get_instance();

        $configMail = $this->configSmtp();
        $CI->load->library('email');
        $CI->email->initialize($configMail);
        $CI->email->clear(TRUE);

        $message = $CI->load->view('mail-html/forgot-password', $data, true);

        $CI->email->from('naoresponder@partnerlab.com.br', 'Partnerlab');
        $CI->email->to($data['destination']);
        $CI->email->subject($data['subject']);
        $CI->email->message($message);
        if ($CI->email->send()) {
            return true;
        } else {
            echo $CI->email->print_debugger();
        }
    }

    public function configSmtp($externo=NULL) {
        $configMail['mailtype']     = 'html';
        $configMail['protocol']     = 'smtp';

        if ($externo == 1) {
            $configMail['smtp_host']    = 'smtp.vv.com.br';
            $configMail['smtp_port']    = '587'; 
            $configMail['smtp_user']    = 'ti01@vv.com.br';
            $configMail['smtp_pass']    = 'zaq12wsx';
        } else {
            $configMail['smtp_host'] = '192.0.0.3';
            $configMail['smtp_port'] = '25';
            $configMail['smtp_user'] = 'naoresponder@vv.com.br';
            $configMail['smtp_pass'] = 'naonao';
        }

        $configMail['smtp_auth']    = true;
        $configMail['smtp_timeout'] = '10';
        $configMail['charset']      = 'utf-8';
        $configMail['crlf']         = "\r\n";
        $configMail['newline']      = "\r\n";
        return $configMail;
    }

    /*public function testeExterno($data=NULL, $externo=NULL) 
    {
        $CI =& get_instance();

        $configMail = $this->configSmtpExt();
        $CI->load->library('email');
        $CI->email->initialize($configMail);
        $CI->email->clear(TRUE);

        $mensagem = $CI->load->view('mail-html/esqueci-senha', $data, true);

        $CI->email->from('naoresponder@vv.com.br', 'Gepro');
        $CI->email->to($data['dest']);
        $CI->email->subject($data['assunto']);
        $CI->email->message($mensagem);
        if ($CI->email->send()) {
            return true;
        } else {
            echo $CI->email->print_debugger();
        }
    }

    public function configSmtpExt() {
        $configMail['mailtype']     = 'html';
        $configMail['protocol']     = 'smtp';

        $configMail['smtp_host']    = 'smtp.vv.com.br';
        $configMail['smtp_port']    = '587'; 
        $configMail['smtp_user']    = 'ti01@vv.com.br';
        $configMail['smtp_pass']    = 'zaq12wsx';

        $configMail['smtp_auth']    = true;
        $configMail['smtp_timeout'] = '10';
        $configMail['charset']      = 'utf-8';
        $configMail['crlf']         = "\r\n";
        $configMail['newline']      = "\r\n";
        return $configMail;
    }*/
}
?>