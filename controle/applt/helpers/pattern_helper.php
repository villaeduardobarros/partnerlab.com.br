<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

if (!function_exists('configuracoes'))
{
	function configuracoes($id=NULL, $slug=NULL)
	{
		$CI =& get_instance();
		$CI->load->model('configuracoes/md_configuracoes');

		// verifica quantos itens tem abaixo do item atual
		$config = $CI->md_configuracoes->buscaConfig($id, $slug, 1);

		if ($config->num_rows() > 0) {
			$rowcfg = $config->row();
			return $rowcfg->valor;
		}
	}
}

if (!function_exists('generateCode'))
{
	function generateCode($size, $uppercase=TRUE, $numbers=TRUE, $symbols=FALSE)
	{
		$low 			= 'abcdefghijklmnopqrstuvwxyz';
		$upp 			= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$num 			= '1234567890';
		$sym 			= '!@#$%*-';
		$return 		= '';
		$characters 	= '';
		$characters    .= $low;
		if ($uppercase) $characters .= $upp;
		if ($numbers) $characters .= $num;
		if ($symbols) $characters .= $sym;
		$lenght = strlen($characters);
		for ($n=1; $n<=$size; $n++) {
			$rand = mt_rand(1, $lenght);
			$return .= $characters[$rand-1];
		}
		return $return;
	}
}

if (!function_exists('removerAcentos'))
{
	function removerAcentos($string)
	{
		return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"), $string);
	}
}

if (!function_exists('ajustaTempo'))
{
	// $tempo (yyyy-mm-dd hh:mm:ss)
	// $tpAjuste (y|m|d|h|i|s)
	// $ajuste (-|+)
	// $numAjuste (numero que sera ajustado)
	// $retorno (forma que sera exibido)
	function ajustaTempo($tempo, $tpAjuste, $ajuste, $numAjuste, $retorno)
	{
		//setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
		//date_default_timezone_set('America/Sao_Paulo');

		$tempo = ($tempo) ? $tempo : date('Y-m-d H:i:s');

		list($data, $hora) = explode(' ', $tempo);
		list($y, $m, $d) = explode('-', $data);
		list($h, $i, $s) = explode(':', $hora);

		$y = ($tpAjuste == 'y') ? (($ajuste == '-') ? (int)$y-(int)$numAjuste : (int)$y+(int)$numAjuste) : (int)$y;
		$m = ($tpAjuste == 'm') ? (($ajuste == '-') ? (int)$m-(int)$numAjuste : (int)$m+(int)$numAjuste) : (int)$m;
		$d = ($tpAjuste == 'd') ? (($ajuste == '-') ? (int)$d-(int)$numAjuste : (int)$d+(int)$numAjuste) : (int)$d;
		$h = ($tpAjuste == 'h') ? (($ajuste == '-') ? (int)$h-(int)$numAjuste : (int)$h+(int)$numAjuste) : (int)$h;
		$i = ($tpAjuste == 'i') ? (($ajuste == '-') ? (int)$i-(int)$numAjuste : (int)$i+(int)$numAjuste) : (int)$i;
		$s = ($tpAjuste == 's') ? (($ajuste == '-') ? (int)$s-(int)$numAjuste : (int)$s+(int)$numAjuste) : (int)$s;

		//echo $y.' <> '.$m.' <> '.$d.' <> '.$h.' <> '.$i.' <> '.$s.'<BR>';
		return strftime($retorno, strtotime(date('Y-m-d H:i:s', mktime($h, $i, $s, $m, $d, $y))));
	}
}

if (!function_exists('curl'))
{
	function curl($url, $post)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$retorno = curl_exec($ch);
		curl_close($ch);

		return $retorno;
	}
}