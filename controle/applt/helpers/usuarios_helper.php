<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('montaUsuariosTabelaPrincipal'))
{    
	function montaUsuariosTabelaPrincipal($subcodusr=NULL, $tipo=1)
	{
		$CI =& get_instance();
		$CI->load->model('usuarios/md_usuarios');

		$html = '';

		$usrTipo = 'usr'.$tipo;
		// ($codusr(IN) / $subcodusr / $situacao / $tpdesj(IN) / $tpndesj(IN) / $usuario / $senha
		$$usrTipo = $CI->md_usuarios->buscaUsuario(NULL, $subcodusr, NULL, $tipo);
		if ($$usrTipo->num_rows() > 0) {
            foreach ($$usrTipo->result() as $rowusr) {

	            $contaItem = $CI->md_usuarios->buscaUsuario(NULL, $rowusr->codusr, NULL, $tipo+1);
	            $count = $contaItem->num_rows();

	            $html .= '<tr>
                    <td>';
                    	if ($rowusr->tipo > 1) {
	                        for ($i=0; $i<($tipo * 5)+(($tipo==5)?5:0); $i++) {
	                            $html .= '&nbsp;';
	                        }
                        }
                        $html .= '<a href="usuarios/alt/'.$rowusr->codusr.'">'.$rowusr->nome.'</a>
                        <a href="javascript:permUsr('.$rowusr->codusr.')" class="pull-right" title="Adicionar permissão"><i class="fa fa-lock"></i></a>
                    </td>
                    <td align="center">'.(($rowusr->mtrc) ? $rowusr->mtrc : '-').'</td>
                    <td align="center">'.(($rowusr->usuario) ? $rowusr->usuario : '-').'</td>
                    <td align="center">'.(($rowusr->email) ? $rowusr->email : '-').'</td>
                    <td align="center">'.exibeStcAltDropDown($rowusr->situacao, 'usuarios', 'codusr', $rowusr->codusr, 'usuarios').'</td>
                </tr>';

                // retorna no inicio da funcao para montar os outros tipos
                if ($tipo == 1) {
                	$html .= montaUsuariosTabelaPrincipal($rowusr->codusr, 2);
                	$html .= montaUsuariosTabelaPrincipal($rowusr->codusr, 3);
                }
                if ($tipo == 2) {
                	$html .= montaUsuariosTabelaPrincipal($rowusr->codusr, 3);
        			$html .= montaUsuariosTabelaPrincipal($rowusr->codusr, 4);
                	$html .= montaUsuariosTabelaPrincipal($rowusr->codusr, 5);
        		}
        		if ($tipo == 3) {
        			$html .= montaUsuariosTabelaPrincipal($rowusr->codusr, 4);
        			$html .= montaUsuariosTabelaPrincipal($rowusr->codusr, 5);
        		}
        		if ($tipo == 4) {
        			$html .= montaUsuariosTabelaPrincipal($rowusr->codusr, 5);
        		}

            }
		}	
		
		return $html;
	}
}


if (!function_exists('montaUsuariosInverso'))
{
	//function montaUsuariosInverso($funcao, $codusr, $tipo, $fremover, $tipoauto) 
	function montaUsuariosInverso($codusr, $tipo, $tipoauto) 
	{
		$CI =& get_instance();
		$CI->load->model('usuarios/md_usuarios');

		$html = '';
		// ($codusr(IN) / $subcodusr / $situacao / $tpdesj(IN) / $tpndesj(IN) / $usuario / $senha
		$usuarios = $CI->md_usuarios->buscaUsuario($codusr, NULL, NULL, $tipo, NULL, NULL, 1);
		if ($usuarios->num_rows() > 0) {
            foreach ($usuarios->result() as $rowusr) {

            	if ($tipo == $tipoauto) {

            		$html .= $rowusr->codusr.'|';
            		/*$html .= '<div class="'.$funcao.'-usr-'.$rowusr->codusr.'">
						<div class="col-md-11 col-sm-11">'.$rowusr->nome.(($rowusr->mtrc) ? ' ('.$rowusr->mtrc.')' : NULL).'</div>
					</div>
					<div class="col-md-1 col-sm-1 text-right">
						<a class="btn btn-xs btn-danger '.$funcao.'RemUsr" '.(($fremover == true) ? 'onclick="delUrs(\''.$funcao.'\', '.$rowusr->codusr.')"' : 'disabled').'>
							<i class="fa fa-remove"></i>
						</a>
					</div>|'.$rowusr->codusr;*/

            	} else {
            		$html .= montaUsuariosInverso($rowusr->subcodusr, $rowusr->tipo-1, $tipoauto);
            	}

            }

		} else {
			$html .= montaUsuariosInverso($codusr, $tipo-1, $tipoauto);
		}
		
		return $html;
	}
}


if (!function_exists('montaUsuarios'))
{
	// $tipo: 				(selectbox/selectbox-simples/checkbox)
    // $input: 				nome do campo
    // $inputhd: 			nome do input hidden
    // $nldesj: 			nivel dos usuarios desejados (separados por virgula)
    // $nlndesj: 			nivel dos usuarios não desejados (separados por virgula)
    // $selecionados: 		usuarios selecionados (separados por virgula)
    // $selecionadosTds: 	usuarios selecionados vinculados (separados por virgula)
    // $mslc:				utiliza ou não menu selecionado (TRUE | FALSE)
	function montaUsuarios($tipo, $input, $inputhd=NULL, $nldesj=NULL, $nlndesj=NULL, $selecionados=NULL, $selecionadosTds=NULL, $mslc=TRUE)
	{
		$CI =& get_instance();
		$CI->load->model('usuarios/md_usuarios');

		if ($mslc == TRUE) {
			// busca o codigo do sistema em questão (codigo do menu)
			$codmen 	= $CI->session->userdata('mslc');
		} else {
			$codmen 	= NULL;
		}
			
		// busca os usuarios vinculados a este sistema - $codusr / $situacao / $codstm / $codmen
		$usrVinculados 	= $CI->md_usuarios->buscaUsrPerm(NULL, 1, NULL, $codmen)->row()->usrCod;
		// ($codusr(IN) / $subcodusr / $situacao / $tpdesj(IN) / $tpndesj(IN) / $usuario / $senha
		$usuarios 		= $CI->md_usuarios->buscaUsuario($usrVinculados, NULL, 1, $nldesj, $nlndesj);
		//echo $CI->db->last_query($usuarios);
		//echo $usrVinculados.' <> '.$usuarios;
		
		// ************************************************************
		// ****** Tratando os selecionados ****************************
		// ************************************************************
		// monta array (selecionados)
		$slcUsr = array();
		if (!empty($selecionados)) {
			$slcUsr = explode(',', $selecionados);
		}

		// monta array (todos)
		$tdsUsr = array();
		if (!empty($selecionadosTds)) {
			$tdsUsr = explode(',', $selecionadosTds);

			foreach ($slcUsr as $key => $value) {
				$key = array_search($value, $tdsUsr);
				if ($key !== false) {
					unset($tdsUsr[$key]);
				}
			}
		}

		$selecionados    = implode(',', array_unique($slcUsr));
		$selecionadosTds = implode(',', array_unique($tdsUsr));
		// ************************************************************
		//echo $selecionados.' <> '.$selecionadosTds;exit;

		if ($tipo == 'checkbox') {

			$html = '<div class="form-group">'.montaUsuariosOpcoes($tipo, $usuarios, $selecionados, $selecionadosTds).'</div>
				<input type="hidden" name="'.$inputhd.'" value="'.$selecionados.'" />';

		} else if ($tipo == 'selectbox' || $tipo == 'selectbox-simples') {

			$html = '<select name="'.$input.'" class="form-control select2"'.(($tipo == 'selectbox') ? ' onchange="addUrs(\'selectbox\', \''.$input.'\', \''.$inputhd.'\', \'lista-resp\', true, null, null)"' : NULL).'>
				<option value="">Selecione</option>
				'.montaUsuariosOpcoes('selectbox', $usuarios, $selecionados, $selecionadosTds).'
			</select>';

		} else if ($tipo == 'selectbox-sem-onchange') {

			$html = '<select name="'.$input.'" class="form-control select2">
				<option value="">Selecione o usuário</option>
				'.montaUsuariosOpcoes('selectbox', $usuarios, $selecionados, $selecionadosTds).'
			</select>';

		}
		
		return $html;
	}
}


if (!function_exists('montaUsuariosOpcoes'))
{    
    // $selecionados (usuarios selecionados [separados por virgula])
	function montaUsuariosOpcoes($tipo, $usuarios, $selecionados=NULL, $selecionadosTds=NULL)
	{
		$slcUsr = explode(',', $selecionados);
		$tdsUsr = explode(',', $selecionadosTds);

		$html = '';
		// percorre os usuario
		foreach ($usuarios->result() as $rowusr) {

			$attr = $class = NULL;

			if (count($slcUsr) > 0) {
				if (in_array($rowusr->codusr, $slcUsr)) {
					//$attr  = (($tipo == 'checkbox') ? ' checked disabled' : ' selected disabled');
					$attr  = (($tipo == 'checkbox') ? ' checked disabled' : ' selected');
					$class = 'itm-bloq';
				}
			}

			if (count($tdsUsr) > 0) {
				if (in_array($rowusr->codusr, $tdsUsr)) {
					$attr  = ' disabled';
					$class = 'itm-bloq';
				}
			}

			if ($tipo == 'checkbox') {

				$html .= '<div class="checkbox">
					<label class="'.$class.'">
						<input type="checkbox" name="txtusr[]" value="'.$rowusr->codusr.'"'.$attr.'> 
						'.$rowusr->nome.(($rowusr->mtrc) ? ' ('.$rowusr->mtrc.')' : NULL).'
					</label>
				</div>';

			} else if ($tipo == 'selectbox') {

				$html .= '<option value="'.$rowusr->codusr.'"'.$attr.'>'.$rowusr->nome.(($rowusr->mtrc) ? ' ('.$rowusr->mtrc.')' : NULL).'</option>';

			}

		}

		return $html;
	}
}


if (!function_exists('montaUsuariosDiv'))
{    
    // $idsusr (usuarios selecionados [separados por virgula])
    // $funcao (colocar que será exibido)
    // $fremover (true - exibeo botão de remover | false - não exibe)
	function montaUsuariosDiv($tipo, $inputhd, $lista, $selecionados=NULL, $fremover=NULL)
	{
		$CI =& get_instance();
		$CI->load->model('usuarios/md_usuarios');

		$html = '';
		if ($selecionados) {

			foreach (explode(',', $selecionados) as $codusr) {

				// busca o usuario individualmente
				$rowusr = $CI->md_usuarios->buscaUsuario($codusr, NULL, 1)->row();

				$html .= '<div class="dv-lista '.$lista.$rowusr->codusr.'">
					<div class="col-md-11 col-sm-11">'.$rowusr->nome.(($rowusr->mtrc) ? ' ('.$rowusr->mtrc.')' : NULL).'</div>
					<div class="col-md-1 col-sm-1 text-right">
						<a class="btn btn-xs btn-danger" '.(((bool)$fremover == true) ? 'onclick="delUrs(\''.$tipo.'\', \''.$inputhd.'\', \''.$lista.'\', '.$rowusr->codusr.')"' : 'disabled').'>
							<i class="fa fa-remove"></i>
						</a>
					</div>
				</div>';

			}

		}

		return $html;
	}
}


/*if (!function_exists('montaUsuariosLista'))
{    
    // $idsusr (usuarios selecionados [separados por virgula])
	function montaUsuariosLista($idsusr)
	{
		$CI =& get_instance();
		$CI->load->model('usuarios/md_usuarios');

		$html = '<ul class="form-group">';

		foreach (explode(',', $idsusr) as $codusr) {
			// busca o usuario individualmente
			$usrSelecionados = $CI->md_usuarios->buscaUsuario($codusr, NULL, NULL, NULL, NULL, 1);
			foreach ($usrSelecionados->result() as $rowusr) {

				$html .= '<li>'.$rowusr->nome.(($rowusr->mtrc) ? ' ('.$rowusr->mtrc.')' : NULL).'</li>';

			}	
		}
		
		$html .= '</ul>';
		
		return $html;
	}
}*/