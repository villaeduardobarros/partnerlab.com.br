<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('mountMenuMain'))
{
    function mountMenuMain($type=NULL, $parent=NULL, $level=NULL, $is_active=NULL, $selected=NULL)
    {
        $CI =& get_instance();
        $CI->load->model('menu/md_menu');
        $CI->load->model('users/md_users');

        $html = '';

        $item = 'items'.$level;
        $$item = $CI->md_menu->searchMenu(NULL, $parent, $level, $is_active);
        //echo $CI->db->last_query($$item).'<br>';
        if ($$item->num_rows() > 0) {
                    
            foreach ($$item->result() as $rowitm) {

                // verifica quantos itens tem abaixo do item atual
                $contaItem = $CI->md_menu->searchMenu(NULL, $rowitm->codmen, $level+1, $is_active);
                $count = $contaItem->num_rows();

                // menu principal
                if ($type == 1) {

                    if (in_array($rowitm->codmen, explode(',', $selected))) {

                        $html .= '<li class="treeview">
                            <a href="'.(($level == 0 && empty($rowitm->url)) ? '#' : $rowitm->url).'">
                                <i class="fa '.$rowitm->icon.'"></i><span>'.$rowitm->title.'</span>';

                                if ($count > 0) {
                                    $html .= '<span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>';
                                }
                            $html .= '</a>';

                            if ($count > 0) {
                                $html .= '<ul class="treeview-menu">';

                                    // retorna no inicio da funcao para montar os outros niveis
                                    $html .= mountMenuMain($type, $rowitm->codmen, $level+1, $is_active, $selected);
                                
                                $html .= '</ul>';
                            }
                        $html .= '</li>';

                    }

                // checkbox
                } else if ($type == 2) {

                    $html .= '<li style="list-style:none !important;">
                        <label style="font-weight:normal !important;">
                            <input type="checkbox" name="permission[]" value="'.$rowitm->codmen.'" '.(($selected) ? (in_array($rowitm->codmen, explode(',', $selected)) ? 'checked' : NULL) : NULL).'>
                            &nbsp;&nbsp;<i class="fa '.$rowitm->icon.'"></i>&nbsp;'.$rowitm->title.'
                        </label>';

                        if ($count > 0) {
                            $html .= '<ul>';

                                // retorna no inicio da funcao para montar os outros niveis
                                $html .= mountMenuMain($type, $rowitm->codmen, $level+1, $is_active, $selected);
                            
                            $html .= '</ul>';
                        }
                    $html .= '</li>';

                }

            }
            
        }

        return $html;
    }
}


if (!function_exists('montaMenuTabela'))
{
    function montaMenuTabela($parent=NULL, $nivel=0, $situacao=NULL)
    {
        $CI =& get_instance();
        $CI->load->model('menu/md_menu');

        $permission = NULL;
        $html = '';

        $item = 'itens'.$nivel;
        $$item = $CI->md_menu->searchMenu(NULL, $parent, $nivel, $permission, NULL, $situacao);
        if ($$item->num_rows() > 0) {
            foreach ($$item->result() as $rowitm) {

                // verifica quantos itens tem abaixo do item atual
                $contaItem = $CI->md_menu->searchMenu(NULL, $permission, $rowitm->codmen, $nivel+1, NULL, $situacao);
                $count = $contaItem->num_rows();

                $html .= '<tr>
                    <td>';
                        for ($i = 0; $i < ($rowitm->nivel * 5); $i++) {
                            $html .= '&nbsp;';
                        }
                        $html .= '<a href="menu/alt/'.$rowitm->codmen.'">'.$rowitm->titulo.'</a>
                    </td>
                </tr>';

                // retorna no inicio da funcao para montar os outros niveis
                $html .= montaMenuTabela($rowitm->codmen, $nivel+1, $situacao);
            }
        }

        return $html;
    }   
}


if (!function_exists('montaMenuLista'))
{
    function montaMenuLista($parent=NULL, $nivel=0, $situacao=NULL)
    {
        $CI =& get_instance();
        $CI->load->model('menu/md_menu');

        // verifica se o usuario ja tem alguma permissao vinculada
        //$codusr = $CI->session->userdata('login-cod');
        //$permissoes = $CI->md_usuarios->searchUsrPerm($codusr);
        //if ($permissoes->num_rows() > 0) {
        //    $perm = $permissoes->row()->menCod;
        //}
        $permission = NULL;

        $html = '<div class="box-group" id="accordion">';
        // se tiver permissão cadastrada para o usuário logado exibe o menu
        //if ($permission){

            $item = 'itens'.$nivel;
            $$item = $CI->md_menu->searchMenu(NULL, $parent, $nivel, $permission, NULL, $situacao);
            if ($$item->num_rows() > 0) {
                foreach ($$item->result() as $rowitm) {

                    // verifica quantos itens tem abaixo do item atual
                    $contaItem = $CI->md_menu->searchMenu(NULL, $permission, $rowitm->codmen, $nivel+1, NULL, $situacao);
                    $count = $contaItem->num_rows();

                    $html .= '<div class="box-header with-border">';
                        if ($rowitm->url == NULL) {
                            $html .= '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$rowitm->codmen.'" aria-expanded="false" class="collapsed">'.$rowitm->titulo.'</a>';
                        } else {
                            $html .= $rowitm->titulo;
                        }
                    $html .= '<div class="pull-right">
                            <a href="menu/alt/'.$rowitm->codmen.'">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </div>
                    </div>
                    <div id="collapse'.$rowitm->codmen.'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="box-body">';
                            if ($count > 0) {
                                $html .= '<ul>';

                                    // retorna no inicio da funcao para montar os outros niveis
                                    $html .= montaMenuLista($rowitm->codmen, $nivel+1, $situacao);
                                
                                $html .= '</ul>';
                            }
                        $html .= '</div>
                    </div>';

                }
            }

        //}

        $html .= '</div>';

        return $html;
    }   
}


if (!function_exists('montaMenuSelect'))
{
    function montaMenuSelect($parent=NULL, $nivel=0, $selecionados=NULL, $situacao=NULL)
    {
        $CI =& get_instance();
        $CI->load->model('menu/md_menu');

        $permission = NULL;
        $html = '';
        $item = 'itens'.$nivel;
        $$item = $CI->md_menu->searchMenu(NULL, $parent, $nivel, $permission, NULL, $situacao);
        if ($$item->num_rows() > 0) {
            foreach ($$item->result() as $rowitm) {

                // verifica quantos itens tem abaixo do item atual
                $contaItem = $CI->md_menu->searchMenu(NULL, $permission, $rowitm->codmen, $nivel+1, NULL, $situacao);
                $count = $contaItem->num_rows();

                $html .= '<option value="'.$rowitm->codmen.'" '.(($selecionados && $rowitm->codmen == $selecionados) ? 'selected' : NULL).'>';
                    for ($i = 0; $i < ($rowitm->nivel * 5); $i++) {
                        $html .= '&nbsp;';
                    }
                    $html .= $rowitm->titulo;
                $html .= '</option>';

                // retorna no inicio da funcao para montar os outros niveis
                $html .= montaMenuSelect($rowitm->codmen, $nivel+1, $selecionados, $situacao);
            }
        }

        return $html;
    }   
}


if (!function_exists('mountMenuCheckbox'))
{
    function mountMenuCheckbox($parent=NULL, $level=NULL, $is_active=NULL, $selected=NULL)
    {
        $CI =& get_instance();
        $CI->load->model('menu/md_menu');
        $CI->load->model('users/md_users');

        $html = '';

        $item = 'items'.$level;
        $$item = $CI->md_menu->searchMenu(NULL, $parent, $level, $is_active);
        echo $CI->db->last_query($$item).'<br>';
        if ($$item->num_rows() > 0) {
            foreach ($$item->result() as $rowitm) {

                // verifica quantos itens tem abaixo do item atual
                $contaItem = $CI->md_menu->searchMenu(NULL, $rowitm->codmen, $level+1, $is_active);
                $count = $contaItem->num_rows();

                $html .= '<div class="checkbox" style="padding-left:'.($level * 22).'px;">
                    <label>
                        <input type="checkbox" name="txtpermissao[]" value="'.$rowitm->codmen.'" '.(($selected) ? (in_array($rowitm->codmen, explode(',', $selected)) ? 'checked' : NULL) : NULL).'> '.$rowitm->title.' 
                    </label>
                </div>';

            }
        }

        return $html;
    }
}


if (!function_exists('reverseMenu'))
{
    // funcao utilizada para encontrar os códigos anteriores ao selecionado
    function reverseMenu($codmen, $level, $levelNot) 
    {
        $CI =& get_instance();

        $html = ''; 
        $menu = $CI->md_menu->searchMenu($codmen, NULL, $level, NULL, NULL, NULL, NULL);
        if ($menu->num_rows() > 0) {
            foreach ($menu->result() as $rowmen) {

                if ($level == $levelNot) {

                    $html .= $rowmen->codmen;

                } else {
                    $html .= reverseMenu($rowmen->parent_codmen, $rowmen->level-1, $niveldsj);
                }

            }

        } else {
            $html .= reverseMenu($codmen, $level-1, $niveldsj);
        }
        
        return $html;
    }
}