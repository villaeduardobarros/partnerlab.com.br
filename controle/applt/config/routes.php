<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// login
$route['login/esqueceu-a-senha'] 	= 'login/forgotPassword';
// categorias
$route['categories'] 				= 'categories/categories';
$route['categories/(:num)'] 		= 'categories/categories/index/$1';
// produtos
$route['products'] 					= 'products/products';
$route['products/(:num)'] 			= 'products/products/index/$1';
$route['products/gallery/(:num)'] 	= 'products/products/galleries/$1';
// marcas
$route['brands'] 					= 'brands/brands';
$route['brands/(:num)'] 			= 'brands/brands/index/$1';
// modelos
$route['models'] 					= 'models/models';
$route['models/(:num)'] 			= 'models/models/index/$1';
// banners
$route['banners'] 					= 'banners/banners';
$route['banners/(:num)'] 			= 'banners/banners/index/$1';
// embalagens
$route['packaging'] 				= 'packaging/packaging';
$route['packaging/(:num)'] 			= 'packaging/packaging/index/$1';
// usuarios
$route['users'] 					= 'users/users';
$route['users/(:num)'] 				= 'users/users/index/$1';
// equipamentos
$route['equipments'] 				= 'equipments/equipments';
$route['equipments/(:num)'] 		= 'equipments/equipments/index/$1';
// clientes
$route['customers'] 								= 'customers/customers';
$route['customers/(:num)'] 							= 'customers/customers/index/$1';
$route['customers/(:num)/archives/(:any)'] 			= 'customers/customers/downloadArchive/$1/$2';
$route['customers/traceability'] 					= 'customers/customers/traceability';
$route['customers/traceability/archives/(:any)'] 	= 'customers/customers/downloadTraceability/$1';
// controle de recebimento
$route['receipt-control'] 											= 'receipt_control/receipt_control/index';
$route['receipt-control/(:num)'] 									= 'receipt_control/receipt_control/index/$1';
$route['receipt-control/(:num)/equipment'] 							= 'receipt_control/receipt_control/equipment/$1';
$route['receipt-control/(:num)/equipment/(:num)/gallery'] 	= 'receipt_control/receipt_control/galleries/$1/$2';
$route['receipt-control/(:num)/equipment/(:num)/accessory'] 	= 'receipt_control/receipt_control/accessories/$1/$2';
$route['receipt-control/(:num)/print'] 								= 'receipt_control/receipt_control/printControl/$1';
