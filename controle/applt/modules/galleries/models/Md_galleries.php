<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_galleries extends CI_Model {

	public function searchGalleries($codgal=NULL, $main=NULL, $where=NULL, $value=NULL)
	{
		if ($codgal) {
			$this->db->where('codgal', $codgal);
		}
		if ($main) {
			$this->db->where('main', 1);
		}
		if ($where) {
			$this->db->where($where, $value);
		}
		return $this->db->get('galleries');
		//echo $this->db->last_query();
	}

	public function insertGalleries($data)
	{
		if ($this->db->insert('galleries', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateGalleries($codgal, $data)
	{
		$this->db->where('codgal', $codgal);
		$this->db->update('galleries', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deleteGalleries($codgal)
	{
		$this->db->where('codgal', $codgal);
		$this->db->delete('galleries');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
}