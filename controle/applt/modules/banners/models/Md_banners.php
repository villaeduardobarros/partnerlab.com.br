<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_banners extends CI_Model {

	public function searchBanners($codban=NULL, $is_active=NULL)
	{
		if ($codban) {
			$this->db->where('codban', $codban);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		return $this->db->get('banners');
		//echo $this->db->last_query();
	}

	public function insertBanners($data)
	{
		if ($this->db->insert('banners', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateBanners($codban, $data)
	{
		$this->db->where('codban', $codban);
		$this->db->update('banners', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
	public function deleteBrands($codban)
	{
		$this->db->where('codban', $codban);
		$this->db->delete('banners');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
}