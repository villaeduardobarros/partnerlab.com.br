<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="banners"><i class="fa fa-list"></i> Banners</a></li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="30%">Título</th>
                                    <th width="30%">URL</th>
                                    <th width="20%">Imagens</th>
                                    <th width="10%">Posição</th>
                                    <th width="10%">Situação</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($banners->result() as $banner) { ?>

									<tr>
										<td>
											<a href="banners/<?php echo $banner->codban; ?>" target="_self">
												<?php echo $banner->title; ?>
											</a>
											<div class="pull-right">
												<i class="fa fa-trash" onclick="deleteBanner(<?php echo $banner->codban; ?>)" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td><?php echo ($banner->url) ? $banner->url : '-'; ?></td>
										<td align="center">
											<?php if ($banner->photo) { ?>
												<img src="<?php echo $this->config->item('base_url_site'); ?>uploads/banners/<?php echo $banner->photo; ?>" width="80%">
											<?php } else echo '-'; ?>
										</td>
										<td align="center"><?php echo ($banner->position) ? $banner->position : '-'; ?></td>
										<td align="center">
											<span class="label label-<?php echo ($banner->is_active == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($banner->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="bannersForm" method="post" role="form" enctype="multipart/form-data">
					<input name="action" type="hidden" value="<?php echo $action; ?>">
					<?php if ($action == 'alt') { ?>
						<input name="codban" type="hidden" value="<?php echo $codban; ?>">
					<?php } ?>

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title"><?php echo ($action == 'cad') ? 'Cadastrar Banner' : 'Editar Banner'; ?></h3>
						</div>
						<div class="box-body">
	
							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>

							<div class="form-group">
								<label for="title">Título</label>
								<input type="text" class="form-control" id="title" name="title" value="<?php echo ($action == 'alt') ? $rowban->title : NULL; ?>" autocomplete="off">
							</div>

							<div class="form-group">
								<label for="subtitle">Subtítulo</label>
								<input type="text" class="form-control" id="" name="subtitle" value="<?php echo ($action == 'alt') ? $rowban->subtitle : NULL; ?>" autocomplete="off">
							</div>

							<div class="form-group">
								<label for="url">URL</label>
								<input type="text" class="form-control" id="url" name="url" value="<?php echo ($action == 'alt') ? $rowban->url : NULL; ?>" autocomplete="off">
							</div>

							<div class="form-group">
								<label for="photo">Imagem</label>
								<input type="file" class="form-control" id="photo" name="photo">
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="position">Posição</label>
									<input type="text" class="form-control" id="position" name="position" value="<?php echo ($action == 'alt') ? $rowban->position : NULL; ?>" autocomplete="off">
								</div>

								<div class="col-md-6">
									<label for="is_active">Situação</label>
									<select class="form-control" id="is_active" name="is_active">
										<option value="1"<?php echo (($action == 'alt' && $rowban->is_active == 1) ? ' selected' : NULL); ?>>Ativo</option>
										<option value="0"<?php echo (($action == 'alt' && is_null($rowban->is_active)) ? ' selected' : NULL); ?>>Inativo</option>
									</select>
								</div>
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="banners" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>