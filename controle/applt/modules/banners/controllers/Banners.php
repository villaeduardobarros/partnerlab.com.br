<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('photos');
		$this->load->model('md_banners');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
			exit;
		}
	}

	public function index($codban=NULL)
	{        
		// theme
		$this->output->set_template('theme');
		// theme - css
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
		$this->load->css('assets/css/jquery-ui.css');
		// theme - js
		$this->load->js('assets/js/jquery/jquery-ui.min.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
		$this->load->js('assets/js/jquery/jquery.validate.min.js');
		$this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/banners/banners.js');

		$data['banners'] = $this->md_banners->searchBanners();
		$data['action'] = ($codban) ? 'alt' : 'cad';

		if ($codban) { 

			$data['codban'] = $codban;

			$banner = $this->md_banners->searchBanners($codban);
			if ($banner->num_rows() > 0) {
				$data['rowban'] = $banner->row();
			}

		}

		$this->load->view('banners-form', $data);
	}

	public function action()
	{
		$action     = $this->input->post('action', TRUE);
		$position   = $this->input->post('position', TRUE);

		$arrData = array(
			'title' 		=> $this->input->post('title', TRUE),
			'subtitle' 		=> $this->input->post('subtitle', TRUE),
			'url' 			=> $this->input->post('url', TRUE),
			'position' 		=> ($position) ? $position : NULL,
			'is_active' 	=> (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL)
		);

		if ($action == 'cad') {

			if (!empty($_FILES['photo']['name'])) {

				// verifica se a pasta existe
				if (!is_dir(realpath('../').'/uploads/banners/')) {
					mkdir(realpath('../').'/uploads/banners', 0777, TRUE);
				}

				$ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION); // extensao do arquivo

				$allowedExts = array('gif','jpeg','jpg','png'); // extensoes permitidas

				if (in_array($ext, $allowedExts)) {

					$upload = $this->photos->send('photo', NULL, '/uploads/banners/');
					if ($upload) {
						$arrData['photo'] = $upload;
					}

				} else echo 'extension-not-allowed';

			}

			$registered = $this->md_banners->insertBanners($arrData);
			if ($registered > 0) {

				echo 'success';

			} else echo 'error';

		} else {

			$codban = $this->input->post('codban', TRUE);

			// antes de alterar, busca os dados do usuario para poder
			if (!empty($_FILES['photo']['name'])) {

				$ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION); // extensao do arquivo

				$allowedExts = array('gif','jpeg','jpg','png'); // extensoes permitidas

				if (in_array($ext, $allowedExts)) {
			
					$banner = $this->md_banners->searchBanners($codban);
					if ($banner->num_rows() > 0) {

						$oldPhoto = $banner->row()->photo;
						if (!is_null($oldPhoto)) {

							// apaga o arquivo antigo
							$this->photos->deleteArchive('/uploads/banners/', $oldPhoto);

						}
					}

					$upload = $this->photos->send('photo', NULL, '/uploads/banners/');
					if ($upload) {
						$arrData['photo'] = $upload;
					}

				} else echo 'extension-not-allowed';

			}

			$registered = $this->md_banners->updateBanners($codban, $arrData);
			if ($registered > 0) {

				echo 'success';

			} else echo 'error';
			
		}
	}


	function actionDelete()
	{
		$codban = $this->input->post('codban', TRUE);

		$banner = $this->md_banners->searchBanners($codban);
		if ($banner->num_rows() > 0) {

			$oldPhoto = $banner->row()->photo;
			if (!is_null($oldPhoto)) {

				if ($this->md_banners->deleteBrands($codban)) {

					if ($this->photos->deleteArchive('/uploads/banners/', $oldPhoto)) {

						echo 'success';

					} else echo 'error';

				}

			}

		}
		
	}

}