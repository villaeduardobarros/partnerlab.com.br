<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><i class="fa fa-sign-in"></i> Controle de Recebimento</li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="6%">Nº</th>
                                    <th>Cliente</th>
                                    <th width="14%">Data</th>
                                    <th width="14%">NFe</th>
                                    <th width="14%">Volumes</th>
                                    <th width="14%">Situação</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($controls->result() as $control) { ?>

									<?php $customer = $this->md_customers->searchCustomers($control->customers_codctm); ?>

									<tr>
										<td align="center">
											<a href="receipt-control/<?php echo $control->codrct; ?>" target="_self">
												<?php echo $control->codrct; ?>
											</a>
										</td>
										<td>
											<?php echo ($customer->num_rows() > 0) ? $customer->row()->name : '-'; ?>
											<div class="pull-right">
												<a href="receipt-control/<?php echo $control->codrct; ?>/equipment" title="Equipamentos" target="_self">
													<i class="fa fa-wrench"></i>
												</a>
												&nbsp;&nbsp;
												<a href="receipt-control/<?php echo $control->codrct; ?>/print" title="Imprimir" target="_blank">
													<i class="fa fa-print"></i>
												</a>
												&nbsp;&nbsp;
												<i class="fa fa-trash" onclick="deleteReceipt(<?php echo $control->codrct; ?>)" title="Apagar" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td align="center"><?php echo implode('/', array_reverse(explode('-', $control->receiving_date))); ?></td>
										<td align="center"><?php echo $control->invoice; ?></td>
										<td align="center"><?php echo $control->volumes; ?></td>
										<!--<td align="center">
											<span class="label label-<?php //echo ($control->is_breakdown == 1) ? 'success' : 'danger'; ?>">
												<?php //echo ($control->is_breakdown == 1) ? 'Sim' : 'Não'; ?>
											</span>
										</td>-->
										<td align="center">
											<span class="label label-<?php echo ($control->is_active == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($control->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="receiptControlForm" method="post" role="form">
					<input name="action" type="hidden" value="<?php echo $action; ?>">
					<?php if ($action == 'alt') { ?>
						<input name="codrct" type="hidden" value="<?php echo $codrct; ?>">
					<?php } ?>

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title"><?php echo ($action == 'cad') ? 'Cadastrar Controle de Recebimento' : 'Editar Controle de Recebimento'; ?></h3>
						</div>
						<div class="box-body">
	
							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="receiving_date">Dt Recebimento</label>
									<input type="text" class="form-control" id="receiving_date" name="receiving_date" value="<?php echo ($action == 'alt') ? implode('/', array_reverse(explode('-', $rowrct->receiving_date))) : date('d/m/Y'); ?>" autocomplete="off">
								</div>

								<div class="col-md-6">
									<label for="invoice">NFe</label>
									<input type="text" class="form-control" id="invoice" name="invoice" value="<?php echo ($action == 'alt') ? $rowrct->invoice : NULL; ?>" autocomplete="off">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="volumes">Volume</label>
									<input type="text" class="form-control" id="volumes" name="volumes" value="<?php echo ($action == 'alt') ? $rowrct->volumes : NULL; ?>" autocomplete="off">
								</div>

								<div class="col-md-6">
									<label for="is_breakdown">Alguma avaria?</label>
									<select name="is_breakdown" id="is_breakdown" class="form-control">
										<option value="0"<?php echo (($action == 'alt' && is_null($rowrct->is_breakdown)) ? ' selected' : NULL); ?>>Não</option>
										<option value="1"<?php echo (($action == 'alt' && $rowrct->is_breakdown == 1) ? ' selected' : NULL); ?>>Sim</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="title">Cliente</label>
								<select class="form-control" id="customer" name="customer">
									<?php if ($customers->num_rows() > 0) { ?>

										<option value="">Selecione</option>
									
										<?php foreach ($customers->result() as $customer) { ?>
										
											<option value="<?php echo $customer->codctm; ?>" <?php echo (($action == 'alt' && $rowrct->customers_codctm == $customer->codctm) ? 'selected' : NULL); ?>><?php echo $customer->name; ?></option>

										<?php } ?>
										
									<?php } else { ?>

										<option value="">Cadastre o cliente para continuar</option>

									<?php } ?>
								</select>
							</div>
                            
                            <div class="form-group">
                                <label for="comments">Observações</label>
                                <textarea rows="5" id="comments" name="comments" class="form-control"><?php echo ($action == 'alt') ? preg_replace('#<br[/\s]*>#si', "\n", $rowrct->comments) : NULL; ?></textarea>
                            </div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="packaging">Embalagem</label>
									<select class="form-control" id="packaging" name="packaging">
									<?php if ($packaging->num_rows() > 0) { ?>

										<option value="">Selecione</option>
									
										<?php foreach ($packaging->result() as $pack) { ?>
										
											<option value="<?php echo $pack->codpac; ?>" <?php echo (($action == 'alt' && $pack->codpac == $rowrct->packaging_codpac) ? 'selected' : NULL); ?>><?php echo $pack->title; ?></option>

										<?php } ?>
										
									<?php } else { ?>

										<option value="">Cadastre a embalagem para continuar</option>

									<?php } ?>
								</select>
								</div>

								<div class="col-md-6">
									<label for="transport">Transporte</label>
									<input type="text" class="form-control" id="transport" name="transport" value="<?php echo ($action == 'alt') ? $rowrct->transport : NULL; ?>" autocomplete="off">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="is_active">Situação</label>
									<select name="is_active" id="is_active" class="form-control">
										<option value="1"<?php echo (($action == 'alt' && $rowrct->is_active == 1) ? ' selected' : NULL); ?>>Ativo</option>
										<option value="0"<?php echo (($action == 'alt' && is_null($rowrct->is_active)) ? ' selected' : NULL); ?>>Inativo</option>
									</select>
								</div>
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="receipt-control" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>