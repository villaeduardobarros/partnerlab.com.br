<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="receipt-control"><i class="fa fa-sign-in"></i> Controle de Recebimento</a></li>
		<li><a href="receipt-control/<?php echo $codrct; ?>/equipment">
			<i class="fa fa-sign-in"></i> Equipamentos
		</a></li>
		<li class="active"><i class="fa fa-picture-o"></i> Galerias</li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="65%">Título</th>
                                    <th width="20%">Imagem</th>
                                    <th width="15%">Destaque</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($galleries->result() as $gallery) { ?>

									<tr>
										<td>
											<?php echo $gallery->title; ?>
											<div class="pull-right">
												<i class="fa fa-trash" onclick="deleteGallery(<?php echo $gallery->codgal; ?>)" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td>
											<img src="<?php echo $this->config->item('base_url_site'); ?>uploads/galleries/equipments/<?php echo $gallery->equipments_codeqp; ?>/<?php echo $gallery->photo; ?>" width="100%">
										</td>
										<td align="center">
											<span class="label label-<?php echo ($gallery->main == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($gallery->main == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="galleriesForm" method="post" role="form" enctype="multipart/form-data">
					<input name="codeqp" type="hidden" value="<?php echo $codeqp; ?>">

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title">Cadastrar Galeria do Produto</h3>
						</div>
						<div class="box-body">

							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="title">Título</label>
								<input type="text" class="form-control" id="title" name="title">
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="type">Tipo</label>
									<select class="form-control" id="type" name="type">
									<?php if ($equipType->num_rows() > 0) { ?>

										<option value="">Selecione</option>

										<?php foreach ($equipType->result() as $type) { ?>
											
											<option value="<?php echo $type->codtpt; ?>"><?php echo $type->title; ?></option>

										<?php } ?>
										
									<?php } else { ?>

										<option value="">Cadastre o tipo para continuar</option>

									<?php } ?>
								</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="photo">Imagem</label>
								<input type="file" class="form-control" id="photo" name="photo">
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="receipt-control/equipment/gallery/<?php echo $codeqp; ?>" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>