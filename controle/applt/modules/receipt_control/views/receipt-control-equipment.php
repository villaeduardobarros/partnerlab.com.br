<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="receipt-control"><i class="fa fa-sign-in"></i> Controle de Recebimento</a></li>
		<li class="active"><i class="fa fa-wrench"></i> Equipamentos</li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="45%">Equipamento</th>
                                    <th width="20%">Marca</th>
                                    <th width="20%">Modelo</th>
                                    <th width="15%">OS</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($equipControls->result() as $equipCtl) { ?>

									<?php // marca ?>
									<?php $brand = $this->md_brands->searchBrands($equipCtl->brands_codbrd)->row(); ?>
									
									<?php // modelo ?>
									<?php $model = $this->md_models->searchModels($equipCtl->models_codmod)->row(); ?>
									
									<?php // equipamento ?>
									<?php $equipment = $this->md_equipments->searchEquipments($equipCtl->equipments_codeqp)->row(); ?>

									<tr>
										<td>
											<?php echo $equipment->title; ?>
											<div class="pull-right">
												<a href="receipt-control/<?php echo $codrct; ?>/equipment/<?php echo $equipCtl->codeqp; ?>/gallery" title="Imagens" target="_self">
													<i class="fa fa-picture-o"></i>
												</a>
												<?php if ($equipCtl->accessory == 1) { ?>

													&nbsp;
													<a href="receipt-control/<?php echo $codrct; ?>/equipment/<?php echo $equipCtl->codeqp; ?>/accessory" title="Acessórios" target="_self">
														<i class="fa fa-tag"></i>
													</a>

												<?php } ?>
												&nbsp;
												<i class="fa fa-trash" onclick="deleteRecContEquip(<?php echo $equipCtl->codeqp; ?>)" title="Apagar" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td align="center"><?php echo $brand->title; ?></td>
										<td align="center"><?php echo $model->title; ?></td>
										<td align="center"><?php echo $equipCtl->order_service; ?></td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="receiptControlEquipForm" method="post" role="form">
					<input name="codrct" type="hidden" value="<?php echo $codrct; ?>">

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title">Cadastrar Controle de Recebimento</h3>
						</div>
						<div class="box-body">
	
							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="order_service">OS</label>
									<input type="text" class="form-control" id="order_service" name="order_service" autocomplete="off">
								</div>

								<div class="col-md-6">
									<label for="accessory">Acessório</label>
									<select name="accessory" id="accessory" class="form-control">
										<option value="1">Sim</option>
										<option value="0">Não</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="equipment">Equipamento</label>
								<select class="form-control" id="equipment" name="equipment">
									<?php if ($equipments->num_rows() > 0) { ?>

										<option value="">Selecione</option>

										<?php foreach ($equipments->result() as $equipment) { ?>
											
											<option value="<?php echo $equipment->codeqp; ?>"><?php echo $equipment->title; ?></option>-->

										<?php } ?>
										
									<?php } else { ?>

										<option value="">Cadastre o equipamento para continuar</option>

									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="brand">Marcas</label>
								<select class="form-control" id="brand" name="brand">
									<?php if ($brands->num_rows() > 0) { ?>

										<option value="">Selecione</option>
									
										<?php foreach ($brands->result() as $brand) { ?>
											
											<option value="<?php echo $brand->codbrd; ?>"><?php echo $brand->title; ?></option>

										<?php } ?>
										
									<?php } else { ?>

										<option value="">Cadastre a marca para continuar</option>

									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="model">Modelos</label>
								<select class="form-control" id="model" name="model">
									<?php if ($models->num_rows() > 0) { ?>

										<option value="">Selecione</option>
									
										<?php foreach ($models->result() as $model) { ?>
										
											<option value="<?php echo $model->codmod; ?>"><?php echo $model->title; ?></option>

										<?php } ?>
										
									<?php } else { ?>

										<option value="">Cadastre o modelo para continuar</option>

									<?php } ?>
								</select>
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="receipt-control/equipment/<?php echo $codrct; ?>" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>