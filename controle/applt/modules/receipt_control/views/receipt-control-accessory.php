<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="receipt-control"><i class="fa fa-sign-in"></i> Controle de Recebimento</a></li>
		<li><a href="receipt-control/<?php echo $codrct; ?>/equipment">
			<i class="fa fa-sign-in"></i> Equipamentos
		</a></li>
		<li class="active"><i class="fa fa-tag"></i> Acessórios</li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="38%">Acessório</th>
                                    <th width="20%">Quantidade</th>
                                    <th width="20%">Imagem</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($accessories->result() as $accessory) { ?>

									<?php $gallery = $this->md_galleries->searchGalleries(NULL, NULL, 'accessories_codacs', $accessory->codacs)->row(); ?>

									<tr>
										<td>
											<?php echo $accessory->title; ?>
											<div class="pull-right">
												<i class="fa fa-trash" onclick="deleteAccessory(<?php echo $accessory->codacs; ?>)" title="Apagar" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td align="center"><?php echo $accessory->amount; ?></td>
										<td align="center">
											<img src="<?php echo $this->config->item('base_url_site'); ?>uploads/galleries/accessories/<?php echo $accessory->codacs; ?>/<?php echo $gallery->photo; ?>" width="35%">
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="accessoriesForm" method="post" role="form" enctype="multipart/form-data">
					<input name="codeqp" type="hidden" value="<?php echo $codeqp; ?>">

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title">Cadastrar Acessório</h3>
						</div>
						<div class="box-body">

							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="title">Título</label>
								<input type="text" class="form-control" id="title" name="title">
							</div>
							
							<div class="form-group row">
								<div class="col-md-6">
									<label for="amount">Quantidade</label>
									<input type="text" class="form-control just-number" id="amount" name="amount">
								</div>
							</div>
							
							<div class="form-group">
								<label for="photo">Imagem</label>
								<input type="file" class="form-control" id="photo" name="photo">
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="receipt-control/equipment/gallery/<?php echo $codeqp; ?>" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>