<?php
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="<?php echo base_url(); ?>">
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="assets/css/print.css" media="print">
	<style media="print">
		*{ text-shadow:none !important; filter:none !important; -ms-filter:none !important; }
        body{ line-height:1.4em; font-family:calibri !important; font-size:0.75em;  }
		label{cursor:pointer !important;}
		input[type=checkbox]{display:none !important;}
		input[type="checkbox"] + label:before{border:1px solid #000000 !important;content:"\00a0" !important;display:inline-block !important;height:14px !important;margin:0 .25em 0 0 !important;padding:0 !important;vertical-align:top !important;width:14px !important;border-radius:4px !important;}
		input[type="checkbox"]:checked + label:before{color:#000000 !important;content:"\00D7" !important;font-size:12px !important;text-align:center !important;font-weight:bold !important;}
		input[type="checkbox"]:checked + label:after{font-weight:bold !important;}
	</style>
</head>
<body>
	<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" rowspan="3" width="17%">
				<img src="../assets/images/logo.png" width="90%">
			</td>
			<td align="center" rowspan="3" width="66%">
				<h4>CONTROLE DE RECEBIMENTO DE EQUIPAMENTOS</h4>
			</td>
			<td align="center" width="17%"><strong>CRE Nº.:<br>1727<?php echo date('Y'); ?>-<?php echo $control->codrct; ?></strong></td>
		</tr>
		<tr>
			<td align="center"><strong>FR.REC.001</strong></td>
		</tr>
		<tr>
			<td align="center"><strong>REV.: 00</strong></td>
		</tr>
	</table>

	<br>

	<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2">
				<strong>Cliente:</strong> 
				<?php echo $customer->name; ?> 
				&nbsp;&nbsp;&nbsp;&nbsp;
				<?php echo $customer->cnpj; ?>
			</td>
		</tr>
		<tr>
			<td width="50%"><strong>Data recebimento:</strong> <?php echo implode('/', array_reverse(explode('-', $control->receiving_date))); ?></td>
			<td width="50%"><strong>Resp. pelo preenchimento:</strong> <?php echo $control->user; ?></td>
		</tr>
		<tr>
			<td width="50%"><strong>Qtde. de volumes:</strong> <?php echo $control->volumes; ?></td>
			<td width="50%"><strong>Nº da DANFE:</strong> <?php echo $control->invoice; ?></td>
		</tr>
	</table>

	<br>

	<strong>Equipamentos Recebidos:</strong>
	<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<div class="checkbox">
					<strong>Alguma avaria?&nbsp;&nbsp;</strong>
					<input type="checkbox" <?php echo ($control->is_breakdown == 1) ? 'checked' : NULL; ?> />
					<label for="campo-checkbox1">Sim</label>
					&nbsp;&nbsp;&nbsp;
					<input type="checkbox" <?php echo (is_null($control->is_breakdown)) ? 'checked' : NULL; ?> />
					<label for="campo-checkbox1">Não</label>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="checkbox">
					<strong>Embalagem:&nbsp;&nbsp;</strong>
					<?php if ($packaging->num_rows() > 0) { ?>

						<?php foreach ($packaging->result() as $packing) { ?>

							<input type="checkbox" <?php echo ($packing->codpac == $control->packaging_codpac) ? 'checked' : NULL; ?> />
							<label for="campo-checkbox1"><?php echo $packing->title; ?></label>&nbsp;&nbsp;&nbsp;

						<?php } ?>

					<?php } ?>
				</div>
			</td>
		</tr>
		<tr>
			<td><strong>Transporte:</strong> <?php echo $control->transport; ?></td>
		</tr>
		<?php if ($control->comments) { ?>
			<tr>
				<td>
					<strong>Observações:</strong> <?php echo nl2br($control->comments); ?>
				</td>
			</tr>
		<?php } ?>
	</table>

	<br>

	<?php if ($equipments->num_rows() > 0) { ?>

		<?php foreach ($equipments->result() as $equip) { ?>

			<?php // marca ?>
			<?php $brand = $this->md_brands->searchBrands($equip->brands_codbrd)->row(); ?>
			
			<?php // modelo ?>
			<?php $model = $this->md_models->searchModels($equip->models_codmod)->row(); ?>
			
			<?php // equipamento ?>
			<?php $equipment = $this->md_equipments->searchEquipments($equip->equipments_codeqp)->row(); ?>

			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" width="25%" style="border:1px solid;"><strong>Equipamento</strong></td>
					<td align="center" width="25%" style="border:1px solid;"><strong>Marca</strong></td>
					<td align="center" width="25%" style="border:1px solid;"><strong>Modelo</strong></td>
					<td align="center" width="25%" style="border:1px solid;"><strong>OS</strong></td>
				</tr>
				<tr>
					<td align="center" style="border:1px solid;"><?php echo $equipment->title; ?></td>
					<td align="center" style="border:1px solid;"><?php echo $brand->title; ?></td>
					<td align="center" style="border:1px solid;"><?php echo $model->title; ?></td>
					<td align="center" style="border:1px solid;"><?php echo $equip->order_service; ?></td>
				</tr>
				<tr>
					<td align="center" colspan="4" style="border:1px solid;">

						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<thead>
								<tr style="border:0px;">
									<th width="33.3%"></th>
									<th width="33.3%"></th>
									<th width="33.3%"></th>
								</tr>
							</thead>
							<tbody>

								<?php
									// galerias
									$galleriesEquip = $this->md_galleries->searchGalleries(NULL, NULL, 'equipments_codeqp', $equip->codeqp);
									if ($galleriesEquip->num_rows() > 0) {

										$count = 1;
								?>
										<tr>
										
											<?php foreach ($galleriesEquip->result() as $galEquip) { ?>

												<?php
													// tipo equipamento
													$galleriesEquipType = $this->md_equipments->searchEquipmentTypes($galEquip->equipments_type_codtpt)->row();
												?>

												<td align="center" style="border:1px solid;padding:0 18px 18px;vertical-align:sub;width:33.3% !important;">
													<div><i><strong><?php echo $galleriesEquipType->title; ?></strong></i></div>
													<img src="<?php echo $this->config->item('base_url_site'); ?>uploads/galleries/equipments/<?php echo $galEquip->equipments_codeqp; ?>/<?php echo $galEquip->photo; ?>" width="100%">
												</td>

												<?php echo ($count % 3 == 0) ? '</tr><tr>' : NULL; ?>

												<?php $count++; ?>

											<?php } ?>

										</tr>

								<?php } ?>
							</tbody>
						</table>

					</td>
				</tr>
				<tr>
					<td colspan="4" style="height:50px;vertical-align:middle;">
						<div class="checkbox">
							<strong>Equipamento enviado com acessórios?&nbsp;&nbsp;</strong>
							<input type="checkbox" <?php echo ($equip->accessory == 1) ? 'checked' : NULL; ?> />
							<label for="campo-checkbox1">Sim</label>
							&nbsp;&nbsp;&nbsp;
							<input type="checkbox" <?php echo (is_null($equip->accessory)) ? 'checked' : NULL; ?> />
							<label for="campo-checkbox1">Não</label>
						</div>
					</td>
				</tr>
			</table>

			<?php 
				if ($equip->accessory == 1) {

					// acessorios
					$accessories = $this->md_receipt_control->searchEquipAcces(NULL, $equip->codeqp);
					if ($accessories->num_rows() > 0) {
			?>
						<br>

						<table border="1" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center" width="30%"><strong>Quantidade</strong></td>
								<td align="center" width="40%"><strong>Acessório</strong></td>
								<td align="center" width="30%"><strong>Imagem</strong></td>
							</tr>

							<?php
								foreach ($accessories->result() as $accessory) {

									// galerias
									$galleriesAcces = $this->md_galleries->searchGalleries(NULL, NULL, 'accessories_codacs', $accessory->codacs);
									if ($galleriesAcces->num_rows() > 0) {

										$galleryAcces = $galleriesAcces->row();
							?>
							
										<tr>
											<td align="center"><?php echo $accessory->amount; ?></td>
											<td align="center"><?php echo $accessory->title; ?></td>
											<td align="center" style="padding:5px;">
												<?php echo ($galleryAcces->photo) ? '<img src="'.$this->config->item('base_url_site').'uploads/galleries/accessories/'.$accessory->codacs.'/'.$galleryAcces->photo.'" width="50%">' : NULL; ?>
											</td>
										</tr>

							<?php
									}

								}
							?>
						</table>

					<?php } ?>

			<?php } ?>

			<br><br>

		<?php } ?>

	<?php } ?>

	<script>print();</script>

</body>
</html>