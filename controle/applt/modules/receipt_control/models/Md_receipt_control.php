<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_receipt_control extends CI_Model {

	public function searchReceiptControl($codrct=NULL, $codctm=NULL, $codpac=NULL, $is_active=NULL)
	{
		if ($codrct) {
			$this->db->where('codrct', $codrct);
		}
		if ($codctm) {
			$this->db->where('customers_codctm', $codctm);
		}
		if ($codpac) {
			$this->db->where('packaging_codpac', $codpac);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		$this->db->order_by('receiving_date', 'desc');
		return $this->db->get('receipt_control');
		//echo $this->db->last_query();
	}

	public function insertReceiptControl($data)
	{
		if ($this->db->insert('receipt_control', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateReceiptControl($codrct, $data)
	{
		$this->db->where('codrct', $codrct);
		$this->db->update('receipt_control', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deleteReceiptControl($codrct)
	{
		$this->db->where('codrct', $codrct);
		$this->db->delete('receipt_control');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}


	// equipamentos do controle
	public function searchReceiptEquip($codeqp=NULL, $codrct=NULL)
	{
		if ($codeqp) {
			$this->db->where('codeqp', $codeqp);
		}
		if ($codrct) {
			$this->db->where('receipt_control_codrct', $codrct);
		}
		return $this->db->get('receipt_control_equip');
		//echo $this->db->last_query();
	}

	public function insertReceiptEquip($data)
	{
		if ($this->db->insert('receipt_control_equip', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function deleteReceiptEquip($codeqp)
	{
		$this->db->where('codeqp', $codeqp);
		$this->db->delete('receipt_control_equip');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}


	// equipamentos do controle
	public function searchEquipAcces($codacs=NULL, $codeqp=NULL)
	{
		if ($codacs) {
			$this->db->where('codacs', $codacs);
		}
		if ($codeqp) {
			$this->db->where('equipments_codeqp', $codeqp);
		}
		return $this->db->get('accessories');
		//echo $this->db->last_query();
	}

	public function insertEquipAcces($data)
	{
		if ($this->db->insert('accessories', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function deleteEquipAcces($codacs)
	{
		$this->db->where('codacs', $codacs);
		$this->db->delete('accessories');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
}