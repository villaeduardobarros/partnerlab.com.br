<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receipt_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('photos');
		$this->load->model('md_receipt_control');
		$this->load->model('customers/md_customers');
		$this->load->model('packaging/md_packaging');
		$this->load->model('equipments/md_equipments');
		$this->load->model('galleries/md_galleries');
		$this->load->model('brands/md_brands');
		$this->load->model('models/md_models');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
			exit;
		}
	}


	public function index($codrct=NULL)
	{        
		// theme
		$this->output->set_template('theme');
		// theme - css
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
		$this->load->css('assets/plugins/datepicker/bootstrap-datepicker.css');
		$this->load->css('assets/plugins/datepicker/bootstrap-datepicker3.css');
		$this->load->css('assets/css/jquery-ui.css');
		// theme - js
		$this->load->js('assets/js/jquery/jquery-ui.min.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
		$this->load->js('assets/plugins/datepicker/bootstrap-datepicker.js');
		$this->load->js('assets/js/jquery/jquery.validate.min.js');
		$this->load->js('assets/js/jquery/jquery.mask.min.js');
		$this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/receipt_control/receipt_control.js');

		$data['controls'] = $this->md_receipt_control->searchReceiptControl();
		$data['action'] = ($codrct) ? 'alt' : 'cad';

		$data['customers'] = $this->md_customers->searchCustomers(NULL, TRUE);
		$data['packaging'] = $this->md_packaging->searchPackaging(NULL, TRUE);

		if ($codrct) { 

			$data['codrct'] = $codrct;

			$control = $this->md_receipt_control->searchReceiptControl($codrct);
			if ($control->num_rows() > 0) {
				$data['rowrct'] = $control->row();
			}

		}

		$this->load->view('receipt-control-form', $data);
	}


	public function action()
	{
		$action     = $this->input->post('action', TRUE);

		$arrData = array(
			'receiving_date' 	=> implode('-', array_reverse(explode('/', $this->input->post('receiving_date', TRUE)))),
			'invoice' 			=> $this->input->post('invoice', TRUE),
			'volumes' 			=> $this->input->post('volumes', TRUE),
			'is_breakdown' 		=> (($this->input->post('is_breakdown', TRUE) == 1) ? 1 : NULL),
			'customers_codctm' 	=> $this->input->post('customer', TRUE),
			'comments' 			=> str_replace(PHP_EOL, "<br>", $this->input->post('comments', TRUE)),
			'packaging_codpac' 	=> $this->input->post('packaging', TRUE),
			'transport' 		=> $this->input->post('transport', TRUE),
			'is_active' 		=> (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL),
			'user' 				=> $this->session->userdata('login-adm-name')
		);

		if ($action == 'cad') {

			$registered = $this->md_receipt_control->insertReceiptControl($arrData);
			if ($registered > 0) {

				echo 'success';
			
			} else echo 'error';

		} else {

			$codrct = $this->input->post('codrct', TRUE);

			if ($this->md_receipt_control->updateReceiptControl($codrct, $arrData)) {
				
				echo 'success';

			} else echo 'error';
		}
	}


	public function actionDelete()
	{
		$codrct = $this->input->post('codrct', TRUE);

		if ($this->md_receipt_control->deleteReceiptControl($codrct)) {
			
			echo 'success';

		} else echo 'error';
	}


	public function equipment($codrct=NULL)
	{        
		// theme
		$this->output->set_template('theme');
		// theme - css
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
		$this->load->css('assets/css/jquery-ui.css');
		// theme - js
		$this->load->js('assets/js/jquery/jquery-ui.min.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
		$this->load->js('assets/js/jquery/jquery.validate.min.js');
		$this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/receipt_control/receipt_control.js');

		$data['equipControls'] = $this->md_receipt_control->searchReceiptEquip(NULL, $codrct);
		$data['codrct'] = $codrct;

		$data['brands'] = $this->md_brands->searchBrands(NULL, TRUE);
		$data['models'] = $this->md_models->searchModels(NULL, NULL, TRUE);
		$data['equipments'] = $this->md_equipments->searchEquipments(NULL, NULL, NULL, TRUE, FALSE);

		$this->load->view('receipt-control-equipment', $data);
	}


	public function actionEquipment()
	{
		$action 	= $this->input->post('action', TRUE);

		$arrData = array(
			'order_service'				=> $this->input->post('order_service', TRUE),
			'accessory' 				=> (($this->input->post('accessory', TRUE) == 1) ? 1 : NULL),
			'receipt_control_codrct' 	=> $this->input->post('codrct', TRUE),
			'equipments_codeqp' 		=> $this->input->post('equipment', TRUE),
			'brands_codbrd' 			=> $this->input->post('brand', TRUE),
			'models_codmod' 			=> $this->input->post('model', TRUE)
		);

		$registered = $this->md_receipt_control->insertReceiptEquip($arrData);
		if ($registered > 0) {

			echo 'success';

		} else echo 'error';
	}


	public function actionDeleteEquipment()
	{
		$codeqp = $this->input->post('codeqp', TRUE);

		$galleries = $this->md_galleries->searchGalleries(NULL, NULL, 'equipments_codeqp', $codeqp);
		if ($galleries->num_rows() > 0) {

			foreach ($galleries->result() as $gallery) {

				// apaga a galeria
				if ($this->md_galleries->deleteGalleries($gallery->codgal)) {

					// apaga o arquivo
					$this->photos->deleteArchive('/uploads/galleries/equipments/'.$gallery->equipments_codeqp.'/', $gallery->photo);

				}

				// apaga os acessorios
				if ($this->md_receipt_control->deleteEquipAcces($gallery->accessories_codacs)) {

					// apaga o arquivo
					$this->photos->deleteArchive('/uploads/galleries/accessories/'.$gallery->accessories_codacs.'/', $gallery->photo);

					// apaga a galeria
					$this->md_galleries->deleteGalleries($gallery->codgal);

				}

			}

		}

		if ($this->md_receipt_control->deleteReceiptEquip($codeqp)) {

			echo 'success';

		} else echo 'error';

	}


	public function galleries($codrct=NULL, $codeqp=NULL)
	{
		if (!$codeqp) {
			redirect('receipt-control');
			exit;
		}

		// theme
        $this->output->set_template('theme');
        // theme - css
        $this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
        $this->load->css('assets/css/jquery-ui.css');
        // theme - js
        $this->load->js('assets/js/jquery/jquery-ui.min.js');
        $this->load->js('assets/plugins/datatables/jquery.dataTables.js');
        $this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
        $this->load->js('assets/js/jquery/jquery.validate.min.js');
        $this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/receipt_control/receipt_control.js');

		$data['codeqp'] = $codeqp;
		$data['codrct'] = $codrct;
		$data['galleries'] = $this->md_galleries->searchGalleries(NULL, NULL, 'equipments_codeqp', $codeqp);
		$data['equipType'] = $this->md_equipments->searchEquipmentTypes();


		$this->load->view('receipt-control-gallery', $data);
	}


	public function actionGallery()
	{
		$title = $this->input->post('title', TRUE);
		$codeqp = $this->input->post('codeqp', TRUE);

		$arrData = array(
			'title' 					=> $title,
			'equipments_codeqp' 		=> $codeqp,
			'equipments_type_codtpt' 	=> $this->input->post('type', TRUE)
		);

		if (!empty($_FILES['photo']['name'])) {

			// verifica se a pasta existe
			if (!is_dir(realpath('../').'/uploads/galleries/equipments/'.$codeqp.'/')) {
				mkdir(realpath('../').'/uploads/galleries/equipments/'.$codeqp, 0777, TRUE);
			}

			$ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION); // extensao do arquivo

			$allowedExts = array('gif','jpeg','jpg','png'); // extensoes permitidas

			if (in_array($ext, $allowedExts)) {

				$upload = $this->photos->send('photo', $title, '/uploads/galleries/equipments/'.$codeqp, 800, 600);
				if ($upload) {
					$arrData['photo'] = $upload;
				}

			} else echo 'extension-not-allowed';

		}

		$registered = $this->md_galleries->insertGalleries($arrData);
		if ($registered > 0) {

			echo 'success';

		} else echo 'error';

	}


	public function actionDeleteGalleries()
	{
		$codgal = $this->input->post('codgal', TRUE);

		$galleries = $this->md_galleries->searchGalleries($codgal);
		if ($galleries->num_rows() > 0) {

			$gallery = $galleries->row();

			if ($this->md_galleries->deleteGalleries($codgal)) {

				// apaga o arquivo antigo
				$this->photos->deleteArchive('/uploads/galleries/equipments/'.$gallery->equipments_codeqp.'/', $gallery->photo);

			}

			echo 'success';
		
		} else echo 'error';

	}


	public function accessories($codrct=NULL, $codeqp=NULL)
	{
		if (!$codeqp) {
			redirect('receipt-control');
			exit;
		}

		// theme
        $this->output->set_template('theme');
        // theme - css
        $this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
        $this->load->css('assets/css/jquery-ui.css');
        // theme - js
        $this->load->js('assets/js/jquery/jquery-ui.min.js');
        $this->load->js('assets/plugins/datatables/jquery.dataTables.js');
        $this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
        $this->load->js('assets/js/jquery/jquery.validate.min.js');
        $this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/receipt_control/receipt_control.js');

		$data['codeqp'] = $codeqp;
		$data['codrct'] = $codrct;
		$data['accessories'] = $this->md_receipt_control->searchEquipAcces(NULL, $codeqp);

		$this->load->view('receipt-control-accessory', $data);
	}


	public function actionAccessory()
	{
		$title = $this->input->post('title', TRUE);
		$codeqp = $this->input->post('codeqp', TRUE);

		$arrData = array(
			'title' 				=> $title,
			'amount' 				=> $this->input->post('amount', TRUE),
			'equipments_codeqp' 	=> $this->input->post('codeqp', TRUE)
		);
		$registered = $this->md_receipt_control->insertEquipAcces($arrData);
		if ($registered > 0) {

			$arrData = array(
				'title' 				=> $title,
				'accessories_codacs' 	=> $registered
			);

			if (!empty($_FILES['photo']['name'])) {

				$ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION); // extensao do arquivo

				$allowedExts = array('gif','jpeg','jpg','png'); // extensoes permitidas

				if (in_array($ext, $allowedExts)) {

					// verifica se a pasta do cliente existe
					if (!is_dir(realpath('../').'/uploads/galleries/accessories/'.$registered.'/')) {
						mkdir(realpath('../').'/uploads/galleries/accessories/'.$registered, 0777, TRUE);
					}

					$upload = $this->photos->send('photo', NULL, '/uploads/galleries/accessories/'.$registered, 800, 600);
					if ($upload) {
						$arrData['photo'] = $upload;
					}

				} else echo 'extension-not-allowed';

			}

			$registered = $this->md_galleries->insertGalleries($arrData);
			if ($registered > 0) {

				echo 'success';

			} else echo 'error';

		}

	}


	public function actionDeleteAccessories()
	{
		$codacs = $this->input->post('codacs', TRUE);

		$galleries = $this->md_galleries->searchGalleries(NULL, NULL, 'accessories_codacs', $codacs);
		if ($galleries->num_rows() > 0) {

			$gallery = $galleries->row();

			if ($this->md_receipt_control->deleteEquipAcces($gallery->accessories_codacs)) {

				// apaga o arquivo antigo
				$this->photos->deleteArchive('/uploads/galleries/accessories/'.$gallery->accessories_codacs.'/', $gallery->photo);

				// apaga a galeria
				$this->md_galleries->deleteGalleries($gallery->codgal);

			}

			echo 'success';
		
		} else echo 'error';

	}


	public function printControl($codrct)
	{
		$control = $this->md_receipt_control->searchReceiptControl($codrct);
		if ($control->num_rows() > 0) {

			$controlRec = $control->row();

			$data['control'] = $controlRec;
			$data['customer'] = $this->md_customers->searchCustomers($controlRec->customers_codctm, TRUE)->row();
			$data['packaging'] = $this->md_packaging->searchPackaging($controlRec->packaging_codpac, TRUE);
			$data['equipments'] = $this->md_receipt_control->searchReceiptEquip(NULL, $controlRec->codrct, TRUE);
		}

		$this->load->view('receipt-control-print', $data);
	}

}