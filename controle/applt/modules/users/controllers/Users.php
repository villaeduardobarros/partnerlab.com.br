<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->library('photos');
		$this->load->helper('security');
		$this->load->model('md_users');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
			exit;
		}
	}

	public function index($codusr=NULL)
	{
		// theme
		$this->output->set_template('theme');
		// theme - css
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
		$this->load->css('assets/css/jquery-ui.css');
		// theme - js
		$this->load->js('assets/js/jquery/jquery-ui.min.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
		$this->load->js('assets/js/jquery/jquery.validate.min.js');
		$this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/users/users.js');

		$data['users'] = $this->md_users->searchUsers(NULL, NULL, NULL, NULL, NULL, FALSE);
		$data['action'] = ($codusr) ? 'alt' : 'cad';

		if ($codusr) { 

			$data['codusr'] = $codusr;

			$user = $this->md_users->searchUsers($codusr);
			if ($user->num_rows() > 0) {
				$data['rowusr'] = $user->row();
			}

		}

		$this->load->view('users-form', $data);
	}

	public function action()
	{
		$action 	= $this->input->post('action', TRUE);
		$user 		= $this->input->post('user', TRUE);
		$password 	= $this->input->post('password', TRUE);

		// criptografa a senha
		$passwordCripty	= do_hash($password.$user, 'md5');
		$passwordHash	= hash('whirlpool', $passwordCripty);

		$arrData = array(
			'name' 			=> $this->input->post('name', TRUE), 
			'email' 		=> $this->input->post('email', TRUE), 
			'user' 			=> $user, 
//			'type' 			=> $this->input->post('type', TRUE), 
			'is_active' 	=> (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL)
		);

		if ($action == 'cad') {

			$arrData['password'] = $passwordHash;

			if (!empty($_FILES['photo']['name'])) {

				$ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION); // extensao do arquivo

				$allowedExts = array('gif','jpeg','jpg','png'); // extensoes permitidas

				if (in_array($ext, $allowedExts)) {

					$upload = $this->photos->uploadImg('photo', NULL, '/uploads/users/', 100, 100);
					if ($upload) {
						$arrData['photo'] = $upload;
					}

				} else echo 'extension-not-allowed';

			}

			$registered = $this->md_users->insertUsers($arrData);
			if ($registered > 0) {

				echo 'success';

			} else echo 'error';

		} else {

			$codusr = $this->input->post('codusr', TRUE);

			// se ele digitar algo no campo da senha altera
			if (!empty($password)) {
				$arrData['password'] = $passwordHash;
			}

			// antes de alterar, busca os dados do usuario para poder
			if (!empty($_FILES['photo']['name'])) {

				if (!is_dir(realpath('../').'/uploads/users/')) {
					mkdir(realpath('../').'/uploads/users', 0777, TRUE);
				}

				$ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION); // extensao do arquivo

				$allowedExts = array('gif','jpeg','jpg','png'); // extensoes permitidas

				if (in_array($ext, $allowedExts)) {
			
					$user = $this->md_users->searchUsers($codusr);
					if ($user->num_rows() > 0) {

						$oldPhoto = $user->row()->photo;
						if (!is_null($oldPhoto)) {

							// apaga o arquivo antigo
							$this->photos->deleteArchive('/uploads/users/', $oldPhoto);

						}
						
					}

				} else echo 'extension-not-allowed';

				$upload = $this->photos->uploadImg('photo', NULL, '/uploads/users/', 100, 100);
				if ($upload) {
					$arrData['photo'] = $upload;
				}

			}

			$registered = $this->md_users->updateUsers($codusr, $arrData);
			if ($registered > 0) {

				echo 'success';

			} else echo 'error';

		}
	}


    public function actionDelete()
    {
        $codusr = $this->input->post('codusr', TRUE);

        if ($this->md_users->deleteUsers($codusr)) {

        	$user = $this->md_users->searchUsers($codusr);
			if ($user->num_rows() > 0) {

				$oldPhoto = $user->row()->photo;
				if (!is_null($oldPhoto)) {

					// apaga o arquivo antigo
					$this->photos->deleteArchive('/uploads/users/', $oldPhoto);

				}

			}

            echo 'success';

        } else echo 'error';
    }

}