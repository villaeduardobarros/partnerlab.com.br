<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><i class="fa fa-gear"></i> Usuários</li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="80%">Nome</th>
                                    <th width="10%">E-mail</th>
                                    <th width="10%">Imagem</th>
                                    <th width="10%">Situação</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($users->result() as $user) { ?>

									<tr>
										<td>
											<a href="users/<?php echo $user->codusr; ?>" target="_self"><?php echo $user->name; ?></a>
											<div class="pull-right">
												<i class="fa fa-lock" onclick="loadPermission(<?php echo $user->codusr; ?>)" style="color:#900;cursor:pointer;"></i>
												&nbsp;
												<i class="fa fa-trash" onclick="deleteUsers(<?php echo $user->codusr; ?>)" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td align="center"><?php echo $user->email; ?></td>
										<td align="center">
											<?php if ($user->photo) { ?>
												<img class="img-circle" src="<?php echo $this->config->item('base_url_site'); ?>uploads/users/<?php echo $user->photo; ?>" width="50%">
											<?php } else echo '-'; ?>
										</td>
										<td align="center">
											<span class="label label-<?php echo ($user->is_active == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($user->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>
						
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="usersForm" method="post" role="form" enctype="multipart/form-data">
					<input name="action" type="hidden" value="<?php echo $action; ?>">
					<?php if ($action == 'alt') { ?>
						<input name="codusr" type="hidden" value="<?php echo $codusr; ?>">
					<?php } ?>

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title"><?php echo ($action == 'cad') ? '<i class="fa fa-plus"></i> Cadastrar Usuário' : '<i class="fa fa-edit"></i> Editar Usuário'; ?></h3>
						</div>
						<div class="box-body">

							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>

							<div class="form-group">
								<label for="name">Nome</label>
								<input type="text" class="form-control" id="name" name="name" value="<?php echo ($action == 'alt') ? $rowusr->name : NULL; ?>">
							</div>

							<div class="form-group">
								<label for="email">E-mail</label>
								<input type="text" class="form-control" id="email" name="email" value="<?php echo ($action == 'alt') ? $rowusr->email : NULL; ?>">
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="user">Usuário</label>
									<input type="text" class="form-control" id="user" name="user" value="<?php echo ($action == 'alt') ? $rowusr->user : NULL; ?>">
								</div>

								<div class="col-md-6">
									<label for="password">Senha</label>
									<input type="password" class="form-control" id="password" name="password">
								</div>
							</div>

							<div class="form-group">
								<label for="photo">Foto</label>
								<input type="file" class="form-control" id="photo" name="photo">
							</div>

							<div class="form-group row">
								<!--<div class="col-md-6">
									<label for="type">Tipo</label>
									<select class="form-control" id="type" name="type">
										<option>Selecione</option>
										<option value="1"<?php //echo (($action == 'alt' && $rowusr->type == 1) ? ' selected' : NULL); ?>>Administrador</option>
										<option value="2"<?php //echo (($action == 'alt' && $rowusr->type == 2) ? ' selected' : NULL); ?>>Usuário Comum</option>
									</select>
								</div>-->

								<div class="col-md-6">
									<label for="is_active">Situação</label>
									<select class="form-control" name="is_active" id="is_active">
										<option value="1"<?php echo (($action == 'alt' && $rowusr->is_active == 1) ? ' selected' : NULL); ?>>Ativo</option>
										<option value="0"<?php echo (($action == 'alt' && is_null($rowusr->is_active)) ? ' selected' : NULL); ?>>Inativo</option>
									</select>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="users" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			
		</div>
	</section>
</div>