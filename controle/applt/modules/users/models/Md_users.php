<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_users extends CI_Model {

	public function searchUsers($codusr=NULL, $user=NULL, $password=NULL, $email=NULL, $is_active=NULL)
	{
		if ($codusr) {
			$this->db->where('codusr', $codusr);
		}
        if ($user) {
			$this->db->where('user', $user);
		}
        if ($password) {
			$this->db->where('password', $password);
		}
        if ($email) {
			$this->db->where('email', $email);
		}
		if ($is_active) {
			if ($is_active == TRUE) {
				$this->db->where('is_active', 1);
			} else if ($is_active == FALSE) {
				$this->db->where('is_active IS NULL', NULL, FALSE);
			}
		}
		$this->db->order_by('name', 'asc');
		return $this->db->get('users');
		//echo $this->db->last_query();
	}

	public function insertUsers($data)
	{
		if ($this->db->insert('users', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateUsers($codusr, $data)
	{
		$this->db->where('codusr', $codusr);
		$this->db->update('users', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deleteUsers($codusr)
	{
		$this->db->where('codusr', $codusr);
		$this->db->delete('users');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}


	// Permissões dos usuários
	public function searchUsrPerm($codusr)
	{
		$this->db->select('GROUP_CONCAT(codper) perCod');
		$this->db->select('GROUP_CONCAT(codmen) menCod');
		if ($codusr) {
			$this->db->where('codusr', $codusr);
		}
		return $this->db->get('users_per');
		//echo $this->db->last_query();
	}

    public function inserirUsrPerm($data)
	{
		if ($this->db->insert('users_per', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function deletarUsrPerm($codusr)
	{
		$this->db->where('codusr', $codusr);
		$this->db->delete('users_per');
		$report = $this->db->error();
		return (!$report['code']) ? TRUE : FALSE;
	}
	
}