<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_menu extends CI_Model {

	function searchMenu($codmen=NULL, $parent=NULL, $level=NULL, $is_active=NULL)
	{
		if ($codmen) {
			$this->db->where('codmen', $codmen);
		}
        if ($parent) {
			$this->db->where('parent_codmen IN ('.$parent.')');
		}
		if (isset($level) && $level >= 0) {
			$this->db->where('level', $level);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		$this->db->order_by('position', 'ASC');
		return $this->db->get('menu');
		//echo $this->db->last_query();
	}

	function insertMenu($data)
	{
		if ($this->db->insert('menu', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function updateMenu($id, $data)
	{
		$this->db->where('codmen', $id);
		$this->db->update('menu', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
}