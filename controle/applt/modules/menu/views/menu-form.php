<div class="content-wrapper">
    <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="menu"><i class="fa fa-gear"></i> Menu</a></li>
    </ol>

    <section class="content-header row">
        <div class="col-md-6">
            <div class="btn-group">
                <a class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Ação&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="menu">Listar Menu</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 text-right"></div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div id="messageAlert"></div>
            </div>

            <div class="col-md-6">
                <div class="box box-widget">
                    <div class="box-body">
                        <!--<table class="table table-bordered">
                            <?php //echo $menuTabela; ?>
                            
                        </table>-->

                        <?php echo $menuLista; ?>
                    </div>
                </div>
            </div>
                
            <div class="col-md-6">
                <form id="menForm" method="post" role="form">
                    <input name="acao" type="hidden" value="<?php echo $acao; ?>">
                    <?php if ($acao == 'alt') { ?>
                        <input name="id" type="hidden" value="<?php echo $id; ?>">
                        <input name="txtnivel" type="hidden" value="<?php echo $rowmen->nivel; ?>">
                    <?php } ?>
                
                    <div class="box box-widget">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo ($acao == 'cad') ? '<i class="fa fa-plus"></i> Cadastro' : '<i class="fa fa-edit"></i> Edição'; ?></h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>T&iacute;tulo</label>
                                <div class="pull-right">
                                    <div class="checkbox" style="margin:0 !important;">
                                        <label>
                                            <input type="checkbox" name="txtsistema" value="1" <?php echo ($acao == 'alt' && $rowmen->principal == 1) ? 'checked' : NULL; ?>> Acesso principal ao sistema 
                                        </label>
                                    </div>
                                </div>
                                <input type="text" class="form-control" name="txttitulo" value="<?php echo ($acao == 'alt') ? $rowmen->titulo : NULL; ?>">
                            </div>

                            <div class="form-group">
                                <label>Vincular Menu</label>
                                <select name="txtparent" class="form-control">
                                    <option value="1000">Menu principal</option>
                                    <?php echo $menuForm; ?>
                                </select>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>URL</label>
                                    <input type="text" class="form-control" name="txturl" value="<?php echo ($acao == 'alt') ? $rowmen->url : NULL; ?>">
                                </div>

                                <div class="col-md-6">
                                    <label>ícone</label>
                                    <input type="text" class="form-control" name="txticone" value="<?php echo ($acao == 'alt') ? $rowmen->icone : NULL; ?>">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Posi&ccedil;&atilde;o</label>
                                    <input type="text" class="form-control" name="txtposicao" value="<?php echo ($acao == 'alt') ? $rowmen->posicao : NULL; ?>">
                                </div>

                                <div class="col-md-6">
                                    <label>Situa&ccedil;&atilde;o</label>
                                    <?php echo $situacaoForm; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <a class="btn btn-default" href="menu" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
                                <button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>


<?php //phpinfo(); ?>