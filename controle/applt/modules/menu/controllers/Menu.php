<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
        	parent::__construct();
        	if (!$this->session->userdata('logged-in')) {
        		redirect('logout');
        		exit;
        	}
	}

	public function index() 
	{
        // theme
        $this->output->set_template('theme');
        // theme - css
        $this->load->css('assets/plugins/easyui/tree.css');
        // theme - js
        $this->load->js('assets/plugins/easyui/jquery.easyui.min.js');
        $this->load->js('assets/js/jquery/jquery.validate.min.js');
        $this->load->js('assets/prop/menu/menu.js');
        
        $data['acao']           = 'cad';
        $data['menuLista']      = montaMenuLista();
        $data['menuForm']       = montaMenuSelect();
        $data['situacaoForm']   = exibeStcFormSelect('txtsituacao', 1, false);
        $this->load->view('menu-form', $data);
	}

    public function alt($id)
    {
            // theme
            $this->output->set_template('theme');
            // theme - js
            $this->load->js('assets/js/jquery/jquery.validate.min.js');
            $this->load->js('assets/prop/menu/menu.js');
            
            $data['id']             = $id;
            $data['acao']           = 'alt';
            $data['rowmen']         = $this->md_menu->buscaItens($id)->row();
            $data['menuLista']      = montaMenuLista();
            $data['menuForm']       = montaMenuSelect(NULL, 0, $data['rowmen']->parent_codmen);
            $data['situacaoForm']   = exibeStcFormSelect('txtsituacao', $data['rowmen']->situacao, false);
            $this->load->view('menu-form', $data);
    }

    public function acao()
    {
        $acao      = $this->input->post('acao', TRUE);
        $txtparent = $this->input->post('txtparent', TRUE);

        // busca o nivel para poder acrescentar mais 1
        if ($txtparent != 1000) {
            $item = $this->md_menu->buscaItens($txtparent)->row();
        }

        $arrData = array(
            'parent_codmen'     => (($txtparent == 1000) ? NULL : $txtparent),
            'titulo'            => $this->input->post('txttitulo', TRUE),
            'url'               => (($this->input->post('txturl', TRUE)) ? $this->input->post('txturl', TRUE) : NULL),
            'icone'             => $this->input->post('txticone', TRUE),
            'posicao'           => $this->input->post('txtposicao', TRUE),
            'situacao'          => $this->input->post('txtsituacao', TRUE),
            'principal'         => (($this->input->post('txtsistema', TRUE) == 1) ? 1 : NULL),
            'nivel'             => (($txtparent == 1000) ? 0 : $item->nivel+1)
        );

        //echo (($txtparent == 1000) ? 0 : $item->nivel).' <br> ';
        //echo $txtparent.' <br>';
        //echo '<pre>';
        //    var_dump($arrData);
        //echo '</pre>';
        //exit;

        if ($acao == 'cad') {

            //$arrData['nivel'] = ($this->input->post('txtparent', TRUE) == 1000) ? 0 : $item->nivel+1;

            $idRegCad = $this->md_menu->inserirMenu($arrData);
            if ($idRegCad > 0) {
                // registra log de alteração
                    $arrDataLog = array(
                        'codmen'     => $idRegCad,
                        'codusr'     => $this->session->userdata('login-cod'),
                        'tipo'       => 'I'
                    );
                    if ($this->md_log->inserirLog('menu', $arrDataLog) > 0) {
                        echo 'sucesso';
                    } else {
                        echo 'erro';
                    }
            } else {
                echo 'erro';
            }

        } else {

            $id = $this->input->post('id', TRUE);
            
            // reserva as informações antes de alterar
            $dataBefore = json_encode($arrData);

            //$arrData['nivel'] = ($this->input->post('txtparent', TRUE) == 1000) ? 0 : $this->input->post('txtnivel', TRUE)+1;

            if ($this->md_menu->alterarMenu($id, $arrData)) {

                    // registra log de alteração
                    $arrDataLog = array(
                        'codmen'     => $id,
                        'codusr'     => $this->session->userdata('login-cod'),
                        'tipo'       => 'U',
                        'valores'    => $dataBefore
                    );
                    if ($this->md_log->inserirLog('menu', $arrDataLog) > 0) {
                        echo 'sucesso';
                    } else {
                        echo 'erro';
                    }
            } else {
                echo 'erro';
            }

        }
    }

    // monta a sessao no 
    public function menuSelecionado()
    {
        $codmen = $this->input->post('codmen', TRUE);

        // adiciona o código do menu na sessão
        $this->session->set_userdata('mslc', $codmen);
        
        /*if ($tipo == 'prc') {
            
            // limpa a sessão antes de começar
            $this->session->unset_userdata('mslc');
            $this->session->set_userdata('menprc', $codmen);

        } else {
            if (in_array($codmen, explode(',', $this->session->userdata('mensub')))) {
                
                $this->session->unset_userdata('mensub');
                $this->session->set_userdata('mensub', $codmen);

            } //else {
            //    $this->session->set_userdata('mensub', ($this->session->userdata('mensub').','.$codmen));
            //}
        }

        $item = $this->session->userdata('menprc').','.$this->session->userdata('mensub');*/
    }
}