<div class="content-wrapper">

	<h3 style="text-align:center; color:#f39c12;">Bem-vindo ao sistema de gestão!</h3>
	
	<section class="content">
		<h2 class="page-header">
			Ideologia 
			<a data-toggle="modal" data-target="#modalImg" style="cursor:pointer;">
				<small style="font-size:13px;">Visualizar Mapa Estratégico</small>
			</a>
		</h2>
		<div class="row">
			<?php // Missão ?>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<div class="box box-default">
					<div class="box-header with-border">
						<!--<i class="fa fa-bullhorn"></i>-->
						<h3 class="box-title">Missão</h3>
					</div>
					<div class="box-body">
						<?php echo configuracoes(NULL, 'missao', 1); ?>
					</div>
				</div>
			</div>

			<?php // Visão ?>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<div class="box box-default">
					<div class="box-header with-border">
						<!--<i class="fa fa-bullhorn"></i>-->
						<h3 class="box-title">Visão</h3>
					</div>
					<div class="box-body">
						<?php echo configuracoes(NULL, 'visao', 1); ?>
					</div>
				</div>
			</div>

			<?php // Valores e Crenças ?>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4">
				<div class="box box-default">
					<div class="box-header with-border">
						<!--<i class="fa fa-bullhorn"></i>-->
						<h3 class="box-title">Valores e Crenças</h3>
					</div>
					<div class="box-body">
						<?php echo configuracoes(NULL, 'valores_e_crencas', 1); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>


<!-- imagem do mapa estratégico -->
<div class="modal fade" id="modalImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" style="width:48%;">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Mapa Estratégico</h4>
			</div>
			<div class="modal-body">
				<img src="assets/images/mapa-estrategico.png" width="100%">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>