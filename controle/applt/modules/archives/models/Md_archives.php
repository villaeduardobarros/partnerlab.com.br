<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_archives extends CI_Model {

	function searchArchives($codarc=NULL, $traceability=NULL, $codctm=NULL, $codrec=NULL)
	{
		if ($codarc) {
			$this->db->where('codarc', $codarc);
		}
		if ($traceability) {
			$this->db->where('traceability', 1);
		}
		if ($codctm) {
			$this->db->where('customers_codctm', $codctm);
		}
		if ($codrec) {
			$this->db->where('receipt_control_codrec', $codrec);
		}
		return $this->db->get('archives');
		//echo $this->db->last_query();
	}

	function insertArchives($data)
	{
		if ($this->db->insert('archives', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	function updateArchives($codarc, $data)
	{
		$this->db->where('codarc', $codarc);
		$this->db->update('archives', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	function deleteArchives($codarc)
	{
		$this->db->where('codarc', $codarc);
		$this->db->delete('archives');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
}