<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('md_categories');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
            exit;
		}
	}
	
	public function index($codcat=NULL)
	{        
        // theme
        $this->output->set_template('theme');
        // theme - css
        $this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
        $this->load->css('assets/css/jquery-ui.css');
        // theme - js
        $this->load->js('assets/js/jquery/jquery-ui.min.js');
        $this->load->js('assets/plugins/datatables/jquery.dataTables.js');
        $this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
        $this->load->js('assets/js/jquery/jquery.validate.min.js');
        $this->load->js('assets/prop/pattern/pattern.js');
        $this->load->js('assets/prop/categories/categories.js');

        $data['categories'] = $this->md_categories->searchCategories();
        $data['action'] = ($codcat) ? 'alt' : 'cad';
        
        if ($codcat) { 
            
            $data['codcat'] = $codcat;

            $category = $this->md_categories->searchCategories($codcat);
            if ($category->num_rows() > 0) {
                $data['rowcat'] = $category->row();
            }

        }

        $this->load->view('categories-form', $data);
	}

	public function action()
	{
        $action     = $this->input->post('action', TRUE);
        $position   = $this->input->post('position', TRUE);

		$arrData = array(
            'title'         => $this->input->post('title', TRUE),
            'position'      => ($position) ? $position : NULL,
            'is_active'     => (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL)
		);

		if ($action == 'cad') {

            $registered = $this->md_categories->insertCategories($arrData);
			if ($registered > 0) {
                echo 'success';
            } else {
                echo 'error';
            }

		} else {

			$codcat = $this->input->post('codcat', TRUE);

			if ($this->md_categories->updateCategories($codcat, $arrData)) {
                echo 'success';
            } else {
                echo 'error';
            }
		}
	}


    public function actionDelete()
    {
        $codcat = $this->input->post('codcat', TRUE);

        if ($this->md_categories->deleteCategories($codcat)) {
            echo 'success';
        } else {
            echo 'error';
        }
    }
    
}