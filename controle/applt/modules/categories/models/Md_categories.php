<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_categories extends CI_Model {

	public function searchCategories($codcat=NULL, $parent=NULL, $level=NULL, $is_active=NULL)
	{
		if ($codcat) {
			$this->db->where('codcat', $codcat);
		}
        if ($parent) {
			$this->db->where('parent_codcat IN ('.$parent.')');
		}
		if (isset($level) && $level >= 0) {
			$this->db->where('level', $level);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		$this->db->order_by('title', 'asc');
		return $this->db->get('categories');
		//echo $this->db->last_query();
	}

	public function insertCategories($data)
	{
		if ($this->db->insert('categories', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateCategories($codcat, $data)
	{
		$this->db->where('codcat', $codcat);
		$this->db->update('categories', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deleteCategories($codcat)
	{
		$this->db->where('codcat', $codcat);
		$this->db->delete('categories');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
}