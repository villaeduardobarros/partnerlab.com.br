<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brands extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('md_brands');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
            exit;
		}
	}
	
	public function index($codbrd=NULL)
	{        
        // theme
        $this->output->set_template('theme');
        // theme - css
        $this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
        $this->load->css('assets/css/jquery-ui.css');
        // theme - js
        $this->load->js('assets/js/jquery/jquery-ui.min.js');
        $this->load->js('assets/plugins/datatables/jquery.dataTables.js');
        $this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
        $this->load->js('assets/js/jquery/jquery.validate.min.js');
        $this->load->js('assets/prop/pattern/pattern.js');
        $this->load->js('assets/prop/brands/brands.js');

        $data['brands'] = $this->md_brands->searchBrands();
        $data['action'] = ($codbrd) ? 'alt' : 'cad';
        
        if ($codbrd) { 
            
            $data['codbrd'] = $codbrd;

            $brand = $this->md_brands->searchBrands($codbrd);
            if ($brand->num_rows() > 0) {
                $data['rowbrd'] = $brand->row();
            }

        }

        $this->load->view('brands-form', $data);
	}

	public function action()
	{
        $action     = $this->input->post('action', TRUE);
        $position   = $this->input->post('position', TRUE);

		$arrData = array(
            'title'         => $this->input->post('title', TRUE),
            'position'      => ($position) ? $position : NULL,
            'is_active'     => (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL)
		);

		if ($action == 'cad') {

            $registered = $this->md_brands->insertBrands($arrData);
			if ($registered > 0) {
                
                echo 'success';
            
            } else echo 'error';

		} else {

			$codbrd = $this->input->post('codbrd', TRUE);

			if ($this->md_brands->updateBrands($codbrd, $arrData)) {
                
                echo 'success';
            
            } else echo 'error';
		}
	}


    public function actionDelete()
    {
        $codbrd = $this->input->post('codbrd', TRUE);

        if ($this->md_brands->deleteBrands($codbrd)) {
            
            echo 'success';
        
        } else echo 'error';
    }
    
}