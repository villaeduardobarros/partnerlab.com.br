<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_brands extends CI_Model {

	public function searchBrands($codbrd=NULL, $is_active=NULL)
	{
		if ($codbrd) {
			$this->db->where('codbrd', $codbrd);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		$this->db->order_by('title', 'asc');
		return $this->db->get('brands');
		//echo $this->db->last_query();
	}

	public function insertBrands($data)
	{
		if ($this->db->insert('brands', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateBrands($codbrd, $data)
	{
		$this->db->where('codbrd', $codbrd);
		$this->db->update('brands', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deleteBrands($codbrd)
	{
		$this->db->where('codbrd', $codbrd);
		$this->db->delete('brands');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
}