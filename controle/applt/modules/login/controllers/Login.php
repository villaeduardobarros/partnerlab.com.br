<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->helper('security');
		$this->load->helper('cookie');
		$this->load->helper('pattern');
		$this->load->helper('send');
		$this->load->model('users/md_users');
	}

	public function index()
	{
		if ($this->session->userdata('logged-in') == TRUE) {
			redirect('home');
		} else {
			
			$data['browser'] = $this->agent->browser();
			$this->load->view('login', $data);
		
		}
	}

	public function action()
	{
		$user	= strtolower($this->input->post('user', TRUE));
		$password 	= $this->input->post('password', TRUE);

		if (!empty($user) || !empty($password)) {

			// criptografa a senha
			$passwordCripty	= do_hash($password.$user, 'md5');
			$passwordHash	= hash('whirlpool', $passwordCripty);
			//echo $user.' <> '.$passwordHash; exit;
		
			$userLogin = $this->md_users->searchUsers(NULL, $user, $passwordHash, NULL, TRUE);
			if ($userLogin->num_rows() > 0) {

				// pega os dados do usuario
				$rowusr = $userLogin->row();
					
				$arrLogin = array(
					'login-adm-codusr' 	=> $rowusr->codusr,
					'login-adm-name' 	=> $rowusr->name, 
					'login-adm-email' 	=> $rowusr->email, 
					'login-adm-user' 	=> $rowusr->user, 
					'login-adm-type' 	=> $rowusr->type, 
					'login-adm-photo' 	=> $rowusr->photo, 
					'logged-adm-in' 	=> TRUE
				);
				$this->session->set_userdata($arrLogin);
				
				echo 'success';

			} else {
				echo 'incorrect-data';
			}

		} else {
			echo 'wrong-fill';
		}
	}

	// ****************************************
	// ***** INICIO - Esqueceu sua senha? *****
	public function forgotPassword()
	{
		$this->load->view('login-forgot');
	}

	// solicita uma alteração na senha, mas até agora não alterou nada
	public function actionForgotPassword()
	{
		$txtemail		= $this->input->post('txtemail', TRUE);

		if (!empty($txtemail)) {

			// $codusr=NULL, $user=NULL, $password=NULL, $email=NULL, $is_active=NULL
			$userLogin = $this->md_users->searchUsers(NULL, NULL, NULL, $txtemail, TRUE);
			if ($userLogin->num_rows() > 0) {

				$rowusr = $userLogin->row();

				// gera um codigo aleatorio para autenticar o pedido
				// $size, $uppercase=TRUE, $numbers=TRUE, $symbols=FALSE
				$codrdf = generateCode(8);

				// salva o código de redefinição da senha criptografado
		        $arrData = array(
		        	'codrdf' => do_hash($codrdf, 'md5')
		        );
				if ($this->md_users->updateUser($rowusr->codusr, $arrData)) {

					// envia os dados por e-mail
					$arrEmail = array(
			            'subject' 		=> 'Redefinir senha de acesso', 
			            'codusr' 		=> $rowusr->codusr, 
			            'destination' 	=> $rowusr->email, 
			            'name' 			=> $rowusr->name, 
			            'user' 			=> $rowusr->user,
			            'codrdf' 		=> $codrdf,
			            'urlreturn' 	=> generateCode(3).rtrim(strtr(base64_encode($rowusr->codusr), '+/', '-_'), '=').generateCode(4)
			        );
			        if ($this->send->forgotPassword($arrEmail)) {

			        	echo 'success|'.$rowusr->email;

			        } else {
			            echo 'error-email|';
			        }

		        }

	        } else {
	            echo 'incorrect-data|';
	        }

	    } else {
			echo 'wrong-fill|';
		}
	}
	// ******* FIM - Esqueceu sua senha? ******
	// ****************************************


	// *******************************************
	// ***** INICIO - Confirma a solicitacao *****
	public function confirmsData($codusr)
	{
		if ($codusr) {

			$codusr = substr($codusr, 3);
			$codusr = substr($codusr, 0, -4);
			$codusr = base64_decode(str_pad(strtr($codusr, '-_', '+/'), strlen($codusr) % 4, '=', STR_PAD_RIGHT));
			$data['codusr'] = (is_numeric($codusr)) ? $codusr : 0;

		} else {
			$data['codusr'] = 0;
		}

		$this->load->view('login-confirms', $data);
	}

	// Ao solicitar a alteração da senha o sistema envia no e-mail um 
	// código de verificação, para evitar que um altere a sanha do outro
	public function actionConfirmsData()
	{
		$txtcodusr	= (int)$this->input->post('txtcodusr', TRUE);
		$txtcodrdf	= $this->input->post('txtcodrdf', TRUE);
		$codrdfHash = do_hash($txtcodrdf, 'md5');

		// $codusr=NULL, $user=NULL, $password=NULL, $email=NULL, $is_active=NULL
		$userLogin = $this->md_users->searchUsers($txtcodusr, NULL, NULL, NULL, TRUE);
		if ($userLogin->num_rows() > 0) {

			$rowusr = $userLogin->row();

			if ($codrdfHash == $rowusr->codrdf) {

		        $arrData = array('trocar_senha' => 1);
				if ($this->md_usuarios->alterarUsuario($rowusr->codusr, $arrData)) {

					$arrLogin = array(
						'login-cod'		=> $rowusr->codusr,
						'login-usuario'	=> strtolower($rowusr->usuario), 
						'logged-in'		=> FALSE
					);
					$this->session->set_userdata($arrLogin);

	            	echo 'sucesso|'.geraCodigo(3).rtrim(strtr(base64_encode($rowusr->codusr), '+/', '-_'), '=').geraCodigo(4);

		        }

			} else {
				echo 'error-code|';
			}

        } else {
            echo 'incorrect-data|';
        }
	}
	// ******* FIM - Confirma a solicitacao ******
	// ****************************************


	// ***************************************************
	// ***** INICIO - Trocar dados no primeiro login *****
	public function changePassword($codusr)
	{
		if ($codusr) {

			$codusr = substr($codusr, 3);
			$codusr = substr($codusr, 0, -4);
			$codusr = base64_decode(str_pad(strtr($codusr, '-_', '+/'), strlen($codusr) % 4, '=', STR_PAD_RIGHT));

			if (is_numeric($codusr)) {

				$usuario = $this->md_usuarios->buscaUsuario($codusr, NULL, 1);
				if ($usuario->num_rows() > 0) {

					// pega os dados do usuario
					$rowusr = $usuario->row();
					$data['codusr']	 = $rowusr->codusr;
					$data['usuario'] = $rowusr->usuario;
					
				} else {
					$data['codusr'] = 0;
				}

			} else {
				$data['codusr'] = 0;
			}

		} else {
			$data['codusr'] = 0;
		}

		$this->load->view('login-change', $data);
	}

	public function actionChangePassword()
	{
		// reserva as informações antes de alterar
		$arrDataBefore = array(
			'usuario'   => $this->session->userdata('login-usuario'),
			'senha'     => $this->session->userdata('login-senha')
		);
		$dataBefore = json_encode($arrDataBefore);

		$codusr         = $this->input->post('txtcodusr', TRUE);
		$txtaltsenha	= $this->input->post('txtaltsenha', TRUE);
		$txtuser     = $this->input->post('txtuser', TRUE);
		$txtpass       = $this->input->post('txtpass', TRUE);
		$passwordCripty = do_hash($txtpass, 'md5');
		$passwordHash   = hash('whirlpool', $passwordCripty);

		$arrData = array( 
			'usuario'       => strtolower($txtuser), 
			'senha'         => $passwordHash,
			'trocar_senha'  => NULL
		);
		if ($this->md_usuarios->alterarUsuario($codusr, $arrData)) {

			// Salvar LOG de login
			$arrLog = array( 
				'tipo'      => 'TS', // trocar senha 
				'codusr'    => $codusr,
				'valores'   => $dataBefore,
				'ip'		=> $_SERVER['REMOTE_ADDR']
			);
			$registerLog = $this->md_log->inserirLog('usuarios', $arrLog);
			echo ($registerLog > 0) ? 'sucesso' : 'erro';

		} else {
			echo 'erro';
		}
	}
	// ****** FIM - Trocar dados no primeiro login *******
	// ***************************************************
}
