﻿<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="<?php echo base_url(); ?>">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php //echo $this->config->item('title'); ?></title>
	<link rel="canonical" href="<?php echo current_url(); ?>" />
	<link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/skin-yellow-light.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/ionicons.min.css">
	<link rel="stylesheet" href="assets/css/pattern.css">
	<link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
	<script src="assets/js/jquery/jquery.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/prop/login/login.js"></script>
	<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	<script>
		var baseUrl = '<?php echo base_url(); ?>';
	</script>
</head>
<body class="hold-transition login-page">

	<div class="login-box">
		<div class="login-logo"><img src="assets/images/logo.png"></div>
		<div class="login-box-body">
			<p><strong>Seja bem-vindo,</strong></p>

			<form id="loginForm" method="post" role="form">
				<div class="form-group has-feedback">
					<input type="text" id="user" name="user" class="form-control" placeholder="Usuário" autocomplete="off">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>

				<div class="form-group has-feedback">
					<input type="password" id="password" name="password" class="form-control" placeholder="Senha">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>

				<div class="row" style="height:34px;">
					<div class="col-md-8 col-xs-12">
						<div id="messageAlert" style="padding:7px !important;" class="callout callout-warning">
							<i class="fa fa-exclamation-circle"></i> Informe os dados
						</div>
					</div>
					<div class="col-md-4 col-xs-12">
						<button type="submit" id="buttonForm" class="btn btn-block btn-primary"><i class="fa fa-send"></i> Entrar</button>
					</div>
				</div>
			</form>
		</div>
		
		<br>

		<p style="text-align:center;">
			<a href="login/esqueceu-a-senha" style="color:#cc3d3d !important;">
				<strong>Esqueceu sua senha?</strong>
			</a>
		</p>
	</div>

</body>
</html>