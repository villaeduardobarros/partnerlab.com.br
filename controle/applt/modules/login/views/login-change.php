<?php
if (is_numeric($codusr) && $codusr > 0) {
	$codUsuario = $codusr;
} else {
	echo '<script>alert("Usuário não identificado.\nVerifique a url recebida no e-mail.");window.location="'.base_url().'logout";</script>';
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="<?php echo base_url(); ?>">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $this->config->item('title'); ?></title>
	<link rel="canonical" href="<?php echo current_url(); ?>" />
	<link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/skin-yellow-light.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/ionicons.min.css">
	<link rel="stylesheet" href="assets/css/pattern.css">
	<link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
	<script src="assets/js/jquery/jquery.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/prop/login/login.js"></script>
	<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	<script>
		var baseUrl = '<?php echo base_url(); ?>';
	</script>
</head>
<body class="hold-transition login-page">

	<div class="login-box">
		<div class="login-logo"><img src="assets/images/logo-colorido.png"></div>
		<div class="login-box-body">
			<p class="login-box-msg" style="align:center; padding:0 20px 14px !important;">
				<strong>Alterar senha de acesso</strong><br>
				<small>Crie uma nova senha ou para facilitar utilize os dados de acesso ao sistema da TOTVS Datasul</small>
			</p>

			<div id="messageAlertConcl" style="background:#cbeacc;border:1px solid #6eb56f;border-radius:5px;color:#3c763d;display:none;margin-bottom:10px;padding:7px !important;"></div>

			<form id="loginChangePasswordForm" method="post" role="form">
				<input type="hidden" name="txtcodusr" value="<?php echo $codusr; ?>">

				<div class="form-group has-feedback">
					<input type="text" name="txtuser" class="form-control" placeholder="Usuário" autocomplete="off" value="<?php echo $usuario; ?>" readonly>
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>

				<div class="form-group has-feedback">
					<input type="password" name="txtpass" class="form-control" placeholder="Nova Senha">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>

				<div class="form-group has-feedback">
					<input type="password" name="txtpasscfm" class="form-control" placeholder="Confirmar Senha">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>

				<div class="row" style="height:34px;">
					<div class="col-md-8 col-xs-8">
						<div id="messageAlert" style="padding:7px !important;"></div>
					</div>
					<div class="col-md-4 text-right">
						<button type="submit" id="buttonForm" class="btn btn-block btn-primary"><i class="fa fa-save"></i> Gravar</button>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>