﻿<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="<?php echo base_url(); ?>">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $this->config->item('title'); ?></title>
	<link rel="canonical" href="<?php echo current_url(); ?>" />
	<link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/skin-yellow-light.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/ionicons.min.css">
	<link rel="stylesheet" href="assets/css/pattern.css">
	<link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" />
	<script src="assets/js/jquery/jquery.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/prop/login/login.js"></script>
	<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	<script>
		var baseUrl = '<?php echo base_url(); ?>';
	</script>
<body class="hold-transition login-page">

	<div class="login-box">
		<div class="login-logo"><img src="assets/images/logo.png"></div>
		<div class="login-box-body">
			<p class="login-box-msg" style="align:center; padding:0 20px 14px !important;">
				<strong>Esqueceu sua senha?</strong><br>
				Informe seu e-mail cadastrado e receba as instruções para criar um nova senha.
			</p>

			<div id="messageAlertConcl" style="background:#cbeacc;border:1px solid #6eb56f;border-radius:5px;color:#3c763d;display:none;margin-bottom:10px;padding:7px !important;"></div>

			<form id="loginForgotPasswordForm" method="post" role="form">
				<div class="form-group has-feedback">
					<input type="text" name="txtemail" class="form-control" placeholder="E-mail" autocomplete="off">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>

				<div class="row" style="height:34px;">
					<div class="col-md-8 col-xs-12">
						<div id="messageAlert" style="padding:7px !important;"></div>
					</div>
					<div class="col-md-4 col-xs-12">
						<button type="submit" id="buttonForm" class="btn btn-block btn-primary"><i class="fa fa-send"></i> Solicitar</button>
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>