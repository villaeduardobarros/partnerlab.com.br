<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_customers extends CI_Model {

	public function searchCustomers($codctm=NULL, $is_active=NULL)
	{
		if ($codctm) {
			$this->db->where('codctm', $codctm);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		$this->db->order_by('name', 'asc');
		return $this->db->get('customers');
		//echo $this->db->last_query();
	}

	public function insertCustomers($data)
	{
		if ($this->db->insert('customers', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateCustomers($codctm, $data)
	{
		$this->db->where('codctm', $codctm);
		$this->db->update('customers', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deleteCustomers($codctm)
	{
		$this->db->where('codctm', $codctm);
		$this->db->delete('customers');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
}