<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><i class="fa fa-briefcase"></i> Clientes</li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="35%">Nome</th>
                                    <th width="35%">E-mail</th>
                                    <th width="20%">CNPJ</th>
                                    <th width="10%">Situação</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($customers->result() as $client) { ?>

									<?php
										//$password 	= 'P4rtn3r';
										//$passwordCripty	= do_hash($password.'cust'.$client->user, 'md5');
										//$passwordHash	= hash('whirlpool', $passwordCripty);
										//echo 'UPDATE customers SET password="'.$passwordHash.'" WHERE codctm='.$client->codctm.';<br>';
									?>

									<tr>
										<td>
											<a href="customers/<?php echo $client->codctm; ?>" target="_self">
												<?php echo $client->name; ?>
											</a>
											<div class="pull-right">
												<a href="customers/archives/<?php echo $client->codctm; ?>" target="_self"><i class="fa fa-file-pdf-o"></i></a>
												&nbsp;
												<a href="javascript:deleteClient(<?php echo $client->codctm; ?>)" target="_self"><i class="fa fa-trash" style="color:#900;"></i></a>
											</div>
										</td>
										<td align="center"><?php echo $client->email; ?></td>
										<td align="center"><?php echo $client->cnpj; ?></td>
										<td align="center">
											<span class="label label-<?php echo ($client->is_active == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($client->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="customersForm" method="post" role="form">
					<input name="action" type="hidden" value="<?php echo $action; ?>">
					<?php if ($action == 'alt') { ?>
						<input name="codctm" type="hidden" value="<?php echo $codctm; ?>">
					<?php } ?>

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title"><?php echo ($action == 'cad') ? 'Cadastrar Cliente' : 'Editar Cliente'; ?></h3>
						</div>
						<div class="box-body">
	
							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>

							<div class="form-group">
								<label for="name">Nome</label>
								<input type="text" class="form-control" id="name" name="name" value="<?php echo ($action == 'alt') ? $rowctm->name : NULL; ?>">
							</div>

							<div class="form-group">
								<label for="email">E-mail</label>
								<input type="text" class="form-control" id="email" name="email" value="<?php echo ($action == 'alt') ? $rowctm->email : NULL; ?>">
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="cnpj">CNPJ</label>
									<input type="text" class="form-control cnpj" id="cnpj" name="cnpj" value="<?php echo ($action == 'alt') ? $rowctm->cnpj : NULL; ?>">
								</div>

								<div class="col-md-6">
									<label for="phone">Telefone</label>
									<input type="text" class="form-control phone" id="phone" name="phone" value="<?php echo ($action == 'alt') ? $rowctm->phone : NULL; ?>">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="user">Usuário</label>
									<input type="text" class="form-control" id="user" name="user" value="<?php echo ($action == 'alt') ? $rowctm->user : NULL; ?>">
								</div>

								<div class="col-md-6">
									<label for="password">Senha</label>
									<input type="password" class="form-control" id="password" name="password">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="is_active">Situação</label>
									<select name="is_active" id="is_active" class="form-control">
										<option value="1"<?php echo (($action == 'alt' && $rowctm->is_active == 1) ? ' selected' : NULL); ?>>Ativo</option>
										<option value="0"<?php echo (($action == 'alt' && is_null($rowctm->is_active)) ? ' selected' : NULL); ?>>Inativo</option>
									</select>
								</div>
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="customers" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>