<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="customers"><i class="fa fa-briefcase"></i> Clientes</a></li>
		<li class="active"><i class="fa fa-file-pdf-o"></i> Arquivos de Rastreabilidade</li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="70%">Título</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($archives->result() as $archive) { ?>

									<?php list($date, $hour) = explode(' ', $archive->datetime); ?>

									<tr>
										<td>
											<?php echo $archive->title; ?>
											<div class="pull-right">
												<a href="<?php echo $this->config->item('base_url_site'); ?>uploads/archives/traceability/<?php echo $archive->file; ?>" title="Baixar arquivo" target="_blank">
													<i class="fa fa-file-pdf-o"></i>
												</a>
												&nbsp;&nbsp;
												<i class="fa fa-trash" onclick="deleteTraceability(<?php echo $archive->codarc; ?>)" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="archiveTraceabilityForm" method="post" role="form" enctype="multipart/form-data">

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title">Cadastrar Rastreabilidade</h3>
						</div>
						<div class="box-body">
	
							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>

							<div class="form-group">
								<label for="title">Título do Certificado</label>
								<input type="text" class="form-control" id="title" name="title">
							</div>

							<div class="form-group">
								<label for="file">Arquivo</label>
								<input type="file" class="form-control" id="file" name="file">
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="customers/traceability" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>