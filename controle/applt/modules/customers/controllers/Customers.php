<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('photos');
		$this->load->library('user_agent');
		$this->load->helper('security');
		$this->load->model('md_customers');
		$this->load->model('archives/md_archives');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
			exit;
		}
	}

	public function index($codctm=NULL)
	{        
		// theme
		$this->output->set_template('theme');
		// theme - css
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
		$this->load->css('assets/css/jquery-ui.css');
		// theme - js
		$this->load->js('assets/js/jquery/jquery-ui.min.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
		$this->load->js('assets/js/jquery/jquery.validate.min.js');
		$this->load->js('assets/js/jquery/jquery.mask.min.js');
		$this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/customers/customers.js');

		$data['customers'] = $this->md_customers->searchCustomers();
		$data['action'] = ($codctm) ? 'alt' : 'cad';

		if ($codctm) { 

			$data['codctm'] = $codctm;

			$client = $this->md_customers->searchCustomers($codctm);
			if ($client->num_rows() > 0) {
				$data['rowctm'] = $client->row();
			}

		}

		$this->load->view('customers-form', $data);
	}


	public function action()
	{
		$action     = $this->input->post('action', TRUE);
		$user 		= strtolower($this->input->post('user', TRUE));
		$password 	= $this->input->post('password', TRUE);

		// criptografa a senha
		$passwordCripty	= do_hash($password.'cust'.$user, 'md5');
		$passwordHash	= hash('whirlpool', $passwordCripty);

		$arrData = array(
			'name' 			=> $this->input->post('name', TRUE),
			'email' 		=> $this->input->post('email', TRUE),
			'cnpj' 			=> $this->input->post('cnpj', TRUE),
			'phone' 		=> $this->input->post('phone', TRUE),
			'user' 			=> $user,
			'is_active' 	=> (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL)
		);

		if ($action == 'cad') {

			$arrData['password'] = $passwordHash;

			$registered = $this->md_customers->insertCustomers($arrData);
			if ($registered > 0) {

				echo 'success';

			} else echo 'error';

		} else {

			$codctm = $this->input->post('codctm', TRUE);

			// se ele digitar algo no campo da senha altera
			if (!empty($password)) {
				$arrData['password'] = $passwordHash;
			}

			if ($this->md_customers->updateCustomers($codctm, $arrData)) {
				
				echo 'success';
			
			} else echo 'error';
		
		}
	}


	public function actionDelete()
	{
		$codctm = $this->input->post('codctm', TRUE);

		$archives = $this->md_archives->searchArchives(NULL, NULL, $codctm);
		if ($archives->num_rows() > 0) {

			foreach ($archives->result() as $archive) {

				if ($this->photos->deleteArchive('/uploads/archives/customers/'.$codctm.'/', $archive->file)) {

					$this->md_archives->deleteArchives($archive->codarc);

				}

			}

		}

		if ($this->photos->deleteFolder('/uploads/archives/customers/'.$codctm.'/')) {
				
			if ($this->md_customers->deleteCustomers($codctm)) {

				echo 'success';

			} else echo 'error';
			
		}
	}


	public function archives($codctm=NULL)
	{
		// theme
		$this->output->set_template('theme');
		// theme - css
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
		$this->load->css('assets/css/jquery-ui.css');
		// theme - js
		$this->load->js('assets/js/jquery/jquery-ui.min.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
		$this->load->js('assets/js/jquery/jquery.validate.min.js');
		$this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/customers/customers.js');

		$client = $this->md_customers->searchCustomers($codctm);
		if ($client->num_rows() > 0) {

			$data['codctm'] = $codctm;

			$data['rowctm'] = $client->row();

			$data['archives'] = $this->md_archives->searchArchives(NULL, NULL, $codctm);

		} else redirect('customers');

		$this->load->view('customers-archives', $data);
	}


	public function actionArchive()
	{
		$codctm = $this->input->post('codctm', TRUE);
		$title = $this->input->post('title', TRUE);

		$nameArchive = (($title) ? url_title($title) : (rand(100, 99999).date('YmdHi')));

		$arrData = array(
			'title' 			=> $title,
			'customers_codctm' 	=> $codctm
		);

		if (!empty($_FILES['file']['name'])) {

			// verifica se a pasta do cliente existe
			if (!is_dir(realpath('../').'/uploads/archives/customers/'.$codctm.'/')) {
				mkdir(realpath('../').'/uploads/archives/customers/'.$codctm, 0777, TRUE);
			}

			$name 		= $_FILES['file']['name'];
			$tmp_name 	= $_FILES['file']['tmp_name'];
			$size 		= $_FILES['file']['size'];

			$ext = pathinfo($name, PATHINFO_EXTENSION); // extensao do arquivo

			$allowedExts = array('pdf'); // extensoes permitidas

			if (in_array($ext, $allowedExts)) {

				if ($size <= 12582912) { // 12Mb

					$newName = str_replace('-', '.', $nameArchive).'.'.$ext;

					// move o arquivo
					if (move_uploaded_file($_FILES['file']['tmp_name'], realpath('../').'/uploads/archives/customers/'.$codctm.'/'.$newName)) {
						
						$arrData['file'] = $newName;

						$registered = $this->md_archives->insertArchives($arrData);
						if ($registered > 0) {

							echo 'success|'.$codctm;

						} else {
				            echo 'error|';
				        }
					
					} else echo 'file-not-send|';

				} else echo 'oversize|';

			} else echo 'extension-not-allowed|';

		} else echo 'file-not-selected|';

	}


	public function actionArchiveDelete()
	{
		$codarc = $this->input->post('codarc', TRUE);

		$archives = $this->md_archives->searchArchives($codarc);
		if ($archives->num_rows() > 0) {

			$archive = $archives->row();

			if ($this->md_archives->deleteArchives($codarc)) {

				if ($this->photos->deleteArchive('/uploads/archives/customers/'.$archive->customers_codctm.'/', $archive->file)) {
				 	
				 	echo 'success';

				} else echo 'error';

			}

		}
	
	}


	/*public function downloadArchive($codctm, $nameArchive)
    {
        $this->load->helper('download');
        force_download($nameArchive, $this->config->item('base_url_site').'uploads/archives/customers/'.$codctm.'/'.$nameArchive);
    }*/


	public function traceability()
	{
		// theme
		$this->output->set_template('theme');
		// theme - css
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
		$this->load->css('assets/css/jquery-ui.css');
		// theme - js
		$this->load->js('assets/js/jquery/jquery-ui.min.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
		$this->load->js('assets/js/jquery/jquery.validate.min.js');
		$this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/customers/customers.js');

		$data['archives'] = $this->md_archives->searchArchives(NULL, TRUE);
		$this->load->view('customers-traceability', $data);
	}


	public function actionTraceability()
	{
		$title = $this->input->post('title', TRUE);

		$nameArchive = (($title) ? url_title($title) : (rand(100, 99999).date('YmdHi')));

		$arrData = array(
			'title' 		=> $title,
			'traceability' 	=> 1
		);

		if (!empty($_FILES['file']['name'])) {

			// verifica se a pasta do cliente existe
			if (!is_dir(realpath('../').'/uploads/archives/traceability/')) {
				mkdir(realpath('../').'/uploads/archives/traceability', 0777, TRUE);
			}

			$name 		= $_FILES['file']['name'];
			$tmp_name 	= $_FILES['file']['tmp_name'];
			$size 		= $_FILES['file']['size'];

			$ext = pathinfo($name, PATHINFO_EXTENSION); // extensao do arquivo

			$allowedExts = array('pdf'); // extensoes permitidas

			if (in_array($ext, $allowedExts)) {

				if ($size <= 12582912) { // 12Mb

					$newName = str_replace('-', '.', $nameArchive).'.'.$ext;

					// move o arquivo
					if (move_uploaded_file($_FILES['file']['tmp_name'], realpath('../').'/uploads/archives/traceability/'.$newName)) {
						
						$arrData['file'] = $newName;

						$registered = $this->md_archives->insertArchives($arrData);
						if ($registered > 0) {

							echo 'success';

						} else {
				            echo 'error';
				        }
					
					} else echo 'file-not-send';

				} else echo 'oversize';

			} else echo 'extension-not-allowed';

		} else echo 'file-not-selected';

	}


	public function actionDeleteTraceability()
	{
		$codarc = $this->input->post('codarc', TRUE);

		$archives = $this->md_archives->searchArchives($codarc, TRUE);
		if ($archives->num_rows() > 0) {

			$archive = $archives->row();

			if ($this->md_archives->deleteArchives($codarc)) {

				if ($this->photos->deleteArchive('/uploads/archives/traceability/'.$archive->file)) {
				 	
				 	echo 'success';

				} else echo 'error';

			}

		}
	
	}


	/*public function downloadTraceability($archive)
    {
    	$path = realpath('../').'/uploads/archives/traceability/';

		if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

		$this->load->helper('file');

		$mime = get_mime_by_extension($path);

		// Build the headers to push out the file properly.
		header('Pragma: public');     // required
		header('Expires: 0');         // no cache
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
		header('Cache-Control: private',false);
		header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
		header('Content-Disposition: attachment; filename="'.basename($archive).'"');  // Add the file name
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.filesize($path)); // provide file size
		header('Connection: close');
		readfile($path); // push it out
		exit();
    }*/
}