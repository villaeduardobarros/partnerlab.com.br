<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Packaging extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('md_packaging');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
            exit;
		}
	}
	
	public function index($codpac=NULL)
	{        
        // theme
        $this->output->set_template('theme');
        // theme - css
        $this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
        $this->load->css('assets/css/jquery-ui.css');
        // theme - js
        $this->load->js('assets/js/jquery/jquery-ui.min.js');
        $this->load->js('assets/plugins/datatables/jquery.dataTables.js');
        $this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
        $this->load->js('assets/js/jquery/jquery.validate.min.js');
        $this->load->js('assets/prop/pattern/pattern.js');
        $this->load->js('assets/prop/packaging/packaging.js');

        $data['packaging'] = $this->md_packaging->searchPackaging();
        $data['action'] = ($codpac) ? 'alt' : 'cad';
        
        if ($codpac) { 
            
            $data['codpac'] = $codpac;

            $packing = $this->md_packaging->searchPackaging($codpac);
            if ($packing->num_rows() > 0) {
                $data['rowpac'] = $packing->row();
            }

        }

        $this->load->view('packaging-form', $data);
	}

	public function action()
	{
        $action     = $this->input->post('action', TRUE);
        $position   = $this->input->post('position', TRUE);

		$arrData = array(
            'title'         => $this->input->post('title', TRUE),
            'position'      => ($position) ? $position : NULL,
            'is_active'     => (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL)
		);

		if ($action == 'cad') {

            $registered = $this->md_packaging->insertPackaging($arrData);
			if ($registered > 0) {
                echo 'success';
            } else {
                echo 'error';
            }

		} else {

			$codpac = $this->input->post('codpac', TRUE);

			if ($this->md_packaging->updatePackaging($codpac, $arrData)) {
                echo 'success';
            } else {
                echo 'error';
            }
		}
	}


    public function actionDelete()
    {
        $codpac = $this->input->post('codpac', TRUE);

        if ($this->md_packaging->deletePackaging($codpac)) {
            echo 'success';
        } else {
            echo 'error';
        }
    }
    
}