<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_packaging extends CI_Model {

	public function searchPackaging($codpac=NULL, $is_active=NULL)
	{
		if ($codpac) {
			$this->db->where('codpac', $codpac);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		$this->db->order_by('title', 'asc');
		return $this->db->get('packaging');
		//echo $this->db->last_query();
	}

	public function insertPackaging($data)
	{
		if ($this->db->insert('packaging', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updatePackaging($codpac, $data)
	{
		$this->db->where('codpac', $codpac);
		$this->db->update('packaging', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deletePackaging($codpac)
	{
		$this->db->where('codpac', $codpac);
		$this->db->delete('packaging');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
}