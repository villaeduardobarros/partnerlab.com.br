<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Modal_users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('users/md_users');
        $this->load->model('menu/md_menu');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
			exit;
		}
	}


	public function index() 
	{
		//
	}	


	public function loadPermission()
	{
		$data['codusr'] 	= $this->input->post('codusr', TRUE);
		//$data['main'] 		= $this->md_menu->searchMenu(NULL, NULL, 0, TRUE);

		// verifica se o usuario ja tem alguma permissao vinculada
        $permissions 	= $this->md_users->searchUsrPerm($data['codusr']);
        $selected 		= (($permissions->num_rows() > 0) ? $permissions->row()->menCod : NULL);

        // carrega as informações do arquivo menu (helper)
        $data['html'] = mountMenuMain(2, NULL, 0, TRUE, $selected);
		
        echo $this->load->view('modal/users/modal-user-permission', $data);
	}


	// inseri a permissão ao usuário
    public function inserirPerm()
    {
    	$codusr 	= $this->input->post('codusr', TRUE);
        $perms  	= ($this->input->post('perms', TRUE)) ? explode(',', $this->input->post('perms', TRUE)) : NULL;

		// apaga todas as permissoes por sistema
		if ($this->md_users->deletarUsrPerm($codusr)) {

			if (count($perms) > 0) {
				// percorre as atualizações selecionadas
				for ($i=0; $i<count($perms); $i++) {
					// vincula o usuario ao projeto
					$arrDataPerm = array(
						'codusr'  => $codusr,
						'codmen'  => $perms[$i]
					);
					$this->md_users->inserirUsrPerm($arrDataPerm);
				}
			}

		}

		echo 'success';
    }

}