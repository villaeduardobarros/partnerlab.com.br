<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Modal_status extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
			exit;
		}
	}

	public function index() 
	{
		// NULL
	}

	// monta o modal simples (somente mensagem)
	public function mountsSimpleNotice()
	{
		$item = $this->input->post('item');
		$data['title'] 		= $item['title'];
		$data['message'] 	= $item['message'];
		$data['button'] 	= $item['button'];
        echo $this->load->view('modal/status/modal-notice', $data);
	}

	// monta o modal confirmacao (funcao no botao OK)
	public function mountsWarningConfirmation()
	{
		$item = $this->input->post('item');
		$data['title'] 		= $item['title'];
		$data['message'] 	= $item['message'];
		$data['button'] 	= $item['button'];
        echo $this->load->view('modal/status/modal-confirmation', $data);
	}

}