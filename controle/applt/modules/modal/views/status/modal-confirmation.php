<!-- modal usuarios -->
<div class="modal fade" id="modalStsWarning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title"><?php echo $title; ?></h4>
            </div>
            
            <div class="modal-body modal-ajuste">
                <?php echo $message; ?>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left btn-cancel" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary btn-ok"><?php echo $button; ?></button>
            </div>
        
        </div>
    </div>
</div>