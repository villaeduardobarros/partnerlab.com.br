<!-- modal permissoes -->
<div class="modal fade" id="modalPermission" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="permUsrForm" method="post" role="form">
                <input type="hidden" name="codusr" value="<?php echo $codusr; ?>">

                <div class="modal-header">
                    <h4 class="modal-title pull-left" style="margin:2px 0 0;">Permissões</h4>
                    <!--<div class="pull-right col-md-4" style="padding:0;">
                        <select class="form-control input-sm" name="system_main" id="system_main">
                            <option value="">Menu Principal</option>
                            <?php //foreach ($main->result() as $rowmain) { ?>
                                <option value="<?php //echo $rowmain->codmen; ?>"><?php //echo $rowmain->title; ?></option>
                            <?php //} ?>
                        </select>
                    </div>-->
                </div>
                <div class="modal-body list-permission modal-ajuste">

                    <div class="row">
                        <div class="col-md-12">
                            <div id="messageAlertModal"></div>
                        </div>
                    </div>
                            
                    <?php echo $html; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                    <button type="button" id="vincUrsPerm" class="btn btn-primary" onClick="validarPerm()">Vincular</button>
                </div>
            </form>

        </div>
    </div>
</div>