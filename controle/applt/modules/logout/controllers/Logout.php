<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->helper('cookie');
		$this->load->library('session');
	}
	
	public function index()
	{
		if ($this->session->userdata('logged-in')) {

			// salvar log de acesso
	        $arrLog = array( 
	            'tipo'		=> 'LG', 
	            'codusr' 	=> $this->session->userdata('login-cod'),
	            'ip'		=> $_SERVER['REMOTE_ADDR']
	        );
	        $registerLog = $this->md_log->inserirLog('usuarios', $arrLog);
	        if ($registerLog > 0) {
	            
				// Elimina as sessões criadas
				$this->session->unset_userdata('login-cod');
				$this->session->unset_userdata('login-mtrc');
				$this->session->unset_userdata('login-usuario');
				$this->session->unset_userdata('login-senha');
				$this->session->unset_userdata('login-nome');
				$this->session->unset_userdata('login-email');
				$this->session->unset_userdata('login-cc');
				$this->session->unset_userdata('login-foto');
				$this->session->unset_userdata('login-visprj');
				$this->session->unset_userdata('logged-in');

			}
	        
	    }

        redirect('login');
	}

}