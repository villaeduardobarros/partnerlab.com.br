<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="equipments"><i class="fa fa-list"></i> Equipamentos</a></li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="80%">Título</th>
                                    <th width="10%">Posição</th>
                                    <th width="10%">Situação</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($equipments->result() as $equipment) { ?>

									<tr>
										<td>
											<a href="equipments/<?php echo $equipment->codeqp; ?>" target="_self">
												<?php echo $equipment->title; ?>
											</a>
											<div class="pull-right">
												<i class="fa fa-trash" onclick="deleteEquipment(<?php echo $equipment->codeqp; ?>)" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td align="center"><?php echo ($equipment->position) ? $equipment->position : NULL; ?></td>
										<td align="center">
											<span class="label label-<?php echo ($equipment->is_active == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($equipment->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="equipmentsForm" method="post" role="form">
					<input name="action" type="hidden" value="<?php echo $action; ?>">
					<?php if ($action == 'alt') { ?>
						<input name="codeqp" type="hidden" value="<?php echo $codeqp; ?>">
					<?php } ?>

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title"><?php echo ($action == 'cad') ? 'Cadastrar Equipamento' : 'Editar Equipamento'; ?></h3>
						</div>
						<div class="box-body">
	
							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>

							<div class="form-group">
								<label for="title">Título</label>
								<input type="text" class="form-control" id="title" name="title" value="<?php echo ($action == 'alt') ? $roweqp->title : NULL; ?>">
							</div>
                            
                            <div class="form-group">
                                <label for="description">Descrição</label>
                                <textarea rows="5" id="description" name="description" class="form-control"><?php echo ($action == 'alt') ? preg_replace('#<br[/\s]*>#si', "\n", $roweqp->description) : NULL; ?></textarea>
                            </div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="position">Posição</label>
									<input type="text" class="form-control" id="position" name="position" value="<?php echo ($action == 'alt') ? $roweqp->position : NULL; ?>">
								</div>

								<div class="col-md-6">
									<label for="is_active">Situação</label>
									<select name="is_active" id="is_active" class="form-control">
										<option value="1"<?php echo (($action == 'alt' && $roweqp->is_active == 1) ? ' selected' : NULL); ?>>Ativo</option>
										<option value="0"<?php echo (($action == 'alt' && is_null($roweqp->is_active)) ? ' selected' : NULL); ?>>Inativo</option>
									</select>
								</div>
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="equipments" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>