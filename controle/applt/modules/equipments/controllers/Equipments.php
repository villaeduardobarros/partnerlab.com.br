<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Equipments extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('md_equipments');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
			exit;
		}
	}

	public function index($codeqp=NULL)
	{        
		// theme
		$this->output->set_template('theme');
		// theme - css
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
		$this->load->css('assets/css/jquery-ui.css');
		// theme - js
		$this->load->js('assets/js/jquery/jquery-ui.min.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
		$this->load->js('assets/js/jquery/jquery.validate.min.js');
		$this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/equipments/equipments.js');

		$data['equipments'] = $this->md_equipments->searchEquipments(NULL, NULL, NULL, NULL, FALSE);
		$data['action'] = ($codeqp) ? 'alt' : 'cad';

		if ($codeqp) { 

			$data['codeqp'] = $codeqp;

			$equipment = $this->md_equipments->searchEquipments($codeqp);
			if ($equipment->num_rows() > 0) {
				$data['roweqp'] = $equipment->row();
			}

		}

		$this->load->view('equipments-form', $data);
	}

	public function action()
	{
		$action     = $this->input->post('action', TRUE);
		$position   = $this->input->post('position', TRUE);

		$arrData = array(
			'title'         => $this->input->post('title', TRUE),
			'description'   => str_replace(PHP_EOL, "<br>", $this->input->post('description', TRUE)),
			'position'      => ($position) ? $position : NULL,
			'is_active'     => (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL)
		);

		if ($action == 'cad') {

			$registered = $this->md_equipments->insertEquipments($arrData);
			if ($registered > 0) {
				echo 'success';
			} else {
				echo 'error';
			}

		} else {

			$codeqp = $this->input->post('codeqp', TRUE);

			if ($this->md_equipments->updateEquipments($codeqp, $arrData)) {
				echo 'success';
			} else {
				echo 'error';
			}
		}
	}


	public function actionDelete()
	{
		$codeqp = $this->input->post('codeqp', TRUE);

		if ($this->md_equipments->deleteEquipments($codeqp)) {
			echo 'success';
		} else {
			echo 'error';
		}
	}

}