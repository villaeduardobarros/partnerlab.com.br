<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_equipments extends CI_Model {

	public function searchEquipments($codeqp=NULL, $codbrd=NULL, $codmod=NULL, $is_active=NULL)
	{
		if ($codeqp) {
			$this->db->where('codeqp', $codeqp);
		}
		if ($codbrd) {
			$this->db->where('brands_codbrd', $codbrd);
		}
		if ($codmod) {
			$this->db->where('models_codmod', $codmod);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		$this->db->order_by('title', 'asc');
		return $this->db->get('equipments');
		//echo $this->db->last_query();
	}

	public function insertEquipments($data)
	{
		if ($this->db->insert('equipments', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateEquipments($codeqp, $data)
	{
		$this->db->where('codeqp', $codeqp);
		$this->db->update('equipments', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deleteEquipments($codeqp)
	{
		$this->db->where('codeqp', $codeqp);
		$this->db->delete('equipments');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}


	// tipos
	public function searchEquipmentTypes($codtpt=NULL)
	{
		if ($codtpt) {
			$this->db->where('codtpt', $codtpt);
		}
		return $this->db->get('equipments_type');
		//echo $this->db->last_query();
	}
	
}