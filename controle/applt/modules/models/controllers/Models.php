<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Models extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('md_models');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
            exit;
		}
	}
	
	public function index($codmod=NULL)
	{        
        // theme
        $this->output->set_template('theme');
        // theme - css
        $this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
        $this->load->css('assets/css/jquery-ui.css');
        // theme - js
        $this->load->js('assets/js/jquery/jquery-ui.min.js');
        $this->load->js('assets/plugins/datatables/jquery.dataTables.js');
        $this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
        $this->load->js('assets/js/jquery/jquery.validate.min.js');
        $this->load->js('assets/prop/pattern/pattern.js');
        $this->load->js('assets/prop/models/models.js');

        $data['models'] = $this->md_models->searchModels();
        $data['action'] = ($codmod) ? 'alt' : 'cad';
        
        if ($codmod) { 
            
            $data['codmod'] = $codmod;

            $model = $this->md_models->searchModels($codmod);
            if ($model->num_rows() > 0) {
                $data['rowmod'] = $model->row();
            }

        }

        $this->load->view('models-form', $data);
	}

	public function action()
	{
        $action     = $this->input->post('action', TRUE);
        $position   = $this->input->post('position', TRUE);

		$arrData = array(
            'title'         => $this->input->post('title', TRUE),
            'position'      => ($position) ? $position : NULL,
            'is_active'     => (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL)
		);

		if ($action == 'cad') {

            $registered = $this->md_models->insertModels($arrData);
			if ($registered > 0) {
                
                echo 'success';
            
            } else echo 'error';

		} else {

			$codmod = $this->input->post('codmod', TRUE);

			if ($this->md_models->updateModels($codmod, $arrData)) {
                
                echo 'success';
            
            } else echo 'error';
		}
	}


    public function actionDelete()
    {
        $codmod = $this->input->post('codmod', TRUE);

        if ($this->md_models->deleteModels($codmod)) {
                
            echo 'success';
        
        } else echo 'error';
    }
    
}