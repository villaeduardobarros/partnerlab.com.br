<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="models"><i class="fa fa-list"></i> Modelos</a></li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="80%">Título</th>
                                    <th width="10%">Posição</th>
                                    <th width="10%">Situação</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($models->result() as $model) { ?>

									<tr>
										<td>
											<a href="models/<?php echo $model->codmod; ?>" target="_self">
												<?php echo $model->title; ?>
											</a>
											<div class="pull-right">
												<i class="fa fa-trash" onclick="deleteModel(<?php echo $model->codmod; ?>)" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td><?php echo ($model->position) ? $model->position : NULL; ?></td>
										<td align="center">
											<span class="label label-<?php echo ($model->is_active == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($model->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="modelsForm" method="post" role="form">
					<input name="action" type="hidden" value="<?php echo $action; ?>">
					<?php if ($action == 'alt') { ?>
						<input name="codmod" type="hidden" value="<?php echo $codmod; ?>">
					<?php } ?>

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title"><?php echo ($action == 'cad') ? 'Cadastrar Modelo' : 'Editar Modelo'; ?></h3>
						</div>
						<div class="box-body">
	
							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>

							<div class="form-group">
								<label>Título</label>
								<!--<div class="pull-right">
									<div class="checkbox" style="margin:0 !important;">
										<label>
											<input type="checkbox" name="title" value="1" <?php //echo ($action == 'alt' ) ? 'checked' : NULL; ?>> Acesso principal ao sistema 
										</label>
									</div>
								</div>-->
								<input type="text" class="form-control" name="title" value="<?php echo ($action == 'alt') ? $rowmod->title : NULL; ?>">
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label>Posição</label>
									<input type="text" class="form-control" name="position" value="<?php echo ($action == 'alt') ? $rowmod->position : NULL; ?>">
								</div>

								<div class="col-md-6">
									<label>Situação</label>
									<select name="is_active" class="form-control">
										<option value="1"<?php echo (($action == 'alt' && $rowmod->is_active == 1) ? ' selected' : NULL); ?>>Ativo</option>
										<option value="0"<?php echo (($action == 'alt' && is_null($rowmod->is_active)) ? ' selected' : NULL); ?>>Inativo</option>
									</select>
								</div>
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="models" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>