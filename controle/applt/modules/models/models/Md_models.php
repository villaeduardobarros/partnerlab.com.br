<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_models extends CI_Model {

	public function searchModels($codmod=NULL, $brand=NULL, $is_active=NULL)
	{
		if ($codmod) {
			$this->db->where('codmod', $codmod);
		}
		if ($brand) {
			$this->db->where('brands_codbrd', $brand);
		}
		if ($is_active == TRUE) {
			$this->db->where('is_active', 1);
		}
		$this->db->order_by('title', 'asc');
		return $this->db->get('models');
		//echo $this->db->last_query();
	}

	public function insertModels($data)
	{
		if ($this->db->insert('models', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateModels($codmod, $data)
	{
		$this->db->where('codmod', $codmod);
		$this->db->update('models', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deleteModels($codmod)
	{
		$this->db->where('codmod', $codmod);
		$this->db->delete('models');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
}