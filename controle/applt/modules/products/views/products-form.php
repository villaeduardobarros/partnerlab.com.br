<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><i class="fa fa-cubes"></i> Productos</li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="80%">Título</th>
                                    <th width="10%">Posição</th>
                                    <th width="10%">Imagem</th>
                                    <th width="10%">Situação</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($products->result() as $prod) { ?>

									<tr>
										<td>
											<a href="products/<?php echo $prod->codprd; ?>" target="_self"><?php echo $prod->title; ?></a>
											<div class="pull-right">
												<a href="products/gallery/<?php echo $prod->codprd; ?>" target="_self"><i class="fa fa-picture-o"></i></a>
												&nbsp;&nbsp;
												<i class="fa fa-trash" onclick="deleteProduct(<?php echo $prod->codprd; ?>)" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td align="center"><?php echo ($prod->position) ? $prod->position : NULL; ?></td>
										<td align="center"><?php //echo $prod->photo; ?></td>
										<td align="center">
											<span class="label label-<?php echo ($prod->is_active == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($prod->is_active == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="productsForm" method="post" role="form">
					<input name="action" type="hidden" value="<?php echo $action; ?>">
					<?php if ($action == 'alt') { ?>
						<input name="codprd" type="hidden" value="<?php echo $codprd; ?>">
					<?php } ?>

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title"><?php echo ($action == 'cad') ? 'Cadastrar Produto' : 'Editar Produto'; ?></h3>
						</div>
						<div class="box-body">

							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="title">Título</label>
								<input type="text" class="form-control" id="title" name="title" value="<?php echo ($action == 'alt') ? $rowprd->title : NULL; ?>">
							</div>
							
							<div class="form-group">
								<label for="description">Descrição</label>
								<textarea class="form-control" id="description" name="description" rows="6"><?php echo ($action == 'alt') ? str_replace('<br>', "\n", $rowprd->description) : NULL; ?></textarea>
							</div>
							
							<div class="form-group">
								<label for="category">Categoria</label>
								<select class="form-control" id="category" name="category">
									<option>Selecione</option>
									<?php foreach ($categories->result() as $categ) { ?>

										<option value="<?php echo $categ->codcat; ?>" <?php echo ($action == 'alt' && $rowprd->categories_codcat == $categ->codcat) ? 'selected' : NULL; ?>><?php echo $categ->title; ?></option>

									<?php } ?>
								</select>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="position">Posição</label>
									<input type="text" class="form-control" id="position" name="position" value="<?php echo ($action == 'alt') ? $rowprd->position : NULL; ?>">
								</div>

								<div class="col-md-6">
									<label for="is_active">Situação</label>
									<select id="is_active" name="is_active" class="form-control">
										<option value="1"<?php echo (($action == 'alt' && $rowprd->is_active == 1) ? ' selected' : NULL); ?>>Ativo</option>
										<option value="0"<?php echo (($action == 'alt' && is_null($rowprd->is_active)) ? ' selected' : NULL); ?>>Inativo</option>
									</select>
								</div>
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="products" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>