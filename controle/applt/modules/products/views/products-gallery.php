<div class="content-wrapper">
	<ol class="breadcrumb">
		<li><a href="home"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="products"><i class="fa fa-cubes"></i> Productos</a></li>
		<li class="active"><i class="fa fa-picture-o"></i> Galerias</a></li>
	</ol>

	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-widget">
					<div class="box-body">

						<table id="tableItems" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="65%">Título</th>
                                    <th width="20%">Imagem</th>
                                    <th width="15%">Destaque</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php foreach ($galleries->result() as $gallery) { ?>

									<tr>
										<td>
											<?php echo ($gallery->title) ? $gallery->title : $gallery->photo; ?>
											<div class="pull-right">
												<i class="fa fa-trash" onclick="deleteGallery(<?php echo $gallery->codgal; ?>)" style="color:#900;cursor:pointer;"></i>
											</div>
										</td>
										<td>
											<img src="<?php echo $this->config->item('base_url_site'); ?>uploads/galleries/products/<?php echo $gallery->products_codprd; ?>/<?php echo $gallery->photo; ?>" width="100%">
										</td>
										<td align="center">
											<span class="label label-<?php echo ($gallery->main == 1) ? 'success' : 'danger'; ?>">
												<?php echo ($gallery->main == 1) ? 'Ativo' : 'Inativo'; ?>
											</span>
										</td>
									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<form id="galleriesForm" method="post" role="form" enctype="multipart/form-data">
					<input name="codprd" type="hidden" value="<?php echo $codprd; ?>">

					<div class="box box-widget">
						<div class="box-header with-border">
							<h3 class="box-title">Cadastrar Galeria do Produto</h3>
						</div>
						<div class="box-body">

							<div class="row">
								<div class="col-md-12">
									<div id="messageAlert"></div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="title">Título</label>
								<input type="text" class="form-control" id="title" name="title">
							</div>
							
							<div class="form-group">
								<label for="photo">Imagem</label>
								<input type="file" class="form-control" id="photo" name="photo">
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label for="photo">Principal?</label>
									<select id="main" name="main" class="form-control">
										<option value="0">Não</option>
										<option value="1">Sim</option>							
									</select>
								</div>
							</div>
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<a class="btn btn-default" href="products/gallery/<?php echo $codprd; ?>" style="margin-right:15px;"><i class="fa fa-remove"></i> Cancelar</a>
								<button type="submit" id="buttonForm" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>