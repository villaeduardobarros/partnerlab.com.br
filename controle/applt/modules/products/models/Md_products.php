<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_products extends CI_Model {

	public function searchProducts($codprd=NULL, $is_active=NULL)
	{
		if ($codprd) {
			$this->db->where('codprd', $codprd);
		}
		if (isset($is_active)) {
			if ($is_active == TRUE) {
				$this->db->where('is_active', 1);
			} else {
				$this->db->where('is_active', NULL);
			}
		}
		$this->db->order_by('title', 'asc');
		return $this->db->get('products');
		//echo $this->db->last_query();
	}

	public function insertProducts($data)
	{
		if ($this->db->insert('products', $data)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function updateProducts($codprd, $data)
	{
		$this->db->where('codprd', $codprd);
		$this->db->update('products', $data);
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}

	public function deleteProducts($codprd)
	{
		$this->db->where('codprd', $codprd);
		$this->db->delete('products');
		$report = $this->db->error();
        return (!$report['code']) ? TRUE : FALSE;
	}
	
}