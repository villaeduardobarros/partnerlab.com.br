<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('photos');
		$this->load->model('md_products');
		$this->load->model('galleries/md_galleries');
		$this->load->model('categories/md_categories');
		if (!$this->session->userdata('logged-adm-in')) {
			redirect('logout');
			exit;
		}
	}

	public function index($codprd=NULL)
	{        
		// theme
        $this->output->set_template('theme');
        // theme - css
        $this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
        $this->load->css('assets/css/jquery-ui.css');
        // theme - js
        $this->load->js('assets/js/jquery/jquery-ui.min.js');
        $this->load->js('assets/plugins/datatables/jquery.dataTables.js');
        $this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
        $this->load->js('assets/js/jquery/jquery.validate.min.js');
        $this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/products/products.js');

		$data['products'] = $this->md_products->searchProducts();
		$data['action'] = ($codprd) ? 'alt' : 'cad';
		$data['categories'] = $this->md_categories->searchCategories(NULL, NULL, NULL, TRUE);

		if ($codprd) { 

			$data['codprd'] = $codprd;

			$product = $this->md_products->searchProducts($codprd);
			if ($product->num_rows() > 0) {
				$data['rowprd'] = $product->row();
			}

		}

		$this->load->view('products-form', $data);
	}


	public function action()
	{
		$action 	= $this->input->post('action', TRUE);
		$position	= $this->input->post('position', TRUE);

		$arrData = array(
			'title'				=> $this->input->post('title', TRUE),
			'description'		=> str_replace(PHP_EOL, "<br>", $this->input->post('description', FALSE)),
			'position'			=> ($position) ? $position : NULL,
			'is_active'			=> (($this->input->post('is_active', TRUE) == 1) ? 1 : NULL),
			'categories_codcat'	=> $this->input->post('category', TRUE)
		);

		// se cadastro
		if ($action == 'cad') {

			$registered = $this->md_products->insertProducts($arrData);
			if ($registered > 0) {

				echo 'success';

			} else echo 'error';

		} else {

			$codprd = $this->input->post('codprd', TRUE);

			if ($this->md_products->updateProducts($codprd, $arrData)) {

				echo 'success';
			
			} else echo 'error';

		}
	}


	public function actionDelete()
	{
		$codprd = $this->input->post('codprd', TRUE);

		if ($this->md_products->deleteProducts($codprd)) {

			// apaga a pasta
			$this->photos->deleteFolder('/uploads/galleries/products/'.$codprd);

			echo 'success';
		
		} else echo 'error';
	}


	public function galleries($codprd)
	{
		if (!$codprd) {
			redirect('products');
			exit;
		}

		// theme
        $this->output->set_template('theme');
        // theme - css
        $this->load->css('assets/plugins/datatables/dataTables.bootstrap.css');
        $this->load->css('assets/css/jquery-ui.css');
        // theme - js
        $this->load->js('assets/js/jquery/jquery-ui.min.js');
        $this->load->js('assets/plugins/datatables/jquery.dataTables.js');
        $this->load->js('assets/plugins/datatables/dataTables.bootstrap.js');
        $this->load->js('assets/js/jquery/jquery.validate.min.js');
        $this->load->js('assets/prop/pattern/pattern.js');
		$this->load->js('assets/prop/products/products.js');

		$data['codprd'] = $codprd;
		$data['galleries'] = $this->md_galleries->searchGalleries(NULL, NULL, 'products_codprd', $codprd);
		$this->load->view('products-gallery', $data);
	}


	public function actionGallery()
	{
		$codprd = $this->input->post('codprd', TRUE);

		$arrData = array(
			'title' 			=> $this->input->post('title', TRUE),
			'main' 				=> (($this->input->post('main', TRUE) == 1) ? 1 : NULL),
			'products_codprd' 	=> $codprd
		);

		if (!empty($_FILES['photo']['name'])) {

			$ext = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION); // extensao do arquivo

			$allowedExts = array('gif','jpeg','jpg','png'); // extensoes permitidas

			if (in_array($ext, $allowedExts)) {

				// verifica se a pasta do cliente existe
				if (!is_dir(realpath('../').'/uploads/galleries/products/'.$codprd.'/')) {
					mkdir(realpath('../').'/uploads/galleries/products/'.$codprd, 0777, TRUE);
				}

				$upload = $this->photos->send('photo', NULL, '/uploads/galleries/products/'.$codprd, 800, 600);
				if ($upload) {
					$arrData['photo'] = $upload;
				}

			} else echo 'extension-not-allowed';

		}

		$registered = $this->md_galleries->insertGalleries($arrData);
		if ($registered > 0) {

			echo 'success';

		} else echo 'error';

	}


	public function actionDeleteGalleries()
	{
		$codgal = $this->input->post('codgal', TRUE);

		if ($this->md_galleries->deleteGalleries($codgal)) {

			$galleries = $this->md_galleries->searchGalleries($codgal);
			if ($galleries->num_rows() > 0) {

				$gallery = $galleries->row();

				// apaga o arquivo antigo
				$this->photos->deleteArchive('/uploads/galleries/products/'.$gallery->products_codprd, $gallery->photo);

			}

			echo 'success';
		
		} else echo 'error';
	}

}