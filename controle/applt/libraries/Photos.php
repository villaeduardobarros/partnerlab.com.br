<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Photos {

	public function send($inputName=NULL, $nameArchive=NULL, $folder=NULL, $w=NULL, $h=NULL) {
		
		$CI = & get_instance();

		$name =  ($nameArchive) ? url_title($nameArchive) : rand(100, 99999).date('YmdHi');

		list($width, $height) = getimagesize($_FILES[$inputName]['tmp_name']);

		include_once('UploadVerot.php');
		$handle = new Upload($_FILES[$inputName]);
		if ($handle->uploaded) {

			$handle->image_resize 			= true;
			$handle->image_ratio_crop 		= true;
			$handle->image_ratio_y 			= false;
			$handle->image_x 				= (($w) ? $w : $width);
			$handle->image_y 				= (($h) ? $h : $height);
			$handle->file_new_name_body 	= $name;
			$handle->image_convert 			= 'jpeg';
			$handle->Process(realpath('../').$folder);

			if ($handle->processed) {

				return $handle->file_dst_name;

			} else {

				echo $handle->error;
				return $handle->error . "/" . (1);

			}

		}

		$handle->Clean();
	}


	public function uploadImg($inputName=NULL, $nameArchive=NULL, $folder=NULL, $w=NULL, $h=NULL) 
	{
		$CI = & get_instance();

		$name =  ($nameArchive) ? strtolower($nameArchive) : (rand(100, 99999).date('YmdHi'));

		include_once('UploadVerot.php');
		$handle = new Upload($_FILES[$inputName]);
		if ($handle->uploaded) {

			$handle->image_resize           = true;
			$handle->image_ratio_crop       = true;
			$handle->image_ratio_y          = false;
			$handle->image_x                = $w;
			$handle->image_y                = $h;
			$handle->file_new_name_body     = $name;
			$handle->image_convert 			= 'jpeg';
			$handle->file_safe_name         = false;
			$handle->file_auto_rename       = false;
			$handle->auto_create_dir        = true;
			$handle->dir_auto_chmod         = true;
			$handle->dir_chmod              = 0777;
			$handle->Process(realpath('../').$folder);

			if ($handle->processed) {

				return $handle->file_dst_name;

			} else {

				echo $handle->error;
				return $handle->error . "/" . (1);

			}
		}

		$handle->Clean();
	}


	// apaga um arquivo especifico
	public function deleteArchive($folder=NULL, $nameArchive=NULL)
	{
		$archiveSearched = realpath('../').$folder.$nameArchive;

		if (file_exists($archiveSearched)) {

			if (unlink($archiveSearched)) {

				return true;

			} else return false;

		} else return true;
	}


	//  apaga a pasta principal, subpastas (caso tenha) e os arquivos (caso tenha)
	public function deleteFolder($folder=NULL)
	{
		$dirPath = realpath('../').$folder;

		if (file_exists($dirPath)) {

			if (!is_dir($dirPath)) {
				throw new InvalidArgumentException("$dirPath must be a directory");
			}

			if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
				$dirPath .= '/';
			}
		
			$files = glob($dirPath . '*', GLOB_MARK);
			foreach ($files as $file) {
				if (is_dir($file)) {
					self::deleteFolder($folder);
				} else {
					unlink($file);
				}
			}
			rmdir($dirPath);

			if (is_dir($dirPath)) {
				rmdir($dirPath);
			}

		}

		return true;
	}

}

?>