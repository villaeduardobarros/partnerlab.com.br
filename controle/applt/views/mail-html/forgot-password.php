<html>
<head>
<meta charset="utf-8" />
</head>
<body style="font-family:Arial, Helvetica, sans-serif; margin:0; padding:0;">

    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;font-family:tahoma;font-size:13px;">
        <tr style="background:#f39c12;color:#ffffff;">
            <td width="50%" style="padding:20px;">
                <img src="../../assets/images/logo.png" width="50">
            </td>
            <td width="50%" style="text-align:right;padding:20px;font-size:20px;"><?php echo $subject; ?></td>
        </tr>
        <tr>
            <td colspan="2" style="padding:30px;">
                <p>Prezado, <strong><?php echo $name; ?></strong></p>
                
                <p>Alguém solicitou recentemente para redefinir sua senha acesso ao sistema.</p>

                <p><strong>Código de redefinição: <?php echo $codrdf; ?></strong></p>

                <p>
                    Clique no link abaixo ou copie na barra de endereço do seu navegador e informe o código de redefinição.<br>
                    <a href="http://linuxbd/gepro/login/confirma-senha/<?php echo $urlreturn; ?>" target="_blank">
                        http://linuxbd/gepro/login/confirma-senha/<?php echo $urlreturn; ?>
                    </a>
                </p>

                <p>
                    <strong>Não solicitou esta alteração?</strong>
                    Se você não solicitou uma nova senha de acesso, por favor ignore este e-mail.
                    <br>
                    Caso suspeite que a segurança de sua conta pode estar comprometida, por favor, entre em contato conosco (<strong>xxxxx@partnerlab.com.br</strong>).
                </p>
            </td>
        </tr>
    </table>

</body>
</html>