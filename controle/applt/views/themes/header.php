<header class="main-header">
	<a href="home" class="logo">
		<span class="logo-mini"><b>ADM</b></span>
		<span class="logo-lg"><img src="assets/images/logo-bg-dark.png" height="36"></span>
	</a>

	<nav class="navbar navbar-static-top">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
		  
				<li class="dropdown user user-menu">
					<?php $photo = ($this->session->userdata('login-adm-photo')) ? $this->session->userdata('login-adm-photo') : 'partnerlab.png'; ?>

					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo $this->config->item('base_url_site'); ?>uploads/users/<?php echo $photo; ?>" class="user-image user-img-thumb" alt="User Image">
						<span class="hidden-xs"><?php echo $this->session->userdata('login-adm-name'); ?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-header">
							<img src="<?php echo $this->config->item('base_url_site'); ?>uploads/users/<?php echo $photo; ?>" class="img-circle user-img-thumb" alt="<?php echo $this->session->userdata('login-adm-name'); ?>" title="<?php echo $this->session->userdata('login-adm-name'); ?>">
							<p>
								<strong><?php echo $this->session->userdata('login-adm-name'); ?></strong><br>
								<small>
									(<?php echo $this->session->userdata('login-adm-user'); ?>) - <?php echo $this->session->userdata('login-adm-email'); ?>
								</small>
							</p>
						</li>
						<li class="user-footer">
							<div class="pull-left">
								<a href="home" class="btn btn-default btn-flat">Inicial</a>
							</div>
							<div class="pull-right">
								<a href="logout" class="btn btn-default btn-flat">Sair</a>
							</div>
						</li>
					</ul>
				</li>

			</ul>
		</div>
	</nav>
</header>