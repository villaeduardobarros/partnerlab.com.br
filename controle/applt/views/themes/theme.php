<?php
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="<?php echo base_url(); ?>">
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo $this->config->item('title'); ?></title>
	<link rel="canonical" href="<?php echo current_url(); ?>" />
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/png">
	<link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/png">
	<link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/skin.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/ionicons.min.css">
	<link rel="stylesheet" href="assets/css/print.css" media="print">
	<link rel="stylesheet" href="assets/css/partnerlab.css">
	<link rel="stylesheet" href="assets/css/pattern.css">
	<script src="assets/js/jquery/jquery.min.js"></script>
	<?php
		// css
		$countCss = 0;
		$countTotCss = count($css);
		foreach ($css as $file) {
			echo "<link href=\"".$file."\" rel=\"stylesheet\" type=\"text/css\">\n";
			echo ($countCss < $countTotCss-1) ? "\t" : '';
			$countCss++;
		}
	?>
	<script>var baseUrl = '<?php echo base_url(); ?>';</script>
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.min.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>
<body class="fixed sidebar-mini skin-yellow-light" onhashchange="myFunction()">

	<div id="preloader">
		<img src="assets/images/loading.gif" width="140px">
	</div>

	<?php $photo = ($this->session->userdata('login-adm-photo')) ? $this->session->userdata('login-adm-photo') : 'partnerlab.png'; ?>

	<div class="wrapper">
		<?php // header ?>
		<?php include('header.php'); ?>

		<?php // menu ?>
		<aside class="main-sidebar">
			<section class="sidebar">
				<ul class="sidebar-menu">
					<?php 
						// verifica se o usuario ja tem alguma permissao vinculada
			            $codusr = $this->session->userdata('login-adm-codusr');
			            $permissions = $this->md_users->searchUsrPerm($codusr);
			            $selected = (($permissions->num_rows() > 0) ? $permissions->row()->menCod : NULL);
						echo mountMenuMain(1, NULL, 0, TRUE, $selected);
					?>
				</ul>
			</section>
		</aside>

		<?php // conteudo ?>
		<?php echo $output; ?>

		<?php // footer ?>
		<footer class="main-footer">
			<strong>Partnerlab</strong> &copy; 2019 - Todos os direitos reservados.
			<div class="pull-right hidden-xs"><b>Versão</b> 1.0.0</div>
		</footer>
	</div>

	<?php // modal padrão ?>
	<div id="getModal"></div>
	<div id="getModalSts"></div>

	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/app.min.js"></script>
	<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<?php
		// js
		$countJs = 0;
		$countTotJs = count($js);
		foreach ($js as $file) {
			echo "<script src=\"".$file."\"></script>\n";
			echo ($countJs < $countTotJs-1) ? "\t" : '';
			$countJs++;
		}
	?>

</body>
</html>