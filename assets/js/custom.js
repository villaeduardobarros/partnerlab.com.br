$(document).ready(function(){

	var $mainNav = $('#navigation').children('ul');
	$mainNav.on('mouseenter', 'li', function(){

		var $this = $(this),
			$subMenu = $this.children('ul');

		if ($subMenu.length) $this.addClass('hover');
		$subMenu.hide().stop(true, true).slideDown('fast');

	}).on('mouseleave', 'li', function(){

		$(this).removeClass('hover').children('ul').stop(true, true).slideUp('fast');

	});


	if ($('.picture a').hasClass('image-overlay-zoom')) {
		$('.picture a').hover(function(){
			$(this).find('.image-overlay-zoom, .image-overlay-link').stop().fadeTo('fast', 1);
		},function(){
			$(this).find('.image-overlay-zoom, .image-overlay-link').stop().fadeTo('fast', 0);
		});
	}


	// efeito do link
	if ($('a').hasClass('swipebox')) {
		$('.swipebox').swipebox();
	}


	// contato
	if (document.getElementById('sessionHome') !== null) 
	{
		$('.flexslider').flexslider();
	}


	// contato
	if (document.getElementById('sessionContact') !== null) 
	{
		$('#contactForm').validate({
			ignore: [], 
			rules: {
				name: 		'required',
				email: 		'required',
				phone: 		'required',
				city: 		'required',
				uf: 		'required',
				message: 	'required'
			},
			messages: {
				name: 		'Campo obrigatório',
				email: 		'Campo obrigatório',
				phone: 		'Campo obrigatório',
				city: 		'Campo obrigatório',
				uf: 		'Campo obrigatório',
				message: 	'Campo obrigatório'
			}, 
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				$.post('contact/contact/sendContact', $(form).serialize(), function(dataReturn){

					if (dataReturn == 'success') {

						// limpa tudo relacionado ao form
						$('#contactForm')[0].reset();

						$('.alerts').css('background-color', '#00a65a').css('color', '#ffffff').html('Contato enviado com sucesso!');
						$('body,html').animate({ scrollTop:0 }, 600);
						setTimeout('document.location.reload(true);', 1800);
						$('#buttonForm').prop('disabled', false);

					} else {

						('.alerts').css('background-color', '#dd4b39').css('color', '#ffffff').html('Não foi possível enviar o contato, tente novamente mais tarde por favor!');
						$('body,html').animate({ scrollTop:0 }, 600);
						setTimeout('document.location.reload(true);', 1800);
						$('#buttonForm').prop('disabled', false);
					}

				});

			}

		});
	}


	// login
	if (document.getElementById('sessionLogin') !== null) 
	{
		$('#loginForm').validate({
			ignore: '', 
			rules: {
				user: 		'required',
				password: 	'required'
			}, 
			messages: {
				user: 		'Campo obrigatório',
				password: 	'Campo obrigatório'
			}, 
			submitHandler: function(form) {

				$('#buttonForm').prop('disabled', true);

				$.post('login/login/action', $(form).serialize(), function(dataReturn) {

					if (dataReturn == 'success') {

						$('#loginForm')[0].reset();

						$('.alerts').css('background-color', '#3a6f0b').css('color', '#ffffff').html('Sucesso... Redirecionando!');
						$('body,html').animate({ scrollTop:0 }, 600);
						setTimeout(function(){
							window.location = 'area-cliente/home';
						}, 1800);

					} else if (dataReturn == 'incorrect-data') {

						$('.alerts').css('background-color', '#dd4b39').css('color', '#ffffff').html('Dados inválidos!');
						$('body,html').animate({ scrollTop:0 }, 600);
						$('#buttonForm').prop('disabled', false);

					} else {

						$('.alerts').css('background-color', '#dd4b39').css('color', '#ffffff').html('Não foi possível enviar o contato, tente novamente mais tarde por favor!');
						$('body,html').animate({ scrollTop:0 }, 600);
						setTimeout('document.location.reload(true);', 1800);
						$('#buttonForm').prop('disabled', false);
					}

				});

			}

		});
	}


	if (document.getElementById('sessionArea') !== null) 
	{
		// Lista de Registros - DATATABLE
        $('#tableItems').dataTable({
			"language": {
				"lengthMenu": "Mostrar _MENU_ registros",
				"info": "Exibir página _PAGE_ de _PAGES_",
				"infoEmpty": "Nenhum registro encontrado",
				"search": "Buscar"
			}
		});
	}


	if (document.getElementById('sessionAreaTrac') !== null) 
	{
		$('button#btnSearch').click(function(){

			var input = $('input#search');
			if (input.val() == '') {

				alert('Informe o que deseja pesquisar!');

			} else {

				$.post('area/area/searchTraceability', {title:input.val()}, function(dataReturn){

					$('.list-trac').html(dataReturn)

					// Lista de Registros - DATATABLE
			        $('#tableItems').dataTable({
						"language": {
							"lengthMenu": "Mostrar _MENU_ registros",
							"info": "Exibir página _PAGE_ de _PAGES_",
							"infoEmpty": "Nenhum registro encontrado",
							"search": "Buscar"
						}
					});

				});

			}

		});
		
	}

});