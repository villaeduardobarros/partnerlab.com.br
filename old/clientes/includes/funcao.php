<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

if ($_GET['acao'] == 'buscar') {

	$input = anti_injection($_POST['input']);

	$query_qArqRastreab = sprintf("SELECT * FROM rastreabilidade WHERE titulo = %s", 
		GetSQLValueString($input, "text"));
	$qArqRastreab = mysql_query($query_qArqRastreab, $conexao) or die(mysql_error());
	$totalRows_qArqRastreab = mysql_num_rows($qArqRastreab);

	$html = '';

	if ($totalRows_qArqRastreab > 0) {

		while ($row_qArqRastreab = mysql_fetch_assoc($qArqRastreab)) {

			list($data, $hora) = explode(' ', $row_qArqRastreab['datahora']);

			$html .= '<tr>
				<td>
					<a href="../arquivos/rastreabilidade/'.$row_qArqRastreab['arquivo'].'" target="_blank">'.$row_qArqRastreab['titulo'].'</a>
				</td>
				<td>'.implode('/', array_reverse(explode('-', $data))).' '.$hora.'</td>
			</tr>';

		}

	} else {

		$html .= '<tr>
			<td colspan="2">Nenhum arquivo de rastreabilidade foi encontrado</td>
		</tr>';

	}

	echo $html;

}
?>