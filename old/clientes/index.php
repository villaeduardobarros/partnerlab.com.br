<?php
  require_once('../Connections/conexao.php');

  // *** Validate request to login to this site.
  if (!isset($_SESSION)) {
    session_start();
  }

  $loginFormAction = $_SERVER['PHP_SELF'];
  if (isset($_GET['accesscheck'])) {
    $_SESSION['PrevUrl'] = $_GET['accesscheck'];
  }

  if (isset($_POST['login'])) {

    $loginUsername = $_POST['login'];
    $loginPassword = $_POST['senha'];
    $password = ucfirst($loginUsername.$loginPassword);
    $codificada = hash('whirlpool', $password);
    // echo $loginUsername.' <> '.$loginPassword.' <> '.$codificada; exit;
    // original:    48720082d1e96e1f0a6e5ee3b1d7e2005a2e40fff3b8f9bfb5edc62ba9c22efef938e9237d9acd4a298d237723466a43e39772bc467e614ba431092f8d682f93
    // partnerlab:  bbe2f7e77066d3a120130eef7dea124c1cba16026a3fec5c60836e0bd7758ed344bf417555005ea647b0f18e1fe1997f73d8e6f779b5a3809ec7ba541b9b1cbe

    $MM_fldUserAuthorization = "";
    $MM_redirectLoginSuccess = "home/";
    $MM_redirectLoginFailed = "index.php?info=erro";
    $MM_redirecttoReferrer = false;

    $LoginRS__query=sprintf("SELECT * FROM clientes WHERE login=%s AND senha=%s AND status=1",
    GetSQLValueString(anti_injection($loginUsername), "text"),
    GetSQLValueString(anti_injection($codificada), "text"));
    $LoginRS = mysql_query($LoginRS__query, $conexao) or die(mysql_error());
    $loginFoundUser = mysql_num_rows($LoginRS);

    if ($loginFoundUser) {

      $loginStrGroup = "";

      $row_Login = mysql_fetch_assoc($LoginRS);

      //declare two session variables and assign them
      $_SESSION['MM_cliUsername']   = $loginUsername;
      $_SESSION['MM_cliUserGroup']  = $loginStrGroup;
      $_SESSION['MM_cliNome']       = $row_Login['nome'];
      $_SESSION['MM_cliId']         = $row_Login['id'];

      if (isset($_SESSION['PrevUrl']) && false) {
        $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];
      }
      header("Location: " . $MM_redirectLoginSuccess );

    } else {
      header("Location: ". $MM_redirectLoginFailed );
    }

  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Área do Cliente</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="../admin/css/bootstrap.css" rel="stylesheet">
<link href="../admin/css/bootstrap-responsive.css" rel="stylesheet">
<link href="../admin/css/bootstrap-overrides.css" rel="stylesheet">
<link href="../admin/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
<link href="../admin/css/slate.css" rel="stylesheet">
<link href="../admin/css/components/signin.css" rel="stylesheet" type="text/css">
<script src="../admin/js/jquery-1.7.2.min.js"></script>
<script src="../admin/js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="../admin/js/jquery.ui.touch-punch.min.js"></script>
<script src="../admin/js/bootstrap.js"></script>
</head>

<body>
  <div class="account-container login">

    <div class="content clearfix">

    <form name="login" id="login_form" method="post"  action="<?php echo $loginFormAction; ?>">
      <h1>Área do Cliente</h1>

      <div class="login-fields">
        <p>Entre com seu usuário e senha:</p>
        <?php if($_GET['info'] == "erro"){ ?>
          <span class="alert-error">Usuário ou Senha Errado. Tente novamente.</span>
        <?php } ?>
        <div class="field">
          <label for="login">Usuário:</label>
          <input type="text" id="login" name="login" value="" placeholder="Usuário:" class="login username-field" />
        </div> <!-- /field -->

        <div class="field">
          <label for="senha">Senha:</label>
          <input type="password" id="senha" name="senha" value="" placeholder="Senha:" class="login password-field"/>
        </div> <!-- /password -->
      </div> <!-- /login-fields -->

      <div class="login-actions">
        <button class="button btn btn-secondary btn-large">Entrar</button>
      </div> <!-- .actions -->
    </form>

    </div> <!-- /content -->

  </div> <!-- /account-container -->
</body>
</html>
