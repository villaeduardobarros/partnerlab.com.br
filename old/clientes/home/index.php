<?php require_once('../../Connections/conexao.php'); ?>
<?php require_once('../includes/login.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Área de Cliente</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="../../admin/css/bootstrap.css" rel="stylesheet">
<link href="../../admin/css/bootstrap-responsive.css" rel="stylesheet">
<link href="../../admin/css/bootstrap-overrides.css" rel="stylesheet">
<link href="../../admin/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
<link href="../../admin/css/slate.css" rel="stylesheet">
<link href="../../admin/css/slate-responsive.css" rel="stylesheet">
<link href="../../admin/css/pages/reports.css" rel="stylesheet">
<script src="../../admin/js/jquery-1.7.2.min.js"></script>
<script src="../../admin/js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="../../admin/js/jquery.ui.touch-punch.min.js"></script>
<script src="../../admin/js/bootstrap.js"></script>
<script src="../../admin/js/plugins/excanvas/excanvas.min.js"></script>
<script src="../../admin/js/plugins/flot/jquery.flot.js"></script>
<script src="../../admin/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="../../admin/js/plugins/flot/jquery.flot.orderBars.js"></script>
<script src="../../admin/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="../../admin/js/Slate.js"></script>
<script src="../../admin/js/demos/charts/area.js"></script>
<script src="../../admin/js/demos/charts/pie.js"></script>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<!-- /#header -->
<?php include("../includes/header.php"); ?>
<!-- /#header -->

<!-- /#menu -->
<?php include("../includes/menu.php"); ?>
<!-- /#menu -->

<div id="content">
  <div class="container">
    <div id="page-title" class="clearfix">
      <h1>Painel do Cliente</h1>
      <ul class="breadcrumb">
        <li> <a href="../">Home</a> <span class="divider">/</span> </li>
        <li class="active">Painel do Cliente</li>
      </ul>
    </div>
    <!-- /.page-title -->
    
    <div class="row">
      <div class="span12">
        <div id="big-stats-container" class="widget">
          <div class="widget-content">
            <div id="big_stats" class="cf">
              
              Bem vindo ao painel do cliente.<br>
        Navegue no menu superior e acesse as áreas que deseja administrar. 
            
            </div>
          </div>
          <!-- /widget-content --> 
          
        </div>
        <!-- /widget --> 
        
      </div>
      <!-- /span12 -->
      
      
    </div>
    <!-- /row -->
    
    
    <!-- /row --> 
    
  </div>
  <!-- /.container --> 
  
</div>
<!-- /#content -->

<!-- /#rodape -->
<?php include("../includes/rodape.php"); ?>
<!-- /#rodape -->

</body>
</html>
