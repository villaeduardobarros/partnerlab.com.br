<?php
  //initialize the session
  if (!isset($_SESSION)) {
    session_start();
  }

  if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
    //to fully log out a visitor we need to clear the session varialbles
    $_SESSION['MM_cliUsername'] = NULL;
    $_SESSION['MM_cliUserGroup'] = NULL;
    $_SESSION['MM_cliNome'] = NULL;
    $_SESSION['MM_cliId'] = NULL;
    $_SESSION['PrevUrl'] = NULL;

    unset($_SESSION['MM_cliUsername']);
    unset($_SESSION['MM_cliUserGroup']);
    unset($_SESSION['MM_cliNome']);
    unset($_SESSION['MM_cliId']);
    unset($_SESSION['PrevUrl']);

    header("Location:index.php");
    exit;
  }
?>
