<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

$cl = anti_injection($_SESSION['MM_cliId']);

$query_qClienteArq = sprintf("SELECT * FROM clientes_arq WHERE clientes_id = %s", 
	GetSQLValueString($cl, "int"));
$qClienteArq = mysql_query($query_qClienteArq, $conexao) or die(mysql_error());
$totalRows_qClienteArq = mysql_num_rows($qClienteArq);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Área do Cliente</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link href="../../admin/css/bootstrap.css" rel="stylesheet">
	<link href="../../admin/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../../admin/css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../../admin/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../../admin/js/plugins/datatables/DT_bootstrap.css" rel="stylesheet">
	<link href="../../admin/js/plugins/responsive-tables/responsive-tables.css" rel="stylesheet"> 
	<link href="../../admin/css/slate.css" rel="stylesheet">
	<link href="../../admin/css/slate-responsive.css" rel="stylesheet"> 
	<script src="../../admin/js/jquery-1.7.2.min.js"></script>
	<script src="../../admin/js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../../admin/js/jquery.ui.touch-punch.min.js"></script>
	<script src="../../admin/js/bootstrap.js"></script>
	<script src="../../admin/js/plugins/datatables/jquery.dataTables.js"></script>
	<script src="../../admin/js/plugins/datatables/DT_bootstrap.js"></script>
	<script src="../../admin/js/plugins/responsive-tables/responsive-tables.js"></script>
	<script src="../../admin/js/Slate.js"></script>
	<script src="../../admin/js/demos/demo.tables.js"></script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script>
		$(document).ready(function(){

			$('button#btn_search').click(function(){

				if ($('input#search').val() == '') {

					alert('O campo de pesquisa não pode ficar vazio');
					$('input#search').focus();

				} else {

					$('#rastreab tbody').load('../includes/funcao.php?acao=buscar', {input:$('input#search').val()});

				}
			});

		});
	</script>
</head>
<body>
<!-- /#header -->
<?php include("../includes/header.php"); ?>
<!-- /#header -->

<!-- /#menu -->
<?php include("../includes/menu.php"); ?>
<!-- /#menu -->

<div id="content">
	<div class="container">
		<div id="page-title" class="clearfix">
			<h1>Arquivos</h1>
			<ul class="breadcrumb">
				<li> <a href="../home/">Home</a> <span class="divider">/</span> </li>
				<li class="active">Lista de Arquivos</li>
			</ul>
		</div>
		<!-- /.page-title -->

		<div class="row">

			<div class="span12">
				<div class="widget widget-table"> 
					<div class="widget-header">           
						<h3><i class="icon-th-list"></i> Certificados</h3>
					</div> <!-- /widget-header -->

					<div class="widget-content">

						<table class="table table-striped table-bordered table-highlight" id="example">
							<thead>
								<tr>
									<th width="70%">Título</th>
									<th width="30%">Data/Hora</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($totalRows_qClienteArq > 0) { ?>

									<?php while ($row_qClienteArq = mysql_fetch_assoc($qClienteArq)) { ?>

										<tr class="odd gradeX">
											<td>
												<a href="../arquivos/<?php echo $row_qClienteArq['arquivo']; ?>" target="_blank">
													<?php echo $row_qClienteArq['titulo']; ?>
												</a>
											</td>
											<td>
												<?php 
													list($data, $hora) = explode(' ', $row_qClienteArq['datahora']);
													echo implode('/', array_reverse(explode('-', $data))).' '.$hora;
												?>
											</td>
										</tr>

									<?php } ?>

								<?php } ?>

								<?php //if ($totalRows_qArqRastreab > 0) { ?>

									<?php //while ($row_qArqRastreab = mysql_fetch_assoc($qArqRastreab)) { ?>

										<!--<tr class="odd gradeX">
											<td>
												<a href="../arquivos/rastreabilidade/<?php //echo $row_qArqRastreab['arquivo']; ?>" target="_blank">
													<?php //echo $row_qArqRastreab['titulo']; ?>
												</a>
											</td>
											<td>Rastreabilidade</td>
											<td>
												<?php 
													//list($data, $hora) = explode(' ', $row_qArqRastreab['datahora']);
													//echo implode('/', array_reverse(explode('-', $data))).' '.$hora;
												?>
											</td>
										</tr>-->

									<?php //} ?>

								<?php //} ?>  
							</tbody>
						</table>

					</div> <!-- /widget-content -->
				</div> <!-- /widget --> 
			</div>

		</div>
		<!-- /row --> 

		<div class="row">

			<div class="span12">
				<div class="widget widget-table"> 
					<div class="widget-header">           
						<h3><i class="icon-th-list"></i> Rastreabilidade</h3>
						<div style="float:right;color:#000;margin:0 12px;">
							<input name="search" id="search" type="text" placeholder="Pesquisar" style="margin:7px 0;width:180px;"> 
							<button type="button" class="btn btn-warning" id="btn_search" style="padding:3px 5px 3px 8px;margin:-1px 0 0;"><i class="icon-search"></i></a>
						</div>
					</div> <!-- /widget-header -->

					<div class="widget-content">

						<table class="table table-striped table-bordered table-highlight" id="rastreab">
							<thead>
								<tr>
									<th width="70%">Título</th>
									<th width="30%">Data/Hora</th>
								</tr>
							</thead>
							<tbody>

								<tr>
									<td colspan="2">Nenhum arquivo de rastreabilidade foi encontrado</td>
								</tr>

							</tbody>
						</table>

					</div> <!-- /widget-content -->
				</div> <!-- /widget --> 
			</div>

		</div>
		<!-- /row --> 

	</div>
	<!-- /.container --> 

</div>
<!-- /#content -->

<!-- /#rodape -->
<?php include("../includes/rodape.php"); ?>
<!-- /#rodape -->
</body>
</html>