<?php 
if (!isset($_SESSION)) { session_start(); }

$_SESSION = array();

include("captcha_code.php");

$_SESSION['captcha'] = simple_php_captcha();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Partnerlab</title>

<!-- CSS -->
<link rel="icon" type="image/png" href="images/favicon.png" />
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<!-- JS -->
<script src="js/jquery.min.js"></script>
<script src="js/flexslider.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#envia_contato").click(function() {
		  	if( $("#captcha").val() === $("#captcha_code").val() ) {
				$("#form").submit();
			  } else {
				alert("Digite o Código corretamente!");
			  }
		});
	});
</script>

</head>
<body>
<?php include("includes/header.php"); ?>

<div class="banner-interno"></div>

<div id="wrapper">
	
    <div class="container">
        <div class="sixteen columns">
            <div id="page-title">
                <h2>Contato</h2>
                <div id="bolded-line"></div>
            </div>
        </div>
    </div>
    
  	<div class="container">

	<div class="eight columns">
		<div>
			<form method="post" action="enviar_contato.php" name="contato" id="form">
				
				<div class="field">
					<input type="text" name="nome" id="nome" class="text" placeholder="Nome" required />
				</div>
				
				<div class="field">
					<input type="text" name="email" id="email" class="text" placeholder="Email" required />
				</div>
                
                <div class="field">
					<input type="text" name="telefone" id="telefone" class="text" placeholder="Telefone" required/>
				</div>
                <div class="clear"></div>
                
                <div class="field">
					<input type="text" name="cidade" id="cidade" class="text" placeholder="Cidade" required/>
				</div>
                
                <div class="field">
					<input type="text" name="estado" id="estado" class="text" placeholder="UF" required/>
				</div>
                
				<div class="field">
					<textarea name="mensagem" id="mensagem" class="text textarea" placeholder="Mensagem" required></textarea>
				</div>
                
                <div class="field">
                 <?php echo '<img src="'.$_SESSION['captcha']['image_src'].'" alt="CAPTCHA Code">'; ?><br><br>
                 <input type="hidden" name="captcha_code" id="captcha_code" value="<?php echo $_SESSION['captcha']['code']; ?>"/>
                 <input type="text" name="captcha" id="captcha" class="text" placeholder="Digite o Código" required/>
				</div>
                
				<div class="field">
					<input type="button" value="Enviar" id="envia_contato" class="button color" style="margin-right:20px;"/> <input type="reset" value="Limpar" class="button color"/>
				</div>
				
			</form>
		</div>
	</div>
		
    <div class="eight columns google-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.3941976755646!2d-47.760220099999984!3d-21.176493899999986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94b9c0a1bcfd0359%3A0x23b852349b9aa274!2sR.+Virg%C3%ADlio+de+Carvalho+Neves+Neto%2C+926+-+Res.+e+Comercial+Palmares%2C+Ribeir%C3%A3o+Preto+-+SP%2C+14092-440!5e0!3m2!1sen!2sbr!4v1435078468455" height="400" frameborder="0" style="border:0; width:100%;"></iframe>
        <div class="info-contato">
            
            Rua Virgilio de Carvalho Neves Neto, 926<br>
            Ribeirão Preto – SP - CEP: 14092-440<br>
            <strong>Email:</strong> partnerlab@partnerlab.com.br

            
        </div>
    </div>
    
    <div class="sixteen columns fone">
    	<span>16</span> 3624 – 7700  |  3237 -3861
    </div>

</div>
</div>

<?php include("includes/footer.php"); ?>

</body>
</html>