﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Partnerlab</title>

<!-- CSS -->
<link rel="icon" type="image/png" href="images/favicon.png" />
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<link rel="stylesheet" type="text/css" href="css/swipebox.css">
<!-- JS -->
<script src="js/jquery.min.js"></script>
<script src="js/flexslider.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.swipebox').swipebox();
	});
</script>


</head>
<body>
<?php include("includes/header.php"); ?>

<div class="banner-interno"></div>

<div id="wrapper">
	
    <div class="container">
        <div class="sixteen columns">
            <div id="page-title">
                <h2>Serviços</h2>
                <div id="bolded-line"></div>
            </div>
        </div>
    </div>
    
  	<div class="container">
    
        <div class="sixteen columns">
            <div class="headline no-margin"><h3>Conheça</h3></div>
            <p>Empresa preparada para realizar MANUTENÇÃO PREVENTIVA, CORRETIVA e CALIBRAÇÃO de equipamentos de laboratório, mantendo a qualidade dos equipamentos de acordo com as características de fabricação, proporcionando elevada confiabilidade de resultados no processo de trabalho.<br><br>
                Equipe técnica qualificada com treinamentos nas empresas autorizadas e parceiras.<br><br>
                Oferecemos contrato de serviço de manutenção e calibração com gerenciamento dos equipamentos na base do cliente, evitando assim desconforto e preocupação com vencimento do próximo serviço.<br><br>
                Empresa Credenciada no IPEM para realização de serviços de manutenção em balanças com capacidade até 100Kg.
                </p>
            <div class="clear"></div>
        </div>
        
        <hr>
        
        <div class="sixteen columns">
            <div class="headline no-margin"><h3>Galeria</h3></div>
        </div>
        
    	<div class="one-third column"><a href="images/servicos/img1_g.jpg" class="swipebox" rel="swipebox"><img src="images/servicos/img1.jpg"></a></div>
        <div class="one-third column"><a href="images/servicos/img2_g.jpg" class="swipebox" rel="swipebox"><img src="images/servicos/img2.jpg"></a></div>
        <div class="one-third column"><a href="images/servicos/img3_g.jpg" class="swipebox" rel="swipebox"><img src="images/servicos/img3.jpg"></a></div>
    	<div class="clear"></div>
        
        <br><br>
        
        
    </div>
</div>

<?php include("includes/footer.php"); ?>

</body>
</html>