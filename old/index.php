﻿<?php
require_once('Connections/conexao.php');

$query_qBanner = sprintf("SELECT * FROM banners");
$qBanner = mysql_query($query_qBanner, $conexao) or die(mysql_error());
$row_qBanner = mysql_fetch_assoc($qBanner);
$totalRows_qBanner = mysql_num_rows($qBanner);

$query_qProduto = sprintf("SELECT * FROM produtos ORDER BY rand() limit 4");
$qProduto = mysql_query($query_qProduto, $conexao) or die(mysql_error());
$row_qProduto = mysql_fetch_assoc($qProduto);
$totalRows_qProduto = mysql_num_rows($qProduto);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Partnerlab</title>

<!-- CSS -->
<link rel="shortcut icon" type="image/png" href="http://partnerlab.com.br/images/favicon.png" />
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<!-- JS -->
<script src="js/jquery.min.js"></script>
<script src="js/flexslider.js"></script>
<script src="js/custom.js"></script>


</head>
<body>
<?php include("includes/header.php"); ?>

<?php if($totalRows_qBanner > 0){ ?>
<div class="slider">
    <div class="flexslider">
        <ul class="slides">
            <?php do { ?>
            <li><img src="images/banners/<?php echo $row_qBanner['img']; ?>" alt="" />
            	<?php if($row_qBanner['texto'] != ""){ ?>
                <div class="slide-caption">
                	<h3><?php echo $row_qBanner['titulo']; ?></h3>
                    <p><?php echo $row_qBanner['texto']; ?></p>
                    <?php if($row_qBanner['url'] != ""){ ?><a href="<?php echo $row_qBanner['url']; ?>" class="button color medium">Saiba Mais</a><?php } ?>
                </div>
                <?php } ?>
            </li>
            <?php } while ($row_qBanner = mysql_fetch_assoc($qBanner)); ?>
      </ul>
    </div>
</div>
<?php } ?>

<div id="wrapper">
    <div class="container">
        <div>
            <div class="one-third column box">
                <h3>Conheça a Partner<span>lab</span></h3>
                <a href="empresa.php"><img src="images/img-conheca.jpg" width="300"></a>
              	<p>Temos uma estrutura formada para atendimentos realizados tanto em nossa empresa, quanto na planta de nossos clientes.</p>
                <a href="empresa.php" class="button color medium">Saiba Mais</a>
            </div>
    
            <div class="one-third column box">
                <h3>Calibração</h3>
                <a href="calibracao.php"><img src="images/img-calibracao.jpg" width="300"></a>
              	<p>Nosso laboratório possui monitoramento de temperatura e Umidade, visando sempre a melhor condição para realização da calibração.</p>
                <a href="calibracao.php" class="button color medium">Saiba Mais</a>
            </div>
     
          <div class="one-third column box">
            <h3>Entre em Contato</h3>
            <a href="contato.php"><img src="images/img-contato.jpg" width="300"></a>
            <p>Acesse nossa página de contato, envie suas dúvidas ou sugestões, nossa equipe irá atendê-lo.</p>
            <a href="contato.php" class="button color medium">Fale Conosco</a>
          </div>
            
        </div>
        
    </div>

<div class="bg-products">
    <div class="container">
                <div class="sixteen columns">
                    <h3>Produtos em <span>Destaque</span></h3>
                </div>
                <?php if($totalRows_qProduto > 0){ ?>
                <?php do { ?>
                <!-- item -->
                <div class="four columns">
                    <div class="picture"><a href="detalhe-produto.php?id=<?php echo $row_qProduto['id']; ?>"><img src="images/produtos/thumb_<?php echo $row_qProduto['img']; ?>" alt=""/><div class="image-overlay-link"></div></a></div>
                    <div class="item-description">
                        <h5><a href="detalhe-produto.php?id=<?php echo $row_qProduto['id']; ?>"><?php echo $row_qProduto['titulo']; ?></a></h5>
                        <p><?php echo strip_tags(cortaString($row_qProduto['texto'], 80)); ?></p>
                    </div>
                </div>
                <?php } while ($row_qProduto = mysql_fetch_assoc($qProduto)); ?>
				<?php } else { ?>
                 <div class="sixteen columns">
                    <h5>Nenhum produto cadastrado!</h5>
                	</div>
                <?php } ?>
                
      </div>
  </div>
  
 
</div>

<?php include("includes/footer.php"); ?>

</body>
</html>
