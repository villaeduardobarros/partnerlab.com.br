<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$query_qItemMarca = "SELECT * FROM marcas WHERE status=1";
$qItemMarca = mysql_query($query_qItemMarca, $conexao) or die(mysql_error());
$totalRows_qItemMarca = mysql_num_rows($qItemMarca);

$query_qItemModelo = "SELECT * FROM modelos WHERE status=1";
$qItemModelo = mysql_query($query_qItemModelo, $conexao) or die(mysql_error());
$totalRows_qItemModelo = mysql_num_rows($qItemModelo);

if (isset($_GET['cr'])) {
	$id_ctrl_receb = anti_injection($_GET['cr']);

	// equipamento
	$query_qItemEquipamento = "SELECT * FROM equipamentos WHERE status=1";
	$qItemEquipamento = mysql_query($query_qItemEquipamento, $conexao) or die(mysql_error());
	$totalRows_qItemEquipamento = mysql_num_rows($qItemEquipamento);

	//tipo de fotos
	$query_qItemEquipTipo = "SELECT * FROM equipamentos_tipos_fotos WHERE status=1";
	$qItemEquipTipo = mysql_query($query_qItemEquipTipo, $conexao) or die(mysql_error());
	$totalRows_qItemEquipTipo = mysql_num_rows($qItemEquipTipo);

	if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

		/*if ($_POST['enviado_acessorio'] == 1) {

			$acessorios_enviado 	= $_POST['enviado_acessorio'];
			$qtd_acessorio 			= $_POST['quantidade'];
			$nome_acessorio 		= $_POST['acessorio'];

			if ($_FILES['img_acessorio']['size'] > 0 && $_FILES['img_acessorio']['error'] == 0) {
				$foto 				= uploadEquipamentoAcessorio('img_acessorio');
			}

		} else {

			$acessorios_enviado 	= NULL;
			$qtd_acessorio 			= NULL;
			$nome_acessorio 		= NULL;
			$foto 					= NULL;
		
		}

		$insertSQL = sprintf("INSERT INTO ctrl_recebimentos_equip (os, acessorios, acessorios_qtde, acessorios_nome, acessorios_imagem, status, ctrl_recebimentos_id, equipamentos_id, marcas_id, modelos_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
			GetSQLValueString(anti_injection($_POST['os']), "text"),
			GetSQLValueString(anti_injection($acessorios_enviado), "int"),
			GetSQLValueString(anti_injection($qtd_acessorio), "int"),
			GetSQLValueString(anti_injection($nome_acessorio), "text"), 
			GetSQLValueString(anti_injection($foto), "text"), 
			GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : 1), "int"),
			GetSQLValueString(anti_injection($id_ctrl_receb), "int"), 
			GetSQLValueString(anti_injection($_POST['equipamento']), "int"), 
			GetSQLValueString(anti_injection($_POST['marca']), "int"), 
			GetSQLValueString(anti_injection($_POST['modelo']), "int"));*/

		$insertSQL = sprintf("INSERT INTO ctrl_recebimentos_equip (os, status, ctrl_recebimentos_id, equipamentos_id, marcas_id, modelos_id) VALUES (%s, %s, %s, %s, %s, %s)", 
			GetSQLValueString(anti_injection($_POST['os']), "text"),
			GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : 1), "int"),
			GetSQLValueString(anti_injection($id_ctrl_receb), "int"), 
			GetSQLValueString(anti_injection($_POST['equipamento']), "int"), 
			GetSQLValueString(anti_injection($_POST['marca']), "int"), 
			GetSQLValueString(anti_injection($_POST['modelo']), "int"));
		mysql_query($insertSQL, $conexao) or die(mysql_error());
		$last_id = mysql_insert_id();

		// verifica as imagens
		while ($row_qItemEquipTipo = mysql_fetch_assoc($qItemEquipTipo)) {

			if ($_FILES['img'.$row_qItemEquipTipo['id']]['size'] > 0 && $_FILES['img'.$row_qItemEquipTipo['id']]['error'] == 0) {

				$foto = uploadEquipamento('img'.$row_qItemEquipTipo['id'], 'cr'.$id_ctrl_receb.'-eq'.$last_id.'-tp'.$row_qItemEquipTipo['id'].'-os'.$_POST['os']);

				$insertSQLimg = sprintf("INSERT INTO ctrl_recebimentos_equip_fotos (imagem, status, ctrl_recebimentos_equip_id, equipamentos_tipos_fotos_id) VALUES (%s, %s, %s, %s)", 
					GetSQLValueString(anti_injection($foto), "text"), 
					GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : 1), "int"),
					GetSQLValueString(anti_injection($last_id), "int"),
					GetSQLValueString(anti_injection($row_qItemEquipTipo['id']), "int"));
				mysql_query($insertSQLimg, $conexao) or die(mysql_error());

			}
		}

		header("Location: index_equip.php?cr=".$id_ctrl_receb);
		
	}

} else {
	echo '<script>alert("Controle de recebimento não encontrado"); window.location.href="index_equip.php?cr='.$id_ctrl_receb.';</script>';
	return false;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet">
	<link href="../css/pages/calendar.css" rel="stylesheet">
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/Slate.js"></script>
	<script src="../js/plugins/flot/jquery.flot.js"></script>
	<script src="../js/plugins/flot/jquery.flot.orderBars.js"></script>
	<script src="../js/plugins/flot/jquery.flot.pie.js"></script>
	<script src="../js/plugins/flot/jquery.flot.resize.js"></script>
	<script src="../js/mask.js"></script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>Controle de Recebimentos - Equipamentos</h1>
				<ul class="breadcrumb">
					<li><a href="../home/">Home</a> <span class="divider">/</span></li>
					<li><a href="index.php">Controle de Recebimentos</a> <span class="divider">/</span></li>
					<li><a href="index_equip.php?cr=<?php echo $id_ctrl_receb; ?>">Equipamentos</a> <span class="divider">/</span></li>
					<li class="active">Vincular Equipamento</li>
				</ul>
			</div>

			<div class="row">

				<div class="span8">

					<div id="horizontal" class="widget widget-form">

						<div class="widget-header">	      				
							<h3><i class="icon-pencil"></i> Adicionar Equipamento</h3>	
						</div>

						<div class="widget-content">
							<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal" enctype="multipart/form-data">
								<fieldset>

									<div class="control-group">
										<label class="control-label" for="status">Status</label>
										<div class="controls">
											<select name="status" class="input-large" id="status" required>
												<option value="1">Ativo</option>
												<option value="0">Inativo</option>
											</select>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="os">OS</label>
										<div class="controls">
											<input name="os" type="text" class="input-large" id="os" required>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="equipamento">Equipamento</label>
										<div class="controls">
											<select name="equipamento" class="input-large" id="equipamento" required>
												<option value="">Selecione</option>
												<?php if ($totalRows_qItemEquipamento > 0) { ?>

													<?php while ($row_qItemEquipamento = mysql_fetch_assoc($qItemEquipamento)) { ?>

														<option value="<?php echo $row_qItemEquipamento['id']; ?>"><?php echo $row_qItemEquipamento['equipamento']; ?></option>

													<?php } ?>

												<?php } else { ?>
													<option value="">Antes cadastre um equipamento</option>
												<?php } ?>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="marca">Marca</label>
										<div class="controls">
											<select name="marca" class="input-large" id="marca">
												<option value="">Selecione a marca</option>
												<?php if($totalRows_qItemMarca > 0){ ?>
													
													<?php while ($row_qItemMarca = mysql_fetch_assoc($qItemMarca)) { ?>

														<option value="<?php echo $row_qItemMarca['id']; ?>"><?php echo $row_qItemMarca['marca']; ?></option>

													<?php } ?>
												
												<?php } else { ?>
													<option value="">Antes cadastre a marca</option>
												<?php } ?>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="modelo">Modelo</label>
										<div class="controls">
											<select name="modelo" class="input-large" id="modelo">
												<option value="">Selecione o modelo</option>
												<?php if($totalRows_qItemModelo > 0){ ?>
													
													<?php while ($row_qItemModelo = mysql_fetch_assoc($qItemModelo)) { ?>

														<option value="<?php echo $row_qItemModelo['id']; ?>"><?php echo $row_qItemModelo['modelo']; ?></option>

													<?php } ?>
												
												<?php } else { ?>
													<option value="">Antes cadastre o modelo</option>
												<?php } ?>
											</select>
										</div>
									</div>

									<div class="control-group">
										<h3>Imagens</h3>
									</div>

									<?php if ($totalRows_qItemEquipTipo > 0) { ?>

										<?php while ($row_qItemEquipTipo = mysql_fetch_assoc($qItemEquipTipo)) { ?>

											<div class="control-group">
												<label class="control-label" for="img<?php echo $row_qItemEquipTipo['id']; ?>"><?php echo $row_qItemEquipTipo['tipo']; ?> (800x600)<br> *Somente .jpg</label>
												<div class="controls">
													<input name="img<?php echo $row_qItemEquipTipo['id']; ?>" type="file" class="input-file" id="img<?php echo $row_qItemEquipTipo['id']; ?>">
												</div>
											</div>

										<?php } ?>

									<?php } ?>

									<!--<div class="control-group">
										<h3>Acessórios?</h3>
									</div>

									<div class="control-group">
										<label class="control-label" for="enviado_acessorio">Enviado com acessório?</label>
										<div class="controls">
											<select name="enviado_acessorio" class="input-large" id="enviado_acessorio" required>
												<option value="0">Não</option>
												<option value="1">Sim</option>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="acessorio">Acessório</label>
										<div class="controls">
											<input name="acessorio" type="text" class="input-large" id="acessorio" >
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="quantidade">Quantidade</label>
										<div class="controls">
											<input name="quantidade" type="text" class="input-large" id="quantidade" >
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="img_acessorio">Imagem (600x600) *Somente .jpg</label>
										<div class="controls">
											<input name="img_acessorio" type="file" class="input-file" id="img_acessorio">
										</div>
									</div>-->

									<div class="form-actions">
										<button type="submit" class="btn btn-primary btn-large">Cadastrar</button>
										<input type="hidden" name="MM_insert" value="form1">
									</div>
								</fieldset>
							</form>	

						</div> <!-- /widget-content -->

					</div>	
					<!-- /.widget -->

				</div> <!-- /span8 -->

				<div class="span4">

					<div id="formToc" class="widget highlight">

						<div class="widget-header">
							<h3>Informações</h3>		    			
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<p>Cadastre ao lado as informações solicitadas.</p>
						</div> <!-- /widget-content -->

					</div> <!-- /widget -->

				</div> <!-- /.span4 -->

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>
