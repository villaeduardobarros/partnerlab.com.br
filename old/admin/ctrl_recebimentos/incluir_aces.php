<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if (isset($_GET['cr']) && isset($_GET['eq'])) {
	
	$id_ctrl_receb = anti_injection($_GET['cr']);
	$id_equip = anti_injection($_GET['eq']);

	if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

		$nome_acessorio 	= $_POST['acessorio'];
		$qtd_acessorio 		= $_POST['quantidade'];

		$insertSQL = sprintf("INSERT INTO ctrl_recebimentos_aces (acessorio, quantidade, equipamentos_id) VALUES (%s, %s, %s)", 
			GetSQLValueString(anti_injection($nome_acessorio), "text"), 
			GetSQLValueString(anti_injection($qtd_acessorio), "int"),
			GetSQLValueString(anti_injection($id_equip), "int"));
		mysql_query($insertSQL, $conexao) or die(mysql_error());
		$last_id = mysql_insert_id();

		if ($_FILES['img_acessorio']['size'] > 0 && $_FILES['img_acessorio']['error'] == 0) {
			
			$foto = uploadEquipamentoAcessorio('img_acessorio', 'cr'.$id_ctrl_receb.'-eq'.$id_equip.'-ac'.$last_id.'-'.$nome_acessorio);

			$updateSQL = sprintf("UPDATE ctrl_recebimentos_aces SET imagem=%s WHERE id=%s", 
				GetSQLValueString(anti_injection($foto), "text"),
				GetSQLValueString(anti_injection($last_id), "int"));
			mysql_query($updateSQL, $conexao) or die(mysql_error());

		}

		header("Location: index_aces.php?cr=".$id_ctrl_receb."&eq=".$id_equip);
		
	}

} else {
	echo '<script>alert("Equipamento não encontrado"); window.location.href="index_equip.php?cr='.$id_ctrl_receb.';</script>';
	return false;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet">
	<link href="../css/pages/calendar.css" rel="stylesheet">
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/Slate.js"></script>
	<script src="../js/plugins/flot/jquery.flot.js"></script>
	<script src="../js/plugins/flot/jquery.flot.orderBars.js"></script>
	<script src="../js/plugins/flot/jquery.flot.pie.js"></script>
	<script src="../js/plugins/flot/jquery.flot.resize.js"></script>
	<script src="../js/mask.js"></script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>Controle de Recebimentos - Equipamentos - Acessórios</h1>
				<ul class="breadcrumb">
					<li><a href="../home/">Home</a> <span class="divider">/</span></li>
					<li><a href="index.php">Controle de Recebimentos</a> <span class="divider">/</span></li>
					<li><a href="index_equip.php?cr=<?php echo $id_ctrl_receb; ?>">Equipamentos</a> <span class="divider">/</span></li>
					<li class="active">Vincular Acessórios</li>
				</ul>
			</div>

			<div class="row">

				<div class="span8">

					<div id="horizontal" class="widget widget-form">

						<div class="widget-header">	      				
							<h3><i class="icon-pencil"></i> Adicionar Acessório</h3>	
						</div>

						<div class="widget-content">
							<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal" enctype="multipart/form-data">
								<fieldset>

									<div class="control-group">
										<label class="control-label" for="acessorio">Acessório</label>
										<div class="controls">
											<input name="acessorio" type="text" class="input-large" id="acessorio" required>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="quantidade">Quantidade</label>
										<div class="controls">
											<input name="quantidade" type="text" class="input-large" id="quantidade" required>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="img_acessorio">Imagem (800x600) *Somente .jpg</label>
										<div class="controls">
											<input name="img_acessorio" type="file" class="input-file" id="img_acessorio" required>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary btn-large">Cadastrar</button>
										<input type="hidden" name="MM_insert" value="form1">
									</div>
								</fieldset>
							</form>	

						</div> <!-- /widget-content -->

					</div>	
					<!-- /.widget -->

				</div> <!-- /span8 -->

				<div class="span4">

					<div id="formToc" class="widget highlight">

						<div class="widget-header">
							<h3>Informações</h3>		    			
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<p>Cadastre ao lado as informações solicitadas.</p>
						</div> <!-- /widget-content -->

					</div> <!-- /widget -->

				</div> <!-- /.span4 -->

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>
