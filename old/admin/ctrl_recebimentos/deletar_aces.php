<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

if (isset($_GET['cr']) && isset($_GET['eq']) && isset($_GET['ac'])) {

	$id_ctrl_receb 	= anti_injection($_GET['cr']);
	$id_equip 		= anti_injection($_GET['eq']);
	$id_aces 		= anti_injection($_GET['ac']);

	// fotos equipamento
	$query_qAces = sprintf("SELECT * FROM ctrl_recebimentos_aces WHERE id=%s", 
		GetSQLValueString($id_aces, "int"));
	$qAces = mysql_query($query_qAces, $conexao) or die(mysql_error());
	$totalRows_qAces = mysql_num_rows($qAces);

	if ($totalRows_qAces > 0) {

		$row_qSelect = mysql_fetch_assoc($qAces);

		if (file_exists('../img/acessorios/'.$row_qSelect['imagem'])) {  

			unlink('../img/acessorios/'.$row_qSelect['imagem']);
			unlink('../img/acessorios/thumb_'.$row_qSelect['imagem']);

		}

	}

	$query_qDelete = sprintf("DELETE FROM ctrl_recebimentos_aces WHERE id=%s", 
		GetSQLValueString(anti_injection($id_aces), "int"));
	$qDelete = mysql_query($query_qDelete, $conexao) or die(mysql_error());
	
	if (mysql_affected_rows() > 0) {

		header("Location: index_aces.php?cr=".$id_ctrl_receb.'&eq='.$id_equip);

	}

}
?>