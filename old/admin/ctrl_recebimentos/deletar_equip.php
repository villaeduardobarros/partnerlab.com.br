<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

if (isset($_GET['cr']) && isset($_GET['eq'])) {

	$id_ctrl_receb 	= anti_injection($_GET['cr']);
	$id_equip 		= anti_injection($_GET['eq']);

	// fotos equipamento
	$query_qEquipFotos = sprintf("SELECT * FROM ctrl_recebimentos_equip_fotos WHERE ctrl_recebimentos_equip_id = %s", 
		GetSQLValueString($id_equip, "int"));
	$qEquipFotos = mysql_query($query_qEquipFotos, $conexao) or die(mysql_error());
	$totalRows_qEquipFotos = mysql_num_rows($qEquipFotos);

	if ($totalRows_qEquipFotos > 0) {

		while ($row_qEquipFotos = mysql_fetch_assoc($qEquipFotos)) {

			if (file_exists('../img/equipamentos/'.$row_qEquipFotos['imagem'])) {  

				unlink('../img/equipamentos/'.$row_qEquipFotos['imagem']);
				unlink('../img/equipamentos/thumb_'.$row_qEquipFotos['imagem']);

			}

		}

		$query_qSelect = sprintf("SELECT * FROM ctrl_recebimentos_aces WHERE equipamentos_id=%s", 
			GetSQLValueString(anti_injection($id_equip), "int"));
		$qSelect = mysql_query($query_qSelect, $conexao) or die(mysql_error());
		$totalRows_qSelect = mysql_num_rows($qSelect);

		if ($totalRows_qSelect > 0) {

			while ($row_qSelect = mysql_fetch_assoc($qSelect)) {

				if (file_exists('../img/acessorios/'.$row_qSelect['imagem'])) {  

					unlink('../img/acessorios/'.$row_qSelect['imagem']);
					unlink('../img/acessorios/thumb_'.$row_qSelect['imagem']);

				}

				$query_qDeleteSelect = sprintf("DELETE FROM ctrl_recebimentos_aces WHERE id=%s", 
					GetSQLValueString(anti_injection($row_qSelect['id']), "int"));
				mysql_query($query_qDeleteSelect, $conexao) or die(mysql_error());

			}

		}

	}

	$query_qDelete = sprintf("DELETE FROM ctrl_recebimentos_equip WHERE id=%s AND ctrl_recebimentos_id=%s", 
		GetSQLValueString(anti_injection($id_equip), "int"),
		GetSQLValueString(anti_injection($id_ctrl_receb), "int"));
	mysql_query($query_qDelete, $conexao) or die(mysql_error());
	
	if (mysql_affected_rows() > 0) {

		header("Location: index_equip.php?cr=".$id_ctrl_receb);

	}

}
?>