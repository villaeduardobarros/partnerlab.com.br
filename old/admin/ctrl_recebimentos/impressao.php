<?php require_once('../../Connections/conexao.php'); ?>
<?php require_once('../includes/login.php');?>
<?php
if (isset($_GET['cr'])) {
	$id_ctrl = anti_injection($_GET['cr']);

	$query_qItemCtrl = sprintf("SELECT * FROM ctrl_recebimentos WHERE id = %s", GetSQLValueString($id_ctrl, "int"));
	$qItemCtrl = mysql_query($query_qItemCtrl, $conexao) or die(mysql_error());
	$row_qItemCtrl = mysql_fetch_assoc($qItemCtrl);
	$totalRows_qItemCtrl = mysql_num_rows($qItemCtrl);

	if ($totalRows_qItemCtrl == 0) {
		echo '<script>alert("Nenhum controle de recebimento n\u00e3o encontrado"); window.location.href="index.php";</script>';
		return false;
	}

	// usuario
	$query_qItemUsuario = "SELECT * FROM usuarios WHERE id_usuario = ".$row_qItemCtrl['usuarios_id'];
	$qItemUsuario = mysql_query($query_qItemUsuario, $conexao) or die(mysql_error());
	$row_qItemUsuario = mysql_fetch_assoc($qItemUsuario);

	// cliente
	$query_qIteCliente = "SELECT * FROM clientes WHERE id = ".$row_qItemCtrl['clientes_id'];
	$qIteCliente = mysql_query($query_qIteCliente, $conexao) or die(mysql_error());
	$row_qIteCliente = mysql_fetch_assoc($qIteCliente);

	// embalagem
	$query_qItemEmbalagem = "SELECT * FROM embalagens WHERE status=1";
	$qItemEmbalagem = mysql_query($query_qItemEmbalagem, $conexao) or die(mysql_error());
	$totalRows_qItemEmbalagem = mysql_num_rows($qItemEmbalagem);

	// equipamentos vinculados
	$query_qItemCtrlEquip = sprintf("SELECT * FROM ctrl_recebimentos_equip WHERE ctrl_recebimentos_id = %s", GetSQLValueString($id_ctrl, "int"));
	$qItemCtrlEquip = mysql_query($query_qItemCtrlEquip, $conexao) or die(mysql_error());
	$totalRows_qItemCtrlEquip = mysql_num_rows($qItemCtrlEquip);

} else {
	echo '<script>alert("Nenhum controle de recebimento n\u00e3o encontrado"); window.location.href="index.php";</script>';
	return false;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Controle de Recebimento de Equipamentos - <?php echo $id_ctrl; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="../css/print.css" rel="stylesheet">
<style type="text/css">
body{font-family:arial !important;font-size:12px;}
label{cursor:pointer;}
input[type=checkbox]{display:none;}
input[type="checkbox"] + label:before{border:1px solid #000000;content:"\00a0";display:inline-block;height:14px;margin:0 .25em 0 0;padding:0;vertical-align:top;width:14px;border-radius:4px;}
input[type="checkbox"]:checked + label:before{color:#000000;content:"\00D7";font-size:12px;text-align:center;font-weight:bold;}
input[type="checkbox"]:checked + label:after{font-weight:bold;}
</style>
</head>
<body>

	<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" rowspan="3" width="17%">
				<img src="../../images/logo.png" width="90%">
			</td>
			<td align="center" rowspan="3" width="66%">
				<h4>CONTROLE DE RECEBIMENTO DE EQUIPAMENTOS</h4>
			</td>
			<td align="center" width="17%"><strong>CRE Nº.:<br>17272019-093</strong></td>
		</tr>
		<tr>
			<td align="center"><strong>FR.REC.001</strong></td>
		</tr>
		<tr>
			<td align="center"><strong>REV.: 00</strong></td>
		</tr>
	</table>

	<br>

	<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2"><strong>Cliente:</strong> <?php echo $row_qIteCliente['nome']; ?></td>
		</tr>
		<tr>
			<td width="50%"><strong>Data Recebimento:</strong> <?php echo date('d/m/Y', strtotime($row_qItemCtrl['datetime'])); ?></td>
			<td width="50%"><strong>Resp. pelo preenchimento:</strong> <?php echo $row_qItemUsuario['nome_usuario']; ?></td>
		</tr>
		<tr>
			<td width="50%"><strong>Qtde. de volumes:</strong> <?php echo $row_qItemCtrl['volumes']; ?></td>
			<td width="50%"><strong>Nº da DANFE:</strong> <?php echo $row_qItemCtrl['nfe']; ?></td>
		</tr>
	</table>

	<br>

	<strong>Equipamentos Recebidos:</strong>
	<table border="1" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<div class="checkbox">
					<strong>Alguma avaria?&nbsp;&nbsp;</strong>
					<input type="checkbox" <?php echo ($row_qItemCtrl['avaria'] == 1) ? 'checked' : NULL; ?> />
					<label for="campo-checkbox1">Sim</label>
					&nbsp;&nbsp;&nbsp;
					<input type="checkbox" <?php echo ($row_qItemCtrl['avaria'] == NULL) ? 'checked' : NULL; ?> />
					<label for="campo-checkbox1">Não</label>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="checkbox">
					<strong>Embalagem:&nbsp;&nbsp;</strong>
					<?php if ($totalRows_qItemEmbalagem > 0) { ?>

						<?php while ($row_qItemEmbalagem = mysql_fetch_assoc($qItemEmbalagem)) { ?>

							<input type="checkbox" <?php echo ($row_qItemEmbalagem['id'] == $row_qItemCtrl['embalagens_id']) ? 'checked' : NULL; ?> />
							<label for="campo-checkbox1"><?php echo $row_qItemEmbalagem['embalagem']; ?></label>&nbsp;&nbsp;&nbsp;

						<?php } ?>

					<?php } ?>
				</div>
			</td>
		</tr>
		<tr>
			<td><strong>Transporte:</strong> <?php echo $row_qItemCtrl['transporte']; ?></td>
		</tr>
		<?php if ($row_qItemCtrl['observacoes']) { ?>
			<tr>
				<td>
					<strong>Observações:</strong> <?php echo nl2br($row_qItemCtrl['observacoes']); ?>
				</td>
			</tr>
		<?php } ?>
	</table>

	<br>

	<?php if ($totalRows_qItemCtrlEquip > 0) { ?>

		<?php while ($row_qItemCtrlEquip = mysql_fetch_assoc($qItemCtrlEquip)) { ?>

			<?php
				// equipamento
				$query_qEquip = sprintf("SELECT * FROM equipamentos WHERE id = %s", GetSQLValueString($row_qItemCtrlEquip['equipamentos_id'], "int"));
				$qEquip = mysql_query($query_qEquip, $conexao) or die(mysql_error());
				$row_qEquip = mysql_fetch_assoc($qEquip);

				// modelos
				$query_qModelo = sprintf("SELECT * FROM modelos WHERE id = %s", GetSQLValueString($row_qItemCtrlEquip['modelos_id'], "int"));
				$qModelo = mysql_query($query_qModelo, $conexao) or die(mysql_error());
				$row_qModelo = mysql_fetch_assoc($qModelo);

				// marcas
				$query_qMarca = sprintf("SELECT * FROM marcas WHERE id = %s", GetSQLValueString($row_qItemCtrlEquip['marcas_id'], "int"));
				$qMarca = mysql_query($query_qMarca, $conexao) or die(mysql_error());
				$row_qMarca = mysql_fetch_assoc($qMarca);

				// marcas
				$query_qAcessorios = sprintf("SELECT * FROM ctrl_recebimentos_aces WHERE equipamentos_id = %s", GetSQLValueString($row_qItemCtrlEquip['id'], "int"));
				$qAcessorios = mysql_query($query_qAcessorios, $conexao) or die(mysql_error());
				$totalRows_qAcessorios = mysql_num_rows($qAcessorios);
			?>

			<table border="1" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td align="center" width="25%"><strong>Equipamento</strong></td>
					<td align="center" width="25%"><strong>Marca</strong></td>
					<td align="center" width="25%"><strong>Modelo</strong></td>
					<td align="center" width="25%"><strong>OS</strong></td>
				</tr>
				<tr>
					<td align="center"><?php echo $row_qEquip['equipamento']; ?></td>
					<td align="center"><?php echo $row_qMarca['marca']; ?></td>
					<td align="center"><?php echo $row_qModelo['modelo']; ?></td>
					<td align="center"><?php echo $row_qItemCtrlEquip['os']; ?></td>
				</tr>

				<?php
					// fotos equipamento
					$query_qEquipFotos = sprintf("SELECT * FROM ctrl_recebimentos_equip_fotos WHERE ctrl_recebimentos_equip_id = %s", GetSQLValueString($row_qItemCtrlEquip['id'], "int"));
					$qEquipFotos = mysql_query($query_qEquipFotos, $conexao) or die(mysql_error());
					$totalRows_qEquipFotos = mysql_num_rows($qEquipFotos);

					if ($totalRows_qEquipFotos > 0) {

						$count = 1;
				?>
						<tr>
						
							<?php while ($row_qEquipFotos = mysql_fetch_assoc($qEquipFotos)) { ?>

								<?php
									// tipo equipamento
									$query_qEquipTipo = sprintf("SELECT * FROM equipamentos_tipos_fotos WHERE id = %s", GetSQLValueString($row_qEquipFotos['equipamentos_tipos_fotos_id'], "int"));
									$qEquipTipo = mysql_query($query_qEquipTipo, $conexao) or die(mysql_error());
									$row_qEquipTipo = mysql_fetch_assoc($qEquipTipo);
								?>

								<!--<td align="center" style="position:relative !important;">
									<div style="position:absolute !important;text-align:center;top:4px;left:0;right:0;height:0;"><i><strong><?php //echo $row_qEquipTipo['tipo']; ?></strong></i></div>
									<img src="../img/equipamentos/<?php //echo $row_qEquipFotos['imagem']; ?>" width="100%">
								</td>-->

								<td align="center" style="vertical-align:sub;">
									<div><i><strong><?php echo $row_qEquipTipo['tipo']; ?></strong></i></div>
									<img src="../img/equipamentos/<?php echo $row_qEquipFotos['imagem']; ?>" width="98%">
								</td>

								<?php echo ($count % 4 == 0) ? '</tr><tr>' : NULL; ?>

							<?php } ?>

						</tr>

				<?php } ?>

			</table>

			<br>

			<div class="checkbox">
				<strong>Equipamento enviado com acessório?&nbsp;&nbsp;</strong>
				<input type="checkbox" <?php echo ($totalRows_qAcessorios > 0) ? 'checked' : NULL; ?> />
				<label for="campo-checkbox1">Sim</label>
				&nbsp;&nbsp;&nbsp;
				<input type="checkbox" <?php echo ($totalRows_qAcessorios == 0) ? 'checked' : NULL; ?> />
				<label for="campo-checkbox1">Não</label>
			</div>

			<?php if ($totalRows_qAcessorios > 0) { ?>

				<br>

				<table border="1" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" width="30%"><strong>Quantidade</strong></td>
						<td align="center" width="40%"><strong>Acessório</strong></td>
						<td align="center" width="30%"><strong>Imagem</strong></td>
					</tr>

					<?php while ($row_qAcessorios = mysql_fetch_assoc($qAcessorios)) { ?>
					
						<tr>
							<td align="center"><?php echo $row_qAcessorios['quantidade']; ?></td>
							<td align="center"><?php echo $row_qAcessorios['acessorio']; ?></td>
							<td align="center"><?php echo ($row_qAcessorios['imagem']) ? '<img src="../img/acessorios/'.$row_qAcessorios['imagem'].'" width="30%">' : NULL; ?></td>
						</tr>

					<?php } ?>
				</table>

			<?php } ?>

			<br><br>

		<?php } ?>	

	<?php } ?>

	<script>print();</script>

</body>
</html>
