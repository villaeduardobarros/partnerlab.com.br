<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

if (isset($_GET['cr'])) {

	$id_ctrl_receb  = anti_injection($_GET['cr']);

	$query_qSelect = sprintf("SELECT * FROM ctrl_recebimentos_equip WHERE ctrl_recebimentos_id=%s", 
		GetSQLValueString(anti_injection($id_ctrl_receb), "int"));
	$qSelect = mysql_query($query_qSelect, $conexao) or die(mysql_error());
	$totalRows_qSelect = mysql_num_rows($qSelect);

	if ($totalRows_qSelect > 0) {

		while ($row_qSelect = mysql_fetch_assoc($qSelect)) {

			// apaga os acessorios
			$query_qSelectAces = sprintf("SELECT * FROM ctrl_recebimentos_aces WHERE equipamentos_id=%s", 
				GetSQLValueString(anti_injection($row_qSelect['id']), "int"));
			$qSelectAces = mysql_query($query_qSelectAces, $conexao) or die(mysql_error());
			$totalRows_qSelectAces = mysql_num_rows($qSelectAces);

			if ($totalRows_qSelectAces > 0) {

				while ($row_qSelectAces = mysql_fetch_assoc($qSelectAces)) {

					if (file_exists('../img/acessorios/'.$row_qSelectAces['imagem'])) {  

						unlink('../img/acessorios/'.$row_qSelectAces['imagem']);
						unlink('../img/acessorios/thumb_'.$row_qSelectAces['imagem']);

					}

					$query_qDeleteSelectAces = sprintf("DELETE FROM ctrl_recebimentos_equip WHERE id=%s", 
						GetSQLValueString(anti_injection($row_qSelectAces['id']), "int"));
					mysql_query($query_qDeleteSelectAces, $conexao) or die(mysql_error());

				}

			}

			// apaga as imagens dos quipamentos
			$query_qEquipFotos = sprintf("SELECT * FROM ctrl_recebimentos_equip_fotos WHERE ctrl_recebimentos_equip_id = %s", 
				GetSQLValueString($row_qSelect['id'], "int"));
			$qEquipFotos = mysql_query($query_qEquipFotos, $conexao) or die(mysql_error());
			$totalRows_qEquipFotos = mysql_num_rows($qEquipFotos);

			if ($totalRows_qEquipFotos > 0) {

				while ($row_qEquipFotos = mysql_fetch_assoc($qEquipFotos)) {

					if (file_exists('../img/equipamentos/'.$row_qEquipFotos['imagem'])) {  

						unlink('../img/equipamentos/'.$row_qEquipFotos['imagem']);
						unlink('../img/equipamentos/thumb_'.$row_qEquipFotos['imagem']);

					}

					$query_qDelete = sprintf("DELETE FROM ctrl_recebimentos_equip_fotos WHERE id=%s", 
						GetSQLValueString(anti_injection($row_qEquipFotos['id']), "int"));
					$qDelete = mysql_query($query_qDelete, $conexao) or die(mysql_error());

				}

			}

			// apaga os equipamentos vinculados
			$query_qDelete = sprintf("DELETE FROM ctrl_recebimentos_equip WHERE id=%s", 
				GetSQLValueString(anti_injection($row_qSelect['id']), "int"));
			$qDelete = mysql_query($query_qDelete, $conexao) or die(mysql_error());

		}

	}

	// apaga o controle
	$query_qDeleteCtrl = sprintf("DELETE FROM ctrl_recebimentos WHERE id=%s", 
		GetSQLValueString(anti_injection($id_ctrl_receb), "int"));
	mysql_query($query_qDeleteCtrl, $conexao) or die(mysql_error());

	if (mysql_affected_rows() > 0) {

		header("Location: index.php");

	}

}
?>