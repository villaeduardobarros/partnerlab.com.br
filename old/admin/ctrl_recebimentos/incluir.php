<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$query_qItemCliente = "SELECT * FROM clientes WHERE status=1";
$qItemCliente = mysql_query($query_qItemCliente, $conexao) or die(mysql_error());
$totalRows_qItemCliente = mysql_num_rows($qItemCliente);

$query_qItemEmbalagem = "SELECT * FROM embalagens WHERE status=1";
$qItemEmbalagem = mysql_query($query_qItemEmbalagem, $conexao) or die(mysql_error());
$totalRows_qItemEmbalagem = mysql_num_rows($qItemEmbalagem);

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {	

	$insertSQL = sprintf("INSERT INTO ctrl_recebimentos (data, volumes, nfe, avaria, transporte, observacoes, status, usuarios_id, clientes_id, embalagens_id) 
		VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
			GetSQLValueString(anti_injection(implode('-', array_reverse(explode('/', $_POST['data'])))), "date"),
			GetSQLValueString(anti_injection($_POST['volume']), "text"),
			GetSQLValueString(anti_injection($_POST['nfe']), "text"),
			GetSQLValueString(anti_injection(($_POST['avaria'] == 0) ? NULL : $_POST['avaria']), "int"),
			GetSQLValueString(anti_injection($_POST['transporte']), "text"),
			GetSQLValueString(anti_injection($_POST['observacao']), "text"),
			GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : $_POST['status']), "int"),
			GetSQLValueString(anti_injection($_SESSION['MM_idUser']), "int"),
			GetSQLValueString(anti_injection($_POST['cliente']), "int"),
			GetSQLValueString(anti_injection($_POST['embalagem']), "int"));
	$Result1 = mysql_query($insertSQL, $conexao) or die(mysql_error());
	
	if (mysql_insert_id() > 0) {
		header("Location: index.php");
		exit;
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet">
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/plugins/validate/jquery.validate.js"></script>
	<script src="../js/Slate.js"></script>
	<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		tinymce.init({
			menubar : false,
			plugins: [
				"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"save table contextmenu directionality emoticons template paste textcolor"
			],
			content_css: "css/content.css",
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor",
			target_list: [
				{title: 'Selecione', value: ''},
				{title: 'Mesma Página', value: '_self'},
				{title: 'Nova Página', value: '_blank'}
			],
			selector: "textarea"
		});
	</script>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>Controle de Recebimentos</h1>
				<ul class="breadcrumb">
					<li><a href="../home/">Home</a> <span class="divider">/</span></li>
					<li><a href="index.php">Recebimentos</a> <span class="divider">/</span></li>
					<li class="active">Cadastrar</li>
				</ul>
			</div>
			<!-- /.page-title -->


			<div class="row">


				<div class="span8">

					<div id="horizontal" class="widget widget-form">

						<div class="widget-header">	      				
							<h3>
								<i class="icon-pencil"></i>
								Adicionar Modelo				
							</h3>	
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal">
								<fieldset>

									<div class="control-group">
										<label class="control-label" for="status">Status</label>
										<div class="controls">
											<select name="status" class="input-large" id="status" required>
												<option value="1">Ativo</option>
												<option value="0">Inativo</option>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="cliente">Clientes</label>
										<div class="controls">
											<select name="cliente" class="input-large" id="cliente" required>
												<option value="">Selecione</option>
												<?php if ($totalRows_qItemCliente > 0) { ?>

													<?php while ($row_qItemCliente = mysql_fetch_assoc($qItemCliente)) { ?>

														<option value="<?php echo $row_qItemCliente['id']; ?>"><?php echo $row_qItemCliente['nome']; ?></option>

													<?php } ?>

												<?php } else { ?>
													<option value="">Antes cadastre um cliente</option>
												<?php } ?>
											</select>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="data">Data de Recebimento</label>
										<div class="controls">
											<input name="data" type="text" class="input-large" id="data" value="<?php echo date('d/m/Y'); ?>" required>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="volume">Volumes</label>
										<div class="controls">
											<input name="volume" type="text" class="input-large" id="volume" required>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="nfe">NFe</label>
										<div class="controls">
											<input name="nfe" type="text" class="input-large" id="nfe" required>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="avaria">Alguma avaria?</label>
										<div class="controls">
											<select name="avaria" class="input-large" id="avaria" required>
												<option>Selecione</option>
												<option value="0">Não</1option>
												<option value="">Sim</option>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="observacao">Observações</label>
										<div class="controls">
											<textarea name="observacao" rows="6" class="input-large" id="observacao" style="width:95% !important; height:360px;"></textarea> 
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="embalagem">Embalagem</label>
										<div class="controls">
											<select name="embalagem" class="input-large" id="embalagem" required>
												<option value="">Selecione</option>
												<?php if ($totalRows_qItemEmbalagem > 0) { ?>

													<?php while ($row_qItemEmbalagem = mysql_fetch_assoc($qItemEmbalagem)) { ?>

														<option value="<?php echo $row_qItemEmbalagem['id']; ?>"><?php echo $row_qItemEmbalagem['embalagem']; ?></option>

													<?php } ?>

												<?php } else { ?>
													<option value="">Antes cadastre uma embalagem</option>
												<?php } ?>
											</select>
										</div>
									</div>     

									<div class="control-group">
										<label class="control-label" for="transporte">Transporte</label>
										<div class="controls">
											<input name="transporte" type="text" class="input-large" id="transporte" required>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary btn-large">Cadastrar</button>
										<input type="hidden" name="MM_insert" value="form1">
									</div>
								</fieldset>
							</form>	

						</div> <!-- /widget-content -->

					</div>	
					<!-- /.widget -->

				</div> <!-- /span8 -->

				<div class="span4">

					<div id="formToc" class="widget highlight">

						<div class="widget-header">
							<h3>Informações</h3>		    			
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<p>Preencha ao lado as informações.</p>

						</div> <!-- /widget-content -->

					</div> <!-- /widget -->

				</div> <!-- /.span4 -->

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>