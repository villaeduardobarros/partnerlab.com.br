<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

if (isset($_GET['cr'])) {
	$id_ctrl_receb = anti_injection($_GET['cr']);
}

// vincular equipamentos
$query_qItem = sprintf("SELECT * FROM ctrl_recebimentos_equip WHERE ctrl_recebimentos_id = %s", GetSQLValueString($id_ctrl_receb, "int"));
$qItem = mysql_query($query_qItem, $conexao) or die(mysql_error());
$row_qItem = mysql_fetch_assoc($qItem);
$totalRows_qItem = mysql_num_rows($qItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../js/plugins/datatables/DT_bootstrap.css" rel="stylesheet">
	<link href="../js/plugins/responsive-tables/responsive-tables.css" rel="stylesheet"> 
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet"> 
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/plugins/datatables/jquery.dataTables.js"></script>
	<script src="../js/plugins/datatables/DT_bootstrap.js"></script>
	<script src="../js/plugins/responsive-tables/responsive-tables.js"></script>
	<script src="../js/Slate.js"></script>
	<script src="../js/demos/demo.tables.js"></script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>
					Controle de Recebimentos 
					<i class="icon-chevron-right" style="padding:11px 8px 0;"></i> Equipamentos
				</h1>
				<ul class="breadcrumb">
					<li> <a href="../home/">Home</a> <span class="divider">/</span> </li>
					<li> <a href="../ctrl_recebimentos/">Controle de Recebimentos</a> <span class="divider">/</span> </li>
					<li class="active">Equipamentos</li>
				</ul>
			</div>
			<!-- /.page-title -->

			<div class="row">

				<div class="span12">
					<div class="widget widget-table">	
						<div class="widget-header">						
							<h3><i class="icon-th-list"></i>Lista dos Equipamentos</h3>
							<a href="incluir_equip.php?cr=<?php echo $id_ctrl_receb; ?>" class="btn btn-warning" style="float:right;color:#000;margin:6px 12px;">Cadastrar Equipamento</a>
						</div> <!-- /widget-header -->

						<div class="widget-content">

							<table class="table table-striped table-bordered table-highlight" id="example">
								<thead>
									<tr>
										<th width="19%">Equipamento</th>
										<th width="19%">Marca</th>
										<th width="19%">Modelo</th>
										<th width="12%">OS</th>
										<th width="12%">Status</th>
										<th width="19%">Ações</th>
									</tr>
								</thead>
								<tbody>
									<?php if($totalRows_qItem > 0){ ?>
										
										<?php do {

											// equipamento
											$query_qEquip = sprintf("SELECT * FROM equipamentos WHERE id = %s", GetSQLValueString($row_qItem['equipamentos_id'], "int"));
											$qEquip = mysql_query($query_qEquip, $conexao) or die(mysql_error());
											$row_qEquip = mysql_fetch_assoc($qEquip);

											// modelos
											$query_qModelo = sprintf("SELECT * FROM modelos WHERE id = %s", GetSQLValueString($row_qItem['modelos_id'], "int"));
											$qModelo = mysql_query($query_qModelo, $conexao) or die(mysql_error());
											$row_qModelo = mysql_fetch_assoc($qModelo);

											// marcas
											$query_qMarca = sprintf("SELECT * FROM marcas WHERE id = %s", GetSQLValueString($row_qItem['marcas_id'], "int"));
											$qMarca = mysql_query($query_qMarca, $conexao) or die(mysql_error());
											$row_qMarca = mysql_fetch_assoc($qMarca);

										?>

											<tr class="odd gradeX">
												<td><?php echo $row_qEquip['equipamento']; ?></td>
												<td><?php echo $row_qModelo['modelo']; ?></td>
												<td><?php echo $row_qMarca['marca']; ?></td>
												<td><?php echo $row_qItem['os']; ?></td>
												<td><?php echo ($row_qItem['status'] == 1) ? 'Ativo' : 'Inativo'; ?></td>
												<td>
													<a href="index_aces.php?cr=<?php echo $id_ctrl_receb; ?>&eq=<?php echo $row_qItem['id']; ?>" class="icon-plus"> Acessórios</a> 
													&nbsp;&nbsp;&nbsp; 
													<?php if (!in_array($_SESSION['MM_UserTipo'], $tipoTravado)) { ?>
														<a href="editar_equip.php?cr=<?php echo $id_ctrl_receb; ?>&eq=<?php echo $row_qItem['id']; ?>" class="icon-book"> Editar</a> 
														&nbsp;&nbsp;&nbsp; 
													<?php } ?>
													<a href="deletar_equip.php?cr=<?php echo $id_ctrl_receb; ?>&eq=<?php echo $row_qItem['id']; ?>" onClick="return confirm('Tem certeza que deseja excluir?');" class="icon-remove"> Excluir</a>
												</td>
											</tr>

										<?php } while ($row_qItem = mysql_fetch_assoc($qItem)); ?>

									<?php } ?>	
								</tbody>
							</table>

						</div> <!-- /widget-content -->
					</div> <!-- /widget -->	
				</div>

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>