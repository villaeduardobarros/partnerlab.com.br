<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

// usuario travado a edicao
if (in_array($_SESSION['MM_UserTipo'], $tipoTravado)) {
	echo "<script>alert('Usuário sem permissão!');location='index.php';</script>";
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$query_qItemCliente = "SELECT * FROM clientes WHERE status=1";
$qItemCliente = mysql_query($query_qItemCliente, $conexao) or die(mysql_error());
$totalRows_qItemCliente = mysql_num_rows($qItemCliente);

$query_qItemEmbalagem = "SELECT * FROM embalagens WHERE status=1";
$qItemEmbalagem = mysql_query($query_qItemEmbalagem, $conexao) or die(mysql_error());
$totalRows_qItemEmbalagem = mysql_num_rows($qItemEmbalagem);

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {

	$updateSQL = sprintf("UPDATE ctrl_recebimentos SET data=%s, volumes=%s, nfe=%s, avaria=%s, transporte=%s, observacoes=%s, status=%s, usuarios_id=%s, clientes_id=%s, embalagens_id=%s  WHERE id=%s",
		GetSQLValueString(anti_injection(implode('-', array_reverse(explode('/', $_POST['data'])))), "date"),
		GetSQLValueString(anti_injection($_POST['volume']), "text"),
		GetSQLValueString(anti_injection($_POST['nfe']), "text"),
		GetSQLValueString(anti_injection(($_POST['avaria'] == 0) ? NULL : $_POST['avaria']), "int"),
		GetSQLValueString(anti_injection($_POST['transporte']), "text"),
		GetSQLValueString(anti_injection($_POST['observacao']), "text"),
		GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : $_POST['status']), "int"),
		GetSQLValueString(anti_injection($_SESSION['MM_idUser']), "int"),
		GetSQLValueString(anti_injection($_POST['cliente']), "int"),
		GetSQLValueString(anti_injection($_POST['embalagem']), "int"),
		GetSQLValueString(anti_injection($_POST['id']), "int"));
	$Result1 = mysql_query($updateSQL, $conexao) or die(mysql_error());

	$msg = "Erro ao atualizar !";
	if($Result1) $msg = "Atualizado com sucesso!";

	echo "<script>
		alert('".$msg."');
		location='index.php';
	</script>";

}

$colname_qCtrl = "-1";
if (isset($_GET['cr'])) {
	$colname_qCtrl = anti_injection($_GET['cr']);
}

$query_qCtrl = sprintf("SELECT * FROM ctrl_recebimentos WHERE id = %s", GetSQLValueString($colname_qCtrl, "int"));
$qCtrl = mysql_query($query_qCtrl, $conexao) or die(mysql_error());
$row_qCtrl = mysql_fetch_assoc($qCtrl);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet">
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/plugins/validate/jquery.validate.js"></script>
	<script src="../js/Slate.js"></script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>Controle de Recebimentos</h1>
				<ul class="breadcrumb">
					<li><a href="../home/">Home</a> <span class="divider">/</span></li>
					<li><a href="index.php">Controle de Recebimentos</a> <span class="divider">/</span></li>
					<li class="active">Editar</li>
				</ul>
			</div>
			<!-- /.page-title -->

			<div class="row">

				<div class="span8">

					<div id="horizontal" class="widget widget-form">

						<div class="widget-header">	      				
							<h3>
								<i class="icon-pencil"></i>
								Editar Marca				
							</h3>	
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal">
								<fieldset>

									<div class="control-group">
										<label class="control-label" for="status">Status</label>
										<div class="controls">
											<select name="status" class="input-large" id="status" required>
												<option value="1" <?php echo ($row_qCtrl['status'] == 1) ? 'selected' : NULL; ?>>Ativo</option>
												<option value="0" <?php echo ($row_qCtrl['status'] == 0) ? 'selected' : NULL; ?>>Inativo</option>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="cliente">Clientes</label>
										<div class="controls">
											<select name="cliente" class="input-large" id="cliente" required>
												<option value="">Selecione</option>
												<?php if ($totalRows_qItemCliente > 0) { ?>

													<?php while ($row_qItemCliente = mysql_fetch_assoc($qItemCliente)) { ?>

														<option value="<?php echo $row_qItemCliente['id']; ?>" <?php echo ($row_qCtrl['clientes_id'] == $row_qItemCliente['id']) ? 'selected' : NULL; ?>><?php echo $row_qItemCliente['nome']; ?></option>

													<?php } ?>

												<?php } else { ?>
													<option value="">Antes cadastre um cliente</option>
												<?php } ?>
											</select>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="data">Data de Recebimento</label>
										<div class="controls">
											<input name="data" type="text" class="input-large" id="data" value="<?php echo ($row_qCtrl['data']) ? implode('/', array_reverse(explode('-', $row_qCtrl['data']))) : date('d/m/Y'); ?>" required>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="volume">Volumes</label>
										<div class="controls">
											<input name="volume" type="text" class="input-large" id="volume" value="<?php echo $row_qCtrl['volumes']; ?>" required>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="nfe">NFe</label>
										<div class="controls">
											<input name="nfe" type="text" class="input-large" id="nfe" value="<?php echo $row_qCtrl['nfe']; ?>" required>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="avaria">Alguma avaria?</label>
										<div class="controls">
											<select name="avaria" class="input-large" id="avaria" required>
												<option value="0" <?php echo ($row_qCtrl['avaria'] == 0) ? 'selected' : NULL; ?>>Não</option>
												<option value="1" <?php echo ($row_qCtrl['avaria'] == 1) ? 'selected' : NULL; ?>>Sim</option>
											</select>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="embalagem">Embalagem</label>
										<div class="controls">
											<select name="embalagem" class="input-large" id="embalagem" required>
												<option value="">Selecione</option>
												<?php if ($totalRows_qItemEmbalagem > 0) { ?>

													<?php while ($row_qItemEmbalagem = mysql_fetch_assoc($qItemEmbalagem)) { ?>

														<option value="<?php echo $row_qItemEmbalagem['id']; ?>" <?php echo ($row_qCtrl['embalagens_id'] == $row_qItemEmbalagem['id']) ? 'selected' : NULL; ?>><?php echo $row_qItemEmbalagem['embalagem']; ?></option>

													<?php } ?>

												<?php } else { ?>
													<option value="">Antes cadastre uma embalagem</option>
												<?php } ?>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="observacao">Observação</label>
										<div class="controls">
											<textarea name="observacao" rows="6" class="input-large" id="observacao" style="width:95% !important; height:360px;"><?php echo $row_qCtrl['observacoes']; ?></textarea> 
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="transporte">Transporte</label>
										<div class="controls">
											<input name="transporte" type="text" class="input-large" id="transporte" value="<?php echo $row_qCtrl['transporte']; ?>" required>
										</div>
									</div> 

									<div class="form-actions">
										<button type="submit" class="btn btn-primary btn-large">Editar</button>
										<input name="id" type="hidden" id="id" value="<?php echo $row_qCtrl['id']; ?>">
										<input type="hidden" name="MM_update" value="form1">
									</div>
								</fieldset>
							</form>	

						</div> <!-- /widget-content -->

					</div>	
					<!-- /.widget -->

				</div> <!-- /span8 -->

				<div class="span4">

					<div id="formToc" class="widget highlight">

						<div class="widget-header">
							<h3>Informações</h3>		    			
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<p>Preencha ao lado as informações</p>

						</div> <!-- /widget-content -->

					</div> <!-- /widget -->

				</div> <!-- /.span4 -->

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>