<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

if (isset($_GET['cl']) && isset($_GET['aq'])) {

	// fotos equipamento
	$query_qClienteArq = sprintf("SELECT * FROM clientes_arq WHERE id=%s AND clientes_id=%s", 
		GetSQLValueString(anti_injection($_GET['aq']), "int"),
		GetSQLValueString(anti_injection($_GET['cl']), "int"));
	$qClienteArq = mysql_query($query_qClienteArq, $conexao) or die(mysql_error());
	$totalRows_qClienteArq = mysql_num_rows($qClienteArq);

	if ($totalRows_qClienteArq > 0) {

		$row_qClienteArq = mysql_fetch_assoc($qClienteArq);

		if (file_exists($_SERVER['DOCUMENT_ROOT'].'clientes/arquivos/'.$row_qClienteArq['arquivo'])) {  

			unlink($_SERVER['DOCUMENT_ROOT'].'clientes/arquivos/'.$row_qClienteArq['arquivo']);

		}

	}

	$query_qDelete = sprintf("DELETE FROM clientes_arq WHERE id=%s AND clientes_id=%s", 
		GetSQLValueString(anti_injection($_GET['aq']), "int"),
		GetSQLValueString(anti_injection($_GET['cl']), "int"));
	$qDelete = mysql_query($query_qDelete, $conexao) or die(mysql_error());
	
	if (mysql_affected_rows() > 0) {

		header("Location: index_certif.php?cl=".$_GET['cl']);

	}

}
?>