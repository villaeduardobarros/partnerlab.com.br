<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

if (!isset($_GET['cl'])) {
	echo '<script>alert("Cliente não encontrado!");location="index.php";</script>';
}

$cl = anti_injection($_GET['cl']);

$query_qClienteArq = sprintf("SELECT * FROM clientes_arq WHERE clientes_id = %s", 
	GetSQLValueString($cl, "int"));
$qClienteArq = mysql_query($query_qClienteArq, $conexao) or die(mysql_error());
$totalRows_qClienteArq = mysql_num_rows($qClienteArq);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../js/plugins/datatables/DT_bootstrap.css" rel="stylesheet">
	<link href="../js/plugins/responsive-tables/responsive-tables.css" rel="stylesheet"> 
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet"> 
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/plugins/datatables/jquery.dataTables.js"></script>
	<script src="../js/plugins/datatables/DT_bootstrap.js"></script>
	<script src="../js/plugins/responsive-tables/responsive-tables.js"></script>
	<script src="../js/Slate.js"></script>
	<script src="../js/demos/demo.tables.js"></script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>Arquivos</h1>
				<ul class="breadcrumb">
					<li> <a href="../home/">Home</a> <span class="divider">/</span> </li>
					<li> <a href="index.php">Clientes</a> <span class="divider">/</span> </li>
					<li class="active">Certificados</li>
				</ul>
			</div>
			<!-- /.page-title -->

			<div class="row">

				<div class="span12">
					<div class="widget widget-table">	
						<div class="widget-header">						
							<h3><i class="icon-th-list"></i> Lista de Arquivos</h3>
							<a href="incluir_certif.php?cl=<?php echo $cl; ?>" class="btn btn-warning" style="float:right;color:#000;margin:6px 12px;">Cadastrar Certificado</a>
						</div> <!-- /widget-header -->

						<div class="widget-content">

							<table class="table table-striped table-bordered table-highlight" id="example">
								<thead>
									<tr>
										<th width="90%">Título</th>
										<th>Ações</th>
									</tr>
								</thead>
								<tbody>
									<?php if ($totalRows_qClienteArq > 0) { ?>

										<?php while ($row_qClienteArq = mysql_fetch_assoc($qClienteArq)) { ?>

											<tr class="odd gradeX">
												<td>
													<a href="../../clientes/arquivos/<?php echo $row_qClienteArq['arquivo']; ?>" target="_blank">
														<?php echo $row_qClienteArq['titulo']; ?>
													</a>
												</td>
												<td>
													<a href="deletar_certif.php?cl=<?php echo $cl; ?>&aq=<?php echo $row_qClienteArq['id']; ?>" onClick="return confirm('Tem certeza que deseja excluir?');" class="icon-remove"> Excluir</a>
												</td>
											</tr>

										<?php } ?>

									<?php } ?>	
								</tbody>
							</table>

						</div> <!-- /widget-content -->
					</div> <!-- /widget -->	
				</div>

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>