<?php require_once('../../Connections/conexao.php'); ?>
<?php require_once('../includes/login.php');?>
<?php
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

	$login = $_POST['login'];
	$senha = $_POST['senha'];
	$password = ucfirst($login.$senha);
	$codificada = hash('whirlpool', $password);

	$insertSQL = sprintf("INSERT INTO clientes (nome, cnpj, email, telefone, status, login, senha) VALUES (%s, %s, %s, %s, %s, %s, %s)",
		GetSQLValueString(anti_injection($_POST['nome']), "text"),
		GetSQLValueString(anti_injection($_POST['cnpj']), "text"),
		GetSQLValueString(anti_injection($_POST['email']), "text"),
		GetSQLValueString(anti_injection($_POST['telefone']), "text"),
		GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : $_POST['status']), "int"),
		GetSQLValueString(anti_injection($login), "text"),
		GetSQLValueString(anti_injection($codificada), "text"));

	mysql_select_db($database_conexao, $conexao);
	$Result1 = mysql_query($insertSQL, $conexao) or die(mysql_error());

	$logoutGoTo = "index.php";
	if ($logoutGoTo) {
		header("Location: $logoutGoTo");
		exit;
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet">
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/plugins/validate/jquery.validate.js"></script>
	<script src="../js/Slate.js"></script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>Clientes</h1>
				<ul class="breadcrumb">
					<li><a href="../home/">Home</a> <span class="divider">/</span></li>
					<li><a href="index.php">Clientes</a> <span class="divider">/</span></li>
					<li class="active">Cadastrar</li>
				</ul>
			</div>
			<!-- /.page-title -->

			<div class="row">

				<div class="span8">

					<div id="horizontal" class="widget widget-form">

						<div class="widget-header">	      				
							<h3>
								<i class="icon-pencil"></i>
								Adicionar Cliente				
							</h3>	
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal">
								<fieldset>

									<div class="control-group">
										<label class="control-label" for="status">Status</label>
										<div class="controls">
											<select name="status" class="input-large" id="status">
												<option value="1">Ativo</option>
												<option value="0">Inativo</option>
											</select>
										</div>
									</div>    

									<div class="control-group">
										<label class="control-label" for="nome">Nome</label>
										<div class="controls">
											<input name="nome" type="text" class="input-large" id="nome" required>
										</div>
									</div>    

									<div class="control-group">
										<label class="control-label" for="cnpj">CNPJ</label>
										<div class="controls">
											<input name="cnpj" type="text" class="input-large" id="cnpj" required>
										</div>
									</div>    

									<div class="control-group">
										<label class="control-label" for="email">E-mail</label>
										<div class="controls">
											<input name="email" type="text" class="input-large" id="email" required>
										</div>
									</div>    

									<div class="control-group">
										<label class="control-label" for="telefone">Telefone</label>
										<div class="controls">
											<input name="telefone" type="text" class="input-large" id="telefone" required>
										</div>
									</div>

									<div class="control-group">
										<h3>Acesso</h3>
									</div>

									<div class="control-group">
										<label class="control-label" for="login">Usuário</label>
										<div class="controls">
											<input name="login" type="text" class="input-large" id="login" required>
										</div>
									</div>    

									<div class="control-group">
										<label class="control-label" for="senha">Senha</label>
										<div class="controls">
											<input name="senha" type="password" class="input-large" id="senha">
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary btn-large">Cadastrar</button>
										<input type="hidden" name="MM_insert" value="form1">
									</div>
								</fieldset>
							</form>	

						</div> <!-- /widget-content -->

					</div>	
					<!-- /.widget -->

				</div> <!-- /span8 -->

				<div class="span4">

					<div id="formToc" class="widget highlight">

						<div class="widget-header">
							<h3>Informações</h3>		    			
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<p>Preencha ao lado as informações.</p>

						</div> <!-- /widget-content -->

					</div> <!-- /widget -->

				</div> <!-- /.span4 -->

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>
