<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

// usuario travado a edicao
if (in_array($_SESSION['MM_UserTipo'], $tipoTravado)) {
	echo "<script>alert('Usuário sem permissão!');location='index.php';</script>";
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {

	$updateSQL = sprintf("UPDATE marcas SET marca=%s, status=%s  WHERE id=%s",
		GetSQLValueString(anti_injection($_POST['marca']), "text"),
		GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : $_POST['status']), "int"),
		GetSQLValueString(anti_injection($_POST['id']), "int"));
	mysql_select_db($database_conexao, $conexao);
	$Result1 = mysql_query($updateSQL, $conexao) or die(mysql_error());

	$msg = "Erro ao atualizar !";
	if($Result1) $msg = "Atualizado com sucesso!";
?>
<script>
	alert("<?php echo $msg; ?>");
	location="index.php";
</script>
<?php
}

$colname_qMarca = "-1";
if (isset($_GET['id'])) {
	$colname_qMarca = anti_injection($_GET['id']);
}

mysql_select_db($database_conexao, $conexao);
$query_qMarca = sprintf("SELECT * FROM marcas WHERE id = %s", GetSQLValueString($colname_qMarca, "int"));
$qMarca = mysql_query($query_qMarca, $conexao) or die(mysql_error());
$row_qMarca = mysql_fetch_assoc($qMarca);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet">
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/plugins/validate/jquery.validate.js"></script>
	<script src="../js/Slate.js"></script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>Marcas</h1>
				<ul class="breadcrumb">
					<li><a href="../home/">Home</a> <span class="divider">/</span></li>
					<li><a href="index.php">Marca</a> <span class="divider">/</span></li>
					<li class="active">Editar</li>
				</ul>
			</div>
			<!-- /.page-title -->

			<div class="row">

				<div class="span8">

					<div id="horizontal" class="widget widget-form">

						<div class="widget-header">	      				
							<h3>
								<i class="icon-pencil"></i>
								Editar Marca				
							</h3>	
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal">
								<fieldset>

									<div class="control-group">
										<label class="control-label" for="status">Status</label>
										<div class="controls">
											<select name="status" class="input-large" id="status" required>
												<option value="1" <?php echo ($row_qMarca['status'] == 1) ? 'selected' : NULL; ?>>Ativo</option>
												<option value="0" <?php echo ($row_qMarca['status'] == 0) ? 'selected' : NULL; ?>>Inativo</option>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="marca">Marca</label>
										<div class="controls">
											<input name="marca" type="text" class="input-large" id="marca" value="<?php echo $row_qMarca['marca']; ?>" required>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary btn-large">Editar</button>
										<input name="id" type="hidden" id="id" value="<?php echo $row_qMarca['id']; ?>">
										<input type="hidden" name="MM_update" value="form1">
									</div>
								</fieldset>
							</form>	

						</div> <!-- /widget-content -->

					</div>	
					<!-- /.widget -->

				</div> <!-- /span8 -->

				<div class="span4">

					<div id="formToc" class="widget highlight">

						<div class="widget-header">
							<h3>Informações</h3>		    			
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<p>Preencha ao lado as informações</p>

						</div> <!-- /widget-content -->

					</div> <!-- /widget -->

				</div> <!-- /.span4 -->

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>