<div id="nav">
	<div class="container"> <a href="javascript:;" class="btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><i class="icon-reorder"></i> </a>
		<div class="nav-collapse">
			<ul class="nav">

				<li> <a href="../home/">Painel </a></li>

				<li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Usuários <b class="caret"></b> </a>
					<ul class="dropdown-menu">
						<li><a href="../usuarios/incluir.php">Cadastrar</a></li>
						<li><a href="../usuarios/">Listar</a></li>
					</ul>
				</li>

				<li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Banners <b class="caret"></b> </a>
					<ul class="dropdown-menu">
						<li><a href="../banners/incluir.php">Cadastrar</a></li>
						<li><a href="../banners/">Listar</a></li>
					</ul>
				</li>

				<li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Categoria Produtos <b class="caret"></b> </a>
					<ul class="dropdown-menu">
						<li><a href="../categoria/incluir.php">Cadastrar</a></li>
						<li><a href="../categoria/">Listar</a></li>
					</ul>
				</li>

				<li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Produtos <b class="caret"></b> </a>
					<ul class="dropdown-menu">
						<li><a href="../produtos/incluir.php">Cadastrar</a></li>
						<li><a href="../produtos/">Listar</a></li>
					</ul>
				</li>

				<li style="margin:0 10px;border:1px solid #ddd;height:32px;width:0px;">&nbsp;</li>

				<!-- MENUS DO SISTEMA -->

				

					<!-- criar separação entre menus site e sistema -->

					<li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Equipamentos <b class="caret"></b> </a>
						<ul class="dropdown-menu">
							<li><a href="../equipamentos/incluir.php">Cadastrar</a></li>
							<li><a href="../equipamentos/">Listar</a></li>
						</ul>
					</li>

					<li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Marcas <b class="caret"></b> </a>
						<ul class="dropdown-menu">
							<li><a href="../marcas/incluir.php">Cadastrar</a></li>
							<li><a href="../marcas/">Listar</a></li>
						</ul>
					</li>

					<li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Modelos <b class="caret"></b> </a>
						<ul class="dropdown-menu">
							<li><a href="../modelos/incluir.php">Cadastrar</a></li>
							<li><a href="../modelos/">Listar</a></li>
						</ul>
					</li>

					<li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Clientes <b class="caret"></b> </a>
						<ul class="dropdown-menu">
							<li><a href="../clientes/incluir.php">Cadastrar</a></li>
							<li><a href="../clientes/">Listar</a></li>
							<li><a href="../rastreabilidade/index.php">Rastreabilidade</a></li>
						</ul>
					</li>

					<li class="dropdown"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Ctrl Recebimentos <b class="caret"></b> </a>
						<ul class="dropdown-menu">
							<li><a href="../ctrl_recebimentos/incluir.php">Cadastrar</a></li>
							<li><a href="../ctrl_recebimentos/">Listar</a></li>
						</ul>
					</li>

				

			</ul>


		</div>
		<!-- /.nav-collapse --> 

	</div>
	<!-- /.container --> 

</div>