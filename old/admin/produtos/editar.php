<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

// usuario travado a edicao
if (in_array($_SESSION['MM_UserTipo'], $tipoTravado)) {
	echo "<script>alert('Usuário sem permissão!');location='index.php';</script>";
}

$colname_qItem = "-1";
if (isset($_GET['id'])) {
  $colname_qItem = anti_injection($_GET['id']);
}

$query_qItem = sprintf("SELECT * FROM produtos WHERE id = %s", GetSQLValueString($colname_qItem, "int"));
$qItem = mysql_query($query_qItem, $conexao) or die(mysql_error());
$row_qItem = mysql_fetch_assoc($qItem);
$totalRows_qItem = mysql_num_rows($qItem);

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
	
	$colname_qProdutoEdi = $_POST['id'];
	mysql_select_db($database_conexao, $conexao);
	$query_qProdutoEdi = sprintf("SELECT * FROM produtos WHERE id = %s", GetSQLValueString($colname_qProdutoEdi, "int"));
	$qProdutoEdi = mysql_query($query_qProdutoEdi, $conexao) or die(mysql_error());
	$row_qProdutoEdi = mysql_fetch_assoc($qProdutoEdi);

	$foto = $row_qProdutoEdi['img'];
	if ($_FILES['img']['name'] != "") {
		$foto = uploadProdutos("img");
	}
	
	
  $updateSQL = sprintf("UPDATE produtos SET id_categoria=%s, titulo=%s, texto=%s, img=%s, status=%s WHERE id=%s",
					   GetSQLValueString(anti_injection($_POST['id_categoria']), "int"),
					   GetSQLValueString(anti_injection($_POST['titulo']), "text"),
					   GetSQLValueString(anti_injection($_POST['texto']), "text"),
					   GetSQLValueString(anti_injection($foto), "text"),
					   GetSQLValueString(anti_injection($_POST['status']), "text"),
                       GetSQLValueString(anti_injection($_POST['id']), "int"));
  $Result1 = mysql_query($updateSQL, $conexao) or die(mysql_error());
 

	$msg = "Erro ao atualizar!";
	if($Result1) $msg = "Atualizado com sucesso!";
	?>
<script>
		alert("<?php echo $msg; ?>");
		location="index.php";
	</script>
<?php
}

$query_qCategoria= "SELECT * FROM categoria ORDER BY titulo ASC";
$qCategoria= mysql_query($query_qCategoria, $conexao) or die(mysql_error());
$row_qCategoria = mysql_fetch_assoc($qCategoria);
$totalRows_qCategoria = mysql_num_rows($qCategoria);

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $tituloAdmin; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<!-- Styles -->
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/bootstrap-responsive.css" rel="stylesheet">
<link href="../css/bootstrap-overrides.css" rel="stylesheet">

<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">

<link href="../css/slate.css" rel="stylesheet">
<link href="../css/slate-responsive.css" rel="stylesheet">

<link href="../css/pages/calendar.css" rel="stylesheet">


<!-- Javascript -->
<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="../js/jquery.ui.touch-punch.min.js"></script>
<script src="../js/bootstrap.js"></script>

<script src="../js/Slate.js"></script>

<script src="../js/plugins/flot/jquery.flot.js"></script>
<script src="../js/plugins/flot/jquery.flot.orderBars.js"></script>
<script src="../js/plugins/flot/jquery.flot.pie.js"></script>
<script src="../js/plugins/flot/jquery.flot.resize.js"></script>

<script src="../js/mask.js"></script>
<script src="../js/valor.js"></script>

<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
	 menubar : false,
	 plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   	],
	content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor",
    target_list: [
        {title: 'Selecione', value: ''},
        {title: 'Mesma Página', value: '_self'},
        {title: 'Nova Página', value: '_blank'}
    ],
    selector: "textarea"
 });
</script>


<style type="text/css">
<!--
table{border:0px;}
td {
	border:0px;
}
-->
</style>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<!-- /#header -->
<?php include("../includes/header.php"); ?>
<!-- /#header -->

<!-- /#menu -->
<?php include("../includes/menu.php"); ?>
<!-- /#menu -->

<div id="content">
  <div class="container">
    <div id="page-title" class="clearfix">
      <h1>Editar Produto</h1>
      <ul class="breadcrumb">
        <li><a href="../home/">Home</a> <span class="divider">/</span></li>
        <li><a href="./">Produtos</a> <span class="divider">/</span></li>
        <li class="active">Editar</li>
      </ul>
    </div>
    <!-- /.page-title -->
    
    
    <div class="row">
      
    
     <div class="span8">
	      		
	      		<div id="horizontal" class="widget widget-form">
	      			
	      			<div class="widget-header">	      				
	      				<h3>
	      					<i class="icon-pencil"></i>
	      					Editar				
      					</h3>	
      				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal" enctype="multipart/form-data">
					        <fieldset>
                            
                            <div class="control-group">
					            <label class="control-label" for="status">Status</label>
					            <div class="controls">
					             <select name="status" id="status">
                                  <option selected="selected" value="Ativo"<?php if (!(strcmp("Ativo",$row_qItem['status']))) {echo "selected=\"selected\"";} ?>>Ativo</option>
                                  <option value="Inativo"<?php if (!(strcmp("Inativo",$row_qItem['status']))) {echo "selected=\"selected\"";} ?>>Inativo</option>
                                </select>
					            </div>
					          </div>
                              
                              <div class="control-group">
                                <label class="control-label" for="id_categoria">Categoria </label>
                                <div class="controls">
                                <select name="id_categoria" id="id_categoria">
                                  <option value="">Selecione uma categoria</option>
                                  <?php do {  ?>
                                  <option value="<?php echo $row_qCategoria['id']?>"<?php if (!(strcmp($row_qCategoria['id'], $row_qItem['id_categoria']))) {echo "selected=\"selected\"";} ?>><?php echo $row_qCategoria['titulo']?></option>
                                  <?php } while ($row_qCategoria = mysql_fetch_assoc($qCategoria));
                                        $rows = mysql_num_rows($qCategoria);
                                        if($rows > 0) {
                                        mysql_data_seek($qCategoria, 0);
                                        $row_qCategoria = mysql_fetch_assoc($qCategoria);
                                        } ?>
                                </select>
                                </div>
                              </div>
                              
                            
					          <div class="control-group">
					            <label class="control-label" for="titulo">Titulo</label>
					            <div class="controls">
					              <input name="titulo" type="text" class="input-large" id="titulo" value="<?php echo $row_qItem['titulo']; ?>" required>
					            </div>
					          </div>
                              
                              <div class="control-group">
					            <label class="control-label" for="texto">Texto</label>
					            <div class="controls">
								<textarea name="texto" rows="6" class="input-large" id="texto" style="width:95% !important; height:360px;" ><?php echo $row_qItem['texto']; ?></textarea> 
					            </div>
					          </div>
                        
                             <div class="control-group">
					            <label class="control-label" for="img">Imagem Destaque *Somente .jpg</label>
					            <div class="controls">
					              <input name="img" type="file" class="input-file" id="img">
					            </div>
                                <div style="float:right;"><img src="../../images/produtos/thumb_<?php echo $row_qItem['img']; ?>" width="120"><br>
Imagem Atual</div>
					          </div>
                              <div class="clear"></div>
                              
      
                              

					          <div class="form-actions">
					            <button type="submit" class="btn btn-primary btn-large">Editar</button>
                                <input name="id" type="hidden" id="id" value="<?php echo $row_qItem['id']; ?>">
            					<input type="hidden" name="MM_update" value="form1">
					          </div>
					        </fieldset>
					      </form>	
						
					</div> <!-- /widget-content -->
						
				</div>	
				 <!-- /.widget -->
				
		    </div> <!-- /span8 -->
		    
		    
		    <div class="span4">
		    	
		    	<div id="formToc" class="widget highlight">
		    		
		    		<div class="widget-header">
		    			<h3>Informações</h3>		    			
		    		</div> <!-- /widget-header -->
		    		
		    		<div class="widget-content">
		    			<p>Preencha as informações ao lado.</p>
		    			
		    		
		    			
		    			
		    		</div> <!-- /widget-content -->
		    		
		    	</div> <!-- /widget -->
		    	
		    	
		    	
		    	
		    </div> <!-- /.span4 -->
      
      
      
    </div>
    <!-- /row --> 
    
  </div>
  <!-- /.container --> 
  
</div>
<!-- /#content -->

<!-- /#rodape -->
<?php include("../includes/rodape.php"); ?>
<!-- /#rodape -->

</body>
</html>
