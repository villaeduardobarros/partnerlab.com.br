<?php require_once('../../Connections/conexao.php'); ?>
<?php require_once('../includes/login.php');?>
<?php
$colname_qItem = "-1";
if (isset($_GET['id'])) {
  $colname_qItem = anti_injection($_GET['id']);
}
mysql_select_db($database_conexao, $conexao);
$query_qItem = sprintf("SELECT * FROM produtos WHERE id = %s", GetSQLValueString($colname_qItem, "int"));
$qItem = mysql_query($query_qItem, $conexao) or die(mysql_error());
$row_qItem = mysql_fetch_assoc($qItem);
$totalRows_qItem = mysql_num_rows($qItem);

$colname_qFotos = $row_qItem['id'];
$query_qFotos = sprintf("SELECT * FROM produto_fotos WHERE id_produto = %s", GetSQLValueString($colname_qFotos, "int"), GetSQLValueString($colname_qFotos, "int"));
$qFotos = mysql_query($query_qFotos, $conexao) or die(mysql_error());
$row_qFotos = mysql_fetch_assoc($qFotos);
$totalRows_qFotos = mysql_num_rows($qFotos);

?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

  $foto = uploadGaleria("foto");
	
  $insertSQL = sprintf("INSERT INTO produto_fotos (id_produto, img) VALUES (%s, %s)",
					   GetSQLValueString(anti_injection($_POST['id_produto']), "int"),
					  GetSQLValueString(anti_injection($foto), "text"));

  mysql_select_db($database_conexao, $conexao);
  $Result1 = mysql_query($insertSQL, $conexao) or die(mysql_error());

  $msg = "Erro ao atualizar!";
	if($Result1) $msg = "Cadastrado com sucesso!";
	?>
<script>
		alert("<?php echo $msg; ?>");
		location="galeria.php?<?php echo $_SERVER['QUERY_STRING'] ?>";
	</script>
<?php
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $tituloAdmin; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<!-- Styles -->
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/bootstrap-responsive.css" rel="stylesheet">
<link href="../css/bootstrap-overrides.css" rel="stylesheet">
<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
<link href="../js/plugins/datatables/DT_bootstrap.css" rel="stylesheet">
<link href="../js/plugins/responsive-tables/responsive-tables.css" rel="stylesheet">
<link href="../css/slate.css" rel="stylesheet">
<link href="../css/slate-responsive.css" rel="stylesheet">
<link href="../css/components/gallery.css" rel="stylesheet">
<link href="../css/colorbox.css" rel="stylesheet"> 
<!-- Javascript -->
<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="../js/jquery.ui.touch-punch.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/plugins/datatables/jquery.dataTables.js"></script>
<script src="../js/plugins/datatables/DT_bootstrap.js"></script>
<script src="../js/plugins/responsive-tables/responsive-tables.js"></script>
<script src="../js/Slate.js"></script>
<script src="../js/jquery.colorbox-min.js"  type="text/javascript"></script>

<script src="../js/demos/demo.gallery.js"></script>
<script>
$(document).ready(function() {
	$(".fotos").colorbox({rel:'fotos'});
})
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<!-- /#header -->
<?php include("../includes/header.php"); ?>
<!-- /#header --> 

<!-- /#menu -->
<?php include("../includes/menu.php"); ?>
<!-- /#menu -->

<div id="content">
  <div class="container">
    <div id="page-title" class="clearfix">
      <h1>Fotos Produto - <?php echo $row_qItem['titulo']; ?></h1>
      <ul class="breadcrumb">
        <li> <a href="../home/">Home</a> <span class="divider">/</span> </li>
        <li class="active">Fotos</li>
      </ul>
    </div>
    <!-- /.page-title -->
    
    <div class="row">
      <div class="span8">
        <div class="widget">
          <div class="widget-header">
            <h3> <i class="icon-picture"></i> Lista de Fotos</h3>
          </div>
          <!-- /widget-header -->
          
          <div class="widget-content">
            <ul class="gallery">
              
              <?php if($totalRows_qFotos > 0){ ?>
          <?php do { ?>
              
              <li>
                <div class="frame">
                  <div class="img"> <img src="../../images/galeria/<?php echo $row_qFotos['img']; ?>" alt="" style="opacity: 1; "> </div>
                  <!-- /img -->
                  
                  <div class="actions" style="display: block; ">
                      <a href="deletar-galeria.php?id=<?php echo $row_qFotos['id']; ?>&id_produto=<?php echo $row_qFotos['id_produto']; ?>" class="edit"> <i class="icon-remove"></i> </a>
                      <a href="../../images/galeria/<?php echo $row_qFotos['img']; ?>" class="zoom ui-lightbox ui-lightbox fotos"> <i class="icon-search"></i> </a>
                  </div>

                  <!-- /actions --> 
                  <img src="../img/gallery/frame.png" alt="Frame"></div>
                <!-- /frame --> 
              </li>
              
              
              <?php } while ($row_qFotos = mysql_fetch_assoc($qFotos)); ?>
          <?php } else { ?>
          Nenhuma foto Cadastrada.
          <?php } ?>
              
            </ul>
          </div>
          <!-- /widget-content --> 
          
        </div>
        <!-- /widget --> 
        
      </div>
      
      
      
      <div class="span4">
        <!-- /widget -->
        
        <div class="widget highlight">
          <div class="widget-header">
            <h3>Adicionar Foto</h3>
          </div>
          <!-- /widget-header -->
          
          <div class="widget-content">
          	<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal" enctype="multipart/form-data">
					        <fieldset>
                            
                              <div class="control-group">
					            <label class="control-label" for="foto" style="width: 100px !important;">Imagem *Somente .jpg</label>
					            <div class="controls" style="margin-left: 120px !important;">
					              <input name="foto" type="file" class="input-file" id="foto" required>
					            </div>
					          </div>

					          <div class="form-actions" style="padding-left: 120px !important;">
					            <button type="submit" class="btn btn-primary btn-large">Cadastrar</button>
                                <input type="hidden" name="MM_insert" value="form1">
                                <input type="hidden" name="id_produto" value="<?php echo $row_qItem['id']; ?>">
					          </div>
					        </fieldset>
					      </form>
          </div>
          <!-- /widget-content --> 
          
        </div>
        <!-- /widget --> 
        
      </div>
    </div>
    <!-- /row --> 
    
  </div>
  <!-- /.container --> 
  
</div>
<!-- /#content --> 

<!-- /#rodape -->
<?php include("../includes/rodape.php"); ?>
<!-- /#rodape -->

</body>
</html>
