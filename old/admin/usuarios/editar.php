<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

// usuario travado a edicao
if (in_array($_SESSION['MM_UserTipo'], $tipoTravado)) {
	echo "<script>alert('Usuário sem permissão!');location='index.php';</script>";
}

$colname_qNoticia = "-1";
if (isset($_GET['id_usuario'])) {
	$colname_qNoticia = $_GET['id_usuario'];
}

$query_qNoticia = sprintf("SELECT * FROM usuarios WHERE id_usuario = %s", 
	GetSQLValueString(anti_injection($colname_qNoticia), "int"));
$qNoticia = mysql_query($query_qNoticia, $conexao) or die(mysql_error());
$row_qNoticia = mysql_fetch_assoc($qNoticia);
$totalRows_qNoticia = mysql_num_rows($qNoticia);

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {

	$colname_qNoticiaEdi = $_POST['id_usuario'];
	$query_qNoticiaEdi = sprintf("SELECT * FROM usuarios WHERE id_usuario = %s", 
		GetSQLValueString(anti_injection($colname_qNoticiaEdi), "int"));
	$qNoticiaEdi = mysql_query($query_qNoticiaEdi, $conexao) or die(mysql_error());
	$row_qNoticiaEdi = mysql_fetch_assoc($qNoticiaEdi);
	$totalRows_qNoticiaEdi = mysql_num_rows($qNoticiaEdi);

	$foto = $row_qNoticiaEdi['img'];
	if ($_FILES['foto']['name'] != "") {
		$foto = uploadUsuario("foto");
		@unlink($uploaddir.$row_qNoticiaEdi['foto']);
	}

	$loginUsername = $_POST['login'];
	$loginPassword = $_POST['senha'];
	if ($loginPassword) {
		$password = ucfirst($loginUsername.$loginPassword);
		$codificada = hash('whirlpool', $password);
	} else {
		$codificada = $row_qNoticia['senha'];
	}

	$updateSQL = sprintf("UPDATE usuarios SET tipo=%s, nome_usuario=%s, login=%s, senha=%s, email=%s, telefone=%s, img=%s WHERE id_usuario=%s",
		GetSQLValueString(anti_injection($_POST['tipo']), "int"),
		GetSQLValueString(anti_injection($_POST['nome_usuario']), "text"),
		GetSQLValueString($loginUsername, "text"),
		GetSQLValueString($codificada, "text"),
		GetSQLValueString(anti_injection($_POST['email']), "text"),
		GetSQLValueString(anti_injection($_POST['telefone']), "text"),
		GetSQLValueString($foto, "text"),    
		GetSQLValueString(anti_injection($_POST['id_usuario']), "int"));
	$Result1 = mysql_query($updateSQL, $conexao) or die(mysql_error());

	$msg = "Erro ao atualizar !";
	if($Result1) $msg = "Atualizado com sucesso!";

	echo "<script>alert('".$msg."');location='index.php';</script>";

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $tituloAdmin.$_SESSION['MM_UserTipo']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<!-- Styles -->
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/bootstrap-responsive.css" rel="stylesheet">
<link href="../css/bootstrap-overrides.css" rel="stylesheet">

<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">

<link href="../css/slate.css" rel="stylesheet">
<link href="../css/slate-responsive.css" rel="stylesheet">


<!-- Javascript -->
<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="../js/jquery.ui.touch-punch.min.js"></script>
<script src="../js/bootstrap.js"></script>

<script src="../js/plugins/validate/jquery.validate.js"></script>

<script src="../js/Slate.js"></script>


<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<!-- /#header -->
<?php include("../includes/header.php"); ?>
<!-- /#header -->

<!-- /#menu -->
<?php include("../includes/menu.php"); ?>
<!-- /#menu -->

<div id="content">
  <div class="container">
	<div id="page-title" class="clearfix">
	  <h1>Usuários Administrativos</h1>
	  <ul class="breadcrumb">
		<li><a href="../home/">Home</a> <span class="divider">/</span></li>
		<li><a href="#">Usuários</a> <span class="divider">/</span></li>
		<li class="active">Editar</li>
	  </ul>
	</div>
	<!-- /.page-title -->
	
	
	<div class="row">
	  
	
	 <div class="span8">
				
				<div id="horizontal" class="widget widget-form">
					
					<div class="widget-header">	      				
						<h3>
							<i class="icon-pencil"></i>
							Editar Usuário				
						</h3>	
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
						<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal" enctype="multipart/form-data">
							<fieldset>
							
							  <div class="control-group">
								<label class="control-label" for="tipo">Tipo</label>
								<div class="controls">
								  <select name="tipo" class="input-large" id="tipo">
								  	<option value="1" <?php echo ($row_qNoticia['tipo'] == 1) ? 'selected' : NULL; ?>>Administrador</option>
								  	<option value="2" <?php echo ($row_qNoticia['tipo'] == 2) ? 'selected' : NULL; ?>>Usuário Comum</option>
								  </select>
								</div>
							  </div>
							
							  <div class="control-group">
								<label class="control-label" for="nome_usuario">Nome</label>
								<div class="controls">
								  <input name="nome_usuario" type="text" class="input-large" id="nome_usuario" value="<?php echo $row_qNoticia['nome_usuario']; ?>">
								</div>
							  </div>
							  
							  <div class="control-group">
								<label class="control-label" for="email">Email</label>
								<div class="controls">
								  <input name="email" type="text" class="input-large" id="email" value="<?php echo $row_qNoticia['email']; ?>">
								</div>
							  </div>
							  
							  <div class="control-group">
								<label class="control-label" for="telefone">Telefone</label>
								<div class="controls">
								  <input name="telefone" type="text" class="input-large" id="telefone" value="<?php echo $row_qNoticia['telefone']; ?>">
								</div>
							  </div>

							  <div class="control-group">
								<label class="control-label" for="login">Usuário de Acesso</label>
								<div class="controls">
								  <input name="login" type="text" class="input-large" id="login" value="<?php echo $row_qNoticia['login']; ?>">
								</div>
							  </div>
							  
							  <div class="control-group">
								<label class="control-label" for="senha">Senha:</label>
								<div class="controls">
								  <input name="senha" type="password" class="input-large" id="senha">
								</div>
							  </div>

							  
							   <div class="control-group">
								<label class="control-label" for="foto">Foto</label>
								<div class="controls">
								  <input class="input-file" id="foto" name="foto" type="file">
								</div>
							  </div>


							  
							  <div class="form-actions">
								<button type="submit" class="btn btn-primary btn-large">Editar</button>
								<input name="id_usuario" type="hidden" id="id_usuario" value="<?php echo $row_qNoticia['id_usuario']; ?>">
								<input type="hidden" name="MM_update" value="form1">
							  </div>
							</fieldset>
						  </form>	
						
					</div> <!-- /widget-content -->
						
				</div>	
				 <!-- /.widget -->
				
			</div> <!-- /span8 -->
			
			
			<div class="span4">
				
				<div id="formToc" class="widget highlight">
					
					<div class="widget-header">
						<h3>Informações</h3>		    			
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
						<p>Cadastre as informações solicitadas ao lado.</p>
	
					</div> <!-- /widget-content -->
					
				</div> <!-- /widget -->
				
				
				
				
			</div> <!-- /.span4 -->
	  
	  
	  
	</div>
	<!-- /row --> 
	
  </div>
  <!-- /.container --> 
  
</div>
<!-- /#content -->

<!-- /#rodape -->
<?php include("../includes/rodape.php"); ?>
<!-- /#rodape -->

</body>
</html>
