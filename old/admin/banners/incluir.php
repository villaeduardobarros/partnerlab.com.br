<?php require_once('../../Connections/conexao.php'); ?>
<?php require_once('../includes/login.php');?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
	
	$foto = uploadBanner("img");
	
	$insertSQL = sprintf("INSERT INTO banners (titulo, texto, url, img) VALUES (%s, %s, %s, %s)",
					   GetSQLValueString(anti_injection($_POST['titulo']), "text"),
					   GetSQLValueString(anti_injection($_POST['texto']), "text"),
					   GetSQLValueString(anti_injection($_POST['url']), "text"),
					   GetSQLValueString(anti_injection($foto), "text"));

  
  mysql_select_db($database_conexao, $conexao);
  $Result1 = mysql_query($insertSQL, $conexao) or die(mysql_error());
	
  $insertGoTo = "index.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $tituloAdmin; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<!-- Styles -->
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/bootstrap-responsive.css" rel="stylesheet">
<link href="../css/bootstrap-overrides.css" rel="stylesheet">

<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">

<link href="../css/slate.css" rel="stylesheet">
<link href="../css/slate-responsive.css" rel="stylesheet">

<link href="../css/pages/calendar.css" rel="stylesheet">


<!-- Javascript -->
<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="../js/jquery.ui.touch-punch.min.js"></script>
<script src="../js/bootstrap.js"></script>

<script src="../js/Slate.js"></script>

<script src="../js/plugins/flot/jquery.flot.js"></script>
<script src="../js/plugins/flot/jquery.flot.orderBars.js"></script>
<script src="../js/plugins/flot/jquery.flot.pie.js"></script>
<script src="../js/plugins/flot/jquery.flot.resize.js"></script>

<script src="../js/mask.js"></script>
<script src="../js/valor.js"></script>

<!-- EDITOR -->
<link rel="stylesheet" href="../jwysiwyg/jquery.wysiwyg.css" type="text/css" />
<script type="text/javascript" src="../jwysiwyg/jquery.wysiwyg.js"></script>
<!-- EDITOR -->

<script>
$(document).ready(function() {
	//$('#texto').wysiwyg();
});
</script>
<style type="text/css">
<!--
table{border:0px;}
td {
	border:0px;
}
-->
</style>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<!-- /#header -->
<?php include("../includes/header.php"); ?>
<!-- /#header -->

<!-- /#menu -->
<?php include("../includes/menu.php"); ?>
<!-- /#menu -->

<div id="content">
  <div class="container">
    <div id="page-title" class="clearfix">
      <h1>Cadastro de Banners</h1>
      <ul class="breadcrumb">
        <li><a href="../home/">Home</a> <span class="divider">/</span></li>
        <li><a href="./">Banners</a> <span class="divider">/</span></li>
        <li class="active">Cadastrar</li>
      </ul>
    </div>
    
   
    <div class="row">
      
    
     <div class="span8">
	      		
	      		<div id="horizontal" class="widget widget-form">
	      			
	      			<div class="widget-header">	      				
	      				<h3>
	      					<i class="icon-pencil"></i>
	      					Adicionar				
      					</h3>	
      				</div>
					
					<div class="widget-content">
						<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal" enctype="multipart/form-data">
					        <fieldset>
                
                           
                            
					          <div class="control-group">
					            <label class="control-label" for="titulo">Titulo</label>
					            <div class="controls">
					              <input name="titulo" type="text" class="input-large" id="titulo" required>
					            </div>
					          </div>
                              
                              <div class="control-group">
					            <label class="control-label" for="texto">Texto</label>
					            <div class="controls">
                                  <textarea name="texto" class="input-large"  style="width:90%; height:80px;"></textarea>
					            </div>
					          </div>
                              
                            
                              <div class="control-group">
					            <label class="control-label" for="texto">Url</label>
					            <div class="controls">
								<input name="url" type="text" class="input-large" id="url" >
					            </div>
					          </div>

                              <div class="control-group">
                              	<h3>Imagem</h3>
                              </div>
                              
                               <div class="control-group">
					            <label class="control-label" for="img">Imagem (1600x390) *Somente .jpg</label>
					            <div class="controls">
					              <input name="img" type="file" class="input-file" id="img" required>
					            </div>
					          </div>

					          <div class="form-actions">
					            <button type="submit" class="btn btn-primary btn-large">Cadastrar</button>
                                <input type="hidden" name="MM_insert" value="form1">
					          </div>
					        </fieldset>
					      </form>	
						
					</div> <!-- /widget-content -->
						
				</div>	
				 <!-- /.widget -->
				
		    </div> <!-- /span8 -->
		    
		    
		    <div class="span4">
		    	
		    	<div id="formToc" class="widget highlight">
		    		
		    		<div class="widget-header">
		    			<h3>Informações</h3>		    			
		    		</div> <!-- /widget-header -->
		    		
		    		<div class="widget-content">
		    			<p>Cadastre ao lado as informações solicitadas.</p>
		    			
		    		
		    			
		    			
		    		</div> <!-- /widget-content -->
		    		
		    	</div> <!-- /widget -->
		    	
		    	
		    	
		    	
		    </div> <!-- /.span4 -->
      
      
      
    </div>
    <!-- /row --> 
    
  </div>
  <!-- /.container --> 
  
</div>
<!-- /#content -->

<!-- /#rodape -->
<?php include("../includes/rodape.php"); ?>
<!-- /#rodape -->

</body>
</html>
