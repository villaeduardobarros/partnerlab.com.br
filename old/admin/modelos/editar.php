<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

// usuario travado a edicao
if (in_array($_SESSION['MM_UserTipo'], $tipoTravado)) {
	echo "<script>alert('Usuário sem permissão!');location='index.php';</script>";
}

$colname_qItemModelo = "-1";
if (isset($_GET['id'])) {
	$colname_qItemModelo = anti_injection($_GET['id']);
}

$query_qModelo = sprintf("SELECT * FROM modelos WHERE id = %s", GetSQLValueString($colname_qItemModelo, "int"));
$qModelo = mysql_query($query_qModelo, $conexao) or die(mysql_error());
$row_qModelo = mysql_fetch_assoc($qModelo);

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}


$query_qItemMarca = "SELECT * FROM marcas WHERE status=1";
$qItemMarca = mysql_query($query_qItemMarca, $conexao) or die(mysql_error());
$totalRows_qItemMarca = mysql_num_rows($qItemMarca);


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {

	$updateSQL = sprintf("UPDATE modelos SET modelo=%s, status=%s  WHERE id=%s",
		GetSQLValueString(anti_injection($_POST['modelo']), "text"),
		GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : $_POST['status']), "int"),
		GetSQLValueString(anti_injection($_POST['id']), "int"));

	/*$updateSQL = sprintf("UPDATE modelos SET modelo=%s, status=%s, marcas_id=%s  WHERE id=%s",
		GetSQLValueString(anti_injection($_POST['modelo']), "text"),
		GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : $_POST['status']), "int"),
		GetSQLValueString(anti_injection($_POST['marca']), "int"),
		GetSQLValueString(anti_injection($_POST['id']), "int"));*/

	$Result1 = mysql_query($updateSQL, $conexao) or die(mysql_error());

	$msg = "Erro ao atualizar !";
	if($Result1) $msg = "Atualizado com sucesso!";
?>
	<script>
		alert("<?php echo $msg; ?>");
		location = "index.php";
	</script>
<?php 
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet">
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/plugins/validate/jquery.validate.js"></script>
	<script src="../js/Slate.js"></script>
	<script>
		$(function(){
			$.post('funcao.php', {
				marca:$('select[name="marca"] option:selected').val(), 
//marca:$('select[name="marca"] option:selected').val(), 
}, function(dataReturn){
	$('select[name="modelo"]').append(dataReturn);
})
		});
	</script>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>Modelos</h1>
				<ul class="breadcrumb">
					<li><a href="../home/">Home</a> <span class="divider">/</span></li>
					<li><a href="index.php">Modelos</a> <span class="divider">/</span></li>
					<li class="active">Editar</li>
				</ul>
			</div>
			<!-- /.page-title -->

			<div class="row">

				<div class="span8">

					<div id="horizontal" class="widget widget-form">

						<div class="widget-header">	      				
							<h3>
								<i class="icon-pencil"></i>
								Editar Modelo				
							</h3>	
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal">
								<fieldset>

									<div class="control-group">
										<label class="control-label" for="status">Status</label>
										<div class="controls">
											<select name="status" class="input-large" id="status" required>
												<option value="1" <?php echo ($row_qModelo['status'] == 1) ? 'selected' : NULL; ?>>Ativo</option>
												<option value="0" <?php echo ($row_qModelo['status'] == 0) ? 'selected' : NULL; ?>>Inativo</option>
											</select>
										</div>
									</div>

									<!--<div class="control-group">
										<label class="control-label" for="marca">Marca</label>
										<div class="controls">
											<select name="marca" class="input-large" id="marca" required>
												<option>Selecione</option>
												<?php //if ($totalRows_qItemMarca > 0) { ?>

													<?php //while ($row_qItemMarca = mysql_fetch_assoc($qItemMarca)) { ?>

														<option value="<?php //echo $row_qItemMarca['id']; ?>" <?php //echo ($row_qItemMarca['id'] == $row_qModelo['marcas_id']) ? 'selected' : NULL; ?>><?php //echo $row_qItemMarca['marca']; ?></option>

													<?php //} ?>

												<?php //} else { ?>
													<option>Antes cadastre uma marca</option>
												<?php //} ?>
											</select>
										</div>
									</div>-->

									<div class="control-group">
										<label class="control-label" for="modelo">Modelo</label>
										<div class="controls">
											<input name="modelo" type="text" class="input-large" id="modelo" value="<?php echo $row_qModelo['modelo']; ?>" required>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary btn-large">Editar</button>
										<input name="id" type="hidden" id="id" value="<?php echo $row_qModelo['id']; ?>">
										<input type="hidden" name="MM_update" value="form1">
									</div>
								</fieldset>
							</form>	

						</div> <!-- /widget-content -->

					</div>	
					<!-- /.widget -->

				</div> <!-- /span8 -->

				<div class="span4">

					<div id="formToc" class="widget highlight">

						<div class="widget-header">
							<h3>Informações</h3>		    			
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<p>Preencha ao lado as informações</p>

						</div> <!-- /widget-content -->

					</div> <!-- /widget -->

				</div> <!-- /.span4 -->

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>
