<?php
require_once('../../Connections/conexao.php');
require_once('../includes/login.php');

// usuario travado a edicao
if (in_array($_SESSION['MM_UserTipo'], $tipoTravado)) {
	echo "<script>alert('Usuário sem permissão!');location='index.php';</script>";
}

$query_qItemMarca = "SELECT * FROM marcas WHERE status=1";
$qItemMarca = mysql_query($query_qItemMarca, $conexao) or die(mysql_error());
$totalRows_qItemMarca = mysql_num_rows($qItemMarca);

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
	$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {

	$updateSQL = sprintf("UPDATE equipamentos SET equipamento=%s, descricao=%s, status=%s WHERE id=%s",
		GetSQLValueString(anti_injection($_POST['equipamento']), "text"),
		GetSQLValueString(anti_injection($_POST['descricao']), "text"),
		GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : $_POST['status']), "int"),
		GetSQLValueString(anti_injection($_POST['id']), "int"));

	/*$updateSQL = sprintf("UPDATE equipamentos SET equipamento=%s, descricao=%s, status=%s, marcas_id=%s, modelos_id=%s WHERE id=%s",
		GetSQLValueString(anti_injection($_POST['equipamento']), "text"),
		GetSQLValueString(anti_injection($_POST['descricao']), "text"),
		GetSQLValueString(anti_injection(($_POST['status'] == 0) ? NULL : $_POST['status']), "int"),
		GetSQLValueString(anti_injection($_POST['marca']), "int"),
		GetSQLValueString(anti_injection($_POST['modelo']), "int"),
		GetSQLValueString(anti_injection($_POST['id']), "int"));*/
	$Result1 = mysql_query($updateSQL, $conexao) or die(mysql_error());

	$msg = "Erro ao atualizar !";
	if($Result1) $msg = "Atualizado com sucesso!";
?>
	<script>
		alert("<?php echo $msg; ?>");
		location="index.php";
	</script>
<?php
}

$colname_qEquipamento = "-1";
if (isset($_GET['id'])) {
	$colname_qEquipamento = anti_injection($_GET['id']);
}

$query_qEquipamento = sprintf("SELECT * FROM equipamentos WHERE id = %s", GetSQLValueString($colname_qEquipamento, "int"));
$qEquipamento = mysql_query($query_qEquipamento, $conexao) or die(mysql_error());
$row_qEquipamento = mysql_fetch_assoc($qEquipamento);
$totalRows_qEquipamento = mysql_num_rows($qEquipamento);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $tituloAdmin; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<!-- Styles -->
	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link href="../css/bootstrap-overrides.css" rel="stylesheet">
	<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href="../css/slate.css" rel="stylesheet">
	<link href="../css/slate-responsive.css" rel="stylesheet">
	<!-- Javascript -->
	<script src="../js/jquery-1.7.2.min.js"></script>
	<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	<script src="../js/jquery.ui.touch-punch.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/plugins/validate/jquery.validate.js"></script>
	<script src="../js/Slate.js"></script>
	<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		tinymce.init({
			menubar : false,
			plugins: [
				"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"save table contextmenu directionality emoticons template paste textcolor"
			],
			content_css: "css/content.css",
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor",
			target_list: [
				{title: 'Selecione', value: ''},
				{title: 'Mesma Página', value: '_self'},
				{title: 'Nova Página', value: '_blank'}
			],
			selector: "textarea"
		});
	</script>
	<!--<script>
		$(function(){
			$.post('../modelos/funcao.php', {
				marca:$('select[name="marca"]').val(), 
				selecionado:$('select[name="modelo"]').data('idselec')
			}, function(dataReturn){
				$('select[name="modelo"]').html(dataReturn);
			});
		});
	</script>-->
	
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
	<!-- /#header -->
	<?php include("../includes/header.php"); ?>
	<!-- /#header -->

	<!-- /#menu -->
	<?php include("../includes/menu.php"); ?>
	<!-- /#menu -->

	<div id="content">
		<div class="container">
			<div id="page-title" class="clearfix">
				<h1>Equipamentos</h1>
				<ul class="breadcrumb">
					<li><a href="../home/">Home</a> <span class="divider">/</span></li>
					<li><a href="index.php">Equipamentos</a> <span class="divider">/</span></li>
					<li class="active">Edição</li>
				</ul>
			</div>
			<!-- /.page-title -->

			<div class="row">

				<div class="span8">

					<div id="horizontal" class="widget widget-form">

						<div class="widget-header">	      				
							<h3>
								<i class="icon-pencil"></i>
								Editar Equipamento				
							</h3>	
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<form action="<?php echo $editFormAction; ?>" name="form1" method="POST" class="form-horizontal">
								<fieldset>

									<div class="control-group">
										<label class="control-label" for="status">Status</label>
										<div class="controls">
											<select name="status" class="input-large" id="status" required>
												<option value="1" <?php echo ($row_qEquipamento['status'] == 1) ? 'selected' : NULL; ?>>Ativo</option>
												<option value="0" <?php echo ($row_qEquipamento['status'] == 0) ? 'selected' : NULL; ?>>Inativo</option>
											</select>
										</div>
									</div>   

									<div class="control-group">
										<label class="control-label" for="equipamento">Equipamento</label>
										<div class="controls">
											<input name="equipamento" type="text" class="input-large" id="equipamento" value="<?php echo $row_qEquipamento['equipamento']; ?>" required>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="descricao">Descrição</label>
										<div class="controls">
											<textarea name="descricao" rows="6" class="input-large" id="descricao" style="width:95% !important; height:360px;"><?php echo $row_qEquipamento['descricao']; ?></textarea>
										</div>
									</div>

									<!--<div class="control-group">
										<label class="control-label" for="marca">Marca</label>
										<div class="controls">
											<select name="marca" class="input-large" id="marca">
												<option value="">Selecione a marca</option>
												<?php //if($totalRows_qItemMarca > 0){ ?>
													<?php //do { ?>

														<option value="<?php //echo $row_qItemMarca['id']; ?>" <?php //echo ($row_qItemMarca['id'] == $row_qEquipamento['marcas_id']) ? 'selected' : NULL; ?>><?php //echo $row_qItemMarca['marca']; ?></option>

													<?php //} while ($row_qItemMarca = mysql_fetch_assoc($qItemMarca)); ?>
												<?php //} else { ?>
													<option value="">Antes cadastre a marca</option>
												<?php //} ?>

											</select>
										</div>
									</div>-->

									<!--<div class="control-group">
										<label class="control-label" for="modelo">Modelo</label>
										<div class="controls">
											<select name="modelo" class="input-large" id="modelo" data-idselec="<?php //echo $row_qEquipamento['modelos_id']; ?>">
												<option value="">Antes selecione a marca</option>
											</select>
										</div>
									</div>-->

									<div class="form-actions">
										<button type="submit" class="btn btn-primary btn-large">Editar</button>
										<input name="id" type="hidden" id="id" value="<?php echo $row_qEquipamento['id']; ?>">
										<input type="hidden" name="MM_update" value="form1">
									</div>
								</fieldset>
							</form>	

						</div> <!-- /widget-content -->

					</div>	
					<!-- /.widget -->

				</div> <!-- /span8 -->

				<div class="span4">

					<div id="formToc" class="widget highlight">

						<div class="widget-header">
							<h3>Informações</h3>		    			
						</div> <!-- /widget-header -->

						<div class="widget-content">
							<p>Preencha ao lado as informações</p>

						</div> <!-- /widget-content -->

					</div> <!-- /widget -->

				</div> <!-- /.span4 -->

			</div>
			<!-- /row --> 

		</div>
		<!-- /.container --> 

	</div>
	<!-- /#content -->

	<!-- /#rodape -->
	<?php include("../includes/rodape.php"); ?>
	<!-- /#rodape -->

</body>
</html>