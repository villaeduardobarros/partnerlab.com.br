<?php require_once('../../Connections/conexao.php'); ?>
<?php require_once('../includes/login.php');

mysql_select_db($database_conexao, $conexao);
$query_qItemEquip = "SELECT * FROM equipamentos ORDER BY id DESC";
$qItemEquip = mysql_query($query_qItemEquip, $conexao) or die(mysql_error());
$totalRows_qItemEquip = mysql_num_rows($qItemEquip);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $tituloAdmin; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Styles -->
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/bootstrap-responsive.css" rel="stylesheet">
<link href="../css/bootstrap-overrides.css" rel="stylesheet">
<link href="../css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
<link href="../js/plugins/datatables/DT_bootstrap.css" rel="stylesheet">
<link href="../js/plugins/responsive-tables/responsive-tables.css" rel="stylesheet"> 
<link href="../css/slate.css" rel="stylesheet">
<link href="../css/slate-responsive.css" rel="stylesheet"> 
<!-- Javascript -->
<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="../js/jquery.ui.touch-punch.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/plugins/datatables/jquery.dataTables.js"></script>
<script src="../js/plugins/datatables/DT_bootstrap.js"></script>
<script src="../js/plugins/responsive-tables/responsive-tables.js"></script>
<script src="../js/Slate.js"></script>
<script src="../js/demos/demo.tables.js"></script>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<!-- /#header -->
<?php include("../includes/header.php"); ?>
<!-- /#header -->

<!-- /#menu -->
<?php include("../includes/menu.php"); ?>
<!-- /#menu -->

<div id="content">
  <div class="container">
    <div id="page-title" class="clearfix">
      <h1>Equipamentos</h1>
      <ul class="breadcrumb">
        <li> <a href="../home/">Home</a> <span class="divider">/</span> </li>
        <li class="active">Equipamento</li>
      </ul>
    </div>
    <!-- /.page-title -->
    
    
    <div class="row">
      
    
      <div class="span12">
	      		<div class="widget widget-table">	
					<div class="widget-header">						
						<h3>
							<i class="icon-th-list"></i>
							Lista de Equipamentos							
						</h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
						

						<table class="table table-striped table-bordered table-highlight" id="example">
							<thead>
								<tr>
								    <th width="60%">Equipamento</th>
								    <!--<th>Marca</th>
								    <th>Modelo</th>-->
								    <th>Status</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody>
                            <?php if($totalRows_qItemEquip > 0){ ?>

                            	<?php while ($row_qItemEquip = mysql_fetch_assoc($qItemEquip)) { ?>

            						<?php
            							// marcas
										//$query_qItemMarca = "SELECT * FROM marcas WHERE id = ".$row_qItemEquip['marcas_id'];
										//$qItemMarca = mysql_query($query_qItemMarca, $conexao) or die(mysql_error());
										//$row_qItemMarca = mysql_fetch_assoc($qItemMarca);

										// modelo
										//$query_qItemModelo = "SELECT * FROM modelos WHERE id = ".$row_qItemEquip['modelos_id'];
										//$qItemModelo = mysql_query($query_qItemModelo, $conexao) or die(mysql_error());
										//$row_qItemModelo = mysql_fetch_assoc($qItemModelo);
									?>

									<tr class="odd gradeX">
										<td><?php echo $row_qItemEquip['equipamento']; ?></td>
										<!--<td><?php //echo $row_qItemMarca['marca']; ?></td>
										<td><?php //echo $row_qItemModelo['modelo']; ?></td>-->
										<td><?php echo ($row_qItemEquip['status'] == 1) ? 'Ativo' : 'Inativo'; ?></td>
										<td>
											<?php if (!in_array($_SESSION['MM_UserTipo'], $tipoTravado)) { ?>
												<a href="editar.php?id=<?php echo $row_qItemEquip['id']; ?>" class="icon-book"> Editar</a> 
												&nbsp;&nbsp;&nbsp; 
											<?php } ?>
											<a href="deletar.php?id=<?php echo $row_qItemEquip['id']; ?>" onClick="return confirm('Tem certeza que deseja excluir?');" class="icon-remove"> Excluir</a></td>
									</tr>

								
								<?php } ?>

                        	<?php } ?>	
							</tbody>
						</table>
						
					</div> <!-- /widget-content -->
				</div> <!-- /widget -->	
		    </div>
      
      
      
    </div>
    <!-- /row --> 
    
  </div>
  <!-- /.container --> 
  
</div>
<!-- /#content -->

<!-- /#rodape -->
<?php include("../includes/rodape.php"); ?>
<!-- /#rodape -->

</body>
</html>
