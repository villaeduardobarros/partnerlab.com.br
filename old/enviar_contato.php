<?php require_once('Connections/conexao.php'); ?>
<?php
date_default_timezone_set('America/Sao_Paulo');
$date = date("d/m/Y H:i");

$email_servidor = "contato@partnerlab.com.br";

// Recebendo os dados passados pela página "contato.php"
$nome =     strip_tags(trim(anti_injection($_POST['nome'])));
$email =    strip_tags(trim(anti_injection($_POST['email'])));
$telefone =  strip_tags(trim(anti_injection($_POST['telefone'])));
$cidade =  strip_tags(trim(anti_injection($_POST['cidade'])));
$estado =  strip_tags(trim(anti_injection($_POST['estado'])));
$mensagem =  strip_tags(trim(anti_injection($_POST['mensagem'])));
			
//Teste de consistência
if($nome=="" || $nome=="NOME" || $email=="" || $email=="EMAIL" || $mensagem ==""){
	echo "<meta http-equiv='refresh' content='0;URL=contato.php'>";
	echo "<script>alert('Ops... Por favor preencha todos os campos.'); window.history.go(-1);</script>";
	} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		echo "<meta http-equiv='refresh' content='0;URL=contato.php'>";
		echo "<script>alert('Digite um e-mail valido.');</script>";
	}
		else{	
			$header_destinatario  = "MIME-Version: 1.0\n";
			$header_destinatario .= "Content-Type:text/html; charset=utf-8\n";
			$header_destinatario .= "From: $email_servidor\n";
			$header_destinatario .= "Return-Path: $email";
	
			// Definindo o aspecto da mensagem
			$assunto_destinatario = "Contato Partnerlab";
			$email_destinatario = "daniel@partnerlab.com.br";
			$email_destinatario2 = "fernando@partnerlab.com.br";
			$destino = $email_destinatario . ', ' . $email_destinatario2;
			
			$mensagem_destinatario = "<strong>Contato Partnerlab</strong><br />";
			$mensagem_destinatario .= "<strong>Nome: </strong>$nome<br />";
			$mensagem_destinatario .= "<strong>E-mail: </strong>$email<br />";
			$mensagem_destinatario .= "<strong>Telefone: </strong>$telefone<br />";
			$mensagem_destinatario .= "<strong>Cidade: </strong>$cidade<br />";
			$mensagem_destinatario .= "<strong>Estado: </strong>$estado<br />";
			$mensagem_destinatario .= "<strong>Mensagem: </strong>$mensagem<br />";
			$mensagem_destinatario .= "<strong>Enviado em </strong>: ".date("d/m/Y H:i");
	
			$envia = mail($destino,$assunto_destinatario,$mensagem_destinatario,$header_destinatario);
	
			// Resposta Automática	
			$assunto_remetente = "Contato Partnerlab - Recebemos seu contato!";		
			$header_remetente  = "MIME-Version: 1.0\n";
			$header_remetente .= "Content-Type:text/html; charset=utf-8\n";
			$header_remetente .= "From: contato@partnerlab.com.br\n";
			$header_remetente .= "Return-Path: contato@partnerlab.com.br";
		  
			// Envia um e-mail para o remetente
			$mensagem_remtente  = "<p>Ol&aacute; <strong>" . $nome . "</strong>. Obrigado por entrar em contato.</p>";
			$mensagem_remtente .= "<p>http://www.partnerlab.com.br</p>";
			$mensagem_remtente .= "<p>Observa&ccedil;&atilde;o - N&atilde;o &eacute; necess&aacute;rio responder esta mensagem.</p>";
			
			if($envia =  mail($email,$assunto_remetente,$mensagem_remtente,$header_remetente)){
				//echo 'sucesso';
				echo "<meta http-equiv='refresh' content='0;URL=contato.php'>";
			    echo "<script>alert('Contato enviado com sucesso!');</script>"; 
			}
			else{
				//echo 'erro';
				echo "<meta http-equiv='refresh' content='0;URL=contato.php'>";
			    echo "<script>alert('Ops... Aconteceu algum erro!');</script>"; 
		}
	}

?>