﻿<?php
require_once('Connections/conexao.php');

$colname_qItem = "-1";
if (isset($_GET['id'])) { $colname_qItem = anti_injection($_GET['id']); }

$query_qItem = sprintf("SELECT * FROM produtos WHERE id = %s", GetSQLValueString($colname_qItem, "int"));
$qItem = mysql_query($query_qItem, $conexao) or die(mysql_error());
$row_qItem = mysql_fetch_assoc($qItem);
$totalRows_qItem = mysql_num_rows($qItem);

$idItem = $row_qItem['id'];
$query_qFoto = sprintf("SELECT * FROM produto_fotos WHERE id_produto = ".$idItem);
$qFoto = mysql_query($query_qFoto, $conexao) or die(mysql_error());
$row_qFoto = mysql_fetch_assoc($qFoto);
$totalRows_qFoto = mysql_num_rows($qFoto);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Partnerlab</title>

<!-- CSS -->
<link rel="icon" type="image/png" href="images/favicon.png" />
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<link rel="stylesheet" type="text/css" href="css/swipebox.css">
<!-- JS -->
<script src="js/jquery.min.js"></script>
<script src="js/flexslider.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.swipebox').swipebox();
	});
</script>


</head>
<body>
<?php include("includes/header.php"); ?>

<div class="banner-interno"></div>

<div id="wrapper">
	
    <div class="container">
        <div class="sixteen columns">
            <div id="page-title">
                <h2>Produto / <?php echo $row_qItem['titulo']; ?></h2>
                <div id="bolded-line"></div>
            </div>
        </div>
    </div>
    
  	<div class="container">
    
    	<div class="sixteen columns">
            <div class="headline no-margin"><h3><?php echo $row_qItem['titulo']; ?></h3></div>
            <p><?php echo $row_qItem['texto']; ?></p>
            <div class="clear"></div>
        </div>
    	
         <hr>
        
        <div class="galeria">
            <h3>GALERIA DE FOTOS</h3>
            <ul>
                <li><div class="picture"><a href="images/produtos/<?php echo $row_qItem['img']; ?>" class="swipebox" rel="swipebox" title=""><img src="images/produtos/thumb_<?php echo $row_qItem['img']; ?>" alt=""/><div class="image-overlay-link"></div></a></div></li>
            	<?php if($totalRows_qFoto > 0){ ?>
					<?php do { ?>
                    <li><div class="picture"><a href="images/galeria/<?php echo $row_qFoto['img']; ?>" class="swipebox" rel="swipebox" title=""><img src="images/galeria/thumb_<?php echo $row_qFoto['img']; ?>" alt=""/><div class="image-overlay-link"></div></a></div></li>
                    <?php } while ($row_qFoto = mysql_fetch_assoc($qFoto)); ?>
                <?php } ?> 
            
            </ul>
        
        </div>
        
    	<div class="clear"></div>

        
    </div>
</div>

<?php include("includes/footer.php"); ?>

</body>
</html>