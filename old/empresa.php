﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Partnerlab</title>

<!-- CSS -->
<link rel="icon" type="image/png" href="images/favicon.png" />
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<link rel="stylesheet" type="text/css" href="css/swipebox.css">
<!-- JS -->
<script src="js/jquery.min.js"></script>
<script src="js/flexslider.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.swipebox').swipebox();
	});
</script>


</head>
<body>
<?php include("includes/header.php"); ?>

<div class="banner-interno"></div>

<div id="wrapper">
	
    <div class="container">
        <div class="sixteen columns">
            <div id="page-title">
                <h2>Empresa</h2>
                <div id="bolded-line"></div>
            </div>
        </div>
    </div>
    
  	<div class="container">
    
    	<div class="sixteen columns">
            <div class="headline no-margin"><h3>Conheça nossa empresa</h3></div>
        </div>
    
    	<div class="one-third column"><a href="images/sobre/img1_g.jpg" class="swipebox" rel="swipebox"><img src="images/sobre/img1.jpg"></a></div>
        <div class="one-third column"><a href="images/sobre/img2_g.jpg" class="swipebox" rel="swipebox"><img src="images/sobre/img2.jpg"></a></div>
        <div class="one-third column"><a href="images/sobre/img3_g.jpg" class="swipebox" rel="swipebox"><img src="images/sobre/img3.jpg"></a></div>
    	<div class="clear"></div>
        
        <hr>
        
        <div class="sixteen columns">
            <div class="headline no-margin"><h3>Partnerlab Equipamentos de Laboratório</h3></div>
            <p>Em novembro de 2012 a Partnerlab Equipamentos de Laboratório, deu início a suas atividades, buscando inovar e surpreender o segmento de laboratórios.<br><br>
            Contando com uma equipe altamente qualificada com mais de 15 anos de experiência no setor, e conhecendo a necessidade de serviços de qualidade à preços acessíveis, instituímos como o nosso nome diz uma parceria com os laboratórios.<br><br>
            Focados na prestação de serviços de manutenção e calibração de equipamentos de laboratório, temos uma estrutura formada para atendimentos realizados tanto em nossa empresa, quanto na planta de nossos clientes.
           <br><br>
 </p>
            <div class="clear"></div>
        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>

</body>
</html>