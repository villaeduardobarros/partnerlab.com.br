﻿<?php require_once('Connections/conexao.php'); ?>
<?php

$colname_qItem = "";
if (isset($_GET['id'])) {
	$colname_qItem = "WHERE id_categoria = ".anti_injection($_GET['id']);

	$query_qCategoria = sprintf("SELECT * FROM categoria WHERE id=".anti_injection($_GET['id']));
	$qCategoria = mysql_query($query_qCategoria, $conexao) or die(mysql_error());
	$row_qCategoria = mysql_fetch_assoc($qCategoria);
	$totalRows_qCategoria = mysql_num_rows($qCategoria);
	
} else {
	$colname_qItem = "";
}

$currentPage = $_SERVER["PHP_SELF"];
$maxRows_qItem = 9;
$pageNum_qItem = 0;
if (isset($_GET['pageNum_qItem'])) {
	$pageNum_qItem = $_GET['pageNum_qItem'];
}
$startRow_qItem = $pageNum_qItem * $maxRows_qItem;

$query_qItem = sprintf("SELECT * FROM produtos $colname_qItem ORDER BY id DESC");
$query_limit_qItem = sprintf("%s LIMIT %d, %d", $query_qItem, $startRow_qItem, $maxRows_qItem);
$qItem = mysql_query($query_limit_qItem, $conexao) or die(mysql_error());
$row_qItem = mysql_fetch_assoc($qItem);
$totalRows_qConta = mysql_num_rows($qItem);

if (isset($_GET['totalRows_qItem'])) {
	$totalRows_qItem = $_GET['totalRows_qItem'];
} else {
	$all_qItem = mysql_query($query_qItem);
	$totalRows_qItem = mysql_num_rows($all_qItem);
}
$totalPages_qItem = ceil($totalRows_qItem/$maxRows_qItem)-1;

$queryString_qItem = "";
if (!empty($_SERVER['QUERY_STRING'])) {
	$params = explode("&", $_SERVER['QUERY_STRING']);
	$newParams = array();
	foreach ($params as $param) {
		if (stristr($param, "pageNum_qItem") == false && 
				stristr($param, "totalRows_qItem") == false) {
			array_push($newParams, $param);
		}
	}
	if (count($newParams) != 0) {
		$queryString_qItem = "&" . htmlentities(implode("&", $newParams));
	}
}
$queryString_qItem = sprintf("&totalRows_qItem=%d%s", $totalRows_qItem, $queryString_qItem);


$query_qLista = sprintf("SELECT * FROM categoria ORDER BY titulo ASC");
$qLista = mysql_query($query_qLista, $conexao) or die(mysql_error());
$row_qLista = mysql_fetch_assoc($qLista);
$totalRows_qLista = mysql_num_rows($qLista);


?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Partnerlab</title>

<!-- CSS -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<!-- JS -->
<script src="js/jquery.min.js"></script>
<script src="js/flexslider.js"></script>
<script src="js/custom.js"></script>


</head>
<body>
<?php include("includes/header.php"); ?>

<div class="banner-interno"></div>

<div id="wrapper">
	
		<div class="container">
		
				<div class="four columns">
							<div id="page-title">
										<h2>Categorias</h2>
										<div id="bolded-line"></div>
								</div>
						
								<div class="categorias">
										<ul>
												<?php do { ?>
												<li><a href="produtos.php?id=<?php echo $row_qLista['id']; ?>" <?php if($row_qLista['id'] == $_GET['id']){ ?> class="selected" <?php } ?> ><?php echo $row_qLista['titulo']; ?></a></li>
												<?php } while ($row_qLista = mysql_fetch_assoc($qLista)); ?>
										</ul>
								</div>
						</div>
		
					<div class="twelve columns">
								<div id="page-title">
												<h2>Produtos <?php echo $row_qCategoria['titulo']; ?></h2>
												<div id="bolded-line"></div>
										</div>
						<?php if($totalRows_qItem > 0){ ?>	
										<ul class="produtos">
					<?php do { ?>
												<!-- item -->
												<li>
														<div class="picture"><a href="detalhe-produto.php?id=<?php echo $row_qItem['id']; ?>" rel="image" title=""><img src="images/produtos/thumb_<?php echo $row_qItem['img']; ?>" alt=""/><div class="image-overlay-link"></div></a></div>
														<div class="item-description alt">
																<h5 style="min-height:48px;"><a href="detalhe-produto.php?id=<?php echo $row_qItem['id']; ?>"><?php echo $row_qItem['titulo']; ?></a></h5>
																<p style="min-height:65px;"><?php echo strip_tags(cortaString($row_qItem['texto'], 80)); ?></p>
														</div>
												</li>
									 <?php } while ($row_qItem = mysql_fetch_assoc($qItem)); ?>
								</ul>
										
										
										<div class="clear"></div>
										
										<ul class="pagination">
											<?php if ($pageNum_qItem > 0) { ?>
												<a href="<?php printf("%s?pageNum_qItem=%d%s", $currentPage, max(0, $pageNum_qItem - 1), $queryString_qItem); ?>"><li>Anterior</li></a>
										<?php } else { ?>
												<a href="javascript:void();" style="opacity:0.2; cursor:none;"><li>Anterior</li></a>
										<?php }  ?>
										<li>Página <?php echo ($pageNum_qItem + 1) ?> de <?php echo ($totalPages_qItem + 1) ?></li>
					<?php if ($pageNum_qItem < $totalPages_qItem) {  ?>
												<a href="<?php printf("%s?pageNum_qItem=%d%s", $currentPage, min($totalPages_qItem, $pageNum_qItem + 1), $queryString_qItem); ?>"><li>Próximo</li></a>
										<?php } else { ?>
												<a href="javascript:void();" style="opacity:0.2; cursor:none;"><li>Próximo</li></a>
										<?php }  ?>
											
										</ul>
									 
										
										
										
										
										
										<div class="clear"></div>
										
								<?php } ?>
								
					</div>
								
					 <div class="clear"></div>
						<div style="height:40px;"></div>     
		</div>
</div>

<?php include("includes/footer.php"); ?>

</body>
</html>