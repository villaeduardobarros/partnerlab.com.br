<?php
$conexao = mysql_connect("mysql.partnerlab.com.br", "partnerlab", "part753lab") or trigger_error(mysql_error(),E_USER_ERROR);
mysql_select_db("partnerlab", $conexao);

mysql_query("SET NAMES 'utf8'");
mysql_query('SET character_set_connection=utf8');
mysql_query('SET character_set_client=utf8');
mysql_query('SET character_set_results=utf8');

$tituloAdmin = "Administrativo";

// os tipos dentro do array estao com a edicao travada
$tipoTravado = array(2);

if (!function_exists("GetSQLValueString")) {
	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
	{
		$theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

		$theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

		switch ($theType) {
			case "text":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
			break;    
			case "long":
			case "int":
				$theValue = ($theValue != "") ? intval($theValue) : "NULL";
			break;
			case "double":
				$theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
			break;
			case "date":
				$theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
			break;
			case "defined":
				$theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
			break;
		}
		return $theValue;
	}
}

if (!function_exists("anti_injection")) {
	function anti_injection($sql){
	   $sql = preg_replace(sql_regcase("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/"), "" ,$sql);
	   $sql = trim($sql);
	   $sql = strip_tags($sql);
	   $sql = (get_magic_quotes_gpc()) ? $sql : addslashes($sql);
	   return $sql;
	}
}

if (!function_exists("cortaString")) {
	function cortaString($frase, $qtde_letras=NULL)
	{
	   $p = explode(' ', $frase);
	   $c = 0;
	   $cortada = '';
	 
	   foreach($p as $p1){
	      if ($c<$qtde_letras && ($c+strlen($p1) <= $qtde_letras)){
	         $cortada .= ' '.$p1;
	         $c += strlen($p1)+1;
	      }else{
	         break;
	      }
	   }
	 
	   return strlen($cortada) < $qtde_letras ? $cortada.'...' : $cortada;
	}
}

if (!function_exists("slug")) {
	function slug($text)
	{
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('~-+~', '-', $text);
		$text = strtolower($text);
		if (empty($text)) {
			return 'n-a';
		}
		return $text;
	}
}

if (!function_exists("uploadArquivo")) {
	function uploadArquivo($nome_campo, $titulo, $file)
	{
		$arquivo = '';

		if($_FILES[$nome_campo]['name'] != ""){

			$extensao 	= pathinfo($_FILES[$nome_campo]['name']);
			$extensao 	= ".".$extensao['extension'];
			$arquivo 	= (($titulo) ? slug($titulo) : date('Ymd').'_'.date("Gis")).$extensao;

			move_uploaded_file($_FILES[$nome_campo]['tmp_name'], $file.$arquivo);

		}

		return $arquivo;
	}
}

function formata_data_extenso($strDate)
{
	$arrDaysOfWeek = array('Domingo','Segunda-feira','Ter�a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S�bado');
	$arrMonthsOfYear = array(1 => 'Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
	$intDayOfWeek = date('w',strtotime($strDate));
	$intDayOfMonth = date('d',strtotime($strDate));
	$intMonthOfYear = date('n',strtotime($strDate));
	$intYear = date('Y',strtotime($strDate));
	$intHour = substr($strDate,10,20);
	return $intDayOfMonth . ' de ' . $arrMonthsOfYear[$intMonthOfYear] . ' de ' . $intYear;
}

function formata_data_adm($strDate)
{
	$arrDaysOfWeek = array('Domingo','Segunda-feira','Ter�a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S�bado');
	$arrMonthsOfYear = array(1 => '01','02','03','04','05','06','07','08','09','10','11','12');
	$intDayOfWeek = date('w',strtotime($strDate));
	$intDayOfMonth = date('d',strtotime($strDate));
	$intMonthOfYear = date('n',strtotime($strDate));
	$intYear = date('Y',strtotime($strDate));
	$intHour = substr($strDate,10,20);
	return $intDayOfMonth . ' de ' . $arrMonthsOfYear[$intMonthOfYear] . ' de ' . $intYear;
}

function dataFormataBarra($data){
	$dataAux = explode("-",$data);
	return $dataAux[2]."/".$dataAux[1]."/".$dataAux[0];
}

function dataFormataTraco($data){
	$dataAux = explode("/",$data);
	return $dataAux[2]."-".$dataAux[1]."-".$dataAux[0];
}

function convert_xml2array(&$result,$root,$rootname='calculo_precos'){
    $n=count($root->children());
    if ($n>0){
        if (!isset($result[$rootname]['@attributes'])){
            $result[$rootname]['@attributes']=array();
            foreach ($root->attributes() as $atr=>$value){
                $result[$rootname]['@attributes'][$atr]=(string)$value;
            }           
        }
       
         foreach ($root->children() as $child){
             $name=$child->getName();    
             convert_xml2array($result[$rootname][],$child,$name);                         
         }
    } else {       
        $result[$rootname]= (array) $root;
        if (!isset($result[$rootname]['@attributes'])){
            $result[$rootname]['@attributes']=array();
        }
    }
}

function uploadUsuario($nome_campo){	
	$foto = "";
	$fileName = $_FILES[$nome_campo]["name"];
	$kaboom = explode(".", $fileName); 
	$fileExt = end($kaboom); 
	
	if($_FILES[$nome_campo]['name'] != ""){
		$uploaddir = '../img/usuarios/';
		$foto = date("Ymd"). "_" .date("Gis"). "_" .$_FILES[$nome_campo]['name'];
		move_uploaded_file($_FILES[$nome_campo]['tmp_name'], $uploaddir.$foto);
		resizeImg($uploaddir.$foto, $uploaddir.$foto, 400, 223);
	
		ak_img_resize($uploaddir.$foto, $uploaddir."medio_".$foto, 120, 120, $fileName);
		ak_img_thumb($uploaddir."medio_".$foto, $uploaddir."thumb_".$foto, 65, 65, $fileName);
					
	}
	return $foto;
}

function uploadEquipamento($nome_campo, $titulo){	
	$foto = "";
	$fileName = $_FILES[$nome_campo]["name"];
	
	$extensao 	= pathinfo($_FILES[$nome_campo]['name']);
	$extensao 	= ".".$extensao['extension'];
	
	if($_FILES[$nome_campo]['name'] != ""){
		$uploaddir = '../img/equipamentos/';
		$foto = slug($titulo).$extensao;
		move_uploaded_file($_FILES[$nome_campo]['tmp_name'], $uploaddir.$foto);
		resizeImg($uploaddir.$foto, $uploaddir.$foto, 800, 600);
		ak_img_resize($uploaddir.$foto, $uploaddir."thumb_".$foto, 520, 390, $fileName);		
	}
	return $foto;
}

function uploadEquipamentoAcessorio($nome_campo, $titulo){	
	$foto = "";
	$fileName = $_FILES[$nome_campo]["name"];

	$extensao 	= pathinfo($_FILES[$nome_campo]['name']);
	$extensao 	= ".".$extensao['extension'];
	
	if($_FILES[$nome_campo]['name'] != ""){
		$uploaddir = '../img/acessorios/';
		$foto = slug($titulo).$extensao;
		move_uploaded_file($_FILES[$nome_campo]['tmp_name'], $uploaddir.$foto);
		resizeImg($uploaddir.$foto, $uploaddir.$foto, 800, 600);
		ak_img_resize($uploaddir.$foto, $uploaddir."thumb_".$foto, 520, 390, $fileName);		
	}
	return $foto;
}

function uploadProdutos($nome_campo){	
	$foto = "";
	$fileName = $_FILES[$nome_campo]["name"];
	$kaboom = explode(".", $fileName); 
	$fileExt = end($kaboom); 
	
	if($_FILES[$nome_campo]['name'] != ""){
		$uploaddir = '../../images/produtos/';
		$foto = date("Ymd"). "_" .date("Gis"). "_" .$_FILES[$nome_campo]['name'];
		move_uploaded_file($_FILES[$nome_campo]['tmp_name'], $uploaddir.$foto);
		resizeImg($uploaddir.$foto, $uploaddir.$foto, 800, 800);
		ak_img_resize($uploaddir.$foto, $uploaddir."medio_".$foto, 600, 240, $fileName);
		ak_img_thumb($uploaddir."medio_".$foto, $uploaddir."thumb_".$foto, 220, 240, $fileName);
					
	}
	return $foto;
}

function uploadBanner($nome_campo){	
	$foto = "";
	$fileName = $_FILES[$nome_campo]["name"];
	$kaboom = explode(".", $fileName); 
	$fileExt = end($kaboom); 
	
	if($_FILES[$nome_campo]['name'] != ""){
		$uploaddir = '../../images/banners/';
		$foto = date("Ymd"). "_" .date("Gis"). "_" .$_FILES[$nome_campo]['name'];
		move_uploaded_file($_FILES[$nome_campo]['tmp_name'], $uploaddir.$foto);
		resizeImg($uploaddir.$foto, $uploaddir.$foto, 1700, 1700);
		ak_img_resize($uploaddir.$foto, $uploaddir."thumb_".$foto, 300, 200, $fileName);
					
	}
	return $foto;
}

function uploadGaleria($nome_campo){	
	$foto = "";
	$fileName = $_FILES[$nome_campo]["name"];
	$kaboom = explode(".", $fileName); 
	$fileExt = end($kaboom); 
	
	if($_FILES[$nome_campo]['name'] != ""){
		$uploaddir = '../../images/galeria/';
		$foto = date("Ymd"). "_" .date("Gis"). "_" .$_FILES[$nome_campo]['name'];
		move_uploaded_file($_FILES[$nome_campo]['tmp_name'], $uploaddir.$foto);
		resizeImg($uploaddir.$foto, $uploaddir.$foto, 800, 800);
		ak_img_resize($uploaddir.$foto, $uploaddir."medio_".$foto, 600, 240, $fileName);
		ak_img_thumb($uploaddir."medio_".$foto, $uploaddir."thumb_".$foto, 220, 240, $fileName);
					
	}
	return $foto;
}


function treat_image($filename,$destImage,$maxLarg,$maxAlt,$maxTam,$mimeImg){ 
  if (!is_file($filename) || is_file($destImage)) return false; 
  if ($destImage===FALSE) $destImage=$filename; 
	list($width, $height, $type, $attr) = getimagesize($filename); // pegando os atributos 
	$percent = min(round(($maxLarg / $width),2),round(($maxAlt/$height),2),1); // verificando se a imagem j� n�o tem um tamanho que seja desnecess�rio interven��o 

	$newwidth = floor($width * $percent); // Pegando a nova largura proporcional para comparar posteriormente 
	$newheight = floor($height * $percent); // O mesmo que a largura s� que para a altura 
	switch ($type){
		/* Mime Types poss�veis 
		1 = GIF, 2 = JPG, 3 = PNG, 4 = SWF, 5 = PSD, 6 = BMP, 7 = TIFF(intel byte order), 8 = TIFF(motorola byte order), 9 = JPC, 10 = JP2, 11 = JPX, 12 = JB2, 13 = SWC, 14 = IFF, 15 = WBMP, 16 = XBM  
		*/ 
		case 2 : $source  = imagecreatefromjpeg ($filename); 
			break; 
		case 1 : $source  = imagecreatefromgif  ($filename); 
			break; 
		case 3 : $source  = imagecreatefrompng  ($filename); 
			break; 
		default : $stop = true; 
			break; 
	} 

	// Load 
	$dest = imagecreatetruecolor($newwidth, $newheight); 

	// Resize 
	imagecopyresized($dest, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
	
	// Output 
	switch($mimeImg){ 
		case "gif":imagegif($dest, preg_replace("@\.\w+$@","",$destImage).".gif" ); 
		case "png":imagepng($dest, preg_replace("@\.\w+$@","",$destImage).".png" ); 
		case "jpg":imagejpeg($dest, preg_replace("@\.\w+$@","",$destImage).".jpg" ); 
	}        
	imagedestroy($source);     
	imagedestroy($dest); 
	return preg_replace("@\.\w+$@","",basename($destImage) ).".$mimeImg"; 
} 

function resizeImg($source_pic, $destination_pic, $max_width, $max_height){
	$src = imagecreatefromjpeg($source_pic);
	list($width,$height)=getimagesize($source_pic);
	
	$x_ratio = $max_width / $width;
	$y_ratio = $max_height / $height;
	
	if( ($width <= $max_width) && ($height <= $max_height) ){
			$tn_width = $width;
			$tn_height = $height;
			}elseif (($x_ratio * $height) < $max_height){
					$tn_height = ceil($x_ratio * $height);
					$tn_width = $max_width;
			}else{
					$tn_width = ceil($y_ratio * $width);
					$tn_height = $max_height;
	}
	
	$tmp=imagecreatetruecolor($tn_width,$tn_height);
	imagecopyresampled($tmp,$src,0,0,0,0,$tn_width, $tn_height,$width,$height);
	
	imagejpeg($tmp,$destination_pic,100);
	imagedestroy($src);
	imagedestroy($tmp);
}

///RESIZE 2 /////////////////////////////////////////////////
function ak_img_resize($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    if (($w / $h) > $scale_ratio) {
           $w = $h * $scale_ratio;
    } else {
           $h = $w / $scale_ratio;
    }
    $img = "";
    $ext = strtolower($ext);
    if ($ext == "gif"){ 
    $img = imagecreatefromgif($target);
    } else if($ext =="png"){ 
    $img = imagecreatefrompng($target);
    } else { 
    $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
    if ($ext == "gif"){ 
        imagegif($tci, $newcopy);
    } else if($ext =="png"){ 
        imagepng($tci, $newcopy);
    } else { 
        imagejpeg($tci, $newcopy, 100);
    }
}

// ------------- THUMBNAIL (CROP) FUNCTION -------------
// Function for creating a true thumbnail cropping from any jpg, gif, or png image files
function ak_img_thumb($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $src_x = ($w_orig / 2) - ($w / 2);
    $src_y = ($h_orig / 2) - ($h / 2);
    $ext = strtolower($ext);
    $img = "";
    if ($ext == "gif"){ 
    $img = imagecreatefromgif($target);
    } else if($ext =="png"){ 
    $img = imagecreatefrompng($target);
    } else { 
    $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    imagecopyresampled($tci, $img, 0, 0, $src_x, $src_y, $w, $h, $w, $h);
    if ($ext == "gif"){ 
        imagegif($tci, $newcopy);
    } else if($ext =="png"){ 
        imagepng($tci, $newcopy);
    } else { 
        imagejpeg($tci, $newcopy, 100);
    }
}
?>