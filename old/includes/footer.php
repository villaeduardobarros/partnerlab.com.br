<div id="footer">
	<div class="container">
	
		<div class="one-third column footer-link">
        	<div class="headline"><h4>Menu</h4></div>
                <div class="left">
                    <a href="./">Home</a><br>
                    <a href="empresa.php">Empresa</a><br>
                    <a href="calibracao.php">Calibração</a>
                </div>
                <div class="left">
                    <a href="produtos.php">Produtos</a><br>
                    <a href="servicos.php">Serviços</a><br>
                    <a href="contato.php">Contato</a>
                </div>
                <div class="clear"></div>
        </div>
        
        <div class="one-third column">
        	<div class="headline"><h4>Localização</h4></div>
            <p>
            Rua Virgilio de Carvalho Neves Neto, 926 <br>
            Ribeirão Preto - SP - CEP: 14092-440<br>
            (16)  3624 – 7700 | 3237 -3861<br />
            Email: partnerlab@partnerlab.com.br
            </p>
        </div>
        
		<div class="one-third column">
            <div class="headline"><h4>Redes Sociais</h4></div>
            <p>Venha fazer parte de nossa rede social! Participe de promoções exclusivas e muito mais...</p>
            <ul class="social-icons">
                <li class="facebook"><a href="#">Facebook</a></li>
                <li class="twitter"><a href="#">Twitter</a></li>
            </ul>
        </div>
			
	</div>
    
    <div id="footer-bottom">
    	<div class="container">
           <div>Partnerlab © 2015. Todos direitos reservados. <span class="ijust"><a href="http://ijust.com.br/" target="_blank"><img src="../images/ijust.png" width="45" height="33" /></a></span></div>
           <div class="clear"></div>
        </div>
  </div>
    
</div>