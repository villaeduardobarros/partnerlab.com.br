<div id="top-info">
	<div class="container">
        <div class="sixteen columns">
        	<div id="contact-details">
				<ul>
                	<li><i><img src="images/icons/icon-phone.png" width="12" height="12"></i>(16) 3624 – 7700 | 3237 -3861</li>
					<li><i><img src="images/icons/icon-mail.png" width="16" height="12"></i><a href="mailto:partnerlab@partnerlab.com.br">partnerlab@partnerlab.com.br</a></li>
				</ul>
			</div>
        </div>
	</div>
    <div class="clear"></div>
</div>

<div id="header">
    <div class="container">
        <div id="logo">
            <a href="./"><img src="images/logo.png" alt="" /></a>
        </div>
        <div id="navigation">
            <ul id="nav">
                <?php $endereco = $_SERVER['PHP_SELF']; ?>
                <li><a href="./" class="<?php if($endereco == "/index.php"){ echo "selected"; } else { echo ""; } ?>">HOME</a></li>
                <li><a href="empresa.php" class="<?php if($endereco == "/empresa.php"){ echo "selected"; } else { echo ""; } ?>">EMPRESA</a></li>
                <li><a href="calibracao.php" class="<?php if($endereco == "/calibracao.php"){ echo "selected"; } else { echo ""; } ?>">CALIBRAÇÃO</a></li>
                <li><a href="produtos.php" class="<?php if($endereco == "/produtos.php" || $endereco == "/detalhe-produto.php"){ echo "selected"; } else { echo ""; } ?>">PRODUTOS</a></li>
                <li><a href="servicos.php" class="<?php if($endereco == "/servicos.php"){ echo "selected"; } else { echo ""; } ?>">SERVIÇOS</a></li>
                <li><a href="contato.php" class="<?php if($endereco == "/contato.php"){ echo "selected"; } else { echo ""; } ?>">CONTATO</a></li>
            </ul>
        </div>
        <div class="clear"></div>		
	</div>
</div>