﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Partnerlab</title>

<!-- CSS -->
<link rel="icon" type="image/png" href="images/favicon.png" />
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<link rel="stylesheet" type="text/css" href="css/swipebox.css">
<!-- JS -->
<script src="js/jquery.min.js"></script>
<script src="js/flexslider.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.swipebox').swipebox();
	});
</script>


</head>
<body>
<?php include("includes/header.php"); ?>

<div class="banner-interno"></div>

<div id="wrapper">
	
    <div class="container">
        <div class="sixteen columns">
            <div id="page-title">
                <h2>Calibração</h2>
                <div id="bolded-line"></div>
            </div>
        </div>
    </div>
    
  	<div class="container">
    
    	<div class="sixteen columns">
            <div class="headline no-margin"><h3>Galeria</h3></div>
        </div>
    
    	<div class="four columns"><a href="images/calibracao/img1_g.jpg" class="swipebox" rel="swipebox"><img src="images/calibracao/img1.jpg"></a></div>
        <div class="four columns"><a href="images/calibracao/img2_g.jpg" class="swipebox" rel="swipebox"><img src="images/calibracao/img2.jpg"></a></div>
        <div class="four columns"><a href="images/calibracao/img3_g.jpg" class="swipebox" rel="swipebox"><img src="images/calibracao/img3.jpg"></a></div>
        <div class="four columns"><a href="images/calibracao/img4_g.jpg" class="swipebox" rel="swipebox"><img src="images/calibracao/img4.jpg"></a></div>
    	<div class="clear"></div>
        
        <hr>
        
        <div class="sixteen columns">
            <div class="headline no-margin"><h3>Conheça</h3></div>
            <p>
            Calibrações realizadas com padrões Rastreados junto à Rede Brasileira de Calibração (RBC), ou órgãos internacionalmente reconhecidos como o NIST e PTB.<br>
            Os serviços de calibração podem ser realizados em nosso laboratório ou nas dependências do cliente. <br>
            Nosso laboratório possui monitoramento de temperatura e Umidade, visando sempre a melhor condição para realização da calibração.
			<br><br>
            </p>
            <div class="clear"></div>
        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>

</body>
</html>